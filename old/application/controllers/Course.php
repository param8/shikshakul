<?php 
class Course extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('course_model');
		$this->load->model('category_model');
    $this->load->model('user_model');
		$this->load->model('subject_model');
	}

	public function index()
	{
		$this->not_admin_logged_in();
    $data['page_title'] = 'Courses';
	  $this->admin_template('course/course',$data);
	}

  public function courses_web()
	{
		
    $data['page_title'] = 'Courses';
		$data['courses'] =$this->course_model->get_courses(array('courses.status'=>1));
	  $this->template('web/courses/courses',$data);
	}

	public function course_panel()
	{
		$this->not_logged_in();
		$this->user_verification();
    $data['page_title'] = 'Courses';
		$data['panel'] = 'Panel';
		$data['courses'] =$this->course_model->get_courses(array('courses.status'=>1));
	  $this->template('web/panel/courses/course-list',$data);
	}

	public function course_deatil_panel(){
		$this->not_logged_in();
		$this->user_verification();
		$data['panel'] = 'Panel';
		$id = $this->uri->segment(2);
		$data['course'] =$this->course_model->get_course(array('courses.slug'=>$id,'courses.status'=>1));
		$data['topics'] = $this->course_model->get_topics(array('topics.courseID'=>$data['course']->id,'topics.categoryID'=>$data['course']->categoryID));
		//print_r($data['topics']);die;
		$data['page_title'] = $data['course']->name;
		$this->template('web/panel/courses/course_deatil_panel',$data);
	 }

	public function course_deatil(){
		
	 $id = $this->uri->segment(2);
	 $data['course'] =$this->course_model->get_course(array('courses.slug'=>$id,'courses.status'=>1));
	 $data['page_title'] = $data['course']->name;
	 $this->template('web/courses/course_deatil',$data);
	}

	public function ajaxCourse(){
		$this->not_admin_logged_in();
		$condition = array('courses.status'=>1);
		$courses = $this->course_model->make_datatables($condition); // this will call modal function for fetching data
		$data = array();
		foreach($courses as $key=>$course) // Loop over the data fetched and store them in array
		{
			$button = '';
			$sub_array = array();
			$button .= '<a href="'.base_url('view-course/'.base64_encode($course['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="View faculty" class="btn  btn-sm  bg-warning-light"><i class="fe fe-eye"></i></a>';
			$button .= '<a href="'.base_url('edit-course/'.base64_encode($course['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit faculty" class="btn btn-sm bg-success-light"><i class="fe fe-edit"></i> </a>';
      $button .= '<a href="javascript:void(0)" onclick="delete_course('.$course['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete faculty" class="btn btn-sm bg-danger-light"><i class="fe fe-trash"></i> </a>';
			
			$sub_array[] = $key+1;
			$image = $course['image'];
			$sub_array[] = '<img src="'.base_url($image	).'" height="50" width="50">';
			$sub_array[] = $course['categoryName'];
			$sub_array[] = $course['name'];
			$sub_array[] = $course['capacity'];
			$sub_array[] = '₹ '.$course['price'];
			$sub_array[] = $course['demo_classes'];
			$sub_array[] = $course['start_date'];
			$sub_array[] = $course['end_date'];
			$sub_array[] = $course['duration'];
			$sub_array[] = date('d-F-Y', strtotime($course['created_at']));
			$sub_array[] = $button;
		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->course_model->get_all_data($condition),
			"recordsFiltered"         =>     $this->course_model->get_filtered_data($condition),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
	  }

	  public function create_course(){
			$this->not_admin_logged_in();
		  $data['page_title'] = 'Create-Course';
		  $data['categories'] = $this->category_model->get_categories(array('status'=>1));
		  $data['faculties'] = $this->user_model->get_users(array('users.status'=>1,'users.user_type'=>'Faculties'));
		  $this->admin_template('course/create-course',$data);
	  }

		public function edit_course(){
			$this->not_admin_logged_in();
			 $data['page_title'] = 'Edit-Course';
			 $courseID = base64_decode($this->uri->segment(2));
			 $data['course'] = $this->course_model->get_course(array('courses.id'=>$courseID));
			 $data['categories'] = $this->category_model->get_categories(array('status'=>1));
			 $data['faculties'] = $this->user_model->get_users(array('users.status'=>1,'users.user_type'=>'Faculties'));
			$this->admin_template('course/edit-course',$data);
			}

			public function view_course(){
				$this->not_admin_logged_in();
				$data['page_title'] = 'View-Course';
				$courseID = base64_decode($this->uri->segment(2));
				$data['course'] = $this->course_model->get_course(array('courses.id'=>$courseID));
				$data['faculties'] = $this->user_model->get_users(array('users.status'=>1,'users.user_type'=>'Faculties'));
			  $this->admin_template('course/view-course',$data);
			 }


   public function store(){
		$this->not_admin_logged_in();
		$name = $this->input->post('name');
		$categoryID = $this->input->post('categoryID'); 
		$facultyID = $this->input->post('faculty');	
		$description = $this->input->post('description');
		$capacity = $this->input->post('capacity');
		$price = $this->input->post('price');
		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date');
		$duration = $this->input->post('duration');
		$slug = str_replace(' ','-',strtolower($name));
		$demo_classes = !empty($this->input->post('demo_classes')) ? $this->input->post('demo_classes') : 0;
   
		$class_type = $this->input->post('class_type');
		$class_link = $this->input->post('class_link');

		$discount_type = $this->input->post('discount_type');
		$discount = $this->input->post('discount');

    $check_course =$this->course_model->get_course(array('courses.name'=>$name,'courses.categoryID'=>$categoryID));
	if(empty($name)){
		echo json_encode(['status'=>403, 'message'=>'Please enter subject name']); 	
		exit();
	}
	if($check_course){
		echo json_encode(['status'=>403, 'message'=>'This course already exists']); 	
		exit();
	}
	if(empty($categoryID)){
		echo json_encode(['status'=>403, 'message'=>'Please choose category']); 	
		exit();
	}
	if(empty($facultyID)){
		echo json_encode(['status'=>403, 'message'=>'Please choose Faculty']); 	
		exit();
	}

  if(empty($capacity)){
		echo json_encode(['status'=>403, 'message'=>'Please enter capacity']);   	
		exit();
	}
 
	// if($price!==''){
	// 	echo json_encode(['status'=>403, 'message'=>'Please enter price']); 	
	// 	exit();
	// }

	if(empty($start_date)){
		echo json_encode(['status'=>403, 'message'=>'Please select start_date']); 	
		exit();
	}
	if(empty($end_date)){
		echo json_encode(['status'=>403, 'message'=>'Please select end_date']); 	
		exit();
	}
	// if(empty($description)){
	// 	echo json_encode(['status'=>403, 'message'=>'Please enter description']); 	
	// 	exit();
	// }
	if(empty($duration)){
		echo json_encode(['status'=>403, 'message'=>'Please enter duration']);  	
		exit();
	}

	if(empty($class_type)){
		echo json_encode(['status'=>403, 'message'=>'Please choose a class type']);  	
		exit();
	}

	if(!empty($discount_type)){
		if(empty($discount)){
			echo json_encode(['status'=>403, 'message'=>'Please enter a discount']);  	
			exit();
		}
	}

	if(!empty($discount)){
		if(empty($discount_type)){
			echo json_encode(['status'=>403, 'message'=>'Please enter a discount type']);  	
			exit();
		}
	}


	$this->load->library('upload');
    if($_FILES['image']['name'] != '')
		{
    $config = array(
      'upload_path' 	=> 'uploads/course',
      'file_name' 	=> str_replace(' ','',$name).uniqid(),
      'allowed_types' => 'jpg|jpeg|png|gif|webp',
      'max_size' 		=> '10000000',
    );
        $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('image'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]);
          exit();
      }
      else
      {
        $type = explode('.',$_FILES['image']['name']);
        $type = $type[count($type) - 1];
        $image = 'uploads/course/'.$config['file_name'].'.'.$type;
      }
     }else{
      $image = 'public/Image_not_available.png';
    }
	//$permission = 
	$user_id = uniqid("ST".date('dmY'));
	$data = array(
    'name'          => $name,
		'categoryID'    => $categoryID,
		'facultyID'     => implode(',',$facultyID),
		'description'   => $description ,
		'capacity'      => $capacity,
		'price'         => $price,
		'start_date'    => $start_date,
        'end_date'      => $end_date,
		'duration'      => $duration,
		'demo_classes'  => $demo_classes,
		'image'         => $image,
		'class_type'    => $class_type,
		'class_link'    => $class_link,
		'discount'      => $discount,
		'discount_type' => $discount_type,
		'slug'          => $slug
	);
	$store = $this->course_model->store_course($data);

	if($store){
		echo json_encode(['status'=>200, 'message'=>'Course add successfully!']);
	}else{
		echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
	}

   }

  public function update(){
		$this->not_admin_logged_in();
		$courseID = $this->input->post('courseID');
		$name = $this->input->post('name');
		$categoryID = $this->input->post('categoryID'); 
		$facultyID = $this->input->post('faculty');	
		$description = $this->input->post('description');
		$capacity = $this->input->post('capacity');
		$price = $this->input->post('price');
		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date');
		$duration = $this->input->post('duration');
		$demo_classes = !empty($this->input->post('demo_classes')) ? $this->input->post('demo_classes') : 0;
   
		$class_type = $this->input->post('class_type');
		$class_link = $this->input->post('class_link');

		$discount_type = $this->input->post('discount_type');
		$discount = $this->input->post('discount');
		$slug = str_replace(' ','-',strtolower($name));
		$course =$this->course_model->get_course(array('courses.id'=>$courseID));
    $check_course =$this->course_model->get_course(array('courses.name'=>$name,'courses.categoryID'=>$categoryID,'courses.id <>'=>$courseID));
	if(empty($name)){
		echo json_encode(['status'=>403, 'message'=>'Please enter subject name']); 	
		exit();
	}
	if($check_course){
		echo json_encode(['status'=>403, 'message'=>'This course already exists']); 	
		exit();
	}
	if(empty($categoryID)){
		echo json_encode(['status'=>403, 'message'=>'Please choose category']); 	
		exit();
	}
	if(empty($facultyID)){
		echo json_encode(['status'=>403, 'message'=>'Please choose Faculty']); 	
		exit();
	}

  if(empty($capacity)){
		echo json_encode(['status'=>403, 'message'=>'Please enter capacity']);   	
		exit();
	}
 
	// if($price!==''){
	// 	echo json_encode(['status'=>403, 'message'=>'Please enter price']); 	
	// 	exit();
	// }

	if(empty($start_date)){
		echo json_encode(['status'=>403, 'message'=>'Please select start_date']); 	
		exit();
	}
	if(empty($end_date)){
		echo json_encode(['status'=>403, 'message'=>'Please select end_date']); 	
		exit();
	}
	// if(empty($description)){
	// 	echo json_encode(['status'=>403, 'message'=>'Please enter description']); 	
	// 	exit();
	// }
	if(empty($duration)){
		echo json_encode(['status'=>403, 'message'=>'Please enter duration']);  	
		exit();
	}

	if(empty($class_type)){
		echo json_encode(['status'=>403, 'message'=>'Please choose a class type']);  	
		exit();
	}

	if(!empty($discount_type)){
		if(empty($discount)){
			echo json_encode(['status'=>403, 'message'=>'Please enter a discount']);  	
			exit();
		}
	}

	if(!empty($discount)){
		if(empty($discount_type)){
			echo json_encode(['status'=>403, 'message'=>'Please enter a discount type']);  	
			exit();
		}
	}


	$this->load->library('upload');
    if($_FILES['image']['name'] != '')
		{
    $config = array(
      'upload_path' 	=> 'uploads/course',
      'file_name' 	=> str_replace(' ','',$name).uniqid(),
      'allowed_types' => 'jpg|jpeg|png|gif|webp',
      'max_size' 		=> '10000000',
    );
        $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('image'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]);
          exit();
      }
      else
      {
        $type = explode('.',$_FILES['image']['name']);
        $type = $type[count($type) - 1];
        $image = 'uploads/course/'.$config['file_name'].'.'.$type;
      }
     }else{
			if(!empty($course->image)){
				$image = $course->image;
			}else{
				$image = 'public/Image_not_available.png';
			}
      
    }

	$data = array(
        'name'          => $name,
		'categoryID'    => $categoryID,
		'facultyID'     => implode(',',$facultyID),
		'description'   => $description ,
		'capacity'      => $capacity,
		'price'         => $price,
		'start_date'    => $start_date,
        'end_date'      => $end_date,
		'duration'      => $duration,
		'demo_classes'  => $demo_classes,
		'image'         => $image,
		'class_type'    => $class_type,
		'class_link'    => $class_link,
		'discount'      => $discount,
		'discount_type' => $discount_type,
		'slug'          => $slug
	);
	  $update = $this->course_model->update_course($data,$courseID);

	if($update){
		echo json_encode(['status'=>200, 'message'=>'Course updated successfully!']);
	}else{
		echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
	}

  }

  public function delete(){
		$this->not_admin_logged_in();
		$courseID = $this->input->post('id');
		$delete = $this->course_model->delete_course(array('id' => $courseID));
		if($delete){
			$this->subject_model->delete_subject(array('courseID'=>$courseID));
			$this->course_model->delete_topic(array('courseID'=>$courseID));
			echo json_encode(['status'=>200, 'message'=>'Course delete successfully!']);
		}else{
			echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
		}
	
  }

	public function download(){
		$this->load->helper('download');
		$file_name =  base64_decode($this->uri->segment(3));
		force_download($file_name, NULL);
	}

}