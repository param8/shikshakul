<?php 
class Chat extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('setting_model');
		$this->load->model('category_model');
		$this->load->model('user_model');
		$this->load->model('course_model');
    $this->load->model('chat_model');
	}

	public function index()
	{	 
    $data['page_title'] = 'Chat';
    $data['users'] = $this->user_model->get_users(array('users.status'=>1,'users.user_type<>'=>'Admin','users.id<>'=>$this->session->userdata('id')));
    $this->template('web/chat/chat',$data);
		
	}

  public function ownerDetails(){
		$res = $this->chat_model->ownerDetails();
		print_r(json_encode($res));
	}
	public function allUser(){
		$data['data'] = $this->chat_model->allUser();
		$data['last_msg'] = array();
		if(!is_array($data['data'])){
			echo "<p class='text-center'>No user available.</p>";
		}else{
			$count = count($data['data']);
			for($i = 0; $i < $count; $i++){
				$unique_id = $data['data'][$i]['unique_id'];
				$msg = $this->chat_model->getLastMessage($unique_id);
				for($j = 0; $j < count($msg); $j++){

					$time = explode(" ",$msg[0]['time']); //00:00:00.0000
					$time = explode(".", $time[1]);//00:00:00
					$time = explode(":",$time[0]);//00 00 00
					if((int)$time[0] == 12){
						$time = $time[0].":".$time[1]." PM";
					}
					elseif((int)$time[0] > 12){
						$time = ($time[0] - 12).":".$time[1]." PM";
					}else{
						$time = $time[0].":".$time[1]." AM";
					}

					array_push($data['last_msg'],array(
						'message' => $msg[0]['message'],
						'sender_id' => $msg[0]['sender_message_id'],
						'receiver_id' => $msg[0]['receiver_message_id'],
						'time' => $time //00:00
					));
				}
			}
      $this->load->view('web/chat/sampleDataShow',$data);
			
		}
		
	}
	public function getIndividual(){
		$returnVal = $this->chat_model->getIndividual($_POST['data']);
		print_r(json_encode($returnVal,true));
	}

	public function setNoMessage(){
		$data['image'] = $_POST['image'];
		$data['name'] = $_POST['name'];
    $this->load->view('web/chat/notmessageyet',$data);

	}
	public function sendMessage(){
		 $datetime = $this->input->post('datetime');
     $message = $this->input->post('txt_message');
     $reciverID = $this->input->post('uniq');
		//$jsonDecode = json_decode($_POST['data'],true);
		$uniqID = $this->session->userdata('unique_id');
	
 

	
    $this->load->library('upload');
    //echo $_FILES['document']['name'];die;
	$name = $_FILES['document']['name'];
    if($_FILES['document']['name'] != '')
      {
    $config = array(
      'upload_path' 	=> 'uploads/chat_doc',
      'file_name' 	=> uniqid(),
      'allowed_types' => '*',
      //'max_size' 		=> '10000000',
    );
      $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('document'))
      {
        $error = $this->upload->display_errors();
        echo json_encode(['status'=>403, 'message'=>$error]);
        exit();
      }
      else
      {
      $type = explode('.',$_FILES['document']['name']);
      $type = $type[count($type) - 1];
      $documents = 'uploads/chat_doc/'.$config['file_name'].'.'.$type;
      }
    }else{
			if(empty($message)){ 	
				exit();
			}
      $documents = '';
    }

		$arr = array(
			'time' => $datetime,
			'sender_message_id' => $uniqID,
			'receiver_message_id' => $reciverID,
			'message' => $message,
      'documents' => $documents
		);

		$store = 	$this->chat_model->sentMessage($arr);
    if($store){
      echo json_encode(['status'=>200]);
    }else{
      echo json_encode(['status'=>302]);   
    }
		
	}
	public function getMessage(){
		if(isset($_POST['data']) && isset($_SESSION['unique_id'])){
			$data['data'] = $this->chat_model->getmessage($_POST['data']);
			$data['image'] = $_POST['image'];
      $this->load->view('web/chat/sampleMessageShow',$data);

		}
	}
	public function updateBio(){
		if($_POST){
			$this->chat_model->updateBio($_POST);
		}
	}
	public function blockUser(){
		if(isset($_POST['time']) && isset($_POST['uniq'])){
			$arr = array(
				'blocked_from' => $_SESSION['unique_id'],
				'blocked_to' => $_POST['uniq'],
				'time' => $_POST['time']
			);
			$this->chat_model->blockUser($arr);
			return 1;
		}
	}
	public function getBlockUserData(){
		if(isset($_POST['uniq'])){
			$res = $this->chat_model->getBlockUserData($_POST['uniq'],$_SESSION['unique_id']);
			print_r(json_encode($res));
		}
	}
	public function unBlockUser(){
		if(isset($_POST['uniq'])){
			$from = $_SESSION['unique_id'];
			$to = $_POST['uniq'];
			$this->chat_model->unBlockUser($from, $to);
		}
	}


 public function download_file(){
    $filename=base64_decode($this->uri->segment(3));
    $this->load->helper('download');
		force_download($filename, NULL);
}


}