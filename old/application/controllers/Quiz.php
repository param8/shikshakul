<?php 
class Quiz extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('quiz_model');
		$this->load->model('course_model');
		$this->load->model('subject_model');
		$this->load->library('csvimport');
	}

	public function index()
	{
		$this->not_admin_logged_in();
    $data['page_title'] = 'Quiz';
		$data['courses'] = $this->course_model->get_courses(array('courses.status' => 1));
		$data['quizTypes'] = $this->quiz_model->get_quiz_types(array('status' => 1));
	  $this->admin_template('quiz/quiz',$data);
	}

	public function quiz_type()
	{
		$this->not_admin_logged_in();
    $data['page_title'] = 'Quiz Type';
	  $this->admin_template('quiz/quiz-type',$data);
	}


	public function ajaxQuizType(){
		$this->not_admin_logged_in();
		$condition = array('quiz_type.status'=>1);
		$quizTypes = $this->quiz_model->make_datatables_quiz_type($condition); // this will call modal function for fetching data
		$data = array();
		foreach($quizTypes as $key=>$quizType) // Loop over the data fetched and store them in array
		{
			$button = '';
			$sub_array = array();
			$button .= '<a href="javascript:void(0)" onclick="view_quiz_type('.$quizType['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="View Quiz Type" class="btn  btn-sm  text-warning"><i class="fa fa-eye"></i></a>';
			$button .= '<a href="javascript:void(0)" onclick="edit_quiz_type('.$quizType['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit Quiz Type" class="btn btn-sm  text-primary"><i class="fa fa-edit"></i> </a>';
      $button .= '<a href="javascript:void(0)" onclick="delete_quiz_type('.$quizType['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete Quiz Type" class="btn btn-sm  text-danger"><i class="fa fa-trash"></i> </a>';
			$image = '<img src="'.$quizType['image'].'"  style="width:100px;height:100px;">';
			$sub_array[] = $key+1;
			$sub_array[] = $image;
			$sub_array[] = $quizType['title'];
			$sub_array[] = date('d-F-Y', strtotime($quizType['created_at']));
			$sub_array[] = $button;
		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"             =>     intval($_POST["draw"]),
			"recordsTotal"     =>     $this->quiz_model->get_all_data_quiz_type($condition),
			"recordsFiltered"  =>     $this->quiz_model->get_filtered_data_quiz_type($condition),
			"data"             =>     $data
		);
		
		echo json_encode($output);
	}


	public function store_quiz_type(){
		$this->not_admin_logged_in();

		$title = $this->input->post('title');

		if(empty($title)){
			echo json_encode(['status'=>403, 'message'=>'Please enter title']); 	
			exit();
	  }

		$quiz_type = $this->quiz_model->get_quiz_type(array('title'=>$title));

		if($quiz_type){
			echo json_encode(['status'=>403, 'message'=>'Quiz type already exists']); 	
			exit();
		}

		$this->load->library('upload');
    if($_FILES['image']['name'] != '')
		{
    $config = array(
      'upload_path' 	=> 'uploads/quiz_type',
      'file_name' 	=> str_replace(' ','',$title).uniqid(),
      'allowed_types' => 'jpg|jpeg|png|gif|webp',
      'max_size' 		=> '10000000',
    );
        $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('image'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]);
          exit();
      }
      else
      {
        $type = explode('.',$_FILES['image']['name']);
        $type = $type[count($type) - 1];
        $image = 'uploads/quiz_type/'.$config['file_name'].'.'.$type;
      }
     }else{
				$image = 'public/Image_not_available.png';
    }

		$data = array(
			'title' => $title,
			'image' => $image
			);

		$store = $this->quiz_model->store_quiz_type($data);
		if($store){
			echo json_encode(['status'=>200, 'message'=>'Quiz type added successfully']); 	
			exit();
		}else{
			echo json_encode(['status'=>403, 'message'=>'Something went wrong']); 	
			exit();
		}

	}

	public function quiz_type_form(){
		$id  = $this->input->post('id');
		$quiz_type = $this->quiz_model->get_quiz_type(array('id'=>$id));
		?>
<div class="form-group">
  <label for="recipient-name" class="col-form-label">Title:</label>
  <input type="text" class="form-control" name="title" id="title" value="<?=$quiz_type->title?>" placehoder="Title">
</div>

<div class="form-group">
  <label for="recipient-name" class="col-form-label">Upload Image:</label>
  <input type="file" class="form-control" name="image" id="image">
</div>
<div class="form-group">
  <img src="<?=base_url($quiz_type->image)?>" style="width:100px;height:100px;">
</div>
<input type="hidden" name="id" id="id" value="<?=$id?>">
<?php
	}

	public function update_quiz_type(){
    $this->not_admin_logged_in();
     $id = $this->input->post('id');
		$title = $this->input->post('title');

		if(empty($title)){
			echo json_encode(['status'=>403, 'message'=>'Please enter title']); 	
			exit();
	  }

		$quiz_type = $this->quiz_model->get_quiz_type(array('id'=>$id));

		if($quiz_type->title==$title){
			echo json_encode(['status'=>403, 'message'=>'Quiz type already exists']); 	
			exit();
		}

		$this->load->library('upload');
    if($_FILES['image']['name'] != '')
		{
    $config = array(
      'upload_path' 	=> 'uploads/quiz_type',
      'file_name' 	=> str_replace(' ','',$title).uniqid(),
      'allowed_types' => 'jpg|jpeg|png|gif|webp',
      'max_size' 		=> '10000000',
    );
        $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('image'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]);
          exit();
      }
      else
      {
        $type = explode('.',$_FILES['image']['name']);
        $type = $type[count($type) - 1];
        $image = 'uploads/quiz_type/'.$config['file_name'].'.'.$type;
      }
     }else{
			if(!empty($quiz_type->image)){
				$image = $quiz_type->image;
			}else{
				$image = 'public/Image_not_available.png';
			}
				
    }

		$data = array(
			'title' => $title,
			'image' => $image
			);

		$update = $this->quiz_model->update_quiz_type($data,$id);
		if($update){
			echo json_encode(['status'=>200, 'message'=>'Quiz type updated successfully']); 	
			exit();
		}else{
			echo json_encode(['status'=>403, 'message'=>'Something went wrong']); 	
			exit();
		}
	}

	public function quiz_type_form_view(){
		$id  = $this->input->post('id');
		$quiz_type = $this->quiz_model->get_quiz_type(array('id'=>$id));
		?>
<div class="form-group">
  <label for="recipient-name" class="col-form-label">Title:</label>
  <input type="text" class="form-control" name="title" id="title" value="<?=$quiz_type->title?>" placehoder="Title"
    readonly>
</div>

<div class="form-group">
  <img src="<?=base_url($quiz_type->image)?>" style="width:100px;height:100px;">
</div>
<input type="hidden" name="id" id="id" value="<?=$id?>">
<?php
	}

	public function delete_quiz_type(){
		$id = $this->input->post('id');
		$data = array(
			'status' => 0
		);
		$update = $this->quiz_model->update_quiz_type($data,$id);
		if($update){
			// $quiz_data = array(
			// 	'status' => 0
			// );
			//  $this->quiz_model->update_quiz($quiz_data,array('quiz_type'=>$id));
			echo json_encode(['status'=>200, 'message'=>'Quiz type deleted successfully']); 	
			exit();
		}else{
			echo json_encode(['status'=>403, 'message'=>'Something went wrong']); 	
			exit();
		}
	}




	public function quiz()
	{
    $data['page_title'] = 'Quizs';
		$data['quizs'] = $this->quiz_model->get_quiz_types(array('status'=>1));
	  $this->template('web/quiz/quiz',$data);
	}

	public function quiz_list()
	{
        $data['page_title'] = 'Quizs List';
		    $quiz  = $this->uri->segment(2);
        $data['quizType'] = $quiz;
	  $this->template('web/quiz/quiz-list',$data);
	}

	public function ajaxDailyPanelQuiz(){
		
		$quiz_type =  base64_decode($this->uri->segment(3));
		$condition = array('quiz.status'=>1,'quiz_type.title'=>$quiz_type);
		$quizs = $this->quiz_model->make_datatables($condition); // this will call modal function for fetching data
		$recordsTotal = 	$this->quiz_model->get_all_data($condition);
		$recordsFiltered =	$this->quiz_model->get_filtered_data($condition);

		$data = array();
		foreach($quizs as $key=>$quiz) // Loop over the data fetched and store them in array
		{
			$quiz_url = $this->session->userdata('email') ? base_url('start-quiz/'.$quiz['slug']) : base_url('register');
			$sub_array = array();
			$sub_array[] = ' <div class="quizlist">
									<div class="quizparagraph">
										<p>'.$quiz['title'].'</p>
										<span><i class="fas fa-calendar-alt"></i>'.date("d M, Y",strtotime($quiz['created_at'])).'</span>
									</div>
									<div class="quizbutton">
										<a href="'.$quiz_url.'">Start Quiz</a>
									</div>
								</div>';

		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"             =>     intval($_POST["draw"]),
			"recordsTotal"     =>     $recordsTotal,
			"recordsFiltered"  =>     $recordsFiltered,
			"data"             =>     $data
		);
		
		echo json_encode($output);
	}

	public function start_quiz(){
		$this->not_logged_in();
		$this->user_verification();
		$quiz_type =	$this->uri->segment(2);
	  $data['panel'] = 'Panel';
		$data['quiz'] =$this->quiz_model->get_quiz(array('quiz.slug'=>$quiz_type));
		$data['questions'] = $this->quiz_model->get_quiz_questions(array('quizID'=>$data['quiz']->id));
		$data['page_title'] = $data['quiz']->title;
		$data['quiz_type']  = $data['quiz']->quizType;
		$data['quizID']  = $data['quiz']->id;
		$this->template('web/panel/start-quiz',$data);
	}

	public function ajaxQuiz(){
		$this->not_admin_logged_in();
		$condition = array('quiz.status'=>1,'quiz_type.status'=>1);
		$quizs = $this->quiz_model->make_datatables($condition); // this will call modal function for fetching data
		$data = array();
		foreach($quizs as $key=>$quiz) // Loop over the data fetched and store them in array
		{
			$button = '';
			$sub_array = array();
			$button .= '<a href="'.base_url('view-quiz/'.base64_encode($quiz['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="View Quiz" class="btn  btn-sm  text-warning"><i class="fa fa-eye"></i></a>';
			$button .= '<a href="'.base_url('edit-quiz/'.base64_encode($quiz['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit Quiz" class="btn btn-sm  text-primary"><i class="fa fa-edit"></i> </a>';
      $button .= '<a href="javascript:void(0)" onclick="delete_quiz('.$quiz['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete Quiz" class="btn btn-sm  text-danger"><i class="fa fa-trash"></i> </a>';
			
			$sub_array[] = $key+1;
			$sub_array[] = $quiz['title'];
			$sub_array[] = $quiz['courseName'];
			$sub_array[] = $quiz['subjectName'];
			$sub_array[] = $quiz['quizType'];
			$sub_array[] = date('d-F-Y', strtotime($quiz['created_at']));
			$sub_array[] = $button;
		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"             =>     intval($_POST["draw"]),
			"recordsTotal"     =>     $this->quiz_model->get_all_data($condition),
			"recordsFiltered"  =>     $this->quiz_model->get_filtered_data($condition),
			"data"             =>     $data
		);
		
		echo json_encode($output);
	}

		public function create_quiz(){
			$this->not_admin_logged_in();
			$data['page_title'] = 'Create Quiz';
			$data['courses'] = $this->course_model->get_courses(array('courses.status'=>1));
			$data['quizTypes'] = $this->quiz_model->get_quiz_types(array('status'=>1));
			$this->admin_template('quiz/create-quiz',$data);
		}

	public function edit_quiz(){
		$this->not_admin_logged_in();
		$id = base64_decode($this->uri->segment(2)); 
        $data['page_title'] = 'Edit  Quiz';
		$data['courses'] = $this->course_model->get_courses(array('courses.status'=>1));
		$data['quizTypes'] = $this->quiz_model->get_quiz_types(array('status'=>1));
		$data['quiz'] = $this->quiz_model->get_quiz(array('quiz.id' => $id));
		$data['questions'] = $this->quiz_model->get_quiz_questions(array('quizID' => $id));
	  $this->admin_template('quiz/edit-quiz',$data);
	}

	public function view_quiz(){
		$this->not_admin_logged_in();
		$id = base64_decode($this->uri->segment(2)); 
    $data['page_title'] = 'View  Quiz';
		$data['courses'] = $this->course_model->get_courses(array('courses.status'=>1));
		$data['quizTypes'] = $this->quiz_model->get_quiz_types(array('status'=>1));
		$data['quiz'] = $this->quiz_model->get_quiz(array('quiz.id' => $id));
		$data['questions'] = $this->quiz_model->get_quiz_questions(array('quizID' => $id));
	  $this->admin_template('quiz/view-quiz',$data);
	}

	public function store_quiz(){
		$title = $this->input->post('title');
		$courseID = $this->input->post('courseID');
		$subjectID = $this->input->post('subjectID');
		$quizTypeID = $this->input->post('quizType');
		$questions = $this->input->post('question');
		$option = $this->input->post('option');
		$answers = $this->input->post('answer');
		$slug = str_replace(' ','-',strtolower($title));

		$quiz = $this->quiz_model->get_quiz(array('quiz.title'=>$title));

		if(empty($title)){
				echo json_encode(['status'=>403, 'message'=>'Please enter title']); 	
			  exit();
		}

		if($quiz){
			echo json_encode(['status'=>403, 'message'=>'This quiz already exits']); 	
			exit();
		}

		if(empty($courseID)){
			echo json_encode(['status'=>403, 'message'=>'Please select course']); 	
			exit();
	 }

	 if(empty($subjectID)){
		echo json_encode(['status'=>403, 'message'=>'Please select subject']); 	
		exit();
  }

	if(empty($quizTypeID)){
		echo json_encode(['status'=>403, 'message'=>'Please select quiz type']); 	
		exit();
  }

    
		if(empty($questions)){
			echo json_encode(['status'=>403, 'message'=>'Please enter question']); 	
			exit();
		}

		if(empty($option)){
			echo json_encode(['status'=>403, 'message'=>'Please enter options']); 	
			exit();
		}

		if(empty($answers)){
			echo json_encode(['status'=>403, 'message'=>'Please enter answers']); 	
			exit();
		}

		$quiz_data= array(
			'courseID' => $courseID,
			'subjectID' => $subjectID,
			'quizTypeID' => $quizTypeID,
			'title' => $title,
			'slug'  => $slug,
		);

	$quizID = $this->quiz_model->store_quiz($quiz_data);
     
		foreach(array_filter($questions) as $key=>$question){

			$question_data =array(
				'quizID'   => $quizID,
				'question' => trim($question),
			);
			$qID = $this->quiz_model->store_quiz_question($question_data);
				for($i=1; $i<=4; $i++){
					$option_value = $option['question'.$i.$key];
					$option_data = array(
						'qid'    => $qID,
						'option' => trim($option_value),
					);
					$option_store = $this->quiz_model->store_quiz_option($option_data);
				}
				
         $options = $this->quiz_model->get_quiz_options(array('qID'=>$qID,'option'=>$answers['question'.$key]));

				foreach($options as $opt){
					$optionID = $opt->id;
					$answer_data = array(
						'qid'      => $qID,
						'optionID' => $optionID,
					);
					$answer_store = $this->quiz_model->store_quiz_answer($answer_data);
				}
				
		}
		echo json_encode(['status'=>200, 'message'=>'Quiz added successfully ']); 	
		exit();
	}

	public function update_quiz(){
		$id = $this->input->post('quizID');
		
		$questions = $this->quiz_model->get_quiz_questions(array('quizID' => $id));
		foreach ($questions as $question){
      $this->quiz_model->delete_quiz_question($question['id']);
		}
		$delete = $this->quiz_model->delete_quiz($id);

		$title = $this->input->post('title');
		$courseID = $this->input->post('courseID');
		$subjectID = $this->input->post('subjectID');
		$quizTypeID = $this->input->post('quizType');
		$questions = $this->input->post('question');
		$option = $this->input->post('option');
		$answers = $this->input->post('answer');
		$slug = str_replace(' ','-',strtolower($title));

		$quiz = $this->quiz_model->get_quiz(array('quiz.title'=>$title));

		if(empty($title)){
				echo json_encode(['status'=>403, 'message'=>'Please enter title']); 	
			  exit();
		}

		if($quiz){
			echo json_encode(['status'=>403, 'message'=>'This quiz already exits']); 	
			exit();
		}

		if(empty($courseID)){
			echo json_encode(['status'=>403, 'message'=>'Please select course']); 	
			exit();
	 }

	 if(empty($subjectID)){
		echo json_encode(['status'=>403, 'message'=>'Please select subject']); 	
		exit();
  }

	if(empty($quizTypeID)){
		echo json_encode(['status'=>403, 'message'=>'Please select quiz type']); 	
		exit();
  }

    
		if(empty($questions)){
			echo json_encode(['status'=>403, 'message'=>'Please enter question']); 	
			exit();
		}

		if(empty($option)){
			echo json_encode(['status'=>403, 'message'=>'Please enter options']); 	
			exit();
		}

		if(empty($answers)){
			echo json_encode(['status'=>403, 'message'=>'Please enter answers']); 	
			exit();
		}

		$quiz_data= array(
			'courseID' => $courseID,
			'subjectID' => $subjectID,
			'quizTypeID' => $quizTypeID,
			'title' => $title,
			'slug'  => $slug,
		);

	$quizID = $this->quiz_model->store_quiz($quiz_data);
     
		foreach(array_filter($questions) as $key=>$question){

			$question_data =array(
				'quizID'   => $quizID,
				'question' => trim($question),
			);
			$qID = $this->quiz_model->store_quiz_question($question_data);
				for($i=1; $i<=4; $i++){
					$option_value = $option['question'.$i.$key];
					$option_data = array(
						'qid'    => $qID,
						'option' => trim($option_value),
					);
					$option_store = $this->quiz_model->store_quiz_option($option_data);
				}
				
         $options = $this->quiz_model->get_quiz_options(array('qID'=>$qID,'option'=>$answers['question'.$key]));

				foreach($options as $opt){
					$optionID = $opt->id;
					$answer_data = array(
						'qid'      => $qID,
						'optionID' => $optionID,
					);
					$answer_store = $this->quiz_model->store_quiz_answer($answer_data);
				}
				
		}
		echo json_encode(['status'=>200, 'message'=>'Quiz updated successfully ']); 	
		exit();
	}

	public function delete_quiz(){
		$id = $this->input->post('id');
		$questions = $this->quiz_model->get_quiz_questions(array('quizID' => $id));
		foreach ($questions as $question){
      $this->quiz_model->delete_quiz_question($question['id']);
		}
		$delete = $this->quiz_model->delete_quiz($id);
		if($delete){
			echo json_encode(['status'=>200, 'message'=>'Quiz deleted successfully']); 	
			exit();
		}else{
			echo json_encode(['status'=>403, 'message'=>'Something went wrong']); 	
			exit();
		}
	}


	public function download_sample(){
		$this->load->helper('download');
		$pth  = file_get_contents(base_url('uploads/sample_csv/course_quiz_sample.csv'));
		$nme   =  "sample_quiz.csv";
		force_download($nme ,$pth);    
	}


	public function bulk_upload_quiz(){
		$title = $this->input->post('title');
		$courseID=$this->input->post('courseID');
		$subjectID=$this->input->post('subjectID');
		$quizType=$this->input->post('quizType');
		$slug = str_replace(' ','-',strtolower($title));

		$quiz = $this->quiz_model->get_quiz(array('quiz.title'=>$title));

		if(empty($title)){
				echo json_encode(['status'=>403, 'message'=>'Please enter title']); 	
			  exit();
		}

		if($quiz){
			echo json_encode(['status'=>403, 'message'=>'This quiz already exits']); 	
			exit();
		}

		if(empty($courseID)){
			echo json_encode(['status'=>403, 'message'=>'Please select a course ']); 	
			exit();
		}

		if(empty($subjectID)){
			echo json_encode(['status'=>403, 'message'=>'Please select a course ']); 	
			exit();
		}

		if(empty($quizType)){
			echo json_encode(['status'=>403, 'message'=>'Please select a quiz type ']); 	
			exit();
		}
    if (isset($_FILES["quiz_file"])) {
      $config['upload_path']   = "uploads/csv/";
      $config['allowed_types'] = 'text/plain|text/csv|csv';
      $config['max_size']      = '2048';
      $config['file_name']     = $_FILES["quiz_file"]['name'];
      $config['overwrite']     = TRUE;
      $this->load->library('upload', $config);
      $this->upload->initialize($config);
      if (!$this->upload->do_upload("quiz_file")) {
        echo json_encode(['status'=>403, 'message'=>'Please Upload Valid CSV files']);
      } else {
        $file_data = $this->upload->data();
        $file_path = 'uploads/csv/' . $file_data['file_name'];
        if ($this->csvimport->get_array($file_path)) {
					$quiz_data= array(
						'courseID'   => $courseID,
						'subjectID'  => $subjectID,
						'quizTypeID' => $quizType,
						'title'      => $title,
						'slug'       => $slug,
					);
			
				$quizID = $this->quiz_model->store_quiz($quiz_data);
          $csv_array = $this->csvimport->get_array($file_path);
          foreach ($csv_array as $key => $row) {

					$question_data =array(
						'quizID'   => $quizID,
						'question' => trim($row['question']),
					);
					$qID = $this->quiz_model->store_quiz_question($question_data);			
							$options = array($row['option1'], $row['option2'], $row['option3'], $row['option4']);
								foreach($options as $option){
									$option_data = array(
										'qid'    => $qID,
										'option' => trim($option),
									);
									$option_store = $this->quiz_model->store_quiz_option($option_data);
								}
								
								 $opt = $this->quiz_model->get_quiz_option(array('qID'=>$qID,'option'=>$row['answer']));
									$optionID = $opt->id;
									$answer_data = array(
										'qid'      => $qID,
										'optionID' => $optionID,
									);
									$answer_store = $this->quiz_model->store_quiz_answer($answer_data);
								     
          }
          echo json_encode(['status'=>200, 'message'=>'Quiz Uploaded Successfully']);
        } else {
          echo json_encode(['status'=>403, 'message'=>'Quiz Uploaded Failure']);
        }
      }
    } else {
      echo json_encode(['status'=>403, 'message'=>'Please upload CSV']);
    }
  }

	public function store_result(){
		$quiz_detail = json_encode($this->input->post('questions'));
		$correct_answers = $this->input->post('correctAnswerCount');
		$quizID = $this->input->post('quizID');
		$total_question = $this->input->post('total_question');
		$type = $this->input->post('type');
	  $data = array(
			'userID'          => $this->session->userdata('id'),
			'quizID'          => $quizID,
			'quiz_detail'     => $quiz_detail,
			'correct_answers' => $correct_answers,
			'total_question'  => $total_question,
			'type'            => $type
	  );

		$store = $this->quiz_model->store_quiz_result($data);
   if($store){
		echo json_encode(['status'=>200, 'message'=>'Quiz successfully submitted']);
	 } else {
		echo json_encode(['status'=>403, 'message'=>'Quiz Failure']);
	 }
	}

	public function quiz_result(){
		$this->not_logged_in();
		$data['panel'] = 'Panel';
    $data['results'] =$this->quiz_model->get_quiz_result(array('quiz_result.status'=>1,'quiz_result.userID'=>$this->session->userdata('id')));
		$data['page_title'] = 'Quiz Results';
		$this->template('web/panel/quiz-result',$data);
	}

	public function ajaxResultQuiz(){
		
		 $condition = array('quiz_result.status'=>1,'quiz_result.userID'=>$this->session->userdata('id'));
		 $results = $this->quiz_model->make_quiz_result_datatables($condition); // this will call modal function for fetching data
		 $recordsTotal = 	$this->quiz_model->get_result_quiz_all_data($condition);
		 $recordsFiltered =	$this->quiz_model->get_filtered_result_quiz_data($condition);
		
		
		$data = array();
		foreach($results as $key=>$result) // Loop over the data fetched and store them in array
		{
			$sub_array = array();
			$sub_array[] = $key+1;
			$sub_array[] = $result['title'];
			$sub_array[] = $result['quiz_type'];
			$sub_array[] = $result['correct_answers'].'/'.$result['total_question'];
			$sub_array[] = date('d F Y H : i : s',strtotime($result['created_at']));
		    $data[] = $sub_array;
		}
	
		$output = array(
			"draw"             =>     intval($_POST["draw"]),
			"recordsTotal"     =>     $recordsTotal,
			"recordsFiltered"  =>     $recordsFiltered,
			"data"             =>     $data
		);
		
		echo json_encode($output);
	}

	// -------For Questions-------

	public function questions(){
		$this->not_logged_in();
		$data['panel'] = 'Panel';
		$data['page_title'] = 'Question List';
        $data['questions'] = $this->quiz_model->get_questions(array('questions.status' => 1));
		$data['courses'] = $this->course_model->get_courses(array('courses.status' => 1));
	    $this->template('web/panel/question-list',$data);
	}

	public function storeQuestion(){
		$title = $this->input->post('title');
		$course = $this->input->post('course');
		$subject = $this->input->post('subject');

		if(empty($title)){
			echo json_encode(['status'=>403, 'message'=>'Please enter title']); 	
			exit();
		}

		if(empty($course)){
			echo json_encode(['status'=>403, 'message'=>'Please select course']); 	
			exit();
		}
		if(empty($subject)){
			echo json_encode(['status'=>403, 'message'=>'Please select subject']); 	
			exit();
		}

		$this->load->library('upload');
		if($_FILES['questionFile']['name'] != '')
			{
		$config = array(
			'upload_path' 	=> 'uploads/question',
			'file_name' 	=> str_replace(' ','',$name).uniqid(),
			'allowed_types' => 'jpg|jpeg|png|gif|webp|pdf|csv|docx',
			'max_size' 		=> '10000000',
		);
			$this->upload->initialize($config);
		if( ! $this->upload->do_upload('questionFile'))
			{
				$error = $this->upload->display_errors();
				echo json_encode(['status'=>403, 'message'=>$error]);
				exit();
			}
			else
			{
			$type = explode('.',$_FILES['questionFile']['name']);
			$type = $type[count($type) - 1];
			$question_file = 'uploads/question/'.$config['file_name'].'.'.$type;
			}
		}else{
			echo json_encode(['status'=>403, 'message'=>'Please choose File']);
			exit();
		}

			$data = array(
				'facultyID'     => $this->session->userdata('id'),
				'courseID'      => $course,
				'subjectID'     => $subject,
				'title'         => $title,
				'question_file' => $question_file,
		);

		$store = $this->quiz_model->store_question($data);

	    if($store){
			echo json_encode(['status'=>200, 'message'=>'Question successfully submitted']);
		 } else {
			echo json_encode(['status'=>403, 'message'=>'Somthing went wrong!']);
		 }
	}

	public function getCourse(){
		$courses = $this->course_model->get_courses(array('courses.status' => 1));
		?>

<option>Choose Course</option>
<?php foreach($courses as $course){ ?>
<option value="<?=$course->id?>"><?=$course->name?></option>
<?php } ?>

<?php	
	}

	public function getSubject(){
		$courseID = $this->input->post('courseID');
		$subjects = $this->subject_model->get_subjects(array('subject.courseID' => $courseID));

		?>
<?php 
		 if(count($subjects) > 0){?>
<option value="">Select Subject</option>
<?php 
		 foreach($subjects as $subject){ 
			?>
<option value="<?=$subject->id?>"><?=$subject->name?></option>
<?php }
	      }else{
		?>
<option>No Subject Available</option>
<?php } ?>

<?php	
	}


	public function ajaxQuestion(){
		
		$questions = $this->quiz_model->make_datatables_questions(); // this will call modal function for fetching data
		$data = array();
		foreach($questions as $key=>$question) // Loop over the data fetched and store them in array
		{
	        $url = $question['status']== 0 ? 'javascript:void(0)': base_url($question['question_file']);
			$ansUrl = $question['status']== 0 ? '': 'uploadAnswer('.$question['id'].')';
			$button = '';
			$sub_array = array();
            $button .= '<a href="'.$url.'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Download Question" class="btn btn-sm  text-primary" id="downloadQues"><i class="fa fa-download"></i> </a>';
			$button .= '<a href="javascript:void(0)" onclick="'.$ansUrl.'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Upload Answer" class="btn btn-sm  text-primary" id="uploadAns"><i class="fa fa-upload"></i> </a>'; 
			
			$sub_array[] = $key+1;
			$sub_array[] = $question['title'];
			$sub_array[] = $question['course'];
			$sub_array[] = $question['subject'];
			$sub_array[] = $this->session->userdata('user_type') == 'Faculties' ? $question['userName'] : '';
			$sub_array[] = $this->session->userdata('user_type') == 'Student' ? $button : date('d-F-Y', strtotime($question['created_at'])); 
			$data[] = $sub_array;
		}
	
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->quiz_model->get_all_data_questions(),
			"recordsFiltered"         =>     $this->quiz_model->get_filtered_data_questions(),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
	}

	public function storeAnswer(){
		$questionID = $this->input->post('quesid');

		$this->load->library('upload');
		if($_FILES['answerFile']['name'] != '')
			{
		$config = array(
			'upload_path' 	=> 'uploads/answer',
			'file_name' 	=> str_replace(' ','',$name).uniqid(),
			'allowed_types' => 'jpg|jpeg|png|gif|webp|pdf|csv|docx',
			'max_size' 		=> '10000000',
		);
			$this->upload->initialize($config);
		if(!$this->upload->do_upload('answerFile'))
			{
				 $error = $this->upload->display_errors();
				echo json_encode(['status'=>403, 'message'=>$error]);
				exit();
			}
			else
			{
			$type = explode('.',$_FILES['answerFile']['name']);
			$type = $type[count($type) - 1];
			$answer_file = 'uploads/answer/'.$config['file_name'].'.'.$type;
			}
		}else{
			echo json_encode(['status'=>403, 'message'=>'Please choose File']);
			exit();
		}
			$data = array(
				'questionID'    => $questionID,
				'studentID'     => $this->session->userdata('id'),
				'answer_file'   => $answer_file,
			);
			
			$store = $this->quiz_model->store_answer($data);
            $this->quiz_model->update_status(array('status' => 0),$questionID);
	    if($store){
			echo json_encode(['status'=>200, 'message'=>'Answer file upload successfully']);
		 } else {
			echo json_encode(['status'=>403, 'message'=>'Somthing went wrong!']);
		 }
	}

	public function answers(){
		$this->not_logged_in();
		$data['panel'] = 'Panel';
		$data['page_title'] = 'Answer List';
        $data['answers'] = $this->quiz_model->get_answers(array('status' => 1));
	    $this->template('web/panel/answer-list',$data);
	}


	public function ajaxAnswer(){
		$condition = array('answers.status'=> 1);
		$answers = $this->quiz_model->make_datatables_answers($condition); // this will call modal function for fetching data
		$data = array();
		foreach($answers as $key=>$answer) // Loop over the data fetched and store them in array
		{
			if(empty($answer['result'])){
				$result = 'Not Check';
			}else{
				$result = $answer['result'];
			}
	
			$button = '';
			$sub_array = array();
             $button .= '<a href="'.base_url($answer['answer_file']).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Download Answer" class="btn btn-sm  text-primary" id="downloadQues"><i class="fa fa-download"></i> </a>';
			 $button .= '<a href="javascript:void(0)" onclick="uploadResult('.$answer['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Upload Result" class="btn btn-sm  text-primary" id="uploadAns"><i class="fa fa-upload"></i>Result </a>'; 
			
			$sub_array[] = $key+1;
			$sub_array[] = $answer['userName'];
			$sub_array[] = $answer['title'];
			$sub_array[] = $answer['course'];
			$sub_array[] = $answer['subject'];
			$sub_array[] = date('d-F-Y', strtotime($answer['created_at']));
			$sub_array[] = $result;
			$sub_array[] = $button; 
		    $data[] = $sub_array;
		}
	
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->quiz_model->get_all_data_answers($condition),
			"recordsFiltered"         =>     $this->quiz_model->get_filtered_data_answers($condition),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
	}

	public function storeResult(){
		$answerId = $this->input->post('answerId');
		$result = $this->input->post('result');

		if(empty($result)){
			echo json_encode(['status'=>403, 'message'=>'Please enter result']); 	
			exit();
		}

		$data = array(
      'result' => $result
		);

		$store = $this->quiz_model->store_result($data,$answerId);

	    if($store){
			 echo json_encode(['status'=>200, 'message'=>'Result successfully Submited']);
		 } else {
			 echo json_encode(['status'=>403, 'message'=>'Somthing went wrong!']);
		 }
 
	}

}