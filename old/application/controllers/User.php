<?php 
class User extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();


		$this->load->model('user_model');
	}

	public function index()
	{
		$this->not_admin_logged_in();
    $data['page_title'] = 'Staff';
	
    //$data['users'] = $this->user_model->get_all_users(array('users.user_type' => 'User'));
	$this->template('staff',$data);
	}

	 public function profile(){
		$this->not_logged_in();
		$data['page_title'] = 'Profile';
		$data['panel'] = 'Panel';
		$id = $this->session->userdata('id');
		$data['user'] =$this->user_model->get_user(array('users.id'=>$id));
		$data['states'] = $this->Common_model->get_states(array('country_id'=>101));
		$this->template('web/panel/profile',$data);
	 }

	 public function update(){
		$userID = $this->input->post('userID');
		$name = $this->input->post('name');
		// $email = $this->input->post('email');
		// $phone = $this->input->post('phone');
		$address = $this->input->post('address');
		$city = $this->input->post('city');
		$state = $this->input->post('state');
		$pincode = $this->input->post('pincode');
		$gender = $this->input->post('gender');
		$dob = $this->input->post('dob');

		$about = $this->input->post('about');
		$user = $this->user_model->get_user(array('users.id'=>$userID));
		if(empty($name)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your name']); 	
			exit();
		}
		// if(empty($email)){
		// 	echo json_encode(['status'=>403, 'message'=>'Please enter your email address']); 	
		// 	exit();
		// }
		// $checkEmail = $this->user_model->get_staff(array('email'=>$email,'id <>'=>$userID));
		// if($checkEmail){
		// 	echo json_encode(['status'=>403,'message'=>'This email is already in use']);
		// 	exit();
		// }
	
		// if(empty($phone)){
		// 	echo json_encode(['status'=>403, 'message'=>'Please enter your mobile']); 	
		// 	exit();
		// }
	
		// $checkPhone = $this->user_model->get_staff(array('phone'=>$phone,'id <>'=>$userID));
		// if($checkPhone){
		// 	echo json_encode(['status'=>403,'message'=>'This phone no. is already in use']);
		// 	exit();
		// }
	
	
	
		if(empty($state)){
			echo json_encode(['status'=>403, 'message'=>'Please select state']); 	
			exit();
		}
		if(empty($city)){
			echo json_encode(['status'=>403, 'message'=>'Please select city']); 	
			exit();
		}
	
		if(empty($address)){
			echo json_encode(['status'=>403, 'message'=>'Please enter address']);  	
			exit();
		}
	
		$this->load->library('upload');
		if($_FILES['profile_pic']['name'] != '')
			{
		$config = array(
			'upload_path' 	=> 'uploads/user',
			'file_name' 	=> str_replace(' ','',$name).uniqid(),
			'allowed_types' => 'jpg|jpeg|png|gif|webp',
			'max_size' 		=> '10000000',
		);
			$this->upload->initialize($config);
		if ( ! $this->upload->do_upload('profile_pic'))
			{
				$error = $this->upload->display_errors();
				echo json_encode(['status'=>403, 'message'=>$error]);
				exit();
			}
			else
			{
			$type = explode('.',$_FILES['profile_pic']['name']);
			$type = $type[count($type) - 1];
			$profile_pic = 'uploads/user/'.$config['file_name'].'.'.$type;
			}
		}else{
			if(!empty($user->profile_pic)){
				$profile_pic = $user->profile_pic;
			}else{
				$profile_pic = 'public/dummy_user.png';
			}
			
		}
		
		$data = array(
			'name'       => $name,
			'address'    => $address ,
			'city'       => $city,
			'state'      => $state,
			'pincode'    => $pincode,
			'profile_pic'=> $profile_pic,
		);
		$update = $this->user_model->update_user($data,$userID);
		if($update){
			$user_data = array(
				'subject' =>$subject,
				'dob'     =>$dob,
				'gender'  =>$gender,
				'about'   =>$about,
			);
			 $this->user_model->update_user_detail($user_data,$userID);
			 $session_new = array(
				'name'        => $name,
				'phone'       => $phone,
				'address'     => $address,
				'city'        => $city,
				'state'       => $state,
				'pincode'     => $pincode,
				'profile_pic' => $profile_pic,
			);
			$this->session->set_userdata($session_new);
			echo json_encode(['status'=>200, 'message'=>'Student updated successfully!']);
		}else{
			echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
		}
	
		}

		public function reset_password(){
			$this->not_logged_in();
			$data['page_title'] = 'Profile';
			$data['panel'] = 'Panel';
			$this->template('web/panel/reset-password',$data);
		}

		public function update_password(){
			$userID = $this->session->userdata('id');
			$old_password = $this->input->post('old_password');
			$new_password = $this->input->post('new_password');
			$confirm_password = $this->input->post('confirm_password');
      
			$user = $this->user_model->get_user(array('users.id' =>$userID));

			if(empty($old_password)){
				echo json_encode(['status'=>403, 'message'=>'Please enter old password']);  	
				exit();
			}

			if($user->password != md5($old_password)){
				echo json_encode(['status'=>403, 'message'=>'Old password is incorrect']);  	
				exit();
			}

			if(empty($new_password)){
				echo json_encode(['status'=>403, 'message'=>'Please enter new password']);  	
				exit();
			}

			if(strlen($new_password)<6){
				echo json_encode(['status'=>403, 'message'=>'Password minimum length must be at least 6 characters']);  	
				exit();
			}

			if(empty($confirm_password)){
				echo json_encode(['status'=>403, 'message'=>'Please enter confirm password']);  	
				exit();
			}


			if($new_password != $confirm_password){
				echo json_encode(['status'=>403, 'message'=>'New Password and Confirm Password must be the same']);  	
				exit();
			}

			$data = array(
				'password' => md5($new_password)
			);
   
			$update = $this->user_model->update_user($data,$userID);
			if($update){
		  	echo json_encode(['status'=>200, 'message'=>'Password updated successfully please login again !..']); 
			}else{
				echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
			}
		}

		public function forget_password()
		{
			$data['page_title'] = 'Forget Password';	
		  $this->template('web/forget-password',$data);
		}

		public function verify_detail(){
			$user_detail = $this->input->post('user_detail');
     
			if(empty($user_detail)){
				echo json_encode(['status'=>403, 'message'=>'Please enter user detail']);  	
				exit();
			}

			$users = $this->user_model->get_user_detail($user_detail);

			if($users->num_rows() <=0){
				echo json_encode(['status'=>403, 'message'=>'Your details are not valid please try again']);   	
				exit();
			}

			$user = $users->row();
      $otp = rand(0,999999);
			$subject = "Welcome to Shikshakul";
		  $html = 'Dear '.$user->name.',<br>
			Your OTP for reset your password to Shikshakul Classes is '.$otp.' <br><br>
			Best Regards,<br>
			Shikshakul Team<br>';
		 $sendEmail =	sendEmail($user->email,$subject,$html);
      
		 $templateID = '1707168795609801101';
		 $message = urlencode('Your OTP for reset your password to Shikshakul Classes is '.$otp);
		 $sendSMS =	 sendSMS($user->phone,$message,$templateID);
     $decode_sms =  json_decode($sendSMS);
		if($sendEmail==1 && $decode_sms->statusCode==200){
			$session_data = array(
       'detail'     => $user_detail,
			 'verify_otp' => $otp
			);
			$this->session->set_userdata($session_data);
			echo json_encode(['status'=>200, 'message'=>'Password updated successfully please login again !..']); 
		}else{
			echo json_encode(['status'=>403, 'message'=>'Please try again OTP not send !..']); 
		}

		}

		public function change_password(){
			$data['page_title'] = 'Change Password';	
		  $this->template('web/change-password',$data);
		}

		public function password_change(){
			$verify_otp = $this->session->userdata('verify_otp');
			$detail = $this->session->userdata('detail');
			$otp = $this->input->post('otp');
			$new_password = $this->input->post('new_password');
			$confirm_password = $this->input->post('confirm_password');
      
			$users = $this->user_model->get_user_detail($detail);

			$userID = $users->row()->id;

			if(empty($otp)){
				echo json_encode(['status'=>403, 'message'=>'Please enter  OTP']);  	
				exit();
			}

			if($verify_otp != $otp){
				echo json_encode(['status'=>403, 'message'=>'Please enter valid OTP']);  	
				exit();
			}

			if(empty($new_password)){
				echo json_encode(['status'=>403, 'message'=>'Please enter new password']);  	
				exit();
			}

			if(strlen($new_password)<6){
				echo json_encode(['status'=>403, 'message'=>'Password minimum length must be at least 6 characters']);  	
				exit();
			}

			if(empty($confirm_password)){
				echo json_encode(['status'=>403, 'message'=>'Please enter confirm password']);  	
				exit();
			}

			if($new_password != $confirm_password){
				echo json_encode(['status'=>403, 'message'=>'New Password and Confirm Password must be the same']);  	
				exit();
			}

			$data = array(
				'password' => md5($new_password)
			);
   
			$update = $this->user_model->update_user($data,$userID);
			if($update){
		  	echo json_encode(['status'=>200, 'message'=>'Password updated successfully please login again !..']); 
			}else{
				echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
			}
		}
	
}