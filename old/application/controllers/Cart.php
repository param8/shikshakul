<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Cart extends MY_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('course_model');
    $this->load->model('order_model');
    $this->load->model('test_series_model');
    $this->load->library('ccavenue');
  }

  public function index()
  {
    $this->not_logged_in();
    $data['page_title'] = 'Cart'; 
    $this->template('web/cart/cart',$data);
  }

  public function checkout()
  {
    $this->not_logged_in();
    $this->user_verification();
    $data['page_title'] = 'Checkout'; 
    $this->template('web/cart/checkout',$data);
  }

  public function store_cart(){
    $this->not_logged_in();
    $courseID = $this->input->post('courseID');
    $order_type = $this->input->post('order_type');
    if($order_type=='Course'){
      $cartItem = $this->course_model->get_course(array('courses.id'=>$courseID));
      $title =  $cartItem->name;
      $items = $cartItem->categoryID;
      $price =  $cartItem->price;
      $image =  $cartItem->image;
    }

    if($order_type=='Test Series'){
      $cartItem = $this->test_series_model->get_test_series(array('test_series.id'=>$courseID));
      $title =  $cartItem->title;
      $items =  $cartItem->series_type;
      $price =  $cartItem->price;
      $image =  base_url('public/Image_not_available-old.png');
    }

    
    
  
    $data = array(
    'id'   => $courseID,
    'name' => $title,
    'qty' => 1,
    'image' => $image,
    'price' =>$price,
    'items' =>$items,
    'options' => $order_type
    );

     $cart = $this->cart->insert($data);
     $cartItems=$this->cart->contents();
     echo json_encode(['status'=>200, 'totalCount'=> $this->cart->total_items()]);
  }


  public function update_cart(){
    $this->not_logged_in();
   $qty =  $this->input->post('qty');
   $rowid =  $this->input->post('rowid');
   $data = array(
    'rowid' => $rowid,
    'qty'   => $qty,
   );
   $update=$this->cart->update($data);
   echo json_encode(['status'=>200, 'totalCount'=> $this->cart->total_items()]);
  }

  public function removeCartItem() {   
    $this->not_logged_in();
    $rowid =  $this->input->post('rowid');
    $data = array(
        'rowid'   => $rowid,
        'qty'     => 0
    );
    echo json_encode(['status'=>200, 'totalCount'=> $this->cart->total_items()]);
    $update=$this->cart->update($data);
}

public function order_now(){
  $this->not_logged_in();
 $payment_type = $this->input->post('payment_type');
 $tin =strtotime("now");
 $course_price =array();
 $course_id = array();
 $coupen = array();
 $item_discount = array();
 $id =  $this->session->userdata('id');
 $gst = empty($this->session->userdata('gst')) ? ($this->cart->total()*18)/100 : $this->session->userdata('gst');
 $price = empty($this->session->userdata('addPrice')) ? $this->cart->total() : $this->session->userdata('addPrice');
 $totalAmount = empty($this->session->userdata('total_price')) ? $this->cart->total()+$gst : $this->session->userdata('total_price');
 
 $discount = empty($this->session->userdata('discount')) ? 0 : $this->session->userdata('discount');
 $order_id = 'SHIKSHAKUL'.uniqid();
 if($this->session->userdata('course_id')){
  $course_id[] = $this->session->userdata('course_id');
  $course_price[$this->session->userdata('course_id')] = $this->session->userdata('addPrice');
  $coupen[$this->session->userdata('course_id')] = $this->session->userdata('coupon_code');
  $item_discount[$this->session->userdata('course_id')] = $this->session->userdata('discount');
 }

 
 
 $data =array(
   'orderID'        => $order_id,
   'userID'         => $id,
   'price'          => $price,
   'total_price'    => $totalAmount,
   'discount'       => $discount,
   'gst'            => $gst,
   'payment_type'   => $payment_type,
   'payment_detail' => '',
   'coupon_code'    => $this->session->userdata('coupon_code'),
 );
 
 $orderID = $this->order_model->store_order($data);

 $cartItems=$this->cart->contents();
    
  foreach($cartItems as $item){

   $item_data = array(
    'orderID'            => $orderID,
    'courseID'           => $item['id'],
    'qty'                => $item['qty'],
    'price'              => in_array($item['id'],$course_id) ? $course_price[$item['id']] : $item['price'],
    'course_discount'    => in_array($item['id'],$course_id) ? $item_discount[$item['id']] : 0,
    'total_course_price' =>  $item['price'],
    'coupen_course_code' => in_array($item['id'],$course_id) ? $coupen[$item['id']] : '',
    'order_type'         =>  $item['options']
   );
  
  $store_item = $this->order_model->store_item($item_data);
  }

  if($orderID){

    if($payment_type=='Offline'){
    $this->cart->destroy();
    $this->session->unset_userdata('course_id');
    $this->session->unset_userdata('addPrice');
    $this->session->unset_userdata('gst');
    $this->session->unset_userdata('total_price');
    $this->session->unset_userdata('discount');
    $this->session->unset_userdata('course_price');
    $this->session->unset_userdata('coupon_code');
    if($payment_type=='Offline'){
    echo json_encode(['status'=>200,'message'=>'Offline Course Booked Successfully Please wait for approve by admin','payment_type'=>'Offline','order_status'=>base64_encode('Success'),'order_id'=>base64_encode($orderID)]);
    }else{
    echo json_encode(['status'=>403]);
  }
    //redirect(base_url('booking-success/'.base64_encode($orderID)));
    }elseif($payment_type=='Online'){
      $data=array(
        'tid'=>$tin,
        'merchant_id'=>'2621706',
        'order_id'=>$orderID,
        'amount'=>$totalAmount,
        'currency'=>'INR',
        'redirect_url'=>base_url('Payment_handler'),
        'cancel_url'=>base_url('Payment_handler'),
        'language'=>'EN',
        'delivery_name'=>$this->session->userdata('name'),
        'delivery_address'=>$this->session->userdata('address'),
        'delivery_city'=>'',
        'delivery_state'=>'',
        'delivery_zip'=>$this->session->userdata('pincode'),
        'delivery_country'=>'India',
        'delivery_tel'=>$this->session->userdata('mobile'),
        'billing_name'=>$this->session->userdata('name'),
        'billing_address'=>$this->session->userdata('address'),
        'billing_city'=>'',
        'billing_state'=>'',
        'billing_zip'=>$this->session->userdata('pincode'),
        'billing_country'=>'India',
        'billing_tel'=>$this->session->userdata('mobile'),
        'billing_email'=>$this->session->userdata('email')
        );
        $merchant_data='';
        $working_key='26AE120E160E60BFB55975425A04267E';//Shared by CCAVENUES
        $access_code='AVQG84KG24AF94GQFA';//Shared by CCAVENUES
        
        foreach ($data as $key => $value){
            $merchant_data.=$key.'='.urlencode($value).'&';
        }

        $encrypted_data=$this->ccavenue->encrypt($merchant_data,$working_key);// Method for encrypting the data.
        $encript_session =array(
            'data_enc' =>base64_encode($encrypted_data),
            'code' =>base64_encode($access_code)
        );
        $this->session->set_userdata($encript_session);
        echo json_encode(['status'=>200,'message'=>'','payment_type'=>'Online']);
      }else{
        echo json_encode(['status'=>403]);
      }
    }
  }



public function send(){
  $encrypted_data =base64_decode($this->session->userdata('data_enc'));
  $access_code = base64_decode($this->session->userdata('code'));
  ?>
  <form method="post" name="redirect" action="https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction"> 
  <?php
  echo "<input type=hidden name='encRequest' value=$encrypted_data>";
  echo "<input type=hidden name='access_code' value=$access_code>";
  ?>
  </form></center><script language='javascript'>document.redirect.submit();</script>
  <?php
}

public function orders(){
  $this->not_admin_logged_in();
  $data['page_title'] = 'Orders'; 
  $this->admin_template('finance/orders',$data);
}

public function ajaxOrders(){
  $this->not_admin_logged_in();
  
  if($this->session->userdata('order_type')=='Course'){
    $condition = array('items.order_type'=>$this->session->userdata('order_type'));
  $orders = $this->order_model->make_datatables($condition); // this will call modal function for fetching data
  $data = array();
  foreach($orders as $key=>$order) // Loop over the data fetched and store them in array
  {
    $button = '';
    $sub_array = array();
    // $button .= '<a href="'.base_url('view-student/'.base64_encode($student['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="View Staff" class="btn  btn-sm  text-warning"><i class="fa fa-eye"></i></a>';
    // $button .= '<a href="'.base_url('edit-student/'.base64_encode($student['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit Staff" class="btn  btn-sm  text-primary"><i class="fa fa-edit"></i> </a>';
    $button .= $order['orderStatus'] == 0 ? '<a href="javascript:void(0)" onclick="approve_payment('.$order['orderID'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Approve Payment" class="btn  btn-sm  btn-warning">Approve Payment </a>' : '<a href="javascript:void(0)"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Approved" class="btn  btn-sm  text-success">Approved</a>';
    $sub_array[] = $key+1;
    $sub_array[] = $order['userName'];
    $sub_array[] = $order['order_id'];
    $sub_array[] = '₹ '.$order['orderPrice'];
    $sub_array[] = '₹ '.$order['gst'];
    $sub_array[] = '₹ '.$order['discount'];
    $sub_array[] = '₹ '.$order['total_price'];
    $sub_array[] = $order['orderStatus'] == 1 ? '<span class="text-success">Paid</span>' :  '<span class="text-danger">Pending</span>';
    // $sub_array[] = $student['aadharcard'];
    $sub_array[] = date('d-m-Y', strtotime($order['created_at']));
    $sub_array[] = $button;
    $data[] = $sub_array;
  }

  $output = array(
    "draw"                    =>     intval($_POST["draw"]),
    "recordsTotal"            =>     $this->order_model->get_all_data($condition),
    "recordsFiltered"         =>     $this->order_model->get_filtered_data($condition),
    "data"                    =>     $data
  );
  
  echo json_encode($output);
}

elseif($this->session->userdata('order_type')=='Test Series'){
  $condition = array('items.order_type'=>$this->session->userdata('order_type'));
  $orders = $this->order_model->make_datatables_test($condition); // this will call modal function for fetching data
  $data = array();
  foreach($orders as $key=>$order) // Loop over the data fetched and store them in array
  {
    $button = '';
    $sub_array = array();
    // $button .= '<a href="'.base_url('view-student/'.base64_encode($student['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="View Staff" class="btn  btn-sm  text-warning"><i class="fa fa-eye"></i></a>';
    // $button .= '<a href="'.base_url('edit-student/'.base64_encode($student['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit Staff" class="btn  btn-sm  text-primary"><i class="fa fa-edit"></i> </a>';
    $button .= $order['orderStatus'] == 0 ? '<a href="javascript:void(0)" onclick="approve_payment('.$order['orderID'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Approve Payment" class="btn  btn-sm  btn-warning">Approve Payment </a>' : '<a href="javascript:void(0)"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Approved" class="btn  btn-sm  text-success">Approved</a>';
    $sub_array[] = $key+1;
    $sub_array[] = $order['userName'];
    $sub_array[] = $order['order_id'];
    $sub_array[] = '₹ '.$order['orderPrice'];
    $sub_array[] = '₹ '.$order['gst'];
    $sub_array[] = '₹ '.$order['discount'];
    $sub_array[] = '₹ '.$order['total_price'];
    $sub_array[] = $order['orderStatus'] == 1 ? '<span class="text-success">Paid</span>' :  '<span class="text-danger">Pending</span>';
    // $sub_array[] = $student['aadharcard'];
    $sub_array[] = date('d-m-Y', strtotime($order['created_at']));
    $sub_array[] = $button;
    $data[] = $sub_array;
  }

  $output = array(
    "draw"                    =>     intval($_POST["draw"]),
    "recordsTotal"            =>     $this->order_model->get_all_data_test($condition),
    "recordsFiltered"         =>     $this->order_model->get_filtered_data_test($condition),
    "data"                    =>     $data
  );
  
  echo json_encode($output);
}else{
  echo json_encode($output);
}
 

}

public function approve_payment(){
  $id = $this->input->post('id');
  $data = array(
    'payment_status' => 1,
    'status'         => 1,
  );
  $update = $this->order_model->update_order($data, $id);
  if($update)
		{
			echo json_encode(['status'=>200, 'message'=>'Payment Approved successfully!']);
      		exit();
		}else{
			echo json_encode(['status'=>403, 'message'=>'some things went wrong']);
      exit();
		}
 }


 public function delete_address(){
  $id = $this->input->post('id');
  $data = array(
    'status' => 0
  );
  $delete = $this->Cart_model->update_address($data,$id);
  if($delete)
		{
			echo json_encode(['status'=>200, 'message'=>'Delete shipping address successfully!']);
      		exit();
		}else{
			echo json_encode(['status'=>403, 'message'=>'some things went wrong']);
      exit();
		}
 }



 public function store_shippingAddress(){
  $name = $this->input->post('name');
  $email = $this->input->post('email');
  $mobile = $this->input->post('mobile');
  $address = $this->input->post('address');
  $street = $this->input->post('street');
  $pin_code = $this->input->post('pin_code');
  $state = $this->input->post('state');
  $city = $this->input->post('city');
  $userID = $this->session->userdata('id');
  if(empty($name)){
    echo json_encode(['status'=>403, 'message'=> 'Please enter a name']);
      exit();
  }
  if(empty($email)){
    echo json_encode(['status'=>403, 'message'=> 'Please enter a email']);
      exit();
  }
  if(empty($mobile)){
    echo json_encode(['status'=>403, 'message'=> 'Please enter a mobile']);
      exit();
  }
  if(empty($address)){
    echo json_encode(['status'=>403, 'message'=> 'Please enter a address']);
      exit();
  }
  if(empty($street)){
    echo json_encode(['status'=>403, 'message'=> 'Please enter a street']);
      exit();
  }
  if(empty($pin_code)){
    echo json_encode(['status'=>403, 'message'=> 'Please choose a pincode']);
      exit();
  }
  if(empty($state)){
    echo json_encode(['status'=>403, 'message'=> 'Please choose a state']);
      exit();
  }
  if(empty($city)){
    echo json_encode(['status'=>403, 'message'=> 'Please choose a city']);
      exit();
  }
 $data = array(
  'uid' => $userID,
  'name' => $name,
  'mobile' => $mobile,
  'email' => $email,
  'address' => $address,
  'street' => $street,
  'pin_code' => $pin_code,
  'city' => $city,
  'state' => $state,
  'create_date' => date('Y-m-d H:i:s'),
  'modify_date' => date('Y-m-d H:i:s'),
  'status' => 1,
 );

  $store = $this->Cart_model->store_shippingAddress($data);
  if($store)
  {
    echo json_encode(['status'=>200, 'message'=>'Shipping address add successfully!']);
        exit();
  }else{
    echo json_encode(['status'=>403, 'message'=>'some things went wrong']);
    exit();
  }
 }

 public function bookings(){
  $data['page_title'] = 'Bookings';
	$data['panel'] = 'Panel';
	$data['orders'] =$this->order_model->get_orders(array('orders.userID'=>$id));
  $this->template('web/panel/student/bookings',$data);
 }


 public function ajaxBooking(){
  $id  = $this->session->userdata('id');
  $output =array();
  if($this->session->userdata('order_type')=='Course'){
    $condition = array('orders.userID'=>$id,'items.order_type'=>$this->session->userdata('order_type'));
    $bookings = $this->order_model->make_datatables_booking_course($condition); // this will call modal function for fetching data
    $recordsTotal = 	$this->order_model->get_all_data_booking_course($condition);
    $recordsFiltered =	$this->order_model->get_filtered_data_booking_course($condition);
   
   $data = array();
   foreach($bookings as $key=>$booking) // Loop over the data fetched and store them in array
   {
     $sub_array = array();
     $image = '<h2 class="table-avatar">
     <a href="javascript:void(0)" class="avatar avatar-sm me-2"><img class="avatar-img rounded-circle" src="'.base_url($booking['coursePic']).'" alt="User Image"></a> </h2>';
     $status_class = $booking['orderStatus'] == 1 ? 'btn btn-sm text-success' : 'btn btn-sm text-danger';
     $statusName = $booking['orderStatus'] == 1 ? 'Active' : 'Pending';
     $orderStatus = '<a class="'.$status_class.' ">'.$statusName.'</a>';
     $sub_array[] = $key+1;
     $sub_array[] = $image;
     $sub_array[] = $booking['courseName'];
     $sub_array[] = '#'.$booking['order_id'];
     $sub_array[] = '₹ '.$booking['price'];
     $sub_array[] = $orderStatus;
     $sub_array[] = date('d F Y H : i : s',strtotime($booking['created_at']));
      $data[] = $sub_array;
   }
  
   $output = array(
     "draw"             =>     intval($_POST["draw"]),
     "recordsTotal"     =>     $recordsTotal,
     "recordsFiltered"  =>     $recordsFiltered,
     "data"             =>     $data
   );
   
   echo json_encode($output);
  } 

  elseif($this->session->userdata('order_type')=='Test Series'){
    $condition = array('orders.userID'=>$id,'items.order_type'=>$this->session->userdata('order_type'));
    $bookings = $this->order_model->make_datatables_test_series($condition); // this will call modal function for fetching data
    $recordsTotal = 	$this->order_model->get_all_data_booking_test_series($condition);
    $recordsFiltered =	$this->order_model->get_filtered_data_booking_test_series($condition);
   
   $data = array();
   foreach($bookings as $key=>$booking) // Loop over the data fetched and store them in array
   {
     $sub_array = array();
     $image = '<h2 class="table-avatar">
     <a href="javascript:void(0)" class="avatar avatar-sm me-2"><img class="avatar-img rounded-circle" src="'.base_url('public/Image_not_available.png').'" alt="User Image"></a> </h2>';
     $status_class = $booking['orderStatus'] == 1 ? 'btn btn-sm text-success' : 'btn btn-sm text-danger';
     $statusName = $booking['orderStatus'] == 1 ? 'Active' : 'Pending';
     $orderStatus = '<a class="'.$status_class.' ">'.$statusName.'</a>';
     $sub_array[] = $key+1;
     $sub_array[] = $image;
     $sub_array[] = $booking['courseName'];
     $sub_array[] = '#'.$booking['order_id'];
     $sub_array[] = '₹ '.$booking['price'];
     $sub_array[] = $orderStatus;
     $sub_array[] = date('d F Y H : i : s',strtotime($booking['created_at']));
      $data[] = $sub_array;
   }
  
   $output = array(
     "draw"             =>     intval($_POST["draw"]),
     "recordsTotal"     =>     $recordsTotal,
     "recordsFiltered"  =>     $recordsFiltered,
     "data"             =>     $data
   );
   
   echo json_encode($output);
  } else{
    echo json_encode($output);
  }
  

}



}