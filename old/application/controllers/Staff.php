<?php 
class Staff extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_admin_logged_in();
		$this->load->model('user_model');
	}

	public function index()
	{
    $data['page_title'] = 'Staff';
	
    //$data['users'] = $this->user_model->get_all_users(array('users.user_type' => 'User'));
	$this->admin_template('users/staff',$data);
	}

	public function view(){
		$staffID = base64_decode($this->uri->segment(2));
    $data['page_title'] = 'View-Staffs';
    $data['user'] = $this->user_model->get_user(array('users.id' => $staffID));
		$this->admin_template('users/view-staff',$data);
	}

	public function edit(){
		$staffID = base64_decode($this->uri->segment(2));
    $data['page_title'] = 'Edit-Staffs';
    $data['user'] = $this->user_model->get_user(array('users.id' => $staffID)); 
		$data['states'] = $this->Common_model->get_states(array('country_id'=>101));
		$this->admin_template('users/edit-staff',$data);
	}

	public function ajaxStaffs(){
		$condition = array('users.status'=>1,'users.id <>'=>1,'users.user_type'=>'staff');
		$staffs = $this->user_model->make_datatables($condition); // this will call modal function for fetching data
		$data = array();
		foreach($staffs as $key=>$staff) // Loop over the data fetched and store them in array
		{
			$button = '';
			$sub_array = array();
			$button .= '<a href="javascript:void(0)" onclick="reset_password('.$faculty['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Reset faculty Password" class="btn  btn-sm  text-success"><i class="fa fa-lock"></i></a>';
			$button .= '<a href="'.base_url('view-staff/'.base64_encode($staff['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="View Staff" class="btn   btn-sm  text-warning"><i class="fa fa-eye"></i></a>';
			$button .= '<a href="'.base_url('edit-staff/'.base64_encode($staff['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit Staff" class="btn btn-sm  text-primary"><i class="fa fa-edit"></i> </a>';
      $button .= '<a href="javascript:void(0)" onclick="delete_user('.$staff['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete Staff" class="btn btn-sm  text-danger"><i class="fa fa-trash"></i> </a>';
			
			$sub_array[] = $key+1;
			$profile_pic = $staff['profile_pic'];
			$sub_array[] = $button;
			$sub_array[] = '<img src="'.base_url($profile_pic).'" height="50" width="50">';
			$sub_array[] = $staff['name'];
			$sub_array[] = $staff['phone'];
			$sub_array[] = $staff['email'];
			$sub_array[] = $staff['address'];
			$sub_array[] = $staff['cityName'];
			$sub_array[] = $staff['stateName'];
			$sub_array[] = date('d-F-Y', strtotime($staff['created_at']));
		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->user_model->get_all_data($condition),
			"recordsFiltered"         =>     $this->user_model->get_filtered_data($condition),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
	  }

	  public function create(){
		$data['page_title'] = 'Create-Staff';
		$data['states'] = $this->Common_model->get_state(array('country_id'=>101));
		$this->template('create-staff',$data);
	  }





  public function update(){
	$userID = $this->input->post('userID');
	$name = $this->input->post('name');
	// $email = $this->input->post('email');
	// $phone = $this->input->post('phone');
	$address = $this->input->post('address');
	$city = $this->input->post('city');
	$state = $this->input->post('state');
	$user = $this->user_model->get_user(array('users.id'=>$userID));
	if(empty($name)){
		echo json_encode(['status'=>403, 'message'=>'Please enter your name']); 	
		exit();
	}
	// if(empty($email)){
	// 	echo json_encode(['status'=>403, 'message'=>'Please enter your email address']); 	
	// 	exit();
	// }
	// $checkEmail = $this->user_model->get_staff(array('email'=>$email,'id <>'=>$userID));
	// if($checkEmail){
	// 	echo json_encode(['status'=>403,'message'=>'This email is already in use']);
	// 	exit();
	// }

	// if(empty($phone)){
	// 	echo json_encode(['status'=>403, 'message'=>'Please enter your mobile']); 	
	// 	exit();
	// }

	// $checkPhone = $this->user_model->get_staff(array('phone'=>$phone,'id <>'=>$userID));
	// if($checkPhone){
	// 	echo json_encode(['status'=>403,'message'=>'This phone no. is already in use']);
	// 	exit();
	// }



	if(empty($state)){
		echo json_encode(['status'=>403, 'message'=>'Please select state']); 	
		exit();
	}
	if(empty($city)){
		echo json_encode(['status'=>403, 'message'=>'Please select city']); 	
		exit();
	}

	if(empty($address)){
		echo json_encode(['status'=>403, 'message'=>'Please enter address']);  	
		exit();
	}

	$this->load->library('upload');
	if($_FILES['profile_pic']['name'] != '')
		{
	$config = array(
		'upload_path' 	=> 'uploads/user',
		'file_name' 	=> str_replace(' ','',$name).uniqid(),
		'allowed_types' => 'jpg|jpeg|png|gif|webp',
		'max_size' 		=> '10000000',
	);
		$this->upload->initialize($config);
	if ( ! $this->upload->do_upload('profile_pic'))
		{
			$error = $this->upload->display_errors();
			echo json_encode(['status'=>403, 'message'=>$error]);
			exit();
		}
		else
		{
		$type = explode('.',$_FILES['profile_pic']['name']);
		$type = $type[count($type) - 1];
		$profile_pic = 'uploads/user/'.$config['file_name'].'.'.$type;
		}
	}else{
		if(!empty($user->profile_pic)){
			$profile_pic = $user->profile_pic;
		}else{
			$profile_pic = 'public/dummy_user.png';
		}
		
	}
	
	$data = array(
		'name'       => $name,
		'address'    => $address ,
		'city'       => $city,
		'state'      => $state,
		'profile_pic'=> $profile_pic,
	);
	$update = $this->user_model->update_user($data,$userID);
	if($update){
		echo json_encode(['status'=>200, 'message'=>'Staff updated successfully!']);
	}else{
		echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
	}

  }

  public function delete(){
	 $id = $this->input->post('id');
	 $data = array(
		'status' => 2
	 );
	 $delete = $this->user_model->update_user($data,$id);
	if($delete){
		echo json_encode(['status'=>200, 'message'=>'Staff delete successfully!']);
	}else{
		echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
	}
	
  }

  public function resetPassword(){
		$id = $this->input->post('id');
		$password = $this->input->post('password');
		if(empty($password)){
			echo json_encode(['status'=>403, 'message'=>'Please enter password']); 	
			exit();
		}

		if(strlen($password) < 6){
			echo json_encode(['status'=>403, 'message'=>'Password minimum length must be at least 6 characters']); 	
			exit();
		}
		
		$data = array(
			'password' => md5($password)
		);
		$update = $this->user_model->update_user($data,$id);
		if($update){
			echo json_encode(['status'=>200, 'message'=>'Password change successfully!']);
		}else{
			echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
		}
	
	}

}