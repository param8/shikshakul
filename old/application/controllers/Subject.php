<?php 
class Subject extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('course_model');
		$this->load->model('category_model');
        $this->load->model('user_model');
		$this->load->model('subject_model');
	}

	public function index()
	{
	  $this->not_admin_logged_in();
      $data['page_title'] = 'Subjects';
	  $this->admin_template('subject/subject',$data);
	}


	public function ajaxSubject(){
		$this->not_admin_logged_in();
		$condition = array('subject.status'=>1);
		$subjects = $this->subject_model->make_datatables($condition); // this will call modal function for fetching data
		$data = array();
		foreach($subjects as $key=>$subject) // Loop over the data fetched and store them in array
		{
			$button = '';
			$sub_array = array();
			$button .= '<a href="'.base_url('view-subject/'.base64_encode($subject['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="View faculty" class="btn  btn-sm  bg-warning-light"><i class="fe fe-eye"></i></a>';
			$button .= '<a href="'.base_url('edit-subject/'.base64_encode($subject['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit faculty" class="btn btn-sm bg-success-light"><i class="fe fe-edit"></i> </a>';
      $button .= '<a href="javascript:void(0)" onclick="delete_subject('.$subject['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete faculty" class="btn btn-sm bg-danger-light"><i class="fe fe-trash"></i> </a>';
			
			$sub_array[] = $key+1;
			$image = $subject['image'];
			$sub_array[] = '<img src="'.base_url($image	).'" height="50" width="50">';
			$sub_array[] = $subject['categoryName'];
			$sub_array[] = $subject['coursesName'];
			$sub_array[] = $subject['name'];
			$sub_array[] = date('d-F-Y', strtotime($subject['created_at']));
			$sub_array[] = $button;
		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->subject_model->get_all_data($condition),
			"recordsFiltered"         =>     $this->subject_model->get_filtered_data($condition),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
	  }

	  public function create_subject(){
			$this->not_admin_logged_in();
		  $data['page_title'] = 'Create-Subject';
		  $data['categories'] = $this->category_model->get_categories(array('status'=>1));
		  $this->admin_template('subject/create-subject',$data);
	  }

		public function edit_subject(){
			$this->not_admin_logged_in();
			 $data['page_title'] = 'Edit-Subject';
			 $id = base64_decode($this->uri->segment(2));
			 $data['subject'] = $this->subject_model->get_subject(array('subject.id'=>$id));
			 $data['categories'] = $this->category_model->get_categories(array('status'=>1));
			 $this->admin_template('subject/edit-subject',$data);
			}

			public function view_subject(){
			 $this->not_admin_logged_in();
			 $data['page_title'] = 'View-Subject';
			 $id = base64_decode($this->uri->segment(2));
			 $data['subject'] = $this->subject_model->get_subject(array('subject.id'=>$id));
			 $data['categories'] = $this->category_model->get_categories(array('status'=>1));
			 $this->admin_template('subject/view-subject',$data);
			}


   public function store(){
		$this->not_admin_logged_in();
		$name = $this->input->post('name');
		$categoryID = $this->input->post('categoryID'); 
		$courseID = $this->input->post('courseID');	
		$slug = str_replace(' ','-',strtolower($name));
    $check_subject =$this->subject_model->get_subject(array('subject.name'=>$name,'subject.categoryID'=>$categoryID,'subject.courseID'=>$courseID));
	
		if(empty($categoryID)){
			echo json_encode(['status'=>403, 'message'=>'Please choose category']); 	
			exit();
		}

		if(empty($courseID)){
			echo json_encode(['status'=>403, 'message'=>'Please choose course']); 	
			exit();
		}

		if(empty($name)){
		echo json_encode(['status'=>403, 'message'=>'Please enter subject name']); 	
		exit();
	}
	if($check_subject){
		echo json_encode(['status'=>403, 'message'=>'This subject already exists']); 	
		exit();
	}


	$this->load->library('upload');
    if($_FILES['image']['name'] != '')
		{
    $config = array(
      'upload_path' 	=> 'uploads/subject',
      'file_name' 	=> str_replace(' ','',$name).uniqid(),
      'allowed_types' => 'jpg|jpeg|png|gif|webp',
      'max_size' 		=> '10000000',
    );
        $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('image'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]);
          exit();
      }
      else
      {
        $type = explode('.',$_FILES['image']['name']);
        $type = $type[count($type) - 1];
        $image = 'uploads/subject/'.$config['file_name'].'.'.$type;
      }
     }else{
      $image = 'public/Image_not_available.png';
    }

	$data = array(
    'name'          => $name,
		'categoryID'    => $categoryID,
		'courseID'      => $courseID,
		'image'         => $image,
		'slug'          => $slug
	);
	$store = $this->subject_model->store_subject($data);

	if($store){
		echo json_encode(['status'=>200, 'message'=>'Subject add successfully!']);
	}else{
		echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
	}

   }

  public function update(){
		$this->not_admin_logged_in();
		$subjectID = $this->input->post('subjectID');
		$name = $this->input->post('name');
		$categoryID = $this->input->post('categoryID'); 
		$courseID = $this->input->post('courseID');	
		$slug = str_replace(' ','-',strtolower($name));
		
		$subject = $this->subject_model->get_subject(array('subject.id'=>$subjectID));
    $check_subject =$this->subject_model->get_subject(array('subject.name'=>$name,'subject.categoryID'=>$categoryID,'subject.courseID'=>$courseID,'subject.id<>'=>$subjectID));
	
		if(empty($categoryID)){
			echo json_encode(['status'=>403, 'message'=>'Please choose category']); 	
			exit();
		}

		if(empty($courseID)){
			echo json_encode(['status'=>403, 'message'=>'Please choose course']); 	
			exit();
		}

		if(empty($name)){
		echo json_encode(['status'=>403, 'message'=>'Please enter subject name']); 	
		exit();
	}
	if($check_subject){
		echo json_encode(['status'=>403, 'message'=>'This subject already exists']); 	
		exit();
	}


	$this->load->library('upload');
    if($_FILES['image']['name'] != '')
		{
    $config = array(
      'upload_path' 	=> 'uploads/subject',
      'file_name' 	=> str_replace(' ','',$name).uniqid(),
      'allowed_types' => 'jpg|jpeg|png|gif|webp',
      'max_size' 		=> '10000000',
    );
        $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('image'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]);
          exit();
      }
      else
      {
        $type = explode('.',$_FILES['image']['name']);
        $type = $type[count($type) - 1];
        $image = 'uploads/subject/'.$config['file_name'].'.'.$type;
      }
     }else{
			if(!empty($subject->image)){
				$image = $subject->image;
			}else{
				$image = 'public/Image_not_available.png';
			}
     
    }

	$data = array(
    'name'          => $name,
		'categoryID'    => $categoryID,
		'courseID'      => $courseID,
		'image'         => $image,
		'slug'          => $slug
	);
	$update = $this->subject_model->update_subject($data,$subjectID);

	if($update){
		echo json_encode(['status'=>200, 'message'=>'Subject updated successfully!']);
	}else{
		echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
	}

  }

  public function delete(){
		$this->not_admin_logged_in();
		$courseID = $this->input->post('id');
		$delete = $this->subject_model->delete_subject(array('id' => $courseID));
		if($delete){
			$this->subject_model->delete_topic(array('subjectID'=>$subjectID));
			echo json_encode(['status'=>200, 'message'=>'Subject delete successfully!']);
		}else{
			echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
		}
	
  }

	public function download(){
		$this->load->helper('download');
		$file_name =  base64_decode($this->uri->segment(3));
		force_download($file_name, NULL);
	}

}