<?php 
class Home extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('setting_model');
		$this->load->model('category_model');
		$this->load->model('user_model');
		$this->load->model('course_model');
		$this->load->model('order_model');
		$this->load->model('quiz_model');
	}

	public function index()
	{	 
		$data['page_title'] = 'Home';
		$data['banners'] = $this->setting_model->get_banner(array('status'=>1));
		$data['faculties'] = $this->user_model->get_users(array('users.status'=>1,'users.user_type'=>'Faculties'));
		$data['courses'] = $this->course_model->get_courses(array('courses.status'=>1),'Home');
		$data['affairs'] = $this->setting_model->get_affairs(array('status'=>1),'Home');
		$data['whatsNews'] = $this->setting_model->get_whatsNews(array('status'=>1),'Home');
		$data['videos'] = $this->setting_model->get_videos(array('status'=>1),'Home');
		$data['quiz'] = $this->quiz_model->get_quiz(array('quiz.status'=>1,'quiz_type.title'=>'Daily'));
		$data['questions'] = $this->quiz_model->get_quiz_questions(array('quizID'=>$data['quiz']->id));
		$data['toppers'] = $this->setting_model->get_toppers(array('status'=>1));
		$data['interviews'] = $this->setting_model->get_mock_interviews(array('status'=>1));
		$data['remarks'] = $this->setting_model->get_student_remarks(array('status'=>1));
		$data['buildings'] = $this->setting_model->get_building_futures(array('status'=>1));
	  $this->template('web/home',$data);
		
	}

	public function about(){
		 $data['page_title'] = 'About Us';
		$this->template('web/comman_pages/about_us',$data);
	}

	public function privacy_policy(){
		$data['page_title'] = 'Privacy Policy';
	 $this->template('web/comman_pages/privacy_policy',$data);
 }

 public function terms_condition(){
	$data['page_title'] = 'Terms Uses';
 $this->template('web/comman_pages/terms_uses',$data);
}

public function refund_policy(){
	$data['page_title'] = 'Refund Policy';
 $this->template('web/comman_pages/refund_policy',$data);
}

public function current_affair(){	 
  $data['page_title'] = 'Current-Affair';
  $id = rawurldecode($this->uri->segment(2));
  $data['current_affairs'] = $this->setting_model->get_current_affair(array('slug' => $id));
  $this->template('web/comman_pages/current_affair',$data);
}
public function interview(){
	$data['page_title'] = 'Interview';
	$condition = array('status' => 1);
    $data['interviews'] = $this->setting_model->get_mock_interviews($condition);
    $this->template('web/comman_pages/interview',$data);
}

public function toppers_talk(){
   $data['page_title'] = 'Toppers Talk';
   $condition = array('status'=>1);
   $data['toppes_talks'] = $this->setting_model->get_toppers($condition);
   $this->template('web/comman_pages/toppers_talk',$data);
}

public function current_affair_list(){	 
	$data['page_title'] = 'Current-Affairs';
  $id = base64_decode($this->uri->segment(2));
  $condition = !empty($id) ? array('id' => $id) : array('status' => 1);
  $data['current_affairs'] = $this->setting_model->get_current_affair($condition);
  $this->template('web/comman_pages/current_affair_list',$data);
}


public function booking_success(){
	$orderID = base64_decode($this->uri->segment(2));
	$status = base64_decode($this->uri->segment(3));
	$data['page_title'] = 'Order Success';
	$data['order'] =$this->order_model->get_order(array('orders.id'=>$orderID));
	$data['booking_status'] = $status;
  $this->template('web/comman_pages/booking-success',$data);
}


public function contact_us(){
 $data['page_title'] = 'Contact Us';
 $this->template('web/comman_pages/contact_us',$data);
}


public function ajaxCurrentAffair(){
	$type = base64_decode($this->uri->segment(3));
	$condition = array('status'=>1,'type'=>$type);
	$current_affairs = $this->setting_model->make_datatables_affairs($condition); // this will call modal function for fetching data
	$recordsTotal = 	$this->setting_model->get_all_data_affairs($condition);
	$recordsFiltered =	$this->setting_model->get_filtered_data_affairs($condition);

	$data = array();
		foreach($current_affairs as $key=>$current_affair) // Loop over the data fetched and store them in array
		{
			$url = base_url('current-affair/'.$current_affair['slug']);
			$date = empty($current_affair['date'])? '' : date("d M, Y",strtotime($current_affair['date']));
			$sub_array = array();
			$sub_array[] = ' <div class="quizlist">
			<div class="quizparagraph">
				<p>'.$current_affair['title'].'</p>
				<span><i class="fas fa-calendar-alt"></i>'.$date.'</span>
			</div>
			<div class="quizbutton">
				<a href="'.$url.'">View</a>
			</div>
		</div>';

		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"             =>     intval($_POST["draw"]),
			"recordsTotal"     =>     $recordsTotal,
			"recordsFiltered"  =>     $recordsFiltered,
			"data"             =>     $data
		);
		
		echo json_encode($output);
}


public function chat(){
	$data['page_title'] = 'Contact Us';
	$data['users'] = $this->user_model->get_users(array('users.status'=>1,'users.user_type<>'=>'Admin','users.id<>'=>$this->session->userdata('id')));
	$this->template('web/panel/chat',$data);
}

public function vocabulary_list(){	 
  $data['page_title'] = 'Vocabulary-list';
  $id = base64_decode($this->uri->segment(2));
//   $condition = !empty($id) ? array('id' => $id) : array('status' => 1);
//   $data['current_affairs'] = $this->setting_model->get_current_affair($condition);
  $this->template('web/comman_pages/vocabulary_list',$data);
}

public function view_vocabulary(){	 
	$data['page_title'] = 'Vocabulary';
	$id = base64_decode($this->uri->segment(2));
	$data['vocabulary'] = $this->setting_model->get_vocabulary(array('id' => $id));
	//print_r($data['vocabulary']);
	$this->template('web/comman_pages/view_vocabulary',$data);
  }

public function ajaxVocabulary(){
	$condition = array('status'=>1);
	$vocabularies = $this->setting_model->make_datatables_vocabulary($condition); // this will call modal function for fetching data
	$recordsTotal = 	$this->setting_model->get_all_data_vocabulary($condition);
	$recordsFiltered =	$this->setting_model->get_filtered_vocabulary($condition);

	$data = array();
		foreach($vocabularies as $key=>$vocabulary) // Loop over the data fetched and store them in array
		{
			$url = base_url('view-vocabulary/'.base64_encode($vocabulary['id']));
			$sub_array = array();
			$sub_array[] = ' <div class="quizlist">
			<div class="quizparagraph">
				<p>'.$vocabulary['title'].'</p>
				<span><i class="fas fa-calendar-alt"></i>'.date("d M, Y",strtotime($vocabulary['created_at'])).'</span>
			</div>
			<div class="quizbutton">
				<a href="'.$url.'">View</a>
			</div>
		</div>';

		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"             =>     intval($_POST["draw"]),
			"recordsTotal"     =>     $recordsTotal,
			"recordsFiltered"  =>     $recordsFiltered,
			"data"             =>     $data
		);
		
		echo json_encode($output);
}

public function upcoming_exam_list(){	 
	$data['page_title'] = 'Upcoming Exam';
  $id = base64_decode($this->uri->segment(2));
  $condition = !empty($id) ? array('id' => $id) : array('status' => 1);
  $data['upcoming_exam'] = $this->setting_model->get_upcoming_exam($condition);
  $this->template('web/comman_pages/upcoming_exam_list',$data);
}

public function ajaxUpcomingExam(){
	$condition = array('status'=>1);
	$upcomingExams = $this->setting_model->make_datatables_upcomingExam($condition); // this will call modal function for fetching data
	$recordsTotal = 	$this->setting_model->get_all_data_upcomingExam($condition);
	$recordsFiltered =	$this->setting_model->get_filtered_data_upcomingExam($condition);
	$data = array();
		foreach($upcomingExams as $key=>$upcomingExam) // Loop over the data fetched and store them in array
		{
			$url = base_url('view-upcoming-exam/'.base64_encode($upcomingExam['id']));
			$sub_array = array();
			$sub_array[] = ' <div class="quizlist">
			<div class="quizparagraph">
				<p>'.$upcomingExam['title'].'</p>
				<span><i class="fas fa-calendar-alt"></i>'.date("d M, Y",strtotime($upcomingExam['created_at'])).'</span>
			</div>
			<div class="quizbutton">
				<a href="'.$url.'">View</a>
			</div>
		</div>';

		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"             =>     intval($_POST["draw"]),
			"recordsTotal"     =>     $recordsTotal,
			"recordsFiltered"  =>     $recordsFiltered,
			"data"             =>     $data
		);
		
		echo json_encode($output);
}

public function view_upcoming_exam(){	 
  $data['page_title'] = 'View Upcoming Exam';
  $id = base64_decode($this->uri->segment(2));
  $condition = !empty($id) ? array('id' => $id) : array('status' => 1);
  $data['upcoming_exam'] = $this->setting_model->get_upcoming_exam($condition);
  $this->template('web/comman_pages/view_upcoming_exam',$data);
}


}