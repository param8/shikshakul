<?php 
class Authantication extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Auth_model');
		$this->load->model('User_model');
        $this->load->model('setting_model');
	}


	public function index()
	{	
		$data['page_title'] = 'Login';
		$this->login_template('signin',$data);
		// $this->load->view('layout/login_head',$data);
	}

	public function user_login()
	{	
		$data['page_title'] = 'Login';
		if($this->session->userdata('email')){
			redirect(base_url('home'));
		}
		$this->template('web/login',$data);
		// $this->load->view('layout/login_head',$data);
	}

	public function login(){
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$recaptcha = $this->input->post('g-recaptcha-response');
		$secret = "6LcNxXYmAAAAAAQu6QkqSTN1DQC8YimXPp7M7krM";
		if(empty($email)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your email or contact']); 	
			exit();
		}
		if(empty($password)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your password']); 	
			exit();
		}
		if($this->input->post('login_by')=='User'){
			if(empty($recaptcha)){
				echo json_encode(['status'=>403, 'message'=>'Please verify recaptcha']); 	
				exit();
			}
			$responceKey = verify_reCaptcha($secret,$recaptcha);
			if(!$responceKey['success']){
				echo json_encode(['status'=>403, 'message'=>'Recaptcha Failed - Please verify recaptcha']); 	
				exit();
			}
	  }
		$login = $this->Auth_model->login($email,$password);
		if($login==403){
			echo json_encode(['status'=>403, 'message'=>'email or password incorrect!']);
		}elseif($login==302){
			echo json_encode(['status'=>302, 'message'=>'Your are not active please contact the administrator']);   
		}else{
			echo json_encode(['status'=>200, 'message'=>'Login Successfully','user_type'=>$this->session->userdata('user_type'),'verifivation_status'=>$this->session->userdata('verification_status')]);
			// redirect(base_url('dashboard'), 'refresh');
    }
	}


	public function create_user(){
    $data['page_title'] = 'New User';
		$this->admin_template('users/create',$data);
	}

	public function register(){
		if($this->session->userdata('email')){
			redirect(base_url('home'));
		}
    $data['page_title'] = 'Registration';
		$data['states'] = $this->Common_model->get_states(array('country_id'=>101));
		$this->template('web/register',$data);
	}


	public function store(){
		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$phone = $this->input->post('phone');
		$user_type = $this->input->post('user_type');
		$profile_pic = 'public/dummy_user.png';	
		$created_by = $this->input->post('created_by');
		$state = $this->input->post('state');
		$city = $this->input->post('city');
		$address = $this->input->post('address');
		$pincode = $this->input->post('pincode');
 
		$validate_email = $this->User_model->get_user(array('email' => $email));
		$validate_phone = $this->User_model->get_user(array('phone' => $phone));

   if(empty($name)){
			echo json_encode(['status'=>403, 'message'=>'Please enter name']); 	
			exit();
		}
		if(empty($email)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your email ']); 	
			exit();
		}
    
		if($validate_email){
			echo json_encode(['status'=>403, 'message'=>'This email address is already in use.']); 	
			exit();
		}

		if(empty($phone)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your phone ']); 	
			exit();
		}
   
		if($validate_phone){
			echo json_encode(['status'=>403, 'message'=>'This phone number is already in use.']); 	
			exit();
		}

		if(empty($user_type)){
			echo json_encode(['status'=>403, 'message'=>'Please select role ']); 	
			exit();
		}

		if($created_by == 'Self'){
			
			if(empty($state)){
				echo json_encode(['status'=>403, 'message'=>'Please select state ']); 	
				exit();
			}
			if(empty($city)){
				echo json_encode(['status'=>403, 'message'=>'Please select city ']); 	
				exit();
			}
			if(empty($address)){
				echo json_encode(['status'=>403, 'message'=>'Please enter address ']); 	
				exit();
			}
			if(empty($pincode)){
				echo json_encode(['status'=>403, 'message'=>'Please enter pincode']);  	
				exit();
			}
		}
		if($created_by == 'Self'){
			$password = $this->input->post('password');
			$confirm_password = $this->input->post('confirm_password');
			$recaptcha = $this->input->post('g-recaptcha-response');
			$secret = "6LcNxXYmAAAAAAQu6QkqSTN1DQC8YimXPp7M7krM";
  	if(empty($password)){
	  	echo json_encode(['status'=>403, 'message'=>'Please enter your password']); 	
		  exit();
	  }
		if(strlen($password) < 6){
	  	echo json_encode(['status'=>403, 'message'=>'Password minimum length must be at least 6 characters']); 	
		  exit();
	  }
		if(empty($confirm_password)){
	  	echo json_encode(['status'=>403, 'message'=>'Please enter your confirm password']); 	
		  exit();
	  }

		if($password != $confirm_password){
	  	echo json_encode(['status'=>403, 'message'=>'Password and Confirm Password must be the same']); 	
		  exit();
	  }

		if(empty($recaptcha)){
	  	echo json_encode(['status'=>403, 'message'=>'Please verify recaptcha']); 	
		  exit();
	  }
     $responceKey = verify_reCaptcha($secret,$recaptcha);
		if(!$responceKey['success']){
			echo json_encode(['status'=>403, 'message'=>'Recaptcha Failed - Please verify recaptcha']); 	
		  exit();
		}
		}else{
			$password = $phone;
		}
		$unique_id = $user_type == 'Student' ? uniqid("STU".date('dmYHis')) : ($user_type == 'Faculties' ? uniqid("FAC".date('dmYHis')) : uniqid("STF".date('dmYHis')));
	  $verification_user = $created_by == 'Self' ? 0 : 1;
		$data = array(	
			'unique_id'           => $unique_id,
			'name'                => $name,
			'email'               => $email,
			'phone'               => $phone,
			'address'             => $address,
			'city'                => $city,
			'state'               => $state,
			'profile_pic'         => $profile_pic,
			'user_type'           => $user_type,
			'password'            => md5($password),
			'status'              => 1,
			'verification_user'   => $verification_user
		);

    $user_id = $this->User_model->store_user($data);
		if($user_id){
			$email_otp = rand(0,999999);
			$phone_otp = rand(0,999999);
			$validate_data = array(
				'userID'       => $user_id,
				'phone'        => $phone,
				'email'        => $email,
				'phone_status' => $created_by == 'Self' ? 0 : 1,
				'email_status' => $created_by == 'Self' ? 0 : 1,
				'mobile_otp'   => $created_by == 'Self' ? $phone_otp : 0,
				'email_otp'    => $created_by == 'Self' ? $email_otp : 0,
			);
     $store_verification = $this->User_model->store_verification($validate_data);
     if($created_by = 'Self'){
		  $subject = "Registartion Verification";
		  $html = 'Hello '.$name.',<br>Welcome to Shikshakul Classes.'.$email_otp.' is Your email verification code.<br><br>Best Regards,<br><br>Shikshakul Team';
		  sendEmail($email,$subject,$html);
      
			 $templateID = '1707168795597484153';
			 $message = urlencode('Hello '.$name.' Welcome to Shikshakul Classes '.$phone_otp.' is your verification code.');
			 sendSMS($phone,$message,$templateID);
			 $user = $this->User_model->get_user(array('users.id' => $user_id));
				$session = array(
					'id'                  => $user_id,
					'unique_id'           => $unique_id,
					'name'                => $name,
					'email'               => $email,
					'phone'               => $phone,
					'address'             => $address,
					'city'                => $city,
					'state'               => $state,
					'pincode'             => $pincode,
					'profile_pic'         => $profile_pic,
					'password'            => $password,
					'status'              => $user->status,
					'verification_user'   => $user->verification_user,
					'created_at'          => $user->created_at,
					'logged_in'           => TRUE,
					'mobile_otp'          => $phone_otp,
					'email_otp'           => $email_otp,
				);
				$this->session->set_userdata($session);
		 }

		 $user_data =array(
			'userID' => $user_id
		 );

		 $this->User_model->store_user_detail($user_data);

		 echo json_encode(['status'=>200, 'message'=>'Registration Successfully']);   
		}else{
      echo json_encode(['status'=>403, 'message'=>mysqli_error()]);
    }

	}


	public function verification(){
    $data['page_title'] = 'Verification';
	//print_r($this->session->userdata());
	$subject = "Registartion Verification";
	$html = 'Hello '.$this->session->userdata('name').',<br>Welcome to Shikshakul Classes.'.$this->session->userdata('email_otp').' is Your email verification code.<br><br>Best Regards,<br><br>Shikshakul Team';
	  sendEmail($this->session->userdata('email'),$subject,$html); 
	$templateID = '1707168795597484153';
	$message = urlencode('Hello '.$this->session->userdata('name').' Welcome to Shikshakul Classes '.$this->session->userdata('mobile_otp').' is your verification code.');
	sendSMS($this->session->userdata('phone'),$message,$templateID);
	$this->template('web/verification',$data);

	}

	public function verify_otp(){
	 $id = $this->session->userdata('id');
	 $email_otp = $this->input->post('email_otp');
	 $phone_otp = $this->input->post('phone_otp');
	 if(empty($email_otp)){
		echo json_encode(['status'=>403, 'message'=>'Please enter email otp']); 	
		exit();
	 }
	 if($email_otp != $this->session->userdata('email_otp')){
		echo json_encode(['status'=>403, 'message'=>'Please enter valid email otp']); 	
		exit();
	}
	 if(empty($phone_otp)){
			echo json_encode(['status'=>403, 'message'=>'Please enter phone otp']); 	
			exit();
		}
		if($phone_otp != $this->session->userdata('mobile_otp')){
			echo json_encode(['status'=>403, 'message'=>'Please enter valid phone otp']); 	
			exit();
		}
		
	  $data = array(
			'verification_user' => 1,
		);

		$this->User_model->update_user($data,$id);

		$userData = array(
			'phone_status' => 1,
			'email_status' => 1,
			'status'       => 1,
		);
    
		$update = $this->Auth_model->update_verification($userData,$id);

		if($update){
			$user = $this->User_model->get_user(array('users.id' => $id));
			$session = array(
				'verification_user' => $user->verification_user,
			);

			$this->session->set_userdata($session);

			$subject = "Welcome to Shikshakul";
		  $html = 'Dear '.$this->session->userdata('name').',<br>
			Hope you are doing well!<br>
			This email is to confirm that we have received your registration details and you are now officially registered with us.<br>
			If you have any questions or concerns, please feel free to contact our customer support team at support@shikshakul.com<br>
			Thank you for joining us and we look forward to your active participation.<br><br>
			Best Regards,<br>
			Shikshakul Team<br>';
			sendEmail($this->session->userdata('email'),$subject,$html);
      
			 $templateID = '1707168795604533309';
			 $message = urlencode('Thank you for registration with Shikshakul classes. We have received your registration details and you are now officially registered with us. If you have any questions or concerns, please feel free to contact our customer support team at support@shikshakul.com');
			 sendSMS($this->session->userdata('phone'),$message,$templateID);
			 echo json_encode(['status'=>200, 'message'=>'Verification Complete Welcome Shikshakul Class','user_type'=>$this->session->userdata('user_type')]);   
		}else{
      echo json_encode(['status'=>403, 'message'=>mysqli_error()]);
    }

	}
 

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url(), 'refresh');
	}

//   Admin Cradinciales


public function adminLogin(){
	//$this->logged_in();
	 $email = $this->input->post('username');
	 $password = $this->input->post('password');
	 $type = 'Admin';
	if(empty($email)){
		echo json_encode(['status'=>403, 'message'=>'Please enter your email address']); 	
		exit();
	}
	if(empty($password)){
		echo json_encode(['status'=>403, 'message'=>'Please enter your password']); 	
		exit();
	}
	$login = $this->Auth_model->login($email,$password);

	if($login){
		echo json_encode(['status'=>200, 'message'=>'Login successfully!']);
	}else{
		echo json_encode(['status'=>302, 'message'=>'Invalid username or password!']);   
	}
}

public function adminLogout()
{
	$this->session->sess_destroy();
	redirect(base_url(), 'refresh');
}


	
}