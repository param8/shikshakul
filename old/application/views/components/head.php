<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
<title><?=$page_title?></title>
<meta name="keyword" content="<?=$seo->meta_keywords?>" />
<meta name="description" content="<?=$seo->meta_description?>" />
<link rel="icon" href="assets/img/favicon.png" type="image/x-icon">
<link rel="stylesheet" href="<?=base_url('public/web/assets/css/bootstrap.min.css')?>">
<link rel="stylesheet" href="<?=base_url('public/web/assets/plugins/fontawesome/css/fontawesome.min.css')?>">
<link rel="stylesheet" href="<?=base_url('public/web/assets/plugins/fontawesome/css/all.min.css')?>">
<link rel="stylesheet" href="<?=base_url('public/web/assets/plugins/select2/css/select2.min.css')?>">
<link rel="stylesheet" href="<?=base_url('public/web/assets/plugins/aos/aos.css')?>">
<link rel="stylesheet" href="<?=base_url('public/web/assets/plugins/slick/slick.css')?>">
<link rel="stylesheet" href="<?=base_url('public/web/assets/plugins/slick/slick-theme.css')?>">

<script src="<?=base_url('public/web/assets/js/jquery-3.6.0.min.js')?>"></script>
<link rel="stylesheet" href="<?=base_url('public/plugin/toast.css')?>">
<script src="<?=base_url('public/plugin/toast.js')?>"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<link rel="stylesheet" href="<?=base_url('public/web/assets/css/style.css')?>">
<link rel="stylesheet" href="<?=base_url('public/web/assets/css/custom.css')?>">
<link rel="stylesheet" href="<?=base_url('public/web/assets/css/custom_new.css')?>">
<script src="https://www.google.com/recaptcha/api.js" async defer></script>


<link rel="stylesheet" href="https://cdn.datatables.net/1.13.4/css/dataTables.bootstrap4.min.css">
<link href="https://storage.googleapis.com/chydlx/plugins/dlx-quiz/css/main.css" rel="stylesheet">

</head>

