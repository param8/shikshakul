<?php
$dashboard_url = $this->session->userdata('user_type')=='Admin' ? base_url('dashboard') : ($this->session->userdata('user_type')=='Faculties' ? base_url('faculty-dashboard') : base_url('student-dashboard'));?>

<style>
.dataTables_wrapper div.dataTables_length {
    width: auto;
    display: inline-block;
}
div.dataTables_wrapper div.dataTables_filter {
    float: right;
}
div.dataTables_wrapper div.dataTables_filter input {
    padding: 6px 6px!important;
    border-radius: 6px!important;
    border: 1px solid #eee;
}
div.dataTables_wrapper div.dataTables_length label {
    font-weight: normal;
    text-align: left;
    white-space: nowrap;
}
.dataTables_wrapper div.dataTables_length select {
    width: auto;
    display: inline-block;
    height: auto!important;
    border: 1px solid #eee;
    min-width: auto;
    padding: 2px 15px;
}
div.dataTables_wrapper div.dataTables_info {
    padding-top: 0.85em;
    display: inline-block;
}
div.dataTables_wrapper div.dataTables_paginate {
    margin: 0;
    white-space: nowrap;
    float: right;
    display: inline-block;
    padding-top: 11px;
}
a.paginate_button {
    padding: 4px 10px;
    border: 1px solid #eee!important;
}
  </style>

<body>

  <div class="main-wrapper">
    <div class="top_header">
      <div class="left_logo">
        <div class="navbar-header   ">
          <a id="mobile_btn" href="javascript:void(0);">
            <span class="bar-icon bar-icon-eight">
              <span></span>
              <span></span>
              <span></span>
            </span>
          </a>
          <a href="<?=base_url('home')?>" class="navbar-brand navbar-brand-eight logo">
            <img src="<?=base_url($siteinfo->site_logo)?>" class="logo_img" alt="Logo" style="">
            <!-- <img src="../../../" class="logo_img" alt="Logo" style=""> -->
          </a>
        </div>
      </div>
      <!-- start -->
      <div class="right_btn">
        <div class="search">
          <input type="text " placeholder="Search..">
          <i class="fas fa-search"></i>
        </div>
        <?php if(empty($this->session->userdata('email'))){?>
        <div class="search">
          <a href="<?=base_url('login')?>">Login</a>
        </div>
        <div class="search">
          <a href="<?=base_url('register')?>">Register</a>
        </div>
        <?php }else{?>
        <!-- end -->

        <ul class="nav header-navbar-rht ">

          <li class="nav-item dropdown has-arrow logged-item" id="items_div">
            <nav id="main-nav">
              <a class="cart-button" href="javascript:void(0)">
                <span class="bag-count"><?= $this->cart->total_items()?></span>
                <span class="bag-icon">Cart</span>
                <span class="bag-label">View Cart</span>
              </a>
            </nav>
          </li>

          <li class="nav-item dropdown has-arrow logged-item">
            <a href="javascript:void()" class="dropdown-toggle nav-link" data-bs-toggle="dropdown">
              <span class="user-img">
                <img class="rounded-circle" src="<?=base_url($this->session->userdata('profile_pic'))?>" width="31"
                  alt="Darren Elder">
              </span>
            </a>
            <div class="dropdown-menu dropdown-menu-end">
              <div class="user-header">
                <div class="avatar avatar-sm">
                  <img src="<?=base_url($this->session->userdata('profile_pic'))?>" alt="User Image"
                    class="avatar-img rounded-circle">
                </div>
                <div class="user-text">
                  <h6><?=$this->session->userdata('name')?></h6>
                  <p class="text-muted mb-0"><?=$this->session->userdata('user_type')?></p>
                </div>
              </div>

              <a class="dropdown-item" href="<?=$dashboard_url?>">Dashboard</a>
              <a class="dropdown-item" href="javascript:void(0)">Profile Settings</a>
              <a class="dropdown-item" href="<?=base_url('Authantication/logout')?>">Logout</a>
            </div>
          </li>


          <?php }?>
        </ul>

      </div>

    </div>
    <header class="header">
      <div class="header-fixed header-fixed-wrap">
        <nav class="navbar navbar-expand-lg header-nav w100 header-nav-eight">
          <div class="navbar-header d-none ">
            <a id="mobile_btn" href="javascript:void(0);">
              <span class="bar-icon bar-icon-eight">
                <span></span>
                <span></span>
                <span></span>
              </span>
            </a>
            <a href="<?=base_url('home')?>" class="navbar-brand navbar-brand-eight logo">
              <img src="<?=base_url($siteinfo->site_logo)?>" class="logo_img" alt="Logo" style="">
            </a>
          </div>
          <div class="main-menu-wrapper w100 main-menu-wrapper-eight">
            <div class="menu-header menu-header-eight">
              <a href="<?=base_url('home')?>" class="menu-logo">
                <img src="<?=base_url($siteinfo->site_logo)?>" class="img-fluid" alt="Logo">
              </a>
              <a id="menu_close" class="menu-close" href="javascript:void(0);">
                <i class="fas fa-times"></i>
              </a>
            </div>
            <ul class="main-nav main-nav-eight justify-content-center">
              <?php if($this->session->userdata('email')){?>
              <li class="login-link">
                <span>
                  <div class="pro-avatar"><img class="rounded-circle"
                      src="<?=base_url($this->session->userdata('profile_pic'))?>" width="70"></div>
                </span>
                <a href="javscript:void(0)" class="text-success text-center"><?=$this->session->userdata('name')?></a>
              </li>
              <li class="login-link">
                <a href="<?=$dashboard_url?>" target="">Dashboard</a>
              </li>
              <li class="login-link">
                <a href="javascript:void(0)" target="">Profile</a>
              </li>
              <?php } ?>
              <li>
                <a href="<?=base_url('home')?>" target="">Home</a>
              </li>

              <li>
                <a href="<?=base_url('course')?>" target="">Courses</a>
              </li>

              <li>
                <a href="<?=base_url('test-series')?>" target="">Test Series</a>
              </li>

              <li>
                <a href="<?=base_url('interview')?>" target="">Interview</a>
              </li>

              <li>
                <a href="<?=base_url('current-affair-list')?>" target="">Current Affairs</a>
              </li>

              <!-- <li>
              <a href="javascript:void(0)" target="">Resources</a>
            </li> -->

              <li>
                <a href="<?=base_url('faculty')?>" target="">Faculty </a>
              </li>

              <li>
                <a href="<?=base_url('quizs')?>" target="">Quiz </a>
              </li>

              <?php if(empty($this->session->userdata('email'))){?>
              <li class="login-link">
                <a href="<?=base_url('login')?>">Login</a>
              </li>
              <li class="login-link">
                <a href="<?=base_url('register')?>">Register</a>
              </li>
              <?php } else{?>
              <li class="login-link">
                <a href="<?=base_url('Authantication/logout')?>" class="btn btn-danger btn-sm ">Logout</a>
              </li>
              <?php } ?>
            </ul>
          </div>

    
        </nav>
      </div>
    </header>
    <!-- 64897702e4c94 Acces token -->
    <!-- Instant ID 6489772101F2A -->

    <aside id="sidebar-cart">
      <main>
        <a href="javascript:void(0)" class="close-button"><span class="close-icon">X</span></a>
        <h2>Shopping Bag <span class="count"><?= $this->cart->total_items()?></span></h2>
        <ul class="products">

          <?php 
      $cartItems=$this->cart->contents();
      if(count($cartItems) > 0) {
        foreach($cartItems as $item){
    ?>

          <li class="product">
            <a href="javascript:void(0)" class="product-link">
              <span class="product-image">
                <img src="<?=base_url($item['image'])?>" alt="Product Photo">
              </span>
              <span class="product-details">
                <h3><?=$item['name']?></h3>
                <span class="qty-price">
                  <span class="qty">
                    <form action="#" name="qty-form" id="qty-form-1">
                      <!-- <button class="minus-button" id="minus-button-1">-</button> -->
                      <input type="number" id="qty-input-3" class="qty-input" step="1" value="<?=$item['qty']?>" min="1"
                        max="1000" name="qty-input" value="1" pattern="[0-9]*" title="Quantity" inputmode="numeric"
                        readonly>
                      <span class="price">₹ <?=$item['price']?></span>
                      <!-- <button class="plus-button" id="plus-button-1">+</button>
									<input type="hidden" name="item-price" id="item-price-3" value="12.00"> -->
                    </form>
                  </span>

                </span>
              </span>
            </a>
            <a href="javascript:void(0)" class="remove-button" onclick="remove_cart_item('<?=$item['rowid']?>')"><span
                class="remove-icon">X</span></a>
          </li>
          <?php }  } ?>
        </ul>
        <div class="totals">
          <div class="subtotal">
            <span class="label">Subtotal:</span> <span class="amount">₹ <?=$this->cart->total()?></span>
          </div>
        </div>
        <div class="action-buttons">
          <!-- <a class="view-cart-button" href="#">Cart</a> -->
          <a class="checkout-button" href="<?=base_url('checkout')?>">Checkout</a>
        </div>
      </main>
    </aside>
    <div id="sidebar-cart-curtain"></div>