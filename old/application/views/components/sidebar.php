<div class="breadcrumb-bar">
  <div class="container-fluid">
    <div class="row align-items-center">
      <div class="col-md-12 col-12">
        <nav aria-label="breadcrumb" class="page-breadcrumb">
          <ol class="breadcrumb">
            <li class=""><a href="<?=base_url('home')?>">Home</a></li>/
            <li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
          </ol>
        </nav>
        <h2 class="breadcrumb-title"><?=$page_title?></h2>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-4 col-lg-3 col-xl-2 theiaStickySidebar">
        <div class="profile-sidebar">
          <div class="user-widget">
            <div class="pro-avatar"><img class="rounded-circle" src="<?=base_url($this->session->userdata('profile_pic'))?>" width="100" ></div>
            <!-- <div class="rating">
              <i class="fas fa-star filled"></i>
              <i class="fas fa-star filled"></i>
              <i class="fas fa-star filled"></i>
              <i class="fas fa-star filled"></i>
              <i class="fas fa-star"></i>
            </div> -->
            <div class="user-info-cont">
              <h4 class="usr-name"><?=$this->session->userdata('name')?></h4>
              <p class="mentor-type"><?=$this->session->userdata('user_type')?></p>
            </div>
          </div>
          <!-- <div class="progress-bar-custom">
            <h6>Complete your profiles ></h6>
            <div class="pro-progress">
              <div class="tooltip-toggle" tabindex="0"></div>
              <div class="tooltip">80%</div>
            </div>
          </div> -->
          <hr>
          <div class="custom-sidebar-nav">
            <ul>
           <li><a href="<?=$this->session->userdata('user_type') == 'Student' ? base_url('student-dashboard') : base_url('faculty-dashboard')?>" class="active"><i class="fas fa-home"></i>Dashboard <span><i class="fas fa-chevron-right"></i></span></a></li>
              <?php if($this->session->userdata('user_type') == 'Student'){?><li><a href="<?=base_url('bookings')?>"><i class="fas fa-clock"></i>Bookings <span><i class="fas fa-chevron-right"></i></span></a></li><?php } ?>
              <!-- <li><a href="schedule-timings.html"><i class="fas fa-hourglass-start"></i>Schedule Timings <span><i class="fas fa-chevron-right"></i></span></a></li> -->
              <li><a href="<?=base_url('chat')?>"><i class="fas fa-comments"></i>Chat <span><i class="fas fa-chevron-right"></i></span></a></li>
              <!-- <li><a href="invoices.html"><i class="fas fa-file-invoice"></i>Invoices <span><i class="fas fa-chevron-right"></i></span></a></li> -->
              <li><a href="<?=base_url('course-list')?>"><i class="fas fa-book"></i>Courses <span><i class="fas fa-chevron-right"></i></span></a></li>
              <?php if($this->session->userdata('user_type')=='Faculties'){?><li><a href="<?=base_url('topics')?>"><i class="fas fa-book-open"></i>Topics <span><i class="fas fa-chevron-right"></i></span></a></li><?php } ?>
              <?php if($this->session->userdata('user_type') == 'Student'){?><li><a href="<?=base_url('result')?>"><i class="fa fa-check-square"></i>Result <span><i class="fas fa-chevron-right"></i></span></a></li> <?php } ?>
              <?php if($this->session->userdata('user_type')=='Faculties'){?><li><a href="<?=base_url('test-seriess')?>"><i class="fa fa-question-circle"></i>Test Series <span><i class="fas fa-chevron-right"></i></span></a></li><?php } ?>
              <?php if($this->session->userdata('user_type')=='Faculties'){?><li><a href="<?=base_url('schedule')?>"><i class="fas fa-calendar"></i>Schedule <span><i class="fas fa-chevron-right"></i></span></a></li><?php } ?>
              <!-- <?php //if($this->session->userdata('user_type') == 'Faculties'){?><li><a href="<?//=base_url('answer-list')?>"><i class="fa fa-check-square"></i>Answer <span><i class="fas fa-chevron-right"></i></span></a></li> <?php //} ?> -->
              <!-- <li><a href="<?=base_url('assignment')?>"></i> <span><i class="fas fa-chevron-right"></i></span></a></li> -->
              <li><a href="<?=base_url('profile')?>"><i class="fas fa-user-cog"></i>Profile <span><i class="fas fa-chevron-right"></i></span></a></li>
              <li><a href="<?=base_url('reset-password')?>"><i class="fas fa-lock"></i>Reset Password <span><i class="fas fa-chevron-right"></i></span></a></li>
              <li><a href="<?=base_url('Authantication/logout')?>"><i class="fas fa-sign-out-alt"></i>Logout <span><i class="fas fa-chevron-right"></i></span></a></li>
            </ul>
          </div>
        </div>
      </div>