<div class="breadcrumb-bar">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-md-8 col-12">
        <nav aria-label="breadcrumb" class="page-breadcrumb">
          <ol class="breadcrumb">
            <li class=""><a href="<?=base_url('home')?>">Home</a></li>/
            <li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
          </ol>
        </nav>
        <h2 class="breadcrumb-title"><?=$page_title?></h2>
      </div>

    </div>
  </div>
</div>
<div class="content bg-blue">
  <div class="container">
    <div class="row justify-content-center align-items-center">
      <?php foreach($quizs as $quiz){?>
      <div class="col-md-4 col-lg-4 col-xl-4 col-sm-6 col-6">
        <div class="quizgrid">
          <a href="<?=base_url('quiz-list/'.$quiz->slug)?>"><img
              src="<?=base_url($quiz->image)?>"></a>
        </div>
      </div>
      <?php } ?>
      <!-- <div class="col-md-4 col-lg-4 col-xl-4 col-sm-6 col-6">
        <div class="quizgrid">
          <a href="<?=base_url('quiz-list/'.base64_encode('weekly'))?>"><img
              src="<?=base_url('uploads/quiz/weeklyquiz.jpg')?>"></a>
        </div>
      </div>
      <div class="col-md-4 col-lg-4 col-xl-4 col-sm-6 col-6">
        <div class="quizgrid">
          <a href="<?=base_url('quiz-list/'.base64_encode('current_affair'))?>"><img
              src="<?=base_url('uploads/quiz/caquiz.jpg')?>"></a>
        </div>
      </div> -->

      
    </div>
  </div>