<div class="main-wrapper">
  <div class="bg-pattern-style bg-pattern-style-register mt-5 pb-4">
    <div class="content">
      <div class="account-content">
        <div class="account-box">
          <div class="login-right">
            <div class="login-header">
              <h3><span><?=$page_title?></span> </h3>
              <p class="text-muted"><?=$page_title?></p>
            </div>
            <form action="<?=base_url('user/verify_detail')?>" id="verifyDetailForm" method="post">
              <div class="row">

                <div class="col-lg-12">
                  <div class="form-group">
                    <label class="form-control-label">Email / Contact <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" id="user_detail" name="user_detail">
                  </div>
                </div>
              </div>
              <button class="btn btn-primary login-btn" type="submit">Send OTP</button>
            </form>
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$("form#verifyDetailForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  formData.append("login_by", 'User');
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        toastNotif({
          text: data.message,
          color: '#5bc83f',
          timeout: 5000,
          icon: 'valid'
        });
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {
            location.href = "<?=base_url('change-password')?>";
        }, 1000)

      } else if (data.status == 403) {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});
</script>