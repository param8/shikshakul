     <div class="breadcrumb-bar">
        <div class="container-fluid">
          <div class="row align-items-center">
            <div class="col-md-12 col-12">
              <nav aria-label="breadcrumb" class="page-breadcrumb">
                <ol class="breadcrumb">
                  <li class=""><a href="<?=base_url('home')?>">Home</a></li>
                  <li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
                </ol>
              </nav>
              <h2 class="breadcrumb-title"><?=$page_title?></h2>
            </div>
          </div>
        </div>
      </div>
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-7 col-lg-8">
              <div class="card">
                <div class="card-body">
                  <form >
                    
                    <div class="payment-widget">
                      <h4 class="card-title">Payment Method</h4>
                      <div class="payment-list">
                       
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group card-label">
                              <h5 for="card_name">Payment through QR Code</h5>
                              <hr>
                                  <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAH0AAAB9CAMAAAC4XpwXAAAAZlBMVEX///8AAACYmJiqqqr29vZISEg/Pz/z8/NoaGi8vLyEhITj4+NPT08ODg5wcHAuLi7Pz8/q6upUVFQnJydjY2OQkJDCwsLJycmzs7PV1dVaWlo4ODgXFxekpKQzMzOKiop5eXkgICAeLMUOAAAFT0lEQVRoge2b24KiMAyGPYDISa0Igoro+7/k0nSkMQ0UFnV2Z/iv2hLyCaWHtHU2mzTpe3Rc9FC6k6ZO2hQstYMCDByZ3KWtHpCOiL6Y9xHQHJ13tQMfCoC+7OVrMZoeTfTX0C9uu2JCP8vCtQhrsfS4w9eFpbvOrE3eitB9mQzncRzPQ46+8lp9OS5Pb72hlS41lD6b6GPouaYLmcxeRE8Pa6xDSeiz464WIB2ZLG7SLGLpJfGVWukH0jADSmcfZc7SA+LrYKWvh9Kj/vT1T6MPePMvoMNXhxRsHjq8n+6Q66rFIX2WHk70b6J/tN5nYaMjGET7/T4qP0XXConVt9GrH/HsA8Y4MfTZ7WNcGTwrZ+lFVgvNndfHOu9Tek58lVY6K4NOn0rp/TMrpeoX09/w5r1WCUSXeSe9b2sBUia2CaWLdmc8PV61C8VxSZ0956JWAciiTnlLQrf6GhXD2qIJqya6UtrrjpKl00iq7OUrRfTdsofKgtC9UhbDEtRG04uyj7Md1xbtQnQlOp9/qww6jWUm+iCXIJTUeVwgJeZJklz85pJJR/fSvOEcBC3uCsmTdoZaMpK+K6SXdItLPGIF67JHvsXx8zqgZ4SQ2Oh0jFNW0MJ22tQ+q/wddKj3FUtHdUXpglxC9CulQz9ZaFNc74VfK6+CKkjP8topkwWLoKoq+DVJXmcz6E4v1UNB6j+UVZoeQgEkc+ngBpc2tX0FDuKlNChmROhRNlCg29LWa+oHCU3F0CiDxM6/Yhp2fr2imND3mi44Or9CbqXTkPsfpycfpR/kur+Aek/2URSdCpmHTyuW+Wir6RDK525duEJ0WShSWapGYJnaQzLeyWvGjBvRkSA+dLa6EsA21fRzY4UkwFcGSbitq8V10FEMqypBv1NFjxorTN82dHtvM5heEbrx7G+lf/TZjXqHZ1ddM7QMWo2q3tHMF+io126h+yJ8ku8y37yby0u7lfyQYYNrGzHffOgQugOlqw46nepCH2q0d/RUSOzMCtGVjCjSRjf6up9GF9RM0++vpBv1rsf3auk/axnIUV+P71Z62NyaC0IX0o2f1qN+kCE6mtuw63XOmZR20JesFWrvxvjeb61yMJ3tbcwx7jfTod5VH3oiHm9AT0gpGjspvWStUCxj0Pk4ji1Vzu51MHc/zdrjOLizqs3uhxmJ4yh8iIxzFx0xLIyE+zG0ic7SXRsdhuST1eXgNauNPmcF9L0+Z6V+Y17fkEP7OefEjzGdHbxeh7ppfl53b3fD9zZW0bVKROfndaxa+roxdH5O+//Qe67Pqx/iiS956s03eeFp+lbeSgfntlGm196Eoifbh67wwy5N/r7QdN/xPKfoSaelWl170FSIjsa4if5+OgRVAiIcdg7SSbfuw/pZlh3pDEhJXvLBuSP3aX0xmN5vD5rfi2Tf2yB6v/33jr3ID9A79mF/8rNDDL6QJ02QVpruhc05V2XvPoxO7mi6McqA6H7clt2DLt5E73eqc1QsM9GhNCMOaL1f2c9fRdDj6afNs06aLuThw+xWF65hEizWjVUAhxGNHzbmjBmS9iuS5g2h9r7mXsg76OwI+6/TbWPcGPrBSredo3ZOsphOVM9gqw+7+lDvi0ImD+QctSzchTydlTHK0NBL+WWXmSPq7Kpv+Es6HeNUJEXXV0BGBP36c1bV/07v9Z8RTL/EcXzh6bFUG72+8rf/l7nKPHBc2DeDR0m0FWgF2wm3Z7cr9XUIuiM25r9CEWeqtin4U52GXk/vOFP6I+ju10fESO3+30hpS0875r+BS862hHr3SWnO0ydN+qT+AAHvfiQ+26swAAAAAElFTkSuQmCC">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group card-label">
                             
                              <h5 for="card_name">Payment through Account Detail</h5>
                              <hr>
                              <span> <label for="card_number">Account Name : <b>Shikshakul classes private limited</b></label></span>
                              <span> <label for="card_number">Account No. : <b>158005004271</b></label></span><br>
                              <span> <label for="card_number">IFSC Code. : <b>ICIC0001580</b></label></span>
                            </div>
                          </div>

                        </div>
                      </div>
                      
                      <div class="terms-accept">
                        <div class="custom-checkbox">
                          <input type="checkbox" id="terms_accept">
                          <label for="terms_accept">I have read and accepted <a href="javascript:void(0)">Terms &amp; Conditions</a></label>
                        </div>
                      </div>
                      <div class="submit-section mt-4">
                      <div class="custom-checkbox">
                          <input type="radio" name="payment_type" id="payment_type" value="Offline" checked>
                          <label for="payment_type">Offline</a></label>
                          <span class="ml-5">
                          <input type="radio" name="payment_type" id="payment_type" value="Online">
                          <label for="payment_type">Online</a></label>
                        </span>
                      </div>
                     </div>
                      <div class="submit-section mt-4">
                        <a href="javascript:void(0)" onclick="order_place()" class="btn btn-primary submit-btn">Confirm and Pay</a>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <div class="col-md-5 col-lg-4 theiaStickySidebar">
              <div class="card booking-card">
                <div class="card-header">
                  <h4 class="card-title">Booking Summary</h4>
                </div>
                <div class="card-body">
                  <?php 
                    $price = $this->cart->total();
                    $gst = ($this->cart->total()*18)/100;
                    $total_price = $price + $gst;
                  ?>
                  <div class="booking-summary">
                    <div class="booking-item-wrap">
                      <ul class="booking-date bk">
                        <li>Course Price <span>₹ <input type="text" name="" id="" value="<?=empty($this->session->userdata('addPrice')) ? $price : $this->session->userdata('addPrice')?>" disabled style="border:none"></span></li>
                        <li>GST <span>₹ <input type="text" name="" id="" value="<?=empty($this->session->userdata('gst')) ? $gst : $this->session->userdata('gst')?>" disabled style="border:none"></span></li>
                        <li>Discount  <span>₹ <input type="text" name="" id="" value="<?=empty($this->session->userdata('discount')) ? 0 : $this->session->userdata('discount')?>" disabled style="border:none"></span></li>
                      </ul>
                      <!-- <ul class="booking-fee">
                        <li>Discount <span>$100</span></li>
                        <li>Booking Fee <span>$10</span></li>
                        <li>Video Call <span>$50</span></li>
                      </ul> -->
                      <div class="booking-total">
                        <ul class="booking-total-list">
                          <li>
                            <span style="line-height: 42px;">Total</span>
                            <span class="total-cost cost">₹ <input type="text" name="" id="" value="<?=empty($this->session->userdata('total_price')) ? $total_price : $this->session->userdata('total_price')?>" disabled style="border:none"></span>
                          </li>
                        </ul>
                      </div>
                      <div class="booking-total">
                        <ul class="booking-total-list">
                          <li>
                            <h5>Apply Coupon</h5>
                            <div class="coupan">
                            <span class="c_input"> <input type="text" name="apply_coupon" id="apply_coupon" value="<?=$this->session->userdata('coupon_code')?>" class="form-control"></span>
                            <span class="c_btn"> <a class="btn btn-primary btn-sm" onclick="get_discount()">Apply</a></span>
                            <span class="c_btn"> <a class="btn btn-danger btn-sm" onclick="reset_discount()">Reset</a></span>
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <script>
         function get_discount(){
          var apply_coupon = $('#apply_coupon').val();
          $.ajax({
            url: '<?=base_url('Ajax_controller/get_discount')?>',
            type: 'POST',
            data:{apply_coupon},
            dataType: 'json',
            success: function (data) {
             if(data.status==200){
              location.reload();
             }else{
              toastNotif({
                text: data.message,
                color: '#da4848',
                timeout: 5000,
                icon: 'error'
              });
             }
            },
            error: function(){} 
          });

         }

         function reset_discount(){
          $.ajax({
            url: '<?=base_url('Ajax_controller/reset_discount')?>',
            type: 'POST',
            data:{'Restore':'Restore'},
            success: function (data) {
              location.reload();
            },
          });
         }

         function order_place(){
          var payment_type = $('input[type=radio][name="payment_type"]:checked').val();
          if($('input[type=radio][name="payment_type"]:checked').length >  0){
            $.ajax({
              url: '<?=base_url('Cart/order_now')?>',
              type: 'POST',
            data:{payment_type},
            dataType: 'json',
              success: function (data) {
            if(data.status==200){
              if(data.payment_type=='Online'){
              window.location.href = '<?=base_url("make-payment")?>';
              }
              if(data.payment_type=='Offline'){
                var base_url ='<?=base_url()?>';
                var url = base_url+'booking-success/'+data.order_id+'/'+data.order_status;
                toastNotif({
                text: data.message,
                color: '#5bc83f',
                timeout: 5000,
                icon: 'valid'
              });
                //console.log(url);
                setTimeout(function(){
                  window.location.href = url;
                }, 1000) 
             
              }
            }
              },
              error: function(){} 
            });
          }else{
            toastNotif({
                text: 'Please choose Payment Type',
                color: '#da4848',
                timeout: 5000,
                icon: 'error'
              });
          }
        }
   </script>
      