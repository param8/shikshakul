
<div class="col-md-8 col-lg-9 col-xl-10">
<div class="row">
      <div class="col-lg-12 col-md-12">
        <div class="row blog-grid-row">
          <?php 
            $course_id = array();
            $subject_id = array();
            //print_r($subjects);
             foreach($subjects as $subject){
              $course_id[] = $subject->courseID;
              $subject_id[$subject->courseID] = $subject->id;
             }
            foreach($courses as $course){
              if($this->session->userdata('email')){
                $url = base_url('course-detail-panel/'.$course->slug);
              }else{
                $url = base_url('course-detail/'.$course->slug);
              }
              
              ?>
          <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-6">
            <div class="blog grid-blog">
              <div class="blog-image">
                <a href="<?=$url?>"><img class="img-fluid" src="<?=base_url($course->image)?>" alt="Post Image" style="width:100%;height:230px;"></a>
              </div>
              <h6 class="title">
                <a href="<?=$url?>"><?=$course->name?></a>
                <i class="fas fa-check-circle verified"></i>
              </h6>
              <ul class="available-info">
                <li><i class="fa fa-book"></i><?=$course->categoryName?></li>
                <li><i class="fa fa-clock"></i>Days <?=$course->duration?> </li>
                <li class="text-danger"><b>₹ <?=$course->price==0 ? 'Free' : $course->price;?></b> </li>
              </ul>
              <div class="row row-sm">
                <div class="col-6">
                  <a href="<?=$url?>" class="btn view-btn">View Course</a>
                </div>
                <div class="col-6">
                  <a href="javascript:void(0)" onclick="addToCart(<?=$course->id?>,'Course','Add to Cart')" class="btn book-btn">Book Now</a>
                </div>
              </div>
            </div>
          </div>
          <?php } ?>
        </div>
      </div>
    </div>
        
      </div>
    </div>
  </div>
</div>

<script>
  
</script>