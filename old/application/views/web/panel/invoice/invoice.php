
<div class="col-md-8 col-lg-9 col-xl-10">

<div class="card">
                <div class="card-body">
                  <?php //$url = $this->session->userdata('user_type')=='Student' ? base_url('Student/update') : base_url('Faculty/update'); ?>
                  <form action="<?=base_url('User/update')?>" id="updateForm" method="POST" enctype="multipart/form-data">
                    <div class="row form-row">
                      <div class="col-12 col-md-12">
                        <div class="form-group">
                          <div class="change-avatar">
                            <div class="profile-img">
                              <img src="<?=base_url($this->session->userdata('profile_pic'))?>" alt="User Image" >
                            </div>
                            <div class="upload-img">
                              <div class="change-photo-btn">
                                <span><i class="fa fa-upload"></i> Upload Photo</span>
                                <input type="file" class="upload" name="profile_pic" id="profile_pic">
                              </div>
                              <small class="form-text text-muted">Allowed JPG, GIF or PNG. Max size of 2MB</small>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-md-6">
                        <div class="form-group">
                          <label>First Name</label>
                          <input type="text" class="form-control" name="name" id="name" value="<?=$user->name?>">
                        </div>
                      </div>
                      <div class="col-12 col-md-6">
                        <div class="form-group">
                          <label>Email ID</label>
                          <input type="email"  class="form-control" value="<?=$user->email?>" readonly>
                        </div>
                      </div>
                     
                      <div class="col-12 col-md-6">
                        <div class="form-group">
                          <label>Date of Birth</label>
                          <div class="cal-icon">
                            <input type="text" name="dob" id="dob" class="form-control datetimepicker" value="<?=$user->dob?>">
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-md-6">
                        <div class="form-group">
                          <label>Gender</label>
                          <?php $genders=array('Male','Female');?>
                          <select class="form-control select" name="gender" id="gender">
                            <?php foreach($genders as $gender){?>
                            <option value="<?=$gender?>" <?=$gender==$user->gender?>><?=$gender?></option>
                            <?php } ?>
                           
                          </select>
                        </div>
                      </div>
                 
                      <div class="col-12 col-md-6">
                        <div class="form-group">
                          <label>Mobile</label>
                          <input type="text" name="phone" id="phone" value="<?=$user->phone?>" class="form-control">
                        </div>
                      </div>
                      <?php if($this->session->userdata('user_type')=='Faculties'){?>
                       <div class="col-12 col-md-6">
                        <div class="form-group">
                          <label>Subject</label>
                          <input type="text" name="subject" id="subject" class="form-control" value="<?=$user->user_subject?>">
                        </div>
                      </div>
                      <?php } ?>
                      <div class="col-12">
                        <div class="form-group">
                          <label>Address</label>
                          <textarea type="text" name="address" id="address" class="form-control" ><?=$user->address?></textarea>
                        </div>
                      </div>
                      <div class="col-12 col-md-6">
                        <div class="form-group">
                          <label>State</label>
                          <select class="form-control" name="state" id="state" onchange="get_city(this.value)">
                            <?php foreach($states as $state){?>
                              <option value="<?=$state->id?>" <?=$state->id==$user->state ? 'selected' : ''?>><?=$state->name?></option>
                              <?php } ?>
                          </select>
                
                        </div>
                      </div>
                      <div class="col-12 col-md-6">
                        <div class="form-group">
                          <label>City</label>
                          <select class="form-control city" name="city" id="city" >
                              <option value="" >Select City</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-12 col-md-6">
                        <div class="form-group">
                          <label>Zip Code</label>
                          <input type="text" name="pincode" id="pincode" class="form-control" value="<?=$user->pincode?>">
                        </div>
                      </div>
                      <div class="col-12">
                        <div class="form-group">
                          <label>Description</label>
                          <textarea type="text" name="about" id="about" class="form-control" ><?=$user->about?></textarea>
                        </div>
                      </div>
                    </div>
                    <div class="submit-section">
                      <button type="submit" class="btn btn-primary submit-btn">Save Changes</button>
                    </div>
                  </form>
                </div>
              </div>
      </div>
    </div>
  </div>
</div>


http://www.justgosms.com/http-api.php?username=ckbh&password=123456&senderid=SKSHKL&route=12&number=8770134947&message=Your OTP for reset your password to Shikshakul Classes is 12345-1707168795609801101
http://www.justgosms.com/http-api.php?username=ckbh&password=123456&senderid=SKSHKL&route=12&number=8770134947&message=Your OTP for reset your password to Shikshakul Classes is 12345&templateid=1707168795609801101
<script>
  $("form#updateForm").submit(function(e) {
  			$(':input[type="submit"]').prop('disabled', true);
  			e.preventDefault();    
  			var formData = new FormData(this);
        formData.append("userID", '<?=$user->id?>');   
  			$.ajax({
  			url: $(this).attr('action'),
  			type: 'POST',
  			data: formData,
  			cache: false,
  			contentType: false,
  			processData: false,
  			dataType: 'json',
  			success: function (data) { 
  				if(data.status==200) {
  				//$('.modal').modal('hide');
          toastNotif({
  				text: data.message,
  				color: '#5bc83f',
  				timeout: 5000,
  				icon: 'valid'
  			});
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function(){
  
          location.href="<?=base_url('profile')?>"; 	
          
        }, 1000) 		
  				}else if(data.status==403) {
            toastNotif({
                  text: data.message,
                  color: '#da4848',
                  timeout: 5000,
                  icon: 'error'
                });
  
            $(':input[type="submit"]').prop('disabled', false);
  				}else{
            toastNotif({
                text: data.message,
                color: '#da4848',
                timeout: 5000,
                icon: 'error'
              });
            $(':input[type="submit"]').prop('disabled', false);
  				}
  			},
  			error: function(){} 
  			});
  		});
  function getCity(stateID){
    var userID = <?=$user->id?>;
    $.ajax({
      url: '<?=base_url('Ajax_controller/get_city')?>',
      type: 'POST',
      data: {stateID,userID},
      success: function (data) {
        $('#city').html(data);
      }
      });
  }
  
  $( document ).ready(function() {
    getCity(<?=$user->state?>)
  });
</script>