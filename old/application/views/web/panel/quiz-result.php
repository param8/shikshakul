
            <div class="col-md-8 col-lg-8 col-xl-10">
              <h3 class="pb-3"><?=$page_title?></h3>
              <div class="tab-pane show active" id="mentee-list">
                <div class="card card-table">
                  <div class="card-body">
                    <div class="table_bg p-3 ">
                      <table class="table table-hover table-center mb-0" id="quizResultDataTable">
                        <thead>
                          <tr>
                            <th>SNo.</th>
                            <th >Quiz</th>
                            <th >Quiz Type</th>
                            <th >Score</th>
                            <th >Quiz Date</th>
                          </tr>
                        </thead>
                        <tbody>
                     
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <script>
  $(document).ready(function() {
     //alert('dfgfgf');
       // $('#example').DataTable();
       // } );
       var dataTable = $('#quizResultDataTable').DataTable({
           "processing": true,
           "serverSide": true,
           buttons: [{
               extend: 'excelHtml5',
               text: 'Download Excel'
           }],
           "order": [],
           "ajax": {
               url: "<?=base_url('Quiz/ajaxResultQuiz')?>",
               type: "POST"
           },
           "columnDefs": [{
               "targets": [0],
               "orderable": false,
           }, ],
       });
   });
   </script>

     
      