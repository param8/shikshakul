<div class="col-md-8 col-lg-9 col-xl-10">
  <div class="row">
    <div class="col-md-12 col-lg-4 dash-board-list blue">
      <div class="dash-widget">
        <div class="circle-bar">
          <div class="icon-col">
            <i class="fas fa-book"></i>
          </div>
        </div>
        <div class="dash-widget-info">
          <h3><?=count($courses)?></h3>
          <h6>Courses</h6>
        </div>
      </div>
    </div>
    <div class="col-md-12 col-lg-4 dash-board-list yellow">
      <div class="dash-widget">
        <div class="circle-bar">
          <div class="icon-col">
            <i class="fas fa-book-open"></i>
          </div>
        </div>
        <div class="dash-widget-info">
          <h3>
            <?=count($topics)?>
          </h3>
          <h6>Topics</h6>
        </div>
      </div>
    </div>
    <div class="col-md-12 col-lg-4 dash-board-list pink">
      <div class="dash-widget">
        <div class="circle-bar">
          <div class="icon-col">
            <i class="fas fa-calendar-check"></i>
          </div>
        </div>
        <div class="dash-widget-info">
          <h3><?=count($schedules)?></h3>
          <h6>Schedule Classes</h6>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="container">
        <div class="row justify-content-center align-items-center">
        <div class="col-md-12 quizbg">
            <h3>Today Schedule</h3>
          </div>
          <div class="col-md-12 quizbg">
            <!-- <h1 class="heading">Path to Prelims - 2023</h1> -->
            <div class="table-responsive1">
              <table class=" table table-striped table-bordered" id="scheduleDatatable">
                <thead>
                  <tr>
                    <th>Sno.</th>
                    <th>Course</th>
                    <th>Subject</th>
                    <th>Topic</th>
                    <th>Start Time</th>
                    <th>End Time</th>
                    <th>Date</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>

<script>
$(document).ready(function() {
  //alert('dfgfgf');
  // $('#example').DataTable();
  // } );
  var dataTable = $('#scheduleDatatable').DataTable({
    "processing": true,
    "serverSide": true,
    buttons: [{
      extend: 'excelHtml5',
      text: 'Download Excel'
    }],
    "order": [],
    "ajax": {
      url: "<?=base_url('Faculty/ajaxSchedule')?>",
      type: "POST"
    },
    "columnDefs": [{
      "targets": [0],
      "orderable": false,
    }, ],
  });
});
</script>