<div class="col-md-8 col-lg-9 col-xl-10">

  <div class="card">
    <div class="card-body">
    <div class="quezz_body pos">
      <div class="test_quies">
        <div class="panel--header  bg_color">
          <h3> <img src="<?=base_url('public/web/img/feature/f_icon1.png') ?>" class="w100">
            Answer Five
          </h3>
          <ul class="quiz-page" id="quizpage">
            <li class="active">1</li>
            <li>2</li>
            <li>3</li>
            <li>4</li>
            <li>5</li>
          </ul>
        </div>
      </div>
      <div class="panal_body">
        <div id="quiz1"></div>
      </div>
    </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
<!-- <script src="https://storage.googleapis.com/chydlx/plugins/dlx-quiz/js/main.js"></script> -->
<?=$quiz_type?>
<script src="<?=base_url('public/web/assets/js/quiz.js')?>"></script>
<script>
$("#quiz1").dlxQuiz({
  quizData: {
    questions: [
    
      <?php 

    $correct_ans = array();
  
    foreach($questions as $key=>$question){
  
      foreach($question['option'] as $key_option=>$options){
        foreach($question[$key_option]['answer'] as $key_ans=>$answer){
        $correct_ans[$question['id']] = $answer;
        }
      }
    
    ?> {
        quiz_type : '<?=$quiz_type?>',
        quizID : '<?=$quizID?>',
        url : '<?=base_url()?>',
        q: "<?=$question['question']?>",
        a: "<?=$correct_ans[$question['id']]?>",
        options: [
          <?php foreach($question['option'] as $option){?> "<?=$option['option']?>",
          <?php } ?>
        ]
      },
      <?php } ?>


    ]
  }
});
</script>
