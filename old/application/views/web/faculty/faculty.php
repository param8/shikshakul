
      <div class="breadcrumb-bar">
        <div class="container-fluid">
          <div class="row align-items-center">
            <div class="col-md-8 col-12">
              <nav aria-label="breadcrumb" class="page-breadcrumb">
                <ol class="breadcrumb">
                  <li class=""><a href="<?=base_url('home')?>">Home</a></li>/
                  <li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
                </ol>
              </nav>
              <h2 class="breadcrumb-title"><?=$page_title?> List</h2>
            </div>
            <div class="col-md-4 col-12">
              <form class="search-form custom-search-form">
                <div class="input-group">
                  <input type="text" placeholder="Search faculty..." onkeyup="get_faculty(this.value)" class="form-control">
                  <!-- <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button> -->
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            
            <div class="col-md-12 col-lg-12 col-xl-12">
              <div class="row row-grid" id="facultyDataShow">

              </div>
              <div class="blog-pagination mt-4">
                <!-- <nav>
                  <ul class="pagination justify-content-center">
                    <li class="page-item disabled">
                      <a class="page-link" href="#" tabindex="-1"><i class="fas fa-angle-double-left"></i></a>
                    </li>
                    <li class="page-item">
                      <a class="page-link" href="#">1</a>
                    </li>
                    <li class="page-item active">
                      <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="page-item">
                      <a class="page-link" href="#">3</a>
                    </li>
                    <li class="page-item">
                      <a class="page-link" href="#"><i class="fas fa-angle-double-right"></i></a>
                    </li>
                  </ul>
                </nav> -->
              </div>
            </div>
          </div>
        </div>
      </div>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

     
      <script>
 $('#ccnumber').text(function(_, val) {
  return val.replace(/\d{12}(\d{4})/, "************$1");
});

$( document ).ready(function() {
  var value = "";
  $.ajax({
  url: '<?=base_url('Faculty/get_faculties')?>',
  type: 'POST',
  data: {value},
  success: function (data) {
    $('#facultyDataShow').html(data);
  }
  });
});

function get_faculty(value){
  $.ajax({
  url: '<?=base_url('Faculty/get_faculties')?>',
  type: 'POST',
  data: {value},
  success: function (data) {
    $('#facultyDataShow').html(data);
  }
  });
}
      </script>
     