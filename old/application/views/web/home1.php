
<link href="https://storage.googleapis.com/chydlx/plugins/dlx-quiz/css/main.css" rel="stylesheet">
<section class="section section-search-eight pos">
    <div class="ban_ner">
        <div class="slider">
            <div class="home_slider slick">
                <?php foreach($banners as $banner){?>
                <div class="mendor-box border-0">
                    <div class="banne_r pos">
                      
                        <img src="<?=$banner->banner_img?>" alt="1" class="w100" height="">
                    </div>
                </div>
                <?php }?>

                <!--<div class="mendor-box border-0 ">
                    <div class="banne_r pos">
                    <div class="tag_line banner-header">
                        <h1> Building futures, one success story at a time - Choose our  <br>  <span>coaching institute!</span>  </h1>
                        </div>
                        <img src="public/web/assets/img/homebanner/b2.png" alt="1" class="w100" height="">
                    </div>
                </div>
                <div class="mendor-box border-0">
                    <div class="banne_r pos">
                    <div class="tag_line banner-header">
                        <h1> Empowering aspirants, shaping leaders - Enroll in our civil - services  <br>  <span> coaching program!"</span>  </h1>
                        </div>
                        <img src="public/web/assets/img/homebanner/b3.png" alt="1" class="w100" height="">
                    </div>
                </div> -->
              <!-- <div class="form-group search-info-eight loc">
                <i class="material-icons">location_city</i>
                <input type="text" class="form-control"
                  placeholder="Unravel the secrets of success with our civil services coaching program!">
              </div>
              <button type="submit" class="btn search-btn-eight eg_bt mt-0">Search <i
                class="fas fa-long-arrow-alt-right"></i></button>
            </div> -->
        </div>
    </div>

    <!-- <div class="container">
        <div class="banner-wrapper-eight m-auto text-center top_b">
              <div class="banner-header aos" data-aos="fade-up">
                <h1>Search Teacher in <span>Mentoring</span> Appointment</h1>
                <p>Discover the best Mentors & institutions the city nearest to you.</p>
            </div>  
            <div class="search-box-eight aos" data-aos="fade-up">
                <form action="">
                    <div class="form-search">
                        <div class="form-inner">
                             <div class="form-group search-location-eight">
                            <i class="material-icons">my_location</i>
                            <select class="form-control select">
                            <option>Location111</option>
                            <option>Japan</option>
                            <option>France</option>
                            </select>
                        </div> 
                            <div class="form-group search-info-eight loc">
                                <i class="material-icons">location_city</i>
                                <input type="text" class="form-control"
                                    placeholder="Unravel the secrets of success with our civil services coaching program!">
                            </div>
                            <button type="submit" class="btn search-btn-eight eg_bt mt-0">Search <i
                                    class="fas fa-long-arrow-alt-right"></i></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div> -->
</div>
</section>
<section class="search">
<div class="container">
        <div class="banner-wrapper-eight1 m-auto text-center  ">
           
            <div class="search-box-eight aos" data-aos="fade-up">
                <form action="">
                    <div class="form-search">
                        <div class="form-inner">
                            
                            <div class="form-group search-info-eight loc">
                                <i class="material-icons">location_city</i>
                                <input type="text" class="form-control"
                                    placeholder="Enter Course Name">
                            </div>
                            <button type="submit" class="btn search-btn-eight eg_bt mt-0">Search <i
                                    class="fas fa-long-arrow-alt-right"></i></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</section>
<section class="section how-it-works-section  pb-5">
    <div class="container">
        <div class="section-header-eight text-center aos" data-aos="fade-up">
            <!-- <span>Mentoring Flow</span> -->
            <h2>Building Futures, Fne Success Story At A Time </h2>
            <!-- <p class="sub-title"> </p> -->
            <span>- Choose Our Coaching Institute!</span>
            <div class="sec-dots">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <div class="row justify-content-center feature-list">
            <div class="col-md-12">
                <div class="slider">
                    <div class="feature_slider slick">
                        <div class="mendor-box aos" data-aos="fade-up">
                            <div class="feature-grid text-center ">
                                <div class="feature-header-eight">
                                    <div class="feature-icon-eight">
                                        <span class="circle bg-green"><i class="fas fa-sign-in-alt"></i></span>
                                    </div>
                                    <div class="feature-cont">
                                        <div class="feature-text-eight">Daily Quiz</div>
                                    </div>
                                </div>
                                <p>Are you looking to join online Learning? Now it's very simple, Now Sign up</p>
                                <span class="text-green">01</span>
                            </div>
                        </div>
                        <div class="mendor-box aos" data-aos="fade-up">
                            <div class="feature-grid text-center">
                                <div class="feature-header-eight">
                                    <div class="feature-icon-eight">
                                        <span class="circle bg-blue"><i class="material-icons">accessibility</i></span>
                                    </div>
                                    <div class="feature-cont">
                                        <div class="feature-text-eight">UpComing Exam</div>
                                    </div>
                                </div>
                                <p>Collaborate on your own timing, by scheduling with mentor booking</p>
                                <span class="text-blue">02</span>
                            </div>
                        </div>
                        <div class="mendor-box aos" data-aos="fade-up">
                            <div class="feature-grid text-center ">
                                <div class="feature-header-eight">
                                    <div class="feature-icon-eight">
                                        <span class="circle bg-orange"><i class="material-icons">event_seat</i></span>
                                    </div>
                                    <div class="feature-cont">
                                        <div class="feature-text-eight">Daily Vocabulary</div>
                                    </div>
                                </div>
                                <p>you can gather different skill set, and you can become mentor too</p>
                                <span class="text-orange">03</span>
                            </div>
                        </div>

                        <div class="mendor-box aos" data-aos="fade-up">
                            <div class="feature-grid text-center ">
                                <div class="feature-header-eight">
                                    <div class="feature-icon-eight">
                                        <span class="circle bg-green"><i class="fas fa-sign-in-alt"></i></span>
                                    </div>
                                    <div class="feature-cont">
                                        <div class="feature-text-eight">One-to-One Monitoring</div>
                                    </div>
                                </div>
                                <p>Are you looking to join online Learning? Now it's very simple, Now Sign up</p>
                                <span class="text-green">04</span>
                            </div>
                        </div>
                        <div class="mendor-box aos" data-aos="fade-up">
                            <div class="feature-grid text-center">
                                <div class="feature-header-eight">
                                    <div class="feature-icon-eight">
                                        <span class="circle bg-blue"><i class="material-icons">accessibility</i></span>
                                    </div>
                                    <div class="feature-cont">
                                        <div class="feature-text-eight">Inspiration</div>
                                    </div>
                                </div>
                                <p>Collaborate on your own timing, by scheduling with mentor booking</p>
                                <span class="text-blue">05</span>
                            </div>
                        </div>
                        <div class="mendor-box aos" data-aos="fade-up">
                            <div class="feature-grid text-center ">
                                <div class="feature-header-eight">
                                    <div class="feature-icon-eight">
                                        <span class="circle bg-orange"><i class="material-icons">event_seat</i></span>
                                    </div>
                                    <div class="feature-cont">
                                        <div class="feature-text-eight">Toppers Word</div>
                                    </div>
                                </div>
                                <p>you can gather different skill set, and you can become mentor too</p>
                                <span class="text-orange">06</span>
                            </div>
                        </div>
                        <div class="mendor-box aos" data-aos="fade-up">
                            <div class="feature-grid text-center ">
                                <div class="feature-header-eight">
                                    <div class="feature-icon-eight">
                                        <span class="circle bg-green"><i class="fas fa-sign-in-alt"></i></span>
                                    </div>
                                    <div class="feature-cont">
                                        <div class="feature-text-eight">Sign up</div>
                                    </div>
                                </div>
                                <p>Are you looking to join online Learning? Now it's very simple, Now Sign up</p>
                                <span class="text-green">07</span>
                            </div>
                        </div>
                        <div class="mendor-box aos" data-aos="fade-up">
                            <div class="feature-grid text-center ">
                                <div class="feature-header-eight">
                                    <div class="feature-icon-eight">
                                        <span class="circle bg-orange"><i class="material-icons">event_seat</i></span>
                                    </div>
                                    <div class="feature-cont">
                                        <div class="feature-text-eight">Improve & Get Back</div>
                                    </div>
                                </div>
                                <p>you can gather different skill set, and you can become mentor too</p>
                                <span class="text-orange">08</span>
                            </div>
                        </div>
                        <!--  -->
                    </div>
                </div>
            </div>


        </div>
    </div>
   
  </div>
</section>
<!-- end -->
<!-- start -->
<section class="pb-5">
  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-md-6 col-lg-6 col-xl-4 col-xxl-4">
      <div class="quezz_body pos">
          <div class="test_quies">
            <div class="panel--header bg-green">
              <h3><i class="fas fa-edit"></i> Answer Five</h3>
              <ul class="quiz-page" id="quizpage">
                <li class="active">1</li>
                <li>2</li>
                <li>3</li>
                <li>4</li>
                <li>5</li>
              </ul>
            </div>
          </div>
          <div class="panal_body">
          <div id="quiz1"></div>
            
          </div>
         
        </div>
      </div>
      <div class="col-sm-6 col-md-6 col-lg-6 col-xl-4 col-xl-4  ">
        <div class="quezz_body pos">
          <div class="test_quies">
            <div class="panel--header bg-purple">
              <h3><i class="fas fa-edit"></i> Current Affairs & Editorials</h3>
            </div>
          </div>
          <div class="panal_body" style="    background-color: #fff!important;    box-shadow: 0 4px 14px rgba(185, 185, 185, .12)">
            
          <div class="corent_afeirs mt-3">
          <?php foreach($affairs as $affair){?>
              <div class="editorial-box pl-3">
                <span class="label bg-red "><a href="<?=base_url('current-affair/'.base64_encode($affair->id))?>"><?=$affair->title?></a></span>
                <p><a href="<?=base_url('current-affair/'.base64_encode($affair->id))?>"><?=substr($affair->description,0,100)?></a></p>
                <ul class="actions">
                  <li class="date"><?=date('d F Y',strtotime($affair->created_at))?></li>
                </ul>
            </div>
            <?php } ?>
       
            </div>
          </div>
          <div class="quezz_btn pt-1">
            <div class="fineshed">
              <!-- <button class="alert btn btn-danger">ReadMore</button> -->
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-md-6 col-lg-6 col-xl-4 col-xl-4">
        <div class="quezz_body pos">
          <div class="test_quies">
            <div class="panel--header bg-alert">
              <h3><i class="fas fa-edit"></i> What's New</h3>
            </div>
          </div>
          <div class="panal_body q_last">

          <?php foreach($whatsNews as $news){?>
            <div class="editorial-box mt-3  pl-3">
              <ul class="actions m-0 mt-1">
                <li class="date"><?=date('d F Y',strtotime($news->created_at))?></li>
              </ul>
              <p class="br_b"><a href="javascript:void(0)" class="news_d"><?=$news->title?> </a></p>
            </div>
          <?php } ?>
         
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- end -->
<!-- start -->
<section class="section popular-mendors">
  <div class="mendor-title">
    <div class="section-header-eight text-center">
      <div class="container aos" data-aos="fade-up">
        <!-- <span>Mentoring Goals</span> -->
        <h2 class="text-white">Our facility</h2>
        <!-- <p class="sub-title text-white"> Choose your most popular leaning mentors, it will help you to achieve
          your professional goals..</p> -->
        <div class="sec-dots">
          <span></span>
          <span></span>
          <span></span>
        </div>
      </div>
    </div>
  </div>
  <div class="mendor-list">
    <div class="container aos" data-aos="fade-up">
      <div class="mendor-slider slick">
        <?php foreach($faculties as $faculty){?>
        <div class="mendor-box">
          <div class="faciulty_box m_h">
            <div class="mendor-img">
              <a href="profile.html">
              <img class="img-fluid" alt src="<?=base_url($faculty->profile_pic)?>">
              </a>
              <!-- <div class="mendor-country"><i class="fas fa-map-marker-alt"></i> <?//=$faculty->cityName.','.$user->stateName?></div> -->
            </div>
            <div class="mendor-content">
              <h3 class="title m-0"><a href="" class="m-0"><?=$faculty->name?></a></h3>
              <div class="mendor-course">
                <?=$faculty->user_subject?>
              </div>
              <div class="mendor-price-list">
                <!-- <div class="mendor-price">$100 <span>/ hr</span></div> -->
                <div class="mendor-rating">
                  <i class="fas fa-star filled"></i>
                  <i class="fas fa-star filled"></i>
                  <i class="fas fa-star filled"></i>
                  <i class="fas fa-star filled"></i>
                  <i class="fas fa-star filled"></i>
                </div>
              </div>
              
            </div>
          </div>
        </div>
        
        <?php } ?>
        
        
      </div>
    </div>
  </div>
</section>
<section class="section path-section-eight deff">
  <div class="section-header-eight text-center aos" data-aos="fade-up">
    <div class="container">
      <span>Choose the</span>
      <h2>Different All Learning Paths</h2>
      <!-- <p class="sub-title">Are you looking to join online institutions? Now it's very simple, Sign up with
        mentoring</p> -->
      <div class="sec-dots">
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>
  </div>
  <div class="course-sec">
    <div class="container">
      <div class="row">
        <?php foreach($courses as $course){
          if($this->session->userdata('email')){
            $url = base_url('course-detail-panel/'.base64_encode($course->id));
          }else{
            $url = base_url('course-detail/'.base64_encode($course->id));
          }
          ?>
        <div class="col-12 col-md-6 col-lg-3 aos" data-aos="fade-up">
          <div class="course-item">
            <a href="<?=$url?>" class="course-img" >
              <div class="image-col-merge">
                <img src="<?=base_url($course->image)?>" alt="learn" style="width:100%;height:200px;">
                <div class="course-text">
                  <h5><?=$course->name?></h5>
                  <div class="d-flex justify-content-between  rig_ht">
                    <p><?=$course->categoryName?> </p>
                  </div>
                </div>
              </div>
            </a>
          </div>
        </div>
        <?php } ?>
      </div>
      <div class="view-all text-center aos" data-aos="fade-up">
        <a href="<?=base_url('course')?>" class="btn btn-primary btn-view">View All</a>
      </div>
    </div>
  </div>
</section>
<section class="section profile-section">
  <div class="section-header-eight text-center aos" data-aos="fade-up">
    <div class="container">
      <span>MOST VIEWED</span>
      <h2>Our Video</h2>
      <!-- <p class="sub-title">Are you looking to join online institutions? Now it's very simple, Sign up with
        mentoring</p> -->
      <div class="sec-dots">
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="video-list text-center">
    <div class="row  "  >
      <div class="video-slider slick">
        <?php foreach($videos as $video){?>
        <div class="mendor-box  border-0 ">
          <div class="video_box">
          <!-- <video width="100%" height="200px" controls> -->
          <iframe style="width: 97%; height: 210px; margin: auto;"  src="http://www.youtube.com/embed/<?=$video->video?>?autoplay=1" frameborder="0"></iframe>
        <!-- </video> -->
          </div>
        </div>
        <?php } ?>
      </div>
</div>
</div>
    <!-- <div class="row">
      <div class="col-12 col-md-6 col-lg-4 aos" data-aos="fade-up">
        <video width="100%" height="200px" controls>
          <source src="public/web/assets/img/vedio/edu.mp4" type="video/mp4">
        </video>
      </div>
    </div> -->
    <div class="row" style="display:none">
      <div class="col-12 col-md-6 col-lg-3 aos" data-aos="fade-up">
        <div class="profile-list">
          <div class="profile-detail">
            <div class="profile-img-eight">
              <img class="img-fluid" alt src="public/web/assets/img/mendor/mendor-01.jpg">
            </div>
            <div class="profile-content">
              <h4>Donna Yancey</h4>
              <p>UNIX, Calculus, Trigonometry</p>
            </div>
          </div>
          <div class="profile-rating">
            <div class="mendor-rating">
              <i class="fas fa-star filled"></i>
              <i class="fas fa-star filled"></i>
              <i class="fas fa-star filled"></i>
              <i class="fas fa-star filled"></i>
              <i class="fas fa-star"></i>
            </div>
            <div class="price-box">$500 <span>/ hr</span></div>
          </div>
        </div>
      </div>
      <div class="col-12 col-md-6 col-lg-3 aos" data-aos="fade-up">
        <div class="profile-list">
          <div class="profile-detail">
            <div class="profile-img-eight">
              <img class="img-fluid" alt src="public/web/assets/img/mendor/mendor-02.jpg">
            </div>
            <div class="profile-content">
              <h4>Betty Hairston</h4>
              <p>Computer Programming</p>
            </div>
          </div>
          <div class="profile-rating">
            <div class="mendor-rating">
              <i class="fas fa-star filled"></i>
              <i class="fas fa-star filled"></i>
              <i class="fas fa-star filled"></i>
              <i class="fas fa-star filled"></i>
              <i class="fas fa-star"></i>
            </div>
            <div class="price-box">$200 <span>/ hr</span></div>
          </div>
        </div>
      </div>
      <div class="col-12 col-md-6 col-lg-3 aos" data-aos="fade-up">
        <div class="profile-list">
          <div class="profile-detail">
            <div class="profile-img-eight">
              <img class="img-fluid" alt src="public/web/assets/img/mendor/mendor-03.jpg">
            </div>
            <div class="profile-content">
              <h4>Jose Anderson</h4>
              <p>ASP.NET,Computer Gaming</p>
            </div>
          </div>
          <div class="profile-rating">
            <div class="mendor-rating">
              <i class="fas fa-star filled"></i>
              <i class="fas fa-star filled"></i>
              <i class="fas fa-star filled"></i>
              <i class="fas fa-star filled"></i>
              <i class="fas fa-star"></i>
            </div>
            <div class="price-box">$300 <span>/ hr</span></div>
          </div>
        </div>
      </div>
      <div class="col-12 col-md-6 col-lg-3 aos" data-aos="fade-up">
        <div class="profile-list">
          <div class="profile-detail">
            <div class="profile-img-eight">
              <img class="img-fluid" alt src="public/web/assets/img/mendor/mendor-04.jpg">
            </div>
            <div class="profile-content">
              <h4>James Amen</h4>
              <p>Digital Marketer</p>
            </div>
          </div>
          <div class="profile-rating">
            <div class="mendor-rating">
              <i class="fas fa-star filled"></i>
              <i class="fas fa-star filled"></i>
              <i class="fas fa-star filled"></i>
              <i class="fas fa-star filled"></i>
              <i class="fas fa-star"></i>
            </div>
            <div class="price-box">$400 <span>/ hr</span></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="section statistics-section-eight">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-4 aos" data-aos="fade-up">
        <div class="statistics-list-eight">
          <div class="statistics-icon-eight">
            <i class="fas fa-street-view"></i>
          </div>
          <div class="statistics-content-eight">
            <span>500+</span>
            <h3>Happy Clients</h3>
          </div>
        </div>
      </div>
      <div class="col-12 col-md-4 aos" data-aos="fade-up">
        <div class="statistics-list-eight">
          <div class="statistics-icon-eight">
            <i class="fas fa-history"></i>
          </div>
          <div class="statistics-content-eight">
            <span>120+</span>
            <h3>Online Appointments</h3>
          </div>
        </div>
      </div>
      <div class="col-12 col-md-4 aos" data-aos="fade-up">
        <div class="statistics-list-eight">
          <div class="statistics-icon-eight">
            <i class="fas fa-user-check"></i>
          </div>
          <div class="statistics-content-eight">
            <span>100%</span>
            <h3>Job Satisfaction</h3>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- <section class="section section-blogs-eight">
  <div class="container">
    <div class="section-header-eight text-center aos" data-aos="fade-up">
      <span>LATEST</span>
      <h2>Blogs & News</h2>
       <p class="sub-title">Are you looking to join online institutions? Now it's very simple, Sign up with
        mentoring</p> 
      <div class="sec-dots">
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>
    <div class="row blog-grid-row justify-content-center">
      <div class="col-md-6 col-lg-4 col-sm-12 aos" data-aos="fade-up">
        <div class="blog-card">
          <div class="blog-card-image">
            <a href="blog-details.html"><img class="img-fluid" src="public/web/assets/img/blog/blog-01.jpg"
              alt="Post Image"></a>
          </div>
          <div class="blog-card-content">
            <div class="blog-month">04 <span>Dec</span></div>
            <ul class="meta-item-eight">
              <li>
                <div class="post-author-eight">
                  <a href="blog-details.html"><span>Tyrone Roberts</span></a>
                </div>
              </li>
            </ul>
            <h3 class="blog-title-eight"><a href="blog-details.html">What is Lorem Ipsum? Lorem Ipsum is
              simply</a>
            </h3>
            <p>Lorem ipsum dolor sit amet, consectetur em adipiscing elit, sed do eiusmod tempor.</p>
            <a href="blog-details.html" class="read">Read more</a>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-4 col-sm-12 aos" data-aos="fade-up">
        <div class="blog-card">
          <div class="blog-card-image">
            <a href="blog-details.html"><img class="img-fluid" src="public/web/assets/img/blog/blog-16.jpg"
              alt="Post Image"></a>
          </div>
          <div class="blog-card-content">
            <div class="blog-month">05 <span>Jan</span></div>
            <ul class="meta-item-eight">
              <li>
                <div class="post-author-eight">
                  <a href="blog-details.html"><span>Brittany Garcia</span></a>
                </div>
              </li>
            </ul>
            <h3 class="blog-title-eight"><a href="blog-details.html">Contrary to popular belief, Lorem Ipsum
              is</a>
            </h3>
            <p>Lorem ipsum dolor sit amet, consectetur em adipiscing elit, sed do eiusmod tempor.</p>
            <a href="blog-details.html" class="read">Read more</a>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-4 col-sm-12 aos" data-aos="fade-up">
        <div class="blog-card">
          <div class="blog-card-image">
            <a href="blog-details.html"><img class="img-fluid" src="public/web/assets/img/blog/blog-17.jpg"
              alt="Post Image"></a>
          </div>
          <div class="blog-card-content">
            <div class="blog-month">06 <span>May</span></div>
            <ul class="meta-item-eight">
              <li>
                <div class="post-author-eight">
                  <a href="blog-details.html"><span>Allen Davis</span></a>
                </div>
              </li>
            </ul>
            <h3 class="blog-title-eight"><a href="blog-details.html">The standard chunk of Lorem Ipsum
              used</a>
            </h3>
            <p>Lorem ipsum dolor sit amet, consectetur em adipiscing elit, sed do eiusmod tempor.</p>
            <a href="blog-details.html" class="read">Read more</a>
          </div>
        </div>
      </div>
    </div>
    <div class="view-all text-center aos" data-aos="fade-up">
      <a href="blog-list.html" class="btn btn-primary btn-view">View All</a>
    </div>
  </div>
</section> -->




<script src="https://storage.googleapis.com/chydlx/plugins/dlx-quiz/js/main.js"></script> 

<script>

   $("#quiz1").dlxQuiz({
    quizData: {
        questions: [
          <?php 
            $correct_ans = array();
          foreach($questions as $key=>$question){
              
              foreach($question['option'] as $key_option=>$options){
                foreach($question[$key_option]['answer'] as $key_ans=>$answer){
                $correct_ans[$question['id']] = $answer;
                }
              }

            ?>
            {
                q:'<?=$question['question']?>',
                a: '<?=$correct_ans[$question['id']]?>',
                options: [
                    <?php foreach($question['option'] as $option){?>
                      '<?=$option['option']?>',
                    <?php } ?>
                ]
            },
            <?php } ?>
            // {
            //     q: "जिस समिति में लोकतांत्रिक विकेंद्रीकरण और पंचायत राज्य की सिफारिश की उसका सभापति कौन था?",
            //     a: "बलवंत राय मेहता",
            //     options: [
            //         "के.एम पत्रिकर",
            //         "एच.एन.कुंजरू",
            //         "महात्मा गांधी",
            //         "बलवंत राय मेहता"
            //     ]
            // },
            // {
            //     q: "जलियांवाला बाग हत्याकांड के विरोध में निम्नलिखित में से किसने व्यासराय के कार्य प्रसिद्ध से त्यागपत्र दे दिया था?",
            //     a: "सर शंकर नायक",
            //     options: ["रविंद्र नाथ टैगोर", "मदन मोहन मालवीय","सर शंकर नायक","उपयुक्त तीनों"]
            // },
            // {
            //     q: "भारत में किस उद्योग में सर्वाधिक लोग कार्यरत होते हैं?",
            //     a: "लौह इस्पात उद्योग में",
            //     options: ["जूट उद्योग में", "लौह इस्पात उद्योग में", "कपड़ा उद्योग में","चीनी उद्योग में"]
            // },
            // {
            //     q: "किस प्रकार की मिट्टी में कार्बनिक पदार्थ की अधिकता होती है?",
            //     a: "काली",
            //     options: ["पिट", "काली","लेटेराइट","लाल"]
            // }
            
        ]
    }
});
</script>

<script>

 ( function ( $ ) {
  "use strict";
  $.dlxQuiz = function ( element, options ) {
    var plugin = this,
      $element = $( element ),
      _element_id = $element.attr( 'id' ),
      _element = '#' + _element_id,

      /*----------------------------
          Quiz JSON Data
      ----------------------------*/
      question_index = 0,
      quizData = options.quizData,
      //will be populated after json data is parsed
      questions = null,
      questionCount = null,

      /*----------------------------
          Defaults
       ---------------------------*/
      defaults = {

        /* Text
         ---------------------------*/
        questionCount_text: "Question %current_index of %totalQuestions",
        backButton_text: "Previous Question",
        nextButton_text: "Next Question",
        completeButton_text: "Finish Quiz",
        viewResultsButton_text: "View Results",
        resultsHeader_text: "Here's how you did.",
        quizScore_text: "You answered %totalScore out of %totalQuestions questions correctly",
        quizScoreMessage_text: "",
        quizScoreRank_text: {
          a: "Perfect Score!",
          b: "Great Job!",
          c: "At least you passed.",
          d: "Should have studied more.",
          f: "Did you even try?"
        },

        /* Options
         ---------------------------*/
        show_QuestionCount: true,
        //buttons
        showBackButton: true,
        //show radio input
        showRadioButtons: true,
        //results
        showScoreRank: true,
        showScoreMessage: true,
        showViewResultsButton: true,

        /* Misc
         ---------------------------*/
        randomizeQuestions: true,
        randomizeAnswers: true,
      },
      /*----------------------------
          Class name strings
       ---------------------------*/
      class_disabled = "disabled",
      //quiz question elements
      class_quizQuestions = "quizQuestions",
      class_showQuestion = "showQuestion",
      class_questionCount = "questionCount",
      class_questionTitle = "questionTitle",
      class_questionAnswers = "questionAnswers",
      class_selectedAnswer = "selectedAnswer",
      //quiz control elements
      class_quizControls = "quizControls",
      class_ctrlPreviousButton = "ctrlPrev",
      class_ctrlNextButton = "ctrlNext",
      class_ctrlCompleteButton = "ctrlDone",
      //quiz results elements
      class_quizResults = "quizResults",
      class_quizScoreRank = "quizScoreRank",
      class_quizScore = "quizScore",
      class_quizScoreMessage = "quizScoreMessage",
      class_viewResultsButton = "viewResults",
      class_showingResults = "showingResults",

      /*----------------------------
          Class selectors
      ----------------------------*/
      //quiz question elements
      _questions = " ." + class_quizQuestions,
      _question = _questions + ' > li',
      _answers = " ." + class_questionAnswers,
      //quiz control elements
      _controls = " ." + class_quizControls,
      _ctrlPreviousButton = " ." + class_ctrlPreviousButton,
      _ctrlNextButton = " ." + class_ctrlNextButton,
      _ctrlCompleteButton = " ." + class_ctrlCompleteButton,
      //quiz results elements
      _results = " ." + class_quizResults,
      _viewResultsButton = " ." + class_viewResultsButton,
      _showingResults = " ." + _showingResults,

      /*----------------------------
          Element class selectors
      ----------------------------*/
      _quizQuestions = _element + _question,
      _quiz_answer = _element + _answers + ' li',
      //control buttons
      _quizCtrls = _element + _controls,
      _quizCtrlPreviousButton = _element + _ctrlPreviousButton,
      _quizCtrlNextButton = _element + _ctrlNextButton,
      _quizCompleteButton = _element + _ctrlCompleteButton,
      _quizViewResultsButton = _element + _viewResultsButton,
      //results
      _quizResults = _element + _results;

    plugin.config = $.extend( defaults, options );

    /*----------------------------
        Methods
    ----------------------------*/
    plugin.method = {
      buildQuiz: function ( data ) {
        var _quizHTML;

        //set quizData
        quizData = data;
        //set questions
        questions = plugin.config.randomizeQuestions ? plugin.method.randomizeArray( quizData.questions ) : quizData.questions;
        //set question count
        questionCount = questions.length;

        // add quiz class to $element
        if ( !$element.hasClass( "quiz" ) ) {
          $element.addClass( "quiz" );
        }

        /*----------------------------
            build quiz questions
        ----------------------------*/
        _quizHTML = '<ul class="' + class_quizQuestions + '">';

        //list of questions
        $.each( questions, function ( q ) {
          /*----------------------------
              quiz question data
          ----------------------------*/
          var question = questions[ q ];
          question.options = plugin.config.randomizeAnswers ? plugin.method.randomizeArray( question.options ) : question.options;

          /*----------------------------
              build question list
          ----------------------------*/
          _quizHTML += '<li';

          //show first question only
          _quizHTML += ( q === 0 ? ' class="' + class_showQuestion + '">' : '>' );

          //display question count
          if ( plugin.config.show_QuestionCount ) {
            _quizHTML += '<span class="' + class_questionCount + '">';
            _quizHTML += plugin.config.questionCount_text
              .replace( '%current_index', q + 1 )
              .replace( '%totalQuestions', questionCount );
            _quizHTML += '</span>';
          }

          //question title
          _quizHTML += '<h2 class="' + class_questionTitle + '">';
          _quizHTML += question.q;
          _quizHTML += '</h2>';

          //answer options list
          _quizHTML += '<ul class="' + class_questionAnswers + '">';

          /*----------------------------
              build answers list
          ----------------------------*/
          $.each( question.options, function ( a ) {

            var _input_name = _element_id + '-q' + ( q + 1 ),
              _input_id = _input_name + '-a' + ( a + 1 );

            //build options list
            _quizHTML += '<li>';

            //build label
            _quizHTML += '<label for="' + _input_id + '">';

            //build radio input
            _quizHTML += '<input ';
            //show radio input
            _quizHTML += ( plugin.config.showRadioButtons ? '' : 'class="hidden" ' );
            //end show radio input

            _quizHTML += 'type="radio" name="';
            _quizHTML += _input_name + '"';
            _quizHTML += ' id="' + _input_id + '"';
            _quizHTML += ' value="' + question.options[ a ] + '">';

            //input value
            _quizHTML += question.options[ a ];
            _quizHTML += '</label>';
            //end label

            _quizHTML += '</li>';
            //end options list

          } );
          //end answers list
          _quizHTML += '</ul>';

          //end questions list
          _quizHTML += '</li>';

          q += 1;
        } );
        _quizHTML += '</ul>';

        /*----------------------------
            build quiz controls
        ----------------------------*/
        _quizHTML += '<div class="' + class_quizControls + '">';

        //previous button
        if ( plugin.config.showBackButton ) {
          _quizHTML += '<button class="' + class_ctrlPreviousButton + '">';
          _quizHTML += plugin.config.backButton_text + '</button>';
        }
        //next button
        _quizHTML += '<button class="' + class_ctrlNextButton + ' ' + class_disabled + '">';
        _quizHTML += plugin.config.nextButton_text + '</button>';
        //done button
        _quizHTML += '<button class="' + class_ctrlCompleteButton + ' ' + class_disabled + '">';
        _quizHTML += plugin.config.completeButton_text + '</button>';

        //close quiz controls
        _quizHTML += '</div>';

        /*----------------------------
            append quiz html
        ----------------------------*/
        $element.append( _quizHTML );

        /*----------------------------
            enable quiz events
        ----------------------------*/
        plugin.events.init();

      },
      randomizeArray: function ( array ) {
        var m = array.length,
          t, i;

        // While there remain elements to shuffleÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦
        while ( m ) {

          // Pick a remaining elementÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦
          i = Math.floor( Math.random() * m-- );

          // And swap it with the current element.
          t = array[ m ];
          array[ m ] = array[ i ];
          array[ i ] = t;
        }

        return array;

      },
      buildQuizResults: function () {
        var resultsHTML = '',
          correctAnswerCount = 0,
          totalScore;

        function _checkAnswers() {
          //check each questions data
          $.each( questions, function ( index ) {
            //if answer === selected answer
            questions[ index ].answerCorrect = questions[ index ].selected === questions[ index ].a ? true : false;

            if ( questions[ index ].answerCorrect ) {
              //plus one to correct answer count
              correctAnswerCount += 1;
            } else {
              //else count stays the same
              correctAnswerCount = correctAnswerCount;
            }
          } );
          totalScore = ( correctAnswerCount / questionCount ) * 100;
        }
        _checkAnswers();
        /* build quiz results
        ----------------------------*/
        resultsHTML += '<div class="' + class_quizResults + '">';

        //quiz score rank
        resultsHTML += '<h1 class="' + class_quizScoreRank + '">';
        if ( totalScore > 80 ) {
          //rank 1: 80-100
          resultsHTML += plugin.config.quizScoreRank_text.a;
        } else if ( totalScore > 60 ) {
          //rank 2: 60-79
          resultsHTML += plugin.config.quizScoreRank_text.b;
        } else if ( totalScore > 40 ) {
          //rank 3: 40-59
          resultsHTML += plugin.config.quizScoreRank_text.c;
        } else if ( totalScore > 20 ) {
          //rank 4: 20-39
          resultsHTML += plugin.config.quizScoreRank_text.d;
        } else {
          //rank 5: 0-19
          resultsHTML += plugin.config.quizScoreRank_text.f;
        }
        resultsHTML += '</h1>';

        //quiz score total
        resultsHTML += '<p class="' + class_quizScore + '">';
        resultsHTML += plugin.config.quizScore_text
          .replace( '%totalScore', correctAnswerCount )
          .replace( '%totalQuestions', questionCount );
        resultsHTML += '</p>';
        //quiz score message
        if ( plugin.config.showScoreMessage ) {
          resultsHTML += '<p class="' + class_quizScoreMessage + '">';
          resultsHTML += plugin.config.quizScoreMessage_text;
          resultsHTML += '</p>';
        }
        //view results button
        if ( plugin.config.showViewResultsButton ) {
          resultsHTML += '<button class="' + class_viewResultsButton + '">';
          resultsHTML += plugin.config.viewResultsButton_text;
          resultsHTML += '</button>';
        }
        //add to DOM
        $element.append( resultsHTML );

        //if show view results button
        if ( plugin.config.showViewResultsButton ) {
          //init view results button event
          plugin.events.resultsButton();
        }
      }
    };
    /*----------------------------
        Events
    ----------------------------*/
    plugin.events = {
      init: function () {
        this.controls.init();
        this.answerQuestion();
        this.checkQuestion();
      },
      controls: {
        DOM: function () {
          this.plugin = plugin.events;
          this.questionCount = questionCount - 1;
          //buttons
          this.$previous = $( _quizCtrlPreviousButton );
          this.$next = $( _quizCtrlNextButton );
          this.$complete = $( _quizCompleteButton );
          //all buttons
          this.$buttons = this.$previous
            .add( this.$next )
            .add( this.$complete );
        },
        init: function () {
          //cache button Elements
          this.DOM();

          var $buttons = this.$buttons,
            _this = this;

          $buttons.on( 'click', function () {
            var $button = $( this );

            //check if button disabled
            if ( _this.isNotDisabled( $button ) ) {
              //check button classname
              switch ( $button.attr( 'class' ) ) {
                //previous question
                case class_ctrlPreviousButton:
                  _this.plugin.previousQuestion();
                  break;
                  //next question
                case class_ctrlNextButton:
                  _this.plugin.nextQuestion();
                  break;
                case class_ctrlCompleteButton:
                  //hide questions and controls
                  $( _element + ' ' + _questions ).add( _quizCtrls ).remove();
                  //build results
                  plugin.method.buildQuizResults();
                  break;
              }
            }
          } );
        },
        isNotDisabled: function ( button ) {
          //check if button is not disabled
          return !button.hasClass( class_disabled ) ? true : false;
        },
        resetDisabled: function () {

          var totalAnswered = 0;

          switch ( question_index ) {
            case 0:
              this.$previous.addClass( class_disabled );
              this.$complete.hide();
              this.$next.show();
              break;
            case this.questionCount:
              this.$next.addClass( class_disabled ).hide();
              this.$previous.removeClass( class_disabled );
              this.$complete.show();
              break;
            default:
              this.$previous.removeClass( class_disabled );
              this.$next.show();
              this.$complete.hide();
              break;
          }

          this.$next = questions[ question_index ].selected !== undefined ?
            //remove disabled class
            this.$next.removeClass( class_disabled ) :
            //add disabled class
            this.$next.addClass( class_disabled );

          //                    //if answer selected
          //                    if (questions[question_index].selected !== undefined) {
          //                        //remove disabled class
          //                        this.$next.removeClass(class_disabled);
          //                    } else {
          //                        //add disabled class
          //                        this.$next.addClass(class_disabled);
          //                    }

          //check total answered questions
          $.each( questions, function ( i ) {
            //if question is answered
            totalAnswered = questions[ i ].selected ?
              //add one
              totalAnswered += 1 :
              //else total stays the same
              totalAnswered = totalAnswered;
          } );

          //if all questions answered
          if ( totalAnswered === questionCount ) {
            this.$complete.removeClass( class_disabled );
          }
        }
      },
      nextQuestion: function () {
        question_index += 1;
        this.checkQuestion();
      },
      previousQuestion: function () {
        question_index -= 1;
        this.checkQuestion();
      },
      checkQuestion: function () {
        //reset buttons
        this.controls.resetDisabled();
        //remove show question class
        $( _quizQuestions ).removeClass( class_showQuestion );
        //add show question class to current question
        $( $( _quizQuestions )[ question_index ] ).addClass( class_showQuestion );
      },
      answerQuestion: function () {

        function resetAnswerGroup( input ) {
          var _grpName = $( 'input:radio[name="' + input.prop( "name" ) + '"]' ),
            _inputParent = _grpName.parent().parent();

          //remove selected answer for input group
          _inputParent.removeClass( class_selectedAnswer );
        }

        $( _quiz_answer + ' input' ).on( 'click', function ( e ) {
          var _$answer = $( this ),
            _answerParent = _$answer.parent().parent();

          //reset input group
          resetAnswerGroup( _$answer );

          //add selected answer class
          _answerParent.addClass( class_selectedAnswer );

          //store answer value in quiz json data
          questions[ question_index ].selected = _$answer.val();
          //check question
          plugin.events.checkQuestion();
        } );
      },
      resultsButton: function () {
        $( _quizViewResultsButton ).on( 'click', function () {

          var resultsHTML;
          //remove view results button
          $( _quizViewResultsButton ).remove();

          //build results
          resultsHTML = '<h2>' + plugin.config.resultsHeader_text + '</h2>';
          resultsHTML += '<ul class="' + class_showingResults + '">';

          $.each( questions, function ( index ) {

            //question list
            resultsHTML += '<li class="';
            //if answer correct
            resultsHTML += questions[ index ].answerCorrect ?
              //give class answeredCorrect
              'answeredCorrect' :
              //else give class answeredWrong
              'answeredWrong';
            //close opening tag
            resultsHTML += '">';

            //question
            resultsHTML += '<h3 class="questionTitle">';
            resultsHTML += ( index + 1 ) + ". " + questions[ index ].q;
            resultsHTML += '</h3>';

            //show answer
            resultsHTML += '<p>';
            //if answer was incorrect
            if ( !questions[ index ].answerCorrect ) {
              //display user answer
              resultsHTML += '<strong>Your Answer: </strong>';
              resultsHTML += questions[ index ].selected + '<br>';
            }
            //display correct answer
            resultsHTML += '<strong>Correct Answer: </strong>';
            resultsHTML += questions[ index ].a + '<br>';
            //close p tag
            resultsHTML += '</p>';
            resultsHTML += '</li>';
          } );

          resultsHTML += '</ul>';

          $( _quizResults ).append( resultsHTML );

        } );
      }
    };

    //get quizData json
    if ( quizData ) {
      //external json file
      if ( typeof quizData === "string" ) {
        //get json data from url
        $.getJSON( quizData )
          //start quiz
          .then( function ( data ) {
            plugin.method.buildQuiz( data );
          } );
        //json quiz data
      } else if ( typeof options.quizData === "object" ) {
        //start quiz
        plugin.method.buildQuiz( quizData );
      } else {
        //change quizData to null
        quizData = null;
        //throw error message to console
        throw "Error: Check quizData for - " + _element;
      }
    }

  };
  $.fn.dlxQuiz = function ( options ) {
    return this.each( function () {
      var plugin = new $.dlxQuiz( this, options );
    } );
  };
}( jQuery ) );

  </script>
