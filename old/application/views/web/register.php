<div class="main-wrapper">
  <div class="bg-pattern-style bg-pattern-style-register mt-5 pb-4">
    <div class="content">
      <div class="account-content">
        <div class="account-box">
          <div class="login-right">
            <div class="login-header">
              <h3><span>Student</span> Register</h3>
              <p class="text-muted">Access to our dashboard</p>
            </div>
            <form action="<?=base_url('Authantication/store')?>" id="userAddForm" method="post">
              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group">
                    <label class="form-control-label">Full Name <span class="text-danger">*</span></label>
                    <input  type="text" class="form-control" name="name" id="name" autofocus>
                  </div>
                </div>
                <div class="col-lg-6">
                <div class="form-group">
                <label class="form-control-label">Email Address <span class="text-danger">*</span></label>
                <input  type="email" class="form-control" id="email" name="email">
              </div>
                </div>
         
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label">Contact No. <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="phone" id="phone" minlength="10" maxlength="10" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                  </div>
                </div>

                <div class="col-lg-6">
                  <div class="form-group sts_te">
                    <label class="form-control-label stste">State <span class="text-danger">*</span></label>
                    <select class="form-control js-example-basic-single " name="state" id="state" onchange="get_city(this.value)">
                     <option value="">Select State</option>
                     <?php foreach($states as $state){?>
                      <option value="<?=$state->id?>"><?=$state->name?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="col-lg-6">
                  <div class="form-group sts_te">
                    <label class="form-control-label stste">City <span class="text-danger">*</span></label>
                    <select class="form-control js-example-basic-single city " name="city" id="city" >
                     <option value="">Select City</option>
                    </select>
                  </div>
                </div>

                <div class="col-lg-12">
                  <div class="form-group">
                    <label class="form-control-label">Address <span class="text-danger">*</span></label>
                    <textarea   class="form-control" name="address" id="address"></textarea>
                  </div>
                </div>
                <div class="col-lg-12">
                <div class="form-group">
                <label class="form-control-label">Pincode <span class="text-danger">*</span></label>
                <input  type="text" class="form-control" id="pincode" name="pincode" minlength="6" maxlength="6" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
              </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label">Password <span class="text-danger">*</span></label>
                    <input  type="password" class="form-control" name="password" id="password">
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label">Confirm Password <span class="text-danger">*</span></label>
                    <input  type="password" class="form-control" name="confirm_password" id="confirm_password">
                  </div>
                </div>
              </div>
              
              <div class="col-lg-6">
                  <div class="g-recaptcha" data-sitekey="6LcNxXYmAAAAAGoQTPPRAkGPqNYZnRh_OD0dC8DL"></div>
              </div>
         
              <div class="form-group">
                <div class="form-check form-check-xs custom-checkbox">
                  <input type="checkbox" class="form-check-input" name="agreeCheckboxUser" id="agree_checkbox_user">
                  <label class="form-check-label" for="agree_checkbox_user">I agree to Mentoring</label> <a tabindex="-1" href="javascript:void(0);">Privacy Policy</a> &amp; <a tabindex="-1" href="javascript:void(0);"> Terms.</a>
                </div>
              </div>
              <button class="btn btn-primary login-btn" type="submit">Create Account</button>
              <div class="account-footer text-center mt-3">
                Already have an account? <a class="forgot-link mb-0" href="<?=base_url('login')?>">Login</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  $("form#userAddForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();    
  var formData = new FormData(this);
  formData.append("user_type", 'Student');
  formData.append("created_by", 'Self');
  $.ajax({
  url: $(this).attr('action'),
  type: 'POST',
  data: formData,
  cache: false,
  contentType: false,
  processData: false,
  dataType: 'json',
  success: function (data) {
  if(data.status==200) {
    toastNotif({
				text: data.message,
				color: '#5bc83f',
				timeout: 5000,
				icon: 'valid'
			});
  $(':input[type="submit"]').prop('disabled', false);
  setTimeout(function(){

     location.href="<?=base_url('verification')?>"; 	
    
  }, 1000) 
  
  }else if(data.status==403) {
    toastNotif({
				text: data.message,
				color: '#da4848',
				timeout: 5000,
				icon: 'error'
			});

  $(':input[type="submit"]').prop('disabled', false);
  }else{
    toastNotif({
				text: data.message,
				color: '#da4848',
				timeout: 5000,
				icon: 'error'
			});
     $(':input[type="submit"]').prop('disabled', false);
  }
  },
  error: function(){} 
  });
  });
  </script>
