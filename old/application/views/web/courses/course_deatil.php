
<?php 
  $pdf_decode = json_decode($topic->pdf);
  $vedio_decode = json_decode($topic->video);
  //$total_contants =  count($pdf_decode)+count($vedio_decode);
  ?>
<div class="breadcrumb-bar coursedetails" style="background:url(<?=base_url('public/web/assets/img/bgsubject.jpg')?>)">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-xl-9 col-lg-9 col-md-9 col-12">
        <div class="mentor-widget">
          <div class="user-info-left">
            <div class="user-info-cont">
              <h2 class="usr-name">
                <a href="javascript:void(0)"><?=$course->name?></a>
              </h2>
              <p class="mentor-type"><?=$course->courseName?></p>
              <!--<div class="rating">-->
              <!--  <i class="fas fa-star filled"></i>-->
              <!--  <i class="fas fa-star filled"></i>-->
              <!--  <i class="fas fa-star filled"></i>-->
              <!--  <i class="fas fa-star filled"></i>-->
              <!--  <i class="fas fa-star"></i>-->
              <!--  <span class="d-inline-block average-rating">(17)</span>-->
              <!--</div>-->
              <div class="mentor-details">
                <!-- <p class="user-location">
                  <i class="fas fa-map-marker-alt"></i> Florida, USA
                </p> -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-7 col-md-12 offset-md-1">
        <!-- <ul class="nav nav-tabs" id="myTab" role="tablist">
          <li class="nav-item" role="presentation">
            <button class="nav-link active btn_ac" id="home-tab" data-bs-toggle="tab" data-bs-target="#home-tab-pane" type="button" role="tab" aria-controls="home-tab-pane" aria-selected="true">Information</button>
          </li> -->
          <!-- <li class="nav-item" role="presentation">
            <button class="nav-link bg_theam btn_ac" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile-tab-pane" type="button" role="tab" aria-controls="profile-tab-pane" aria-selected="false">Contants (<?=$total_contants ?>)</button>
          </li>
          <li class="nav-item" role="presentation">
            <button class="nav-link bg_theam btn_ac" id="contact-tab" data-bs-toggle="tab" data-bs-target="#contact-tab-pane" type="button" role="tab" aria-controls="contact-tab-pane" aria-selected="false">Reviews</button>
          </li> -->
          <!-- <li class="nav-item" role="presentation">
            <button class="nav-link" id="disabled-tab" data-bs-toggle="tab" data-bs-target="#disabled-tab-pane" type="button" role="tab" aria-controls="disabled-tab-pane" aria-selected="false" disabled>Disabled</button>
            </li> -->
        <!-- </ul> -->
        <div class="tab-content" id="myTabContent">
          <div class="tab-pane fade show active" id="home-tab-pane" role="tabpanel" aria-labelledby="home-tab" tabindex="0">
            <div class="blog-view">
              <div class="blog-single-post">
                <!-- <div class="blog-image">
                  <a href="javascript:void(0);"><img alt src="<?//=base_url('public/web/assets/img/blog/blog-01.jpg')?>" class="img-fluid"></a>
                  </div> -->
                <h2 class="blog-title"><?=$course->name?></h2>
                <div class="blog-info clearfix">
                  <div class="post-left">
                    <ul>
                      <li>
                        <div class="post-author">
                          <a href="javascript:void()"><img alt src="<?=base_url($course->courseImage)?>" alt="Post Author"> <span><?=$course->courseName?></span></a>
                        </div>
                      </li>
                      <li><i class="far fa-calendar"></i><?=date('d-F-Y',strtotime($course->created_at))?></li>
                      <!-- <li><i class="far fa-comments"></i>12 Comments</li>
                        <li><i class="fa fa-tags"></i>HTML</li> -->
                    </ul>
                  </div>
                </div>
                <div class="blog-content">
                  <p>
                    <?=$course->description?>
                  </p>
                </div>
              </div>
            </div>
          </div>

          <!-- <div class="tab-pane fade" id="disabled-tab-pane" role="tabpanel" aria-labelledby="disabled-tab" tabindex="0">...</div> -->
        </div>
      </div>
      <div class="col-lg-3 col-md-12 sidebar-right theiaStickySidebar">
        <div class="card courseimage search-widget">
          <div class="card-body">
            <img src="<?=base_url($course->image)?>" style="width:100%;height:209px;">
          </div>
        </div>
        <div class="card category-widget">
          <div class="card-header price">
            <h4 class="card-title text center">₹ <?=$course->price?></h4>
          </div>
          <div class="card-body buttonprice">
            <div class="submit-section">
              <a href="<?=empty($this->session->userdata('email')) ?  base_url('login') : ($this->session->userdata('verification_user')==0 ? base_url('verification') : base_url('login'))?>" class="btn btn-primary ">Add to Cart</a>
              <a href="<?=empty($this->session->userdata('email')) ?  base_url('login') : ($this->session->userdata('verification_user')==0 ? base_url('verification') : base_url('login'))?>" class="btn btn-transparent ">Buy Now</a>
            </div>
          </div>
        </div>
        <div class="card tags-widget d-none">
          <div class="card-header">
            <h4 class="card-title">Bundle Specifications</h4>
          </div>
          <div class="card-body">
            <ul class="">
              <li><a href="javascript:void()"><i class="fa fa-clock"></i>Duration<span><?=$course->duration?> Days</span></a></li>
              <li><a href="javascript:void()"><i class="fa fa-user-friends"></i>Students<span>0</span></a></li>
              <li><a href="javascript:void()"><i class="fa fa-book"></i>Courses<span>1</span></a></li>
              <li><a href="javascript:void()"><i class="fa fa-calendar-day"></i>Created Date<span><?=date('d-F-Y',strtotime($course->created_at))?></span></a></li>
              <!-- <li><a href="javascript:void()"><i class="fa fa-briefcase-clock"></i>Access Period<span>500 Days</span></a></li> -->
            </ul>
          </div>
        </div>
        <div class="card widget-profile user-widget-profile" style="display:none;">
          <div class="card-header">
            <h4 class="card-title">Instructor Profile</h4>
          </div>
          <div class="card-body">
            <div class="pro-widget-content">
              <div class="profile-info-widget">
                <a href="" class="booking-user-img">
                <img alt src="<?=base_url('public/web/assets/img/user/user.jpg')?>">
                </a>
                <div class="profile-det-info">
                  <h3>
                    <a href="">Richard Wilson</a>
                  </h3>
                  <div class="mentee-details">
                    <h5>
                      <b>course :</b> ABC
                    </h5>
                    <h5 class="mb-0">
                      <i class="fas fa-book"></i>Masters PHD 
                    </h5>
                  </div>
                </div>
              </div>
            </div>
            <div class="mentee-info">
              <ul>
                <li>Phone <span>+1 952 001 8563</span></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade custom-modal" id="video_modal">
      <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content lg">
          <!-- <div class="modal-header">
            <h5 class="modal-title">Edit Time Slots</h5>
            <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div> -->
          <div class="modal-body" style="width:100%;height:100%">
            <div id="show_video"></div>
          </div>
        </div>
      </div>
    </div>

<!-- <div class="modal fade " id="video_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" id="show_video">
      
    </div>
  </div>
</div> -->
<script>
  function checkLogin(){
    toastNotif({
  text: 'Assces denied Please login ',
  color: '#da4848',
  timeout: 5000,
  icon: 'error'
  });
  }

  function paly_video_modal(video){
    $('#video_modal').modal('show');
    $('#show_video').html('<div class="embed-responsive embed-responsive-16by9"><iframe id="vandyplayer" class="embed-responsive-item" allowfullscreen="allowfullscreen" webkitallowfullscreen="webkitallowfullscreen" mozallowfullscreen="mozallowfullscreen" src="https://www.youtube.com/embed/v=2M4jRI7s2JU" style="width:100%;height:100%"></iframe></div>');
    $('.modal').remove();
    $('.modal-backdrop').remove();
    //$('body').removeClass( "modal-open" );

  }
</script>