

<div class="breadcrumb-bar">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-md-8 col-12">
        <nav aria-label="breadcrumb" class="page-breadcrumb">
          <ol class="breadcrumb">
            <li class=""><a href="<?=base_url('home')?>">Home</a></li>/
            <li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
          </ol>
        </nav>
        <h2 class="breadcrumb-title"><?=$page_title?></h2>
      </div>

    </div>
  </div>
</div>
<div class="content">
  <div class="container">
    <div class="row justify-content-center align-items-center">
      <div class="col-md-12 quizbg">
        <!-- <h1 class="heading">Path to Prelims - 2023</h1> -->
        <div class="table-responsive1">
          <table class=" table table-striped table-bordered" id="testSeriesDataTable">
            <thead>
              <tr>
                <th></th>
        
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
 
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {
     //alert('dfgfgf');
       // $('#example').DataTable();
       // } );
       var dataTable = $('#testSeriesDataTable').DataTable({
           "processing": true,
           "serverSide": true,
           buttons: [{
               extend: 'excelHtml5',
               text: 'Download Excel'
           }],
           "order": [],
           "ajax": {
               url: "<?=base_url('Test_series/testSeriesAjax')?>",
               type: "POST"
           },
           "columnDefs": [{
               "targets": [0],
               "orderable": false,
           }, ],
       });
   });
   </script>