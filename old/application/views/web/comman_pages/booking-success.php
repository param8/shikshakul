
<div class="breadcrumb-bar">
        <div class="container-fluid">
          <div class="row align-items-center">
            <div class="col-md-12 col-12">
              <nav aria-label="breadcrumb" class="page-breadcrumb">
                <ol class="breadcrumb">
                  <li class=""><a href="<?=base_url('home')?>">Home</a></li>/
                  <li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
                </ol>
              </nav>
              <h2 class="breadcrumb-title"><?=$page_title?></h2>
            </div>
          </div>
        </div>
      </div>
      <div class="content success-page-cont">
        <div class="container-fluid">
          <div class="row justify-content-center">
            <div class="col-lg-6">
              <div class="card success-card">
                <div class="card-body">
                  <div class="success-cont">
                      <?php if($booking_status=='Success'){?>
                    <i class="fas fa-check text-success"></i>
                    <?php } else{?>
                    <i class="fa fa-close text-danger"></i>
                    <?php }?>
                   
                    <h3>Course booked <?=$booking_status?></h3>
                    <?php if($booking_status=='Success'){
                       if($order->payment_type=='Offline'){
                      ?>
                      <p>Course booked with Order ID <strong><?=$order->orderID?></strong><br> on <strong><?=date('d F Y H : i : s',strtotime($order->created_at))?></strong>
                        <p class="text-danger">Waiting for approve by Admin</p> 
                    </p>
                       <?php }else{?>
                    <p>Course booked with Order ID <strong><?=$order->orderID?></strong><br> on <strong><?=date('d F Y H : i : s',strtotime($order->created_at))?></strong></p>
                    <?php } } else{?>
                    <p>Payment is pending this Order ID <strong><?=$order->orderID?></strong><br> on <strong><?=date('d F Y H : i : s',strtotime($order->created_at))?></strong></p>
                    <?php } ?>
                    <!-- <a href="invoice-view.html" class="btn btn-primary view-inv-btn">View Invoice</a> -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      