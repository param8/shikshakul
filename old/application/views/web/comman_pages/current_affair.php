<section class="pt-5 pb-5">

<div class="container">
    <div class="row">
    <?php foreach($current_affairs as $current_affair){?>
        <div class="col-md-6">
            <div class="discreption">
                <h3><?=$current_affair->title?></h3>
                <p><?=$current_affair->description?></p> 
            </div>
        </div>
        <div class="col-md-6">
            <div class="course_img">
                <img src="<?=base_url($current_affair->image)?>" style="width:100%" alt="">
            </div>
        </div>
        <?php } ?>
    </div>
</div>

</section>