<div class="breadcrumb-bar">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-md-12 col-12">
        <nav aria-label="breadcrumb" class="page-breadcrumb">
          <ol class="breadcrumb">
            <li class=""><a href="<?=base_url('home')?>">Home</a></li>''
            <li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
          </ol>
        </nav>
        <h2 class="breadcrumb-title"><?=$page_title?></h2>
      </div>
    </div>
  </div>
</div>
<div class="content" style="min-height: 94.6px;">
  <section class="inter_view">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="mendor-list1">
        <div class="interview_slider lick-slider">
           <div class="mendor-box"  >
                  <div class="faciulty_box1">
                  <img src="public/web/assets/img/interview/p1.png" alt="p1" class="" style="    width: 95%;height:350px">
                <div class="user_d">
              <h3>  Shri Ashok Kumar</h3>
                <span>(Retd IFS)</span>
                <p>Former Head of Indian Council of World
                Affairs (ICWA) and Former High
                Commissioner to Cyprus.</p>
                </div>
                  </div>
            </div>
            <div class="mendor-box"  >
                  <div class="faciulty_box1">
                  <img src="public/web/assets/img/interview/p2.png" alt="p1" class="" style="    width: 95%;height:350px">
                  <div class="user_d">
              <h3>  Ombir Singh Veerwal</h3>
                <span>(Retd IFS)</span>
                <p>IPOS 1981 batch & Addl Sec (R) GOI.
                Former Chief PMG Rajasthan &
                Former Chief General Manager
                (BD& M Dte ) India Post.</p>
                </div>
                  </div>
            </div>
            <div class="mendor-box"  >
                  <div class="faciulty_box1">
                  <img src="public/web/assets/img/interview/p3.png" alt="p1" class="" style="    width: 95%;height:350px">
                  <div class="user_d">
              <h3>Peeyush Srivastava</h3>
                <span>(I.P.S)</span>
                <p> Retd I.G.U.P.</p>
                </div>
                  </div>
            </div>
            <div class="mendor-box"  >
                  <div class="faciulty_box1">
                  <img src="public/web/assets/img/interview/p4.png" alt="p1" class="" style="    width: 95%;height:350px">
                  <div class="user_d">
              <h3>Shri Arvind Singh </h3>
                <span>(IAS (Retd))</span>
                <p> 
Former Municipal Commissioner/
Regional Food Controller/Managing
Director, (PCF), UP.</p>
                </div>
                  </div>
            </div>
            <div class="mendor-box"  >
                  <div class="faciulty_box1">
                  <img src="public/web/assets/img/interview/p5.png" alt="p1" class="" style="    width: 95%;height:350px">
                  <div class="user_d">
                  <h3>Shri Gyanendra Sharma</h3>
                <span>(Retd HJS)</span>
                <p>  Ex district and session judge
                  (Higher judicial services)</p>
                </div>
                  </div>
            </div>
          </div>
      </div>
     </div>
    </div>
  </div>
  <section>
  <section class="vid_eo">
  <div class="container">
    <div class="row">
      <?php foreach($interviews as $interview){ ?>
    <div class="col-12 col-md-4 col-lg-4  ">
      <a href="<?=$interview->video_link?>" target="_blank"><img src="<?=$interview->image?>" style="width: 97%; height: 210px; margin: auto;"></a>
      <!-- <iframe class="mt-4" style="width: 97%; height: 210px; margin: auto;" src="https://www.youtube.com/embed/2Z1IP4Shn-0" frameborder="0"></iframe> -->
      </div>
      
    <?php } ?>
    </div>
  </div>
  <section>
</section></section></section></section></div>