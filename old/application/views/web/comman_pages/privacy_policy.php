<div class="breadcrumb-bar">
  <div class="container-fluid">
    <div class="row align-items-center">
      <div class="col-md-12 col-12">
        <nav aria-label="breadcrumb" class="page-breadcrumb">
          <ol class="breadcrumb">
            <li class=""><a href="<?=base_url('home')?>">Home</a></li>''
            <li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
          </ol>
        </nav>
        <h2 class="breadcrumb-title"><?=$page_title?></h2>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12 ">
        <p><?=$siteinfo->privacy_policy?></p>
      </div>
    </div>
  </div>
</div>