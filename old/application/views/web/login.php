<div class="main-wrapper">
  <div class="bg-pattern-style bg-pattern-style-register mt-5 pb-4">
    <div class="content">
      <div class="account-content">
        <div class="account-box">
          <div class="login-right">
            <div class="login-header">
              <h3><span><?=$page_title?></span> </h3>
              <p class="text-muted">Access to our dashboard</p>
            </div>
            <form action="<?=base_url('Authantication/login')?>" id="userloginForm" method="post">
              <div class="row">
          
                <div class="col-lg-12">
                <div class="form-group">
                <label class="form-control-label">Email / Contact  <span class="text-danger">*</span></label>
                <input  type="text" class="form-control" id="email" name="email">
              </div>
                </div>
         
     
                </div>
                <div class="col-lg-12">
                  <div class="form-group">
                    <label class="form-control-label">Password <span class="text-danger">*</span></label>
                    <input  type="password" class="form-control" name="password" id="password">
                  </div>
                </div>
        
              
              <div class="col-lg-6">
                  <div class="g-recaptcha" data-sitekey="6LcNxXYmAAAAAGoQTPPRAkGPqNYZnRh_OD0dC8DL"></div>
              </div>
         
          
              <button class="btn btn-primary login-btn" type="submit">Login</button>
            </form>

            <div class="account-footer text-center mt-3">
               If you forget password- <a class="forgot-link mb-0" href="<?=base_url('forgot-password')?>">Forgot Password</a>
              </div>

              <div class="account-footer text-center mt-3">
                Already have an account? <a class="forgot-link mb-0" href="<?=base_url('register')?>">Siginup</a>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  $("form#userloginForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();    
  var formData = new FormData(this);
  formData.append("login_by", 'User');
  $.ajax({
  url: $(this).attr('action'),
  type: 'POST',
  data: formData,
  cache: false,
  contentType: false,
  processData: false,
  dataType: 'json',
  success: function (data) {
  if(data.status==200) {
    toastNotif({
				text: data.message,
				color: '#5bc83f',
				timeout: 5000,
				icon: 'valid'
			});
  $(':input[type="submit"]').prop('disabled', false);
  setTimeout(function(){
    if(data.user_type=='Student'){
       if(data.verifivation_status == 1){
        location.href="<?=base_url('student-dashboard')?>";
       }else{
        location.href="<?=base_url('verification')?>";
       }
    }else{
      location.href="<?=base_url('faculty-dashboard')?>";
    }
      	
    
  }, 1000) 
  
  }else if(data.status==403) {
    toastNotif({
				text: data.message,
				color: '#da4848',
				timeout: 5000,
				icon: 'error'
			});

  $(':input[type="submit"]').prop('disabled', false);
  }else{
    toastNotif({
				text: data.message,
				color: '#da4848',
				timeout: 5000,
				icon: 'error'
			});
     $(':input[type="submit"]').prop('disabled', false);
  }
  },
  error: function(){} 
  });
  });
  </script>
