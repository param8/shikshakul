<link rel="stylesheet" type="text/css" href="<?=base_url('public/web/assets/css/message/messagestyle.css')?>">
<script src="https://cdn.jsdelivr.net/npm/pace-js@latest/pace.min.js"></script>
<link rel="stylesheet" href="<?=base_url('public/web/assets/css/message/loading-bar.css')?>">
<style>
  
</style>
<div class="content">
  <div class="container-fluid">
    <div class="settings-back mb-3">
      <a href="javascript:void(0)" onclick="javascript:history.go(-1)">
      <i class="fas fa-long-arrow-alt-left"></i> <span>Back</span>
      </a>
    </div>
    <div class="row">
      <div class="col-sm-12 mb-4">
        <div class="chat-window">
          <section id="main" class="bg-dark">
            <div id="chat_user_list">
              <div id="owner_profile_details">
                <div id="owner_avtar"
                  style="background-image: url('<?=base_url($this->session->userdata('profile_pic'))?>'); background-size: 100% 100%">
                  <div>
                    <div id="online"></div>
                  </div>
                </div>
                <div id="owner_profile_text" class="">
                  <h6 id="owner_profile_name" class="m-0 p-0"><?=$this->session->userdata('name')?></h6>
                </div>
              </div>
              <div id="search_box_container" class="py-3">
                <input type="text" name="txt_search" class="form-control" autocomplete="off" placeholder="Search User"
                  id="search">
              </div>
              <hr />
              <div id="user_list" class="py-3">
              </div>
            </div>
            <div id="chatbox">
              <div id="data_container" class="">
                <div id="bg_image"></div>
                <h2 class="mt-0">Hi There! Welcome To</h2>
                <h2> Shikshakul Chat</h2>
                <p class="text-center my-2">Connect to your device via Internet. Remember that you <br> must have a
                  stable
                  Internet Connection for a<br> greater experience.
                </p>
              </div>
              <div class="chatting_section" id="chat_area" style="display: none">
                <div id="header" class="py-2">
                  <div id="name_details" class="pt-2">
                    <div id="chat_profile_image" class="mx-2" style="background-size: 100% 100%">
                      <br>
                      
                      <div id="online"></div>
                    </div>
                    <div id="name_last_seen">
                      <h6 class="m-0 pt-2"></h6>
                      <p class="m-0 py-1"></p>
                    </div>
                   
                  </div>
                  <div class="back"><a href="javascript:void(0)" onclick="backUser()"><i class="fas fa-long-arrow-alt-left"></i></a></div>
                  <div id="icons" class="px-4 pt-2">
                    <!-- <div id="send_mail">
                      <a href="" id="mail_link"><i class="fas fa-envelope text-dark"></i></a>
                      </div> -->
                    <!-- <div id="details_btn" class="ml-3">
                      <i class="fas fa-info-circle text-dark"></i>
                      </div> -->
                  </div>
                </div>
                <div id="chat_message_area">
                </div>
                <form action="<?=base_url('Chat/sendMessage')?>" id="chatForm" method="post"
                  enctype="multipart/form-data" style=" width: 100%;">
                  <div id="messageBar" class="py-4 px-4">
                    <div id="textBox_attachment_emoji_container">
                      <div id="text_box_message" style="position :relative">
                        <div class='file1 file--upload'>
                          <label for='document'>
                          <i class="material-icons">cloud_upload</i>
                          </label>
                          <input id='document' name="document" type='file' />
                        </div>
                        <input type="text" maxlength="200" name="txt_message" id="messageText" class="form-control"
                          placeholder="Type your message" style="padding-left: 4%;">
                      </div>
                      <div id="text_counter">
                        <p id="count_text" class="m-0 p-0"></p>
                      </div>
                    </div>
                    <div id="sendButtonContainer">
                      <button type="submit" class="btn" id="send_message">
                      <span class="material-icons">send</span>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <!-- <div id="details_of_user">
              <div id="user_details_container_avtar" style="background-size: 100% 100%"></div>
              <h5 class="text-justify" id="details_of_name"></h5>
              <p class="text-justify" id="details_of_bio"></p>
              <div id="user_details_container_details">
                <p class="text-justify" id="details_of_created"></p>
                <p class="text-justify" id="details_of_birthday"></p>
                <p class="text-justify" id="details_of_mobile"><span></p>
                <p class="text-justify" id="details_of_email"><span></p>
                <p class="text-justify" id="details_of_location"><span></p>
              </div>
              <button class="btn btn-danger" id="btn_block">Block User</button>
              </div> -->
          </section>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  function backUser(){
  //alert('test');
  $('#chatbox').css('width', '0%');
  $('#chat_user_list').css('width', '100%');
  $('#data_container').css('display', 'none')
  }
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.6.1/gsap.min.js"></script>
<script src="<?=base_url('public/web/assets/js/message/main.js')?>"></script>
<script></script>