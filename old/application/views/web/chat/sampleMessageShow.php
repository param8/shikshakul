<?php
    $mysession = $_SESSION['unique_id'];
    $count = count($data);
    $date_time = array();
  

    for($i = 0; $i < $count; $i++){
   
        if($data[$i]['sender_message_id'] == $mysession){
        ?>
<div id="receiver_msg_container">
  
<div class="chet">
    <span class="r_i float-right">
      <svg viewBox="0 0 8 13" height="13" width="8" preserveAspectRatio="xMidYMid meet" class="" version="1.1" x="0px"
        y="0px" enable-background="new 0 0 8 13" xml:space="preserve">
        <path opacity="0.13" d="M5.188,1H0v11.193l6.467-8.625 C7.526,2.156,6.958,1,5.188,1z"></path>
        <path fill="#103c1b" d="M5.188,0H0v11.193l6.467-8.625C7.526,1.156,6.958,0,5.188,0z"></path>
      </svg>
    </span>
    <div class="m_d">
      <?php if(!empty($data[$i]['documents'])){
            $size = filesize($data[$i]['documents']);
            $units = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
            $power = $size > 0 ? floor(log($size, 1024)) : 0;
            $file_size = number_format($size / pow(1024, $power), 2, '.', ',') . ' ' . $units[$power];

            $ext  = (new SplFileInfo($data[$i]['documents']))->getExtension();
            $file_name = explode('/',$data[$i]['documents']);
            
            ?>
      <div class="send">
        <div class="left_s">
          <i class="fas fa-file"></i>
          <span>
            <span><?=$file_name[2];?></span>
            <span><b class="mr-2"><?=$ext?></b>. <?= $file_size?></span>
          </span>
        </div>
        <div class="d_load">
          <a href="<?=base_url('Chat/download_file/'.base64_encode($data[$i]['documents']))?>">
            <i class="fas fa-download"></i>
          </a>
        </div>
      </div>
      <div class="time text-right">
        <span><?=  date('d-M-Y H:i A',strtotime($data[$i]['time']));?></span>
        <p class="m-0" id="receiver_ptag"><?php echo $data[$i]['message'];?></p>
      </div>

      <!-- <div id="receiver_msg">
      <p class="m-0" id="receiver_ptag"><?php //echo $data[$i]['message'];?></p>
    </div> -->

      <?php }else{?>
      <div id="receiver_msg">
        <p class="m-0" id="receiver_ptag">
            <?php echo $data[$i]['message'];?>
           
          </p>
          <small class="ti_me" style="padding:2px;"> <?=  date('d-M-Y H:i A',strtotime($data[$i]['time']));?></small> 
      </div>

      <?php } ?>
    </div>

  </div>
  <div id="receiver_image"
    style="background-size: 100% 100%; background-image:url('<?=base_url($_SESSION['profile_pic']);?>')">
  </div>
</div>

<?php
    }
    else{
        ?>
<!-- <div id="sender_msg_container">
  <div id="sender_image" style="background-size: 100% 100%; background-image:url('<?//= $image;?>')"></div>
  <div id="sender_msg">
    <p class="m-0" id="receiver_ptag"><?php// echo $data[$i]['message'];?></p>
  </div>

</div> -->
<div class="sender">
<div id="receiver_image"
    style="background-size: 100% 100%; background-image:url('<?=$image;?>')">
  </div>
<div class="chet">
    <span class="l_i float-right">
    <svg viewBox="0 0 8 13" height="13" width="8" preserveAspectRatio="xMidYMid meet" class="" version="1.1" x="0px" y="0px" enable-background="new 0 0 8 13" xml:space="preserve"><path opacity="0.13" fill="#157dff" d="M1.533,3.568L8,12.193V1H2.812 C1.042,1,0.474,2.156,1.533,3.568z"></path><path fill="currentColor" d="M1.533,2.568L8,11.193V0L2.812,0C1.042,0,0.474,1.156,1.533,2.568z"></path></svg>
    </span>
    <div class="m_d se_nder">
      <?php if(!empty($data[$i]['documents'])){
            $size = filesize($data[$i]['documents']);
            $units = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
            $power = $size > 0 ? floor(log($size, 1024)) : 0;
            $file_size = number_format($size / pow(1024, $power), 2, '.', ',') . ' ' . $units[$power];

            $ext  = (new SplFileInfo($data[$i]['documents']))->getExtension();
            $file_name = explode('/',$data[$i]['documents']);
            
            ?>
      <div class="send">
        <div class="left_s">
          <i class="fas fa-file"></i>
          <span>
            <span><?=$file_name[2];?></span>
            <span><b class="mr-2"><?=$ext?></b>. <?= $file_size?></span>
          </span>
        </div>
        <div class="d_load">
          <a href="<?=base_url('Chat/download_file/'.base64_encode($data[$i]['documents']))?>">
            <i class="fas fa-download"></i>
          </a>
        </div>
      </div>
      <div class="time text-right">
        <span><?=  date('d-M-Y H:i A',strtotime($data[$i]['time']));?></span>
        <p class="m-0" id="receiver_ptag"><?php echo $data[$i]['message'];?></p>
      </div>

      <!-- <div id="receiver_msg">
      <p class="m-0" id="receiver_ptag"><?php //echo $data[$i]['message'];?></p>
    </div> -->

      <?php }else{?>
      <div id="receiver_msg">
        <p class="m-0" id="receiver_ptag"><?php echo $data[$i]['message'];?></p>
        <small class="ti_me"><?=  date('d-M-Y H:i A',strtotime($data[$i]['time']));?></small> 
      </div>

      <?php } ?>
    </div>

  </div>
 
</div>
</div>

<?php
        }
    }
?>