<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">
      <div class="page-header">
        <div class="row">
          <div class="col-sm-12">
            <h3 class="page-title"><?=$page_title?></h3>
            <ul class="breadcrumb">
              <li><a href="<?=base_url('dashboard')?>">Dashboard/</a></li>
              <li class="breadcrumb-item"><a href="javascript:(0);"><?=$page_title?></a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12 col-lg-12">
          <div class="card">
            <div class="card-body">

              <span> <a href="javascript:void(0)" onclick="set_order_type('Course')"
                  class="btn <?=$this->session->userdata('order_type') == 'Course' ? 'btn-success' : 'btn-primary'?>">Course
                  Order</a>

                <a href="javascript:void(0)" onclick="set_order_type('Test Series')"
                  class="btn <?=$this->session->userdata('order_type') == 'Test Series' ? 'btn-success' : 'btn-primary'?>">Test
                  Series Order</a></span>

            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <div class="card">
              <div class="card-body">
                <div class="table-responsive">
                  <table class=" table table-hover table-center mb-0" id="orderDataTable">
                    <thead>
                      <tr>
                        <th>S.no.</th>
                        <th>Name</th>
                        <th>Order ID</th>
                        <th>Price</th>
                        <th>GST</th>
                        <th>Discount</th>
                        <th>Total Price</th>
                        <th>Status</th>
                        <th>Order Date</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script>
  $(document).ready(function() {
    //alert('dfgfgf');
    // $('#example').DataTable();
    // } );
    var dataTable = $('#orderDataTable').DataTable({
      "processing": true,
      "serverSide": true,
      buttons: [{
        extend: 'excelHtml5',
        text: 'Download Excel'
      }],
      "order": [],
      "ajax": {
        url: "<?=base_url('Cart/ajaxOrders')?>",
        type: "POST"
      },
      "columnDefs": [{
        "targets": [0],
        "orderable": false,
      }, ],
    });
  });

  function approve_payment(id) {

    Swal.fire({
      title: 'Are you approve Payment?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, approve it!'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          url: '<?=base_url('Cart/approve_payment')?>',
          type: 'POST',
          data: {
            id
          },
          dataType: 'json',
          success: function(data) {
            if (data.status == 200) {
              toastNotif({
                text: data.message,
                color: '#5bc83f',
                timeout: 5000,
                icon: 'valid'
              });
              setTimeout(function() {

                location.href = "<?=base_url('orders')?>"

              }, 1000)


            } else if (data.status == 302) {
              toastNotif({
                text: data.message,
                color: '#da4848',
                timeout: 5000,
                icon: 'error'
              });

            }
          }
        })
      }
    })
  }

  function set_order_type(value) {
  $.ajax({
    url: '<?=base_url('Ajax_controller/set_order_type')?>',
    type: 'POST',
    data: {
      value,
    },
    success: function(data) {
      location.reload();
    },
  });
}
  </script>