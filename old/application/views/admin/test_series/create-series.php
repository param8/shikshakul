<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">
      <div class="page-header">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"><?=$page_title?></h4>
              </div>
              <div class="card-body pt-0">
                <form id="createSeriesform" method="post" action="<?=base_url('Test_series/store_series');?>"  enctype="multipart/form-data">
                  <div class="settings-form row">

                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Test Series<span class="star-red">*</span></label>
                        <input type="text" class="form-control" name="test_series_title" id="test_series_title" value="<?=$testSeries->title?>" readonly>
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Series Title<span class="star-red">*</span></label>
                        <input type="text" class="form-control" name="title" id="title" placeholder="Series Title">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Series Mode<span class="star-red">*</span></label>
                        <div>
                        <?php 
                         $modes = array('series_mode_sub'=>'Subjective','series_mode_obj'=>'Objective');
                          foreach($modes as $key=>$mode){?>
                            <input type="checkbox" class="" name="<?=$key?>" id="<?=$key?>"
                              value="<?=$mode?>" onclick="get_subjective_objective()"> <?=$mode?>
                          <?php } ?>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12" id="subjective_id" style="display:none">
                    <div class="form-group">
                        <label>Subjective</label>
                        <input type="file" class="form-control" name="subjective" id="subjective" placeholder="">
                      </div>
                    </div>
                    <hr>
                    <div class="col-md-12" id="objective_id" style="display:none">
                      <p class="settings-label">Add Objective Questions </p>
                      <div class="quiz-form">
                        <div class="links-info">
                          <div class="row form-row links-cont">
                            <div class="col-12 col-md-12">
                              <p class="settings-label">Questions </p>
                              <div class="form-group form-placeholder d-flex">
                                <textarea class="form-control" name="question[0]" placeholder="question"></textarea>
                              </div>
                            </div>
                            <div class="col-6 col-md-6">
                              <p class="settings-label">Option 1 </p>
                              <div class="form-group form-placeholder d-flex">
                                <input type="text" class="form-control" name="option[question10]"
                                  placeholder="Option 1">
                              </div>
                            </div>
                            <div class="col-6 col-md-6">
                              <p class="settings-label">Option 2 </p>
                              <div class="form-group form-placeholder d-flex">
                                <input type="text" class="form-control" name="option[question20]"
                                  placeholder="Option 2">
                              </div>
                            </div>
                            <div class="col-6 col-md-6">
                              <p class="settings-label">Option 3 </p>
                              <div class="form-group form-placeholder d-flex">
                                <input type="text" class="form-control" name="option[question30]"
                                  placeholder="Option 3">
                              </div>
                            </div>
                            <div class="col-6 col-md-6">
                              <p class="settings-label">Option 4 </p>
                              <div class="form-group form-placeholder d-flex">
                                <input type="text" class="form-control" name="option[question40]"
                                  placeholder="Option 4">
                              </div>
                            </div>
                            <div class="col-6 col-md-6">
                              <p class="settings-label">Answer </p>
                              <div class="form-group form-placeholder d-flex">
                                <input type="text" class="form-control" name="answer[question0]" placeholder="Answer">
                              </div>
                            </div>
                            <div class="col-12 col-md-1">
                              <p class="settings-label">Remove </p>
                              <a href="#" class="btn trash btn-danger ml-5">
                                <i class="feather-trash-2"></i>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <a href="javascript:void(0);" class="btn add-links">
                          <i class="fa fa-plus me-1"></i> Add More
                        </a>
                      </div>
                    </div>

                    <div class="form-group mb-0">
                      <div class="settings-btns">
                        <button type="submit" class="btn btn-orange">Submit</button>
                        <!-- <button type="reset" class="btn btn-grey">Cancel</button> -->
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$("form#createSeriesform").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  formData.append("testSeriesID", '<?=$testSeries->id?>');
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        //$('.modal').modal('hide');
        toastNotif({
          text: data.message,
          color: '#5bc83f',
          timeout: 5000,
          icon: 'valid'
        });
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {

          location.href = "<?=base_url('series/'.base64_encode($testSeries->id))?>";

        }, 1000)
      } else if (data.status == 403) {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});

function get_subjective_objective(){
 var subjective = $('#series_mode_sub').is(":checked");
 var objective = $('#series_mode_obj').is(":checked");
if(subjective == true && objective == true){
  $('#objective_id').show();
  $('#subjective_id').show();
}

if(subjective == false && objective == false){
  $('#objective_id').hide();
  $('#subjective_id').hide();
}

if(subjective == true && objective == false){
  $('#objective_id').hide();
  $('#subjective_id').show();
}

if(subjective == false && objective == true){
  $('#objective_id').show();
  $('#subjective_id').hide();
}

}

</script>