<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">
      <div class="page-header">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"><?=$page_title?></h4>
              </div>
              <div class="card-body pt-0">
                <form id="createScheduleform" method="post" action="<?=base_url('Schedule/store');?>" enctype="multipart/form-data">
                  <div class="settings-form row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Course <span class="star-red">*</span></label>
                        <select class="form-control" name="courseID" id="courseID" onchange="getSubject(this.value)">
                          <option value="">Choose Course</option>
                          <?php
                            foreach($courses as $course){
                            ?> 
                          <option value="<?=$course->id; ?>"><?=$course ->name; ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Subject <span class="star-red">*</span></label>
                         <select class="form-control" name="subjectID" id="subjectID">
                          <option value="">Choose Subject</option>
                          
                        </select>
                      </div>
                    </div>
                    <?php if($this->session->userdata('user_type')=='Admin'){?>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Faculty<span class="star-red">*</span></label>
                        <select class="form-control js-example-basic-multiple" name="faculty"  id="faculty">
                          <option value="">Choose faculty</option>
                          <?php
                            foreach($faculties as $facultiy){
                            ?> 
                          <option value="<?=$facultiy->id?>"><?=$facultiy->name?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                    <?php } ?>
                    <!-- <div class="col-md-6">
                      <div class="form-group">
                        <label>Day<span class="star-red">*</span></label>
                        <select class="form-control js-example-basic-multiple" name="day[]"  id="day" multiple="multiple">
                          <option value="">Choose Day</option>
                          <?php
                          // $days = array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday');
                          //   foreach($days as $day){
                            ?> 
                          <option value="<?//=$day?>"><?//=$day?></option>
                          <?php //} ?>
                        </select>
                      </div>
                    </div> -->
                  
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Start Time <span class="star-red">*</span></label>
                        <input type="text" onfocus="(this.type='time')" class="form-control" name="start_time" id="start_time" placeholder="Start Time">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group" id="endDate_div">
                        <label>End Time<span class="star-red">*</span></label>
                        <input type="text" onfocus="(this.type='time')" class="form-control" name="end_time" id="end_time" placeholder="End Time">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Topic <span class="star-red">*</span></label>
                        <input type="text" class="form-control"name="topics" id="topics" placeholder="Topic" >
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Class Date <span class="star-red">*</span></label>
                        <input type="date" class="form-control"name="class_date" id="class_date" placeholder="Class Date" >
                      </div>
                    </div>

                    <div class="form-group mb-0">
                      <div class="settings-btns">
                        <button type="submit" class="btn btn-orange">Submit</button>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $("form#createScheduleform").submit(function(e) {
  			$(':input[type="submit"]').prop('disabled', true);
  			e.preventDefault();    
  			var formData = new FormData(this);
  			$.ajax({
  			url: $(this).attr('action'),
  			type: 'POST',
  			data: formData,
  			cache: false,
  			contentType: false,
  			processData: false,
  			dataType: 'json',
  			success: function (data) { 
  				if(data.status==200) {
  				//$('.modal').modal('hide');
          toastNotif({
  				text: data.message,
  				color: '#5bc83f',
  				timeout: 5000,
  				icon: 'valid'
  			});
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function(){
  
          location.href="<?=base_url('schedule')?>"; 	
          
        }, 1000) 		
  				}else if(data.status==403) {
            toastNotif({
                  text: data.message,
                  color: '#da4848',
                  timeout: 5000,
                  icon: 'error'
                });
  
            $(':input[type="submit"]').prop('disabled', false);
  				}else{
            toastNotif({
                text: data.message,
                color: '#da4848',
                timeout: 5000,
                icon: 'error'
              });
            $(':input[type="submit"]').prop('disabled', false);
  				}
  			},
  			error: function(){} 
  			});
  		});

      function getSubject(courseID){
        $.ajax({
        url: '<?=base_url('Ajax_controller/get_subject')?>',
        type: 'POST',
        data: {
        courseID
    },
    success: function(data) {
      $('#subjectID').html(data);
    }
  });
      }
</script>