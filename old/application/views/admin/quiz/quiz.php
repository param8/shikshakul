<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">
      <div class="page-header">
        <div class="row">
          <div class="col-sm-6">
            <h3 class="page-title"><?=$page_title?></h3>
            <ul class="breadcrumb">
              <li><a href="<?=base_url('dashboard')?>">Dashboard/</a></li>
              <li class="breadcrumb-item"><a href="javascript:(0);"><?=$page_title?></a></li>
            </ul>
          </div>
          <div class="col-sm-3">
            <div class="float-right">
              <button type="button" class="btn btn-warning btn-sm" onclick="open_model()" style="float: right">Upload
                Quiz</button>
              <!-- <a  class="btn btn-warning btn-sm" href="javascript:void(0)" style="float: right" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Upload <?=$page_title?></a> -->
            </div>
          </div>
          <div class="col-sm-3">
            <div class="float-left">
              <a type="button" class="btn btn-primary btn-sm" href="<?=base_url('create-quiz')?>"
                style="float: right">Create <?=$page_title?></a>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <table class=" table table-hover table-center mb-0" id="quizDataTable">
                  <thead>
                    <tr>
                      <th>S.no.</th>
                      <th>Quiz</th>
                      <th>Course</th>
                      <th>Subject</th>
                      <th>Quiz Type</th>
                      <th>Created Date</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New message</h5>
        <button type="button" class="close" data-dismiss="modal" onclick="closeexampleModel()" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="quizUploadForm" action="<?=base_url('Quiz/bulk_upload_quiz')?>" method="POST"
          enctype="multipart/form-data">
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Title:</label>
            <input type="text" class="form-control" name="title" id="title">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Course:</label>
            <select class="form-control" name="courseID" id="courseID" onchange="get_subject(this.value)">
              <option value="">Select Course</option>
              <?php foreach($courses as $course){?>
              <option value="<?=$course->id?>"><?=$course->name?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Subject:</label>
            <select class="form-control" name="subjectID" id="subjectID">
              <option value="">Select Subject</option>
              
            </select>
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Quiz Type:</label>
            <select class="form-control" name="quizType" id="quizType">
              <option value="">Select Quiz Type</option>
              <?php foreach($quizTypes as $quiz_type){?>
              <option value="<?=$quiz_type->id?>"><?=$quiz_type->title?></option>
              <?php } ?>
             
            </select>
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Upload Course Quiz:</label>
            <input type="file" class="form-control" name="quiz_file" id="quiz_file">
          </div>

      </div>
      <div class="modal-footer">
        <a href="<?=base_url('Quiz/download_sample')?>" class="btn btn-warning">Download Sample</a>
        <button type="submit" class="btn btn-primary">Upload</button>
      </div>
      </form>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
  //alert('dfgfgf');
  // $('#example').DataTable();
  // } );
  var dataTable = $('#quizDataTable').DataTable({
    "processing": true,
    "serverSide": true,
    buttons: [{
      extend: 'excelHtml5',
      text: 'Download Excel'
    }],
    "order": [],
    "ajax": {
      url: "<?=base_url('Quiz/ajaxQuiz')?>",
      type: "POST"
    },
    "columnDefs": [{
      "targets": [0],
      "orderable": false,
    }, ],
  });
});

$("form#quizUploadForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        //$('.modal').modal('hide');
        toastNotif({
          text: data.message,
          color: '#5bc83f',
          timeout: 5000,
          icon: 'valid'
        });
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {

          location.href = "<?=base_url('quiz')?>";

        }, 1000)
      } else if (data.status == 403) {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});

function delete_quiz(id) {

  Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
    if (result.value) {
      $.ajax({
        url: '<?=base_url('Quiz/delete_quiz')?>',
        type: 'POST',
        data: {
          id
        },
        dataType: 'json',
        success: function(data) {
          if (data.status == 200) {
            toastNotif({
              text: data.message,
              color: '#5bc83f',
              timeout: 5000,
              icon: 'valid'
            });
            setTimeout(function() {

              location.href = "<?=base_url('quiz')?>"

            }, 1000)


          } else if (data.status == 302) {
            toastNotif({
              text: data.message,
              color: '#da4848',
              timeout: 5000,
              icon: 'error'
            });

          }
        }
      })
    }
  })
}

function get_subject(courseID){
        $.ajax({
        url: '<?=base_url('Ajax_controller/get_subject')?>',
        type: 'POST',
        data: {courseID},
        success: function (data) {
          $('#subjectID').html(data);
        }
        });
      }

function open_model() {
  $('#exampleModal').modal('show');
}

function closeexampleModel() {
  $('#exampleModal').modal('hide');
}
</script>