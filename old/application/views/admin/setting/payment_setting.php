<div class="row">
  
   <div class="col-md-6">
      <div class="card">
      <form id="product_form" method="post" action="<?=base_url('store-payment-form')?>">
         <div class="card-header d-flex justify-content-between align-items-center">
            <h5 class="card-title">Razorpay</h5>
            <div class="status-toggle d-flex justify-content-between align-items-center">
               <input type="checkbox" id="status" name="status" class="check" <?=($payments->status==1)?'checked':''?>>
               <label for="status" class="checktoggle">checkbox</label>
            </div>
         </div>
         <div class="card-body pt-0">
            
               <div class="settings-form">
                  <div class="form-group form-placeholder">
                     <label>API Key <span class="star-red">*</span></label>
                     <input type="text" class="form-control" name="razor_key" value="<?=$payments->razopay_key?>" placeholder="pk_test_AealxxOygZz84AruCGadWvUV00mJQZdLvr">
                  </div>
                  <div class="form-group form-placeholder">
                     <label>Payment Logo <span class="star-red">*</span></label>
                     <input type="file" accept="image/*" class="form-control" name="payment_logo">
                  </div>
                  <div class="upload-images">
                      <img src="<?=$payments->payment_logo?>" alt="Image">
                      <a href="javascript:void(0);" class="btn-icon logo-hide-btn">
                      <i class="feather-x-circle"></i>
                      </a>
                    </div>
                  <div class="form-group mt-2">
                     <div class="settings-btns">
                        <button type="submit" class="btn btn-orange">Save</button>
                        <button type="reset" class="btn btn-grey">Cancel</button>
                     </div>
                  </div>
               </div>
            
         </div>
         </form>
      </div>
   </div>
</div>
</div>
</div>

<script>
$("form#product_form").submit(function(e) {
			$(':input[type="submit"]').prop('disabled', true);
			e.preventDefault();    
			var formData = new FormData(this);
			$.ajax({
			url: $(this).attr('action'),
			type: 'POST',
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			dataType: 'json',
			success: function (data) { 
				if(data.status==200) {
				//$('.modal').modal('hide');
        toastNotif({
				text: data.message,
				color: '#5bc83f',
				timeout: 5000,
				icon: 'valid'
			});
      $(':input[type="submit"]').prop('disabled', false);
      setTimeout(function(){

        location.href="<?=base_url('payment-setting')?>"; 	
        
      }, 1000) 		
				}else if(data.status==403) {
          toastNotif({
                text: data.message,
                color: '#da4848',
                timeout: 5000,
                icon: 'error'
              });

          $(':input[type="submit"]').prop('disabled', false);
				}else{
          toastNotif({
              text: data.message,
              color: '#da4848',
              timeout: 5000,
              icon: 'error'
            });
          $(':input[type="submit"]').prop('disabled', false);
				}
			},
			error: function(){} 
			});
		});


</script>