<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <h5 class="card-title">Banner Settings</h5>
      </div>
      <div class="card-body pt-0">
        <form  id="product_form" method="post" action="<?=base_url('store-banner-form');?>">
          <div class="settings-form row">
            <div class="col-md-4">
              <div class="form-group form-placeholder">
                <label>Banner Title<span class="star-red">*</span></label>
                <input type="text" class="form-control" name="title" placeholder="Optional">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group form-placeholder">
                <label>Link url</label>
                <input type="text" class="form-control" name="url" placeholder="Optional">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group form-placeholder">
                <label>Banner Image<span class="star-red">*</span></label>
                <input type="file" accept="image/*" class="form-control" name="image">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group form-placeholder">
                <label>Image Text</label>
                <input type="text" class="form-control" name="img_text" placeholder="Optional">
              </div>
            </div>
            <div class="form-group mb-0">
              <div class="settings-btns">
                <button type="submit" class="btn btn-orange">Submit</button>
                <button type="reset" class="btn btn-grey">Cancel</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <h5 class="card-title">Banner Settings</h5>
      </div>
      <div class="card-body pt-0">
        <div class="table-responsive">
          <table class=" table table-stripped">
            <thead>
              <tr>
                <th>S.no.</th>
                <th>Banner Title</th>
                <th>Image</th>
                <th>URL</th>
                <th>Image Text</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php $sno=1; if(!empty($banners)){foreach($banners as $banner){?>
              <tr>
                <td><?=$sno?></td>
                <td>
                  <p><?=$banner->banner_title?></p>
                </td>
                <td><img src="<?=$banner->banner_img?>" alt="<?=$banner->banner_title?>" height="100"></td>
                <td>
                  <p><?=$banner->link_url?></p>
                </td>
                <td>
                  <p><?=$banner->img_text?></p>
                </td>
                <td>
                  <div class="form-check form-switch" style="display: inline-flex;">
                    <input class="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckDefault" name="banner_status" <?=$banner->status == 1 ? 'checked' : '' ?>  onclick="updateStatus(<?=$banner->id?>,<?=$banner->status?>)">
                  </div>
                  <span><a href="javascript:void(0)" class="btn trash" onclick="delete_banner(<?=$banner->id?>)"><i class="feather-trash-2"></i></a></span>
                </td>
              </tr>
              <?php $sno++;}}else{?>
              <tr>
                <td>No Data found......</td>
                
              </tr>
              <?php }?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
<script>
  $("form#product_form").submit(function(e) {
  			$(':input[type="submit"]').prop('disabled', true);
  			e.preventDefault();    
  			var formData = new FormData(this);
  			$.ajax({
  			url: $(this).attr('action'),
  			type: 'POST',
  			data: formData,
  			cache: false,
  			contentType: false,
  			processData: false,
  			dataType: 'json',
  			success: function (data) { 
  				if(data.status==200) {
  				//$('.modal').modal('hide');
          toastNotif({
  				text: data.message,
  				color: '#5bc83f',
  				timeout: 5000,
  				icon: 'valid'
  			});
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function(){
  
          location.href="<?=base_url('banner')?>"; 	
          
        }, 1000) 		
  				}else if(data.status==403) {
            toastNotif({
                  text: data.message,
                  color: '#da4848',
                  timeout: 5000,
                  icon: 'error'
                });
  
            $(':input[type="submit"]').prop('disabled', false);
  				}else{
            toastNotif({
                text: data.message,
                color: '#da4848',
                timeout: 5000,
                icon: 'error'
              });
            $(':input[type="submit"]').prop('disabled', false);
  				}
  			},
  			error: function(){} 
  			});
  		});
  
</script>
<script>
  function updateStatus(id,status){
     if(status == 1){
        var msg = "De-active";
     }else{
        var msg = "Active";
     }
     var messageText  = "You want to "+msg+" this banner?";
    var confirmText =  'Yes, Change it!';
    var message  = "This banner "+msg+" Successfully!";
   Swal.fire({
       title: 'Are you sure?',
       text: messageText,
       icon: 'warning',
       showCancelButton: true,
       confirmButtonColor: '#3085d6',
       cancelButtonColor: '#d33',
       confirmButtonText: confirmText
       }).then((result) => {
       if (result.isConfirmed) {
           $.ajax({
               url: "<?=base_url('Setting/updateStatus')?>", 
               method: 'POST',
               data: {id,status},
               dataType:'json',
               success: function(result){
                 if(result.status==200) {
                  
                     toastNotif({
                        text: result.message,
                        color: '#5bc83f',
                        timeout: 5000,
                        icon: 'valid'
               
                        });
                }
               setTimeout(function(){
               //window.location.reload();
               }, 2000);
       }
     });   
       }else{location.reload();}
       })
  }
  
  function delete_banner(id){
  var messageText  = "you want to delete this banner!";
    var confirmText =  'Yes, delete it!';
    var message  ="Banner deleted Successfully!";
   Swal.fire({
       title: 'Are you sure?',
       text: messageText,
       icon: 'warning',
       showCancelButton: true,
       confirmButtonColor: '#3085d6',
       cancelButtonColor: '#d33',
       confirmButtonText: confirmText
       }).then((result) => {
       if (result.isConfirmed) {
           $.ajax({
               url: '<?=base_url('Setting/deleteBanner')?>', 
               method: 'POST',
               data: {id},
               dataType:'json',
               success: function(result){
                if(result.status==200) {
                  toastNotif({
                     text: result.message,
                     color: '#5bc83f',
                     timeout: 5000,
                     icon: 'valid'
            
                     });
             }
               setTimeout(function(){
                  window.location.reload();
               }, 2000);
           }
         });      
         }
       })
  }
</script>