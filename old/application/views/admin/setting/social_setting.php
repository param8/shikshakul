<div class="row">
   <div class="col-lg-6 col-md-8">
      <div class="card">
         <div class="card-header">
            <h5 class="card-title">Social Link Settings</h5>
         </div>
         <div class="card-body pt-0">
            <form id="product_form" action="<?=base_url('store-socialmedia-form')?>" method="post">
               <div class="settings-form">
                  <div class="links-info">
                     <div class="row form-row links-cont">
                        <div class="col-12 col-md-11">
                           <div class="form-group form-placeholder d-flex">
                              <button class="btn social-icon">
                              <i class="feather-facebook"></i>
                              </button>
                              <input type="text" class="form-control" name="facebook" placeholder="https://www.facebook.com/" value="<?= $siteinfo->facebook_url?>">
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="links-info">
                     <div class="row form-row links-cont">
                        <div class="col-12 col-md-11">
                           <div class="form-group form-placeholder d-flex">
                              <button class="btn social-icon">
                              <i class="feather-instagram"></i>
                              </button>
                              <input type="text" class="form-control" name="instagram" placeholder="https://www.instagram.com/" value="<?= $siteinfo->insta_url?>">
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="links-info">
                     <div class="row form-row links-cont">
                        <div class="col-12 col-md-11">
                           <div class="form-group form-placeholder d-flex">
                              <button class="btn social-icon">
                              <i class="feather-twitter"></i>
                              </button>
                              <input type="text" class="form-control" name="twitter" placeholder="https://www.twitter.com/" value="<?= $siteinfo->twitter_url?>">
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="links-info">
                     <div class="row form-row links-cont">
                        <div class="col-12 col-md-11">
                           <div class="form-group form-placeholder d-flex">
                              <button class="btn social-icon">
                              <i class="feather-youtube"></i>
                              </button>
                              <input type="text" class="form-control" name="youtube" placeholder="https://www.youtube.com/" value="<?= $siteinfo->youtube_url?>">
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="links-info">
                     <div class="row form-row links-cont">
                        <div class="col-12 col-md-11">
                           <div class="form-group form-placeholder d-flex">
                              <button class="btn social-icon">
                              <i class="feather-linkedin"></i>
                              </button>
                              <input type="text" class="form-control" name="linkedin" placeholder="https://www.linkedin.com/" value="<?= $siteinfo->linkedin_url?>">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="form-group mb-0">
                  <div class="settings-btns">
                     <button type="submit" class="btn btn-orange">Submit</button>
                     <button type="submit" class="btn btn-grey">Cancel</button>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
</div>
</div>
</div>
</div>

<script>
$("form#product_form").submit(function(e) {
			$(':input[type="submit"]').prop('disabled', true);
			e.preventDefault();    
			var formData = new FormData(this);
			$.ajax({
			url: $(this).attr('action'),
			type: 'POST',
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			dataType: 'json',
			success: function (data) { 
				if(data.status==200) {
				//$('.modal').modal('hide');
        toastNotif({
				text: data.message,
				color: '#5bc83f',
				timeout: 5000,
				icon: 'valid'
			});
      $(':input[type="submit"]').prop('disabled', false);
      setTimeout(function(){

        location.href="<?=base_url('social-setting')?>"; 	
        
      }, 1000) 		
				}else if(data.status==403) {
          toastNotif({
                text: data.message,
                color: '#da4848',
                timeout: 5000,
                icon: 'error'
              });

          $(':input[type="submit"]').prop('disabled', false);
				}else{
          toastNotif({
              text: data.message,
              color: '#da4848',
              timeout: 5000,
              icon: 'error'
            });
          $(':input[type="submit"]').prop('disabled', false);
				}
			},
			error: function(){} 
			});
		});


</script>