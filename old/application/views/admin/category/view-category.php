<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">
      <div class="page-header">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"><?$page_title?></h4>
              </div>
              <div class="card-body pt-0">
                <form >
                  <div class="settings-form row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Category Name <span class="star-red">*</span></label>
                        <input type="text" name="name" id="name" class="form-control" value="<?=$category->name?>" placeholder="Enter Course Name" readonly>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Language <span class="star-red">*</span></label>
                        <!-- <input type="text" name="language" id="language" class="form-control" value="<?=$category->language?>" placeholder="Enter Language"> -->
                        <select class="form-control" name="language" id="language" disabled>
                      <option value="">Choose Language</option>
                      <?php $languages = array('English','Hindi');
                      foreach($languages as $language){
                      ?> 
                    <option value="<?=$language?>" <?=$language == $category->language ? 'selected' : ''?>><?=$language?></option>
                    <?php } ?>
                  </select>
                      </div>
                    </div>
                    
                    <div class="col-md-6">
                      <div class="form-group">
                        <p class="settings-label">Image <span class="star-red">*</span></p>
                        <div class="upload-images">
                          <img src="<?=base_url($category->image)?>" alt="Image">
                          </div>
                      </div>
                    </div>
         
                    
              
                   
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
   CKEDITOR.replace('description');
</script>
