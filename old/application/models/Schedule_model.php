<?php 

class Schedule_model extends CI_Model

{

	public function __construct()

	{
		parent::__construct();
	}

  function get_schedule($condition){
    $this->db->where($condition);
   return $this->db->get('schedule')->row();
  }

  function get_schedules($condition){
    $this->db->select('schedule.*,courses.name as coursesName,subject.name as subjectName,users.name as facultyName');
    $this->db->from('schedule');
    $this->db->join('courses', 'courses.id = schedule.courseID','left');
    $this->db->join('subject', 'subject.id = schedule.subjectID','left');
    $this->db->join('users', 'users.id = schedule.facultyID','left');
    $this->db->where($condition);
   return $this->db->get()->result();
  }

    function make_query($condition)
  {
    $this->db->select('schedule.*,courses.name as coursesName,subject.name as subjectName,users.name as facultyName');
    $this->db->from('schedule');
    $this->db->join('courses', 'courses.id = schedule.courseID','left');
    $this->db->join('subject', 'subject.id = schedule.subjectID','left');
    $this->db->join('users', 'users.id = schedule.facultyID','left');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('courses.name', $_POST["search"]["value"]);
    $this->db->or_like('subject.name', $_POST["search"]["value"]);
   }
   $this->db->order_by('schedule.id','desc');
     
  }
    function make_datatables($condition){
	  $this->make_query($condition);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array();
  }

  function get_filtered_data($condition){
	  $this->make_query($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
  }
  function get_all_data($condition)
  {
    $this->db->select('schedule.*,courses.name as coursesName,subject.name as subjectName,users.name as facultyName');
    $this->db->from('schedule');
    $this->db->join('courses', 'courses.id = schedule.courseID','left');
    $this->db->join('subject', 'subject.id = schedule.subjectID','left');
    $this->db->join('users', 'users.id = schedule.facultyID','left');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('courses.name', $_POST["search"]["value"]);
    $this->db->or_like('subject.name', $_POST["search"]["value"]);
   }
   $this->db->order_by('schedule.id','desc');
	   return $this->db->count_all_results();
  }

  function store($data){
  return $this->db->insert('schedule',$data);
  }
  
  function update($data,$id){
    $this->db->where('id',$id);
    return $this->db->update('schedule',$data);
    }

    function delete($id){
      $this->db->where('id',$id);
      return $this->db->delete('schedule');
      }

    function get_schedules_by_courses($condition){
      $userID = $this->session->userdata('id');
      $this->db->select('schedule.*,courses.name as coursesName,subject.name as subjectName,users.name as facultyName');
      $this->db->from('schedule');
      $this->db->join('items', 'items.courseID = schedule.courseID','left');
      $this->db->join('courses', 'courses.id = items.courseID','left');
      $this->db->join('users', 'users.id = schedule.facultyID','left');
      $this->db->join('orders', 'orders.id =items.orderID', 'left');
      $this->db->join('subject', 'subject.id = schedule.subjectID','left');
      $this->db->where($condition);
      return $this->db->get()->result();
       //echo $this->db->last_query(); die;
       }

}