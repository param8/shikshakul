<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/userguide3/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
// Web Detail
$route['register'] = 'Authantication/register';
$route['verification'] = 'Authantication/verification';
$route['login'] = 'Authantication/user_login';
$route['about-us'] = 'Home/about';
$route['privacy'] = 'Home/privacy_policy';
$route['terms-uses'] = 'Home/terms_condition';
$route['refund'] = 'Home/refund_policy';
$route['course'] = 'Course/courses_web';
$route['course-detail/(:any)'] = 'Course/course_deatil/$1';
$route['faculty'] = 'Faculty/faculties';
$route['profile'] = 'User/profile';
$route['faculty-detail/(:any)'] = 'Faculty/faculty_deatil/$1';
$route['checkout'] = 'Cart/checkout';
$route['current-affair/(:any)'] = 'Home/current_affair/$1';
$route['current-affair-list'] = 'Home/current_affair_list';
$route['interview'] = 'home/interview';
$route['toppers-talk'] = 'home/toppers_talk';
$route['booking-success/(:any)/(:any)'] = 'home/booking_success/$1/$2';
$route['contact-us'] = 'home/contact_us';
$route['make-payment'] = 'Cart/send';
$route['quizs'] = 'Quiz/quiz';
$route['quiz-list/(:any)'] = 'Quiz/quiz_list/$1';
$route['reset-password'] = 'User/reset_password';
$route['forgot-password'] = 'User/forget_password';
$route['change-password'] = 'User/change_password';
$route['vocabularys'] = 'Home/vocabulary_list';
$route['view-vocabulary/(:any)'] = 'Home/view_vocabulary/$1';
$route['upcoming-exams'] = 'Home/upcoming_exam_list';
$route['view-upcoming-exam/(:any)'] = 'Home/view_upcoming_exam/$1';

$route['test-series'] = 'Test_series/test_series';
$route['series-detail/(:any)'] = 'Test_series/series_detail/$1';
$route['objective/(:any)'] = 'Test_series/objective/$1';
// Panel
$route['faculty-dashboard'] = 'Faculty/dashboard';
$route['student-dashboard'] = 'Student/dashboard';
$route['course-list'] = 'Course/course_panel';
$route['course-detail-panel/(:any)'] = 'Course/course_deatil_panel/$1';
$route['bookings'] = 'Cart/bookings';
$route['start-quiz/(:any)'] = 'Quiz/start_quiz/$1';
$route['result'] = 'Quiz/quiz_result';
$route['chat'] = 'Chat';
// Admin Details
$route['signin'] = 'Authantication';
$route['dashboard'] = 'dashboard';
$route['setting'] = 'setting';
$route['staffs'] = 'Staff';
$route['view-staff/(:any)'] = 'Staff/view/$1';
$route['edit-staff/(:any)'] = 'Staff/edit/$1';
$route['students'] = 'Student';
$route['view-student/(:any)'] = 'Student/view/$1';
$route['edit-student/(:any)'] = 'Student/edit/$1';
$route['faculties'] = 'Faculty';
$route['view-faculty/(:any)'] = 'Faculty/view/$1';
$route['edit-faculty/(:any)'] = 'Faculty/edit/$1';
$route['category'] = 'Category';
$route['create-category'] = 'Category/create_category';
$route['edit-category/(:any)'] = 'Category/edit_category/$1';
$route['view-category/(:any)'] = 'Category/view_category/$1';
$route['courses'] = 'Course';
$route['create-course'] = 'course/create_course';
$route['edit-course/(:any)']= 'course/edit_course/$1';
$route['view-course/(:any)'] = 'course/view_course/$1';
$route['subjects'] = 'Subject';
$route['create-subject'] = 'Subject/create_subject';
$route['edit-subject/(:any)']= 'Subject/edit_subject/$1';
$route['view-subject/(:any)'] = 'Subject/view_subject/$1';
$route['topics'] = 'Topics';
$route['create-topic'] = 'Topics/create_topic';
$route['edit-topic/(:any)']= 'Topics/edit_topic';
$route['view-topic/(:any)'] = 'Topics/view_topic';
$route['create'] = 'Authantication/create_user';
$route['general-setting'] = 'Setting';
$route['Store-general-form'] = 'Setting/store_siteInfo';
$route['banner'] = 'Setting/banner_setting';
$route['store-banner-form'] = 'Setting/store_banner';
$route['about'] = 'Setting/about_setting';
$route['store-about-form'] = 'Setting/store_about';
$route['terms-condition'] = 'Setting/terms_setting'; 
$route['store-terms-form'] = 'Setting/store_terms';
$route['privacy-policy'] = 'Setting/privacy_setting';
$route['store-privacy-form'] = 'Setting/store_privacy';
$route['refund-policy'] = 'Setting/refund_setting';
$route['store-refund-form'] = 'Setting/store_refund';
$route['payment-setting'] = 'Setting/payment_setting';
$route['store-payment-form'] = 'Setting/store_payment';
$route['email-setting'] = 'Setting/email_setting';
$route['store-email-form'] = 'Setting/store_email_form';
$route['social-setting'] = 'Setting/social_setting';
$route['store-socialmedia-form'] = 'Setting/store_socialmedia';
$route['seo-setting'] = 'Setting/seo_setting';
$route['store-seo-form'] = 'Setting/store_seo_form';
$route['current-affairs'] = 'Setting/current_affairs';
$route['create-current-affairs'] = 'Setting/create_current_affairs';
$route['edit-current-affairs/(:any)'] = 'Setting/edit_current_affairs/$1';
$route['whats-news'] = 'Setting/whats_news';
$route['create-whats-news'] = 'Setting/create_whats_news';
$route['edit-whats-news/(:any)'] = 'Setting/edit_whats_news/$1';
$route['videos'] = 'Setting/videos';
$route['create-videos'] = 'Setting/create_videos';
$route['edit-videos/(:any)'] = 'Setting/edit_videos/$1';
$route['discount'] = 'Setting/discount';
$route['create-discount'] = 'Setting/create_discount';
$route['edit-discount/(:any)'] = 'Setting/edit_discount/$1';
$route['orders'] = 'Cart/orders';

$route['quiz-type'] = 'Quiz/quiz_type';
$route['create-quiz_type'] = 'Quiz/create_quiz_type';
$route['edit-quiz_type/(:any)'] = 'Quiz/edit_quiz_type/$1';
$route['view-quiz_type/(:any)'] = 'Quiz/view_quiz_type/$1';

$route['quiz'] = 'Quiz';
$route['create-quiz'] = 'Quiz/create_quiz';
$route['edit-quiz/(:any)'] = 'Quiz/edit_quiz/$1';
$route['view-quiz/(:any)'] = 'Quiz/view_quiz/$1';

$route['test-seriess'] = 'Test_series/index';
$route['create-test-series'] = 'Test_series/create_test_series';
$route['edit-test-series/(:any)'] = 'Test_series/edit_test_series/$1';
$route['view-test-series/(:any)'] = 'Test_series/view_test_series/$1';

$route['series/(:any)'] = 'Test_series/series/$1';
$route['create-series/(:any)'] = 'Test_series/create_series/$1';
$route['edit-series/(:any)'] = 'Test_series/edit_series/$1';
$route['view-series/(:any)'] = 'Test_series/view_series/$1';

$route['question-list'] = 'Quiz/questions';
$route['answer-list'] = 'Quiz/answers';

$route['toppers'] = 'Common/toppers';
$route['create-toppers'] = 'Common/create_toppers';
$route['edit-toppers/(:any)'] = 'Common/edit_toppers/$1';

$route['mock-interview'] = 'Common/mock_interview';
$route['create-mock-interview'] = 'Common/create_mock_interview';
$route['edit-mock-interview/(:any)'] = 'Common/edit_mock_interview/$1';

$route['building-futures'] = 'Common/building_futures';
$route['create-building-futures'] = 'Common/create_building_futures';
$route['edit-building-futures/(:any)'] = 'Common/edit_building_futures/$1';

$route['student-remark'] = 'Common/student_remark';
$route['create-student-remark'] = 'Common/create_student_remark';
$route['edit-student-remark/(:any)'] = 'Common/edit_student_remark/$1';

$route['vocabulary'] = 'Common/vocabulary';
$route['create-vocabulary'] = 'Common/create_vocabulary';
$route['edit-vocabulary/(:any)'] = 'Common/edit_vocabulary/$1';

$route['upcoming-exam'] = 'Common/upcoming_exam';
$route['create-upcoming-exam'] = 'Common/create_upcoming_exam';
$route['edit-upcoming-exam/(:any)'] = 'Common/edit_upcoming_exam/$1';

$route['schedule'] = 'Schedule';
$route['create-schedule'] = 'Schedule/create_schedule';
$route['edit-schedule/(:any)'] = 'Schedule/edit_schedule/$1';





