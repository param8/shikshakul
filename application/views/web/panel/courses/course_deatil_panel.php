<?php 
$earlier = new DateTime(date("d-m-Y"));
$later = new DateTime(date('d-m-Y',strtotime($this->session->userdata('created_at'))));
$registrationDate = $later->diff($earlier)->format("%a");
$orderStatus = array();
$this->db->select('items.*,orders.orderID as order_id,orders.discount as discount,orders.gst as gst,orders.price as orderPrice,orders.total_price as total_price,users.name as userName,orders.payment_status as paymentStatus,orders.coupon_code as order_coupon_code,orders.status as orderStatus');
$this->db->from('orders');
$this->db->join('items', 'orders.id=items.orderID','left');
$this->db->join('users', 'users.id=orders.userID','left');
$this->db->where(array('items.courseID'=>$course->id,'orders.userID'=>$this->session->userdata('id'),'orders.status'=>1));
$this->db->group_by('items.orderID');
$this->db->order_by('orders.id', 'desc');
$orders = $this->db->get()->result();
foreach ($orders as $order){
  $orderStatus[] = $order->orderStatus;
}
 $orderStatus =  !empty(array_filter($orderStatus)) ? array_filter($orderStatus) : array(0);

  ?>
<div class="col-md-8 col-lg-9 col-xl-10">
  <div class="breadcrumb-bar coursedetails"
    style="background:url(<?=base_url('public/web/assets/img/bgsubject.jpg')?>)">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-xl-9 col-lg-9 col-md-9 col-12">
          <div class="mentor-widget">
            <div class="user-info-left">
              <div class="user-info-cont">
                <h2 class="usr-name">
                  <a href="javascript:void(0)"><?=$course->name?></a>
                </h2>
                <p class="mentor-type"><?=$course->categoryName?></p>
                <div class="rating">
                  <i class="fas fa-star filled"></i>
                  <i class="fas fa-star filled"></i>
                  <i class="fas fa-star filled"></i>
                  <i class="fas fa-star filled"></i>
                  <i class="fas fa-star"></i>
                  <span class="d-inline-block average-rating">(17)</span>
                </div>
                <div class="mentor-details">
                  <!-- <p class="user-location">
                  <i class="fas fa-map-marker-alt"></i> Florida, USA
                </p> -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-8 col-md-12  ">
          <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item" role="presentation">
              <button class="nav-link active btn_ac" id="home-tab" data-bs-toggle="tab" data-bs-target="#home-tab-pane"
                type="button" role="tab" aria-controls="home-tab-pane" aria-selected="true">Information</button>
            </li>
            <li class="nav-item" role="presentation">
              <button class="nav-link bg_theam btn_ac" id="profile-tab" data-bs-toggle="tab"
                data-bs-target="#profile-tab-pane" type="button" role="tab" aria-controls="profile-tab-pane"
                aria-selected="false">Contants (<?=count($topics) ?>)</button>
            </li>
            <li class="nav-item" role="presentation">
              <button class="nav-link bg_theam btn_ac" id="contact-tab" data-bs-toggle="tab"
                data-bs-target="#contact-tab-pane" type="button" role="tab" aria-controls="contact-tab-pane"
                aria-selected="false">Reviews</button>
            </li>
            <!-- <li class="nav-item" role="presentation">
            <button class="nav-link" id="disabled-tab" data-bs-toggle="tab" data-bs-target="#disabled-tab-pane" type="button" role="tab" aria-controls="disabled-tab-pane" aria-selected="false" disabled>Disabled</button>
            </li> -->
          </ul>
          <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="home-tab-pane" role="tabpanel" aria-labelledby="home-tab"
              tabindex="0">
              <div class="blog-view">
                <div class="blog-single-post">
                  <!-- <div class="blog-image">
                  <a href="javascript:void(0);"><img alt src="<?//=base_url('public/web/assets/img/blog/blog-01.jpg')?>" class="img-fluid"></a>
                  </div> -->
                  <h3 class="blog-title"><?=$course->name?></h3>
                  <div class="blog-info clearfix">
                    <div class="post-left">
                      <ul>
                        <li>
                          <div class="post-author">
                            <a href="javascript:void()"><img alt src="<?=base_url($course->courseImage)?>"
                                alt="Post Author"> <span><?=$course->courseName?></span></a>
                          </div>
                        </li>
                        <li><i class="far fa-calendar"></i><?=date('d-F-Y',strtotime($course->created_at))?></li>
                        <!-- <li><i class="far fa-comments"></i>12 Comments</li>
                        <li><i class="fa fa-tags"></i>HTML</li> -->
                      </ul>
                    </div>
                  </div>
                  <div class="blog-content">
                    <p>
                      <?=$course->description?>
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="profile-tab-pane" role="tabpanel" aria-labelledby="profile-tab" tabindex="0">
              <!-- start -->
              <?php 
                foreach($topics as $topic){?>
              <div class="accordion" id="accordionExample<?=$topic['id']?>">

                <div class="accordion-item mb-3">
                  <h2 class="accordion-header" id="headingTwo">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                      data-bs-target="#collapseTwo<?=$topic['id']?>" aria-expanded="false" aria-controls="collapseTwo">
                      <i class="fa fa-book mr-2 text-secondary"></i>
                      <span class="font-weight-bold text-secondary font-14"><?=$topic['subject']['name']?></span>
                    </button>
                  </h2>
                  <?php
                   $pdf_decode =$topic['pdf']  ? json_decode($topic['pdf']) : array();
                   $vedio_decode = $topic->video ? json_decode($topic->video) : array();
                   $total_contants =  count($pdf_decode)+count($vedio_decode);
                   $count = 1;
                      foreach($pdf_decode as $pdfs){ 
                      foreach($pdfs as $pdfName=>$pdf){
                        $strID = str_replace(' ', '', $pdfName);
                    ?>
                  <div id="collapseTwo<?=$topic['id']?>" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample<?=$topic['id']?>">
                    <div class="accordion" id="subaccordionExample">
                      <div class="accordion-body">
                        <div class="accordion-item mb-3">
                          <h2 class="accordion-header" id="SubheadingTwo">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                              data-bs-target="#SubcollapseTwo<?=$count?>" aria-expanded="false" aria-controls="SubcollapseTwo">
                              <i class="fa fa-file mr-2 text-secondary "></i>
                              <span class="font-weight-bold text-secondary font-14"><?=$pdfName?></span>
                            </button>
                          </h2>
                          <div id="SubcollapseTwo<?=$count?>" class="accordion-collapse collapse" aria-labelledby="SubheadingTwo"
                            data-bs-parent="#subaccordionExample">
                            <div class="accordion-body">
                           
                              <span>
                                <i class="fa fa-cloud-download text-gray mr-5"></i>  <?php 
                                $size = filesize($pdf);
                                $units = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
                                $power = $size > 0 ? floor(log($size, 1024)) : 0;
                                echo number_format($size / pow(1024, $power), 2, '.', ',') . ' ' . $units[$power];
                            ?> 
                            </span>
                                <span class="float-right" style="float:right;">
                                <?php if($registrationDate > $course->demo_classes && in_array(0,$orderStatus) ){ ?>
                                  <a href="javascript:void(0)"  class="btn btn-danger">Please Buy Course</a>

                                  <?php }else{ if($this->session->userdata('email')){?>
                                 <a href="<?=base_url($pdf)?>" target="_blank"
                                  class="btn btn-warning btn-sm text-wight">Open <i class="fa fa-download"></i></a>
                                <!--<a href="<?//=base_url('course/download/'.base64_encode($pdf))?>" target="_blank"-->
                                <!--  class="btn btn-warning btn-sm text-wight">Downlaod <i class="fa fa-download"></i></a>-->
                                <?php } else{?>
                                <a href="javascript:void(0)" onclick="checkLogin()"
                                  class="btn btn-warning btn-sm text-wight">OPen <i class="fa fa-download"></i></a>
                                <?php } } ?>
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php $count++;} } ?>

                  <?php
                   $vedio_decode = $topic->video ? json_decode($topic->video) : array();
                   $total_contants =  count($pdf_decode)+count($vedio_decode);
                   $count1 = 1;
                      foreach($vedio_decode as $videos){ 
                      foreach($videos as $videoName=>$video){
                    ?>
                  <div id="collapseTwo<?=$topic['id']?>" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample<?=$topic['id']?>">
                    <div class="accordion" id="subaccordionExample">
                      <div class="accordion-body">
                        <div class="accordion-item mb-3">
                          <h2 class="accordion-header" id="SubheadingTwo">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                              data-bs-target="#SubcollapseTwo<?=$count?>" aria-expanded="false" aria-controls="SubcollapseTwo">
                              <i class="fa fa-file mr-2 text-secondary "></i>
                              <span class="font-weight-bold text-secondary font-14"><?=$videoName?></span>
                            </button>
                          </h2>
                          <div id="SubcollapseTwo<?=$count?>" class="accordion-collapse collapse" aria-labelledby="SubheadingTwo"
                            data-bs-parent="#subaccordionExample">
                            <div class="accordion-body">
                           
                              <span>
                                <i class="fa fa-cloud-download text-gray mr-5"></i>  <?php 
                                $size = filesize($video);
                                $units = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
                                $power = $size > 0 ? floor(log($size, 1024)) : 0;
                                echo number_format($size / pow(1024, $power), 2, '.', ',') . ' ' . $units[$power];
                            ?> 
                            </span>
                                <span class="float-right" style="float:right;">
                                <?php if($this->session->userdata('email')){?>
                                  <a href="javascript:void(0)" class="btn btn-warning btn-sm text-wight"
                                     onclick="paly_video_modal('<?=$video?>')">Play <i class="fa fa-play"></i></a>
                                <?php } else{?>
                                <a href="javascript:void(0)" onclick="checkLogin()"
                                  class="btn btn-warning btn-sm text-wight">Downlaod <i class="fa fa-download"></i></a>
                                <?php } ?>
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php $count++;} } ?>
                </div>
            
              </div>
              <?php } ?>
              <!-- end -->
            </div>
            <div class="tab-pane fade" id="contact-tab-pane" role="tabpanel" aria-labelledby="contact-tab" tabindex="0">
              <div class="card blog-comments clearfix">
                <!-- <div class="card-header">
                <h4 class="card-title">Comments (12)</h4>
              </div> -->
                <!-- <div class="card-body pb-0">
                <ul class="comments-list">
                  <li>
                    <div class="comment">
                      <div class="comment-author">
                        <img alt src="<?=base_url('public/web/assets/img/user/user4.jpg')?>">
                      </div>
                      <div class="comment-block">
                        <span class="comment-by">
                        <span class="blog-author-name">Michelle Fairfax</span>
                        </span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae, gravida pellentesque urna varius vitae. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        <p class="blog-date">Dec 6, 2017</p>
                        <a class="comment-btn" href="#">
                        <i class="fas fa-reply"></i> Reply
                        </a>
                      </div>
                    </div>
                    <ul class="comments-list reply">
                      <li>
                        <div class="comment">
                          <div class="comment-author">
                            <img alt src="<?=base_url('public/web/assets/img/user/user5.jpg')?>">
                          </div>
                          <div class="comment-block">
                            <span class="comment-by">
                            <span class="blog-author-name">Gina Moore</span>
                            </span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae, gravida pellentesque urna varius vitae.</p>
                            <p class="blog-date">Dec 6, 2017</p>
                            <a class="comment-btn" href="#">
                            <i class="fas fa-reply"></i> Reply
                            </a>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="comment">
                          <div class="comment-author">
                            <img alt src="<?=base_url('public/web/assets/img/user/user3.jpg')?>">
                          </div>
                          <div class="comment-block">
                            <span class="comment-by">
                            <span class="blog-author-name">Carl Kelly</span>
                            </span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae, gravida pellentesque urna varius vitae.</p>
                            <p class="blog-date">December 7, 2017</p>
                            <a class="comment-btn" href="#">
                            <i class="fas fa-reply"></i> Reply
                            </a>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </li>
                  <li>
                    <div class="comment">
                      <div class="comment-author">
                        <img alt src="<?=base_url('public/web/assets/img/user/user6.jpg')?>">
                      </div>
                      <div class="comment-block">
                        <span class="comment-by">
                        <span class="blog-author-name">Elsie Gilley</span>
                        </span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        <p class="blog-date">December 11, 2017</p>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="comment">
                      <div class="comment-author">
                        <img alt src="<?=base_url('public/web/assets/img/user/user7.jpg')?>">
                      </div>
                      <div class="comment-block">
                        <span class="comment-by">
                        <span class="blog-author-name">Joan Gardner</span>
                        </span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        <p class="blog-date">December 13, 2017</p>
                        <a class="comment-btn" href="#">
                        <i class="fas fa-reply"></i> Reply
                        </a>
                      </div>
                    </div>
                  </li>
                </ul>
              </div> -->
              </div>
              <div class="card new-comment clearfix">
                <div class="card-header">
                  <h4 class="card-title">Leave Comment</h4>
                </div>
                <div class="card-body">
                  <form>
                    <div class="form-group">
                      <label>Name <span class="text-danger">*</span></label>
                      <input type="text" class="form-control">
                    </div>
                    <div class="form-group">
                      <label>Your Email Address <span class="text-danger">*</span></label>
                      <input type="email" class="form-control">
                    </div>
                    <div class="form-group">
                      <label>Comments</label>
                      <textarea rows="4" class="form-control"></textarea>
                    </div>
                    <div class="submit-section">
                      <button class="btn btn-primary submit-btn" type="button">Submit</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <!-- <div class="tab-pane fade" id="disabled-tab-pane" role="tabpanel" aria-labelledby="disabled-tab" tabindex="0">...</div> -->
          </div>
        </div>
        <div class="col-lg-4 col-md-12 sidebar-right theiaStickySidebar">
          <div class="card courseimage search-widget">
            <div class="card-body">
              <img src="<?=base_url($course->image)?>" style="width:100%;height:209px;">
            </div>
          </div>
          <div class="card category-widget">
            <div class="card-header price">
              <h4 class="card-title text center">₹ <?=$course->price?></h4>
            </div>
            <div class="card-body buttonprice">
              <div class="submit-section">
                <a href="javascript:void(0)" class="btn btn-primary " onclick="addToCart(<?=$course->id?>,'Course','Add to Cart')"
                  class="btn book-btn">Add to Cart</a>
                <a href="javascript:void(0)" class="btn btn-transparent submit-btn"
                  onclick="addToCart(<?=$course->id?>,'Course','Buy Now')" class="btn book-btn">Buy Now</a>
              </div>
            </div>
          </div>
          <div class="card tags-widget">
            <div class="card-header">
              <h4 class="card-title">Bundle Specifications</h4>
            </div>
            <div class="card-body">
              <ul class="">
                <li><a href="javascript:void()"><i class="fa fa-clock"></i>Duration<span><?=$course->duration?>
                      Days</span></a></li>
                <li><a href="javascript:void()"><i class="fa fa-user-friends"></i>Students<span>0</span></a></li>
                <li><a href="javascript:void()"><i class="fa fa-book"></i>Courses<span>1</span></a></li>
                <li><a href="javascript:void()"><i class="fa fa-calendar-day"></i>Created
                    Date<span><?=date('d-F-Y',strtotime($course->created_at))?></span></a></li>
                <!-- <li><a href="javascript:void()"><i class="fa fa-briefcase-clock"></i>Access Period<span>500 Days</span></a></li> -->
              </ul>
            </div>
          </div>
          <div class="card widget-profile user-widget-profile" style="display:none;">
            <div class="card-header">
              <h4 class="card-title">Instructor Profile</h4>
            </div>
            <div class="card-body">
              <div class="pro-widget-content">
                <div class="profile-info-widget">
                  <a href="profile-mentee.html" class="booking-user-img">
                    <img alt src="<?=base_url('public/web/assets/img/user/user.jpg')?>">
                  </a>
                  <div class="profile-det-info">
                    <h3>
                      <a href="profile-mentee.html">Richard Wilson</a>
                    </h3>
                    <div class="mentee-details">
                      <h5>
                        <b>course :</b> ABC
                      </h5>
                      <h5 class="mb-0">
                        <i class="fas fa-book"></i>Masters PHD
                      </h5>
                    </div>
                  </div>
                </div>
              </div>
              <div class="mentee-info">
                <ul>
                  <li>Phone <span>+1 952 001 8563</span></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade custom-modal" id="video_modal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content lg">
        <!-- <div class="modal-header">
            <h5 class="modal-title">Edit Time Slots</h5>
            <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div> -->
        <div class="modal-body" style="width:100%;height:100%">
          <div id="show_video"></div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>

<script>
function checkLogin() {
  toastNotif({
    text: 'Assces denied Please login ',
    color: '#da4848',
    timeout: 5000,
    icon: 'error'
  });
}

function paly_video_modal(video) {
  $('#video_modal').modal('show');
  $('#show_video').html(
    '<div class="embed-responsive embed-responsive-16by9"><iframe id="vandyplayer" class="embed-responsive-item" allowfullscreen="allowfullscreen" webkitallowfullscreen="webkitallowfullscreen" mozallowfullscreen="mozallowfullscreen" src="https://www.youtube.com/embed/v=2M4jRI7s2JU" style="width:100%;height:100%"></iframe></div>'
  );
  $('.modal').remove();
  $('.modal-backdrop').remove();
  //$('body').removeClass( "modal-open" );

}
</script>