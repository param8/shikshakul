<div class="col-md-8 col-lg-8 col-xl-10">
  <h3 class="pb-3"><?=$page_title?></h3>
  <div class="tab-pane show active" id="mentee-list">
    <div class="card card-table">
      <div class="card-body">
        <div class="table_bg p-3 table-responsive">
          <table class="table table-hover table-center mb-0" id="answerDataTable">
            <thead>
              <tr>
                <th>SNo.</th>
                <th >Student</th>
                <th >Title</th>
                <th >Course</th>
                <th >Subject</th>
                <th>Created At</th>
                <th >Marks</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
<!-- Result Modal -->
<div class="modal fade" id="result_modal" tabindex="0" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form action="<?=base_url('Quiz/storeResult')?>" method="POST" id="resultForm" enctype="multipart/form-data">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Upload Result</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="closeResultModal()">
          <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <lable>Result</lable>
          <input class="form-control" type="text" name="result" id="result" minlength="0" oninput="this.value = this.value.replace(/[^0-9]./g, '').replace(/(\..*)\./g, '$1');">
          <input type="hidden" name="answerId" id="answerId" value="">
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Upload</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!--End Result modal-->
<script>
  $(document).ready(function() {
       var dataTable = $('#answerDataTable').DataTable({
           "processing": true,
           "serverSide": true,
           buttons: [{
               extend: 'excelHtml5',
               text: 'Download Excel'
           }],
           "order": [],
           "ajax": {
               url: "<?=base_url('Quiz/ajaxAnswer')?>",
               type: "POST"
           },
           "columnDefs": [{
               "targets": [0],
               "orderable": false,
           }, ],
       });
   });
  
    function questionModal(){
    $.ajax({
     url: '<?=base_url("Quiz/getCourse")?>',
     type: 'POST',
     success: function (data) {
      $('#question_modal').modal('show');
    $('body').removeClass('modal-open');
     },
   }); 
   }
  
   function closeModal(){
    $('#question_modal').modal('hide');
   }
  
   function getSubject(courseID){
    //alert(courseID);
  $.ajax({
     url: '<?=base_url("Quiz/getSubject")?>',
     type: 'POST',
     data: {courseID},  
     success: function (data) {
      $('#subject').html(data);
     },
   });
  }
  
  $(document).ready(function() {
    getSubject(courseID);
          });
  
  
  
   $("form#questionForm").submit(function(e) {
  			$(':input[type="submit"]').prop('disabled', true);
  			e.preventDefault();    
  			var formData = new FormData(this);  
  			$.ajax({
  			url: $(this).attr('action'),
  			type: 'POST',
  			data: formData,
  			cache: false,
  			contentType: false,
  			processData: false,
  			dataType: 'json',
  			success: function (data) { 
  				if(data.status==200) {
  				//$('.modal').modal('hide');
          toastNotif({
  				text: data.message,
  				color: '#5bc83f',
  				timeout: 5000,
  				icon: 'valid'
  			});
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function(){
  
          location.href="<?=base_url('answer-list')?>"; 	
          
        }, 1000) 		
  				}else if(data.status==403) {
            toastNotif({
                  text: data.message,
                  color: '#da4848',
                  timeout: 5000,
                  icon: 'error'
                });
  
            $(':input[type="submit"]').prop('disabled', false);
  				}else{
            toastNotif({
                text: data.message,
                color: '#da4848',
                timeout: 5000,
                icon: 'error'
              });
            $(':input[type="submit"]').prop('disabled', false);
  				}
  			},
  			error: function(){} 
  			});
  		});
  
        function uploadResult(id){
          $('#result_modal').modal('show');
          $('body').removeClass('modal-open');
          $('#answerId').val(id);
        }
        function closeResultModal(){
          $('#result_modal').modal('hide');
        }
  
        $("form#resultForm").submit(function(e) {
  			$(':input[type="submit"]').prop('disabled', true);
  			e.preventDefault();    
  			var formData = new FormData(this);  
  			$.ajax({
  			url: $(this).attr('action'),
  			type: 'POST',
  			data: formData,
  			cache: false,
  			contentType: false,
  			processData: false,
  			dataType: 'json',
  			success: function (data) { 
  				if(data.status==200) {
  				//$('.modal').modal('hide');
          toastNotif({
  				text: data.message,
  				color: '#5bc83f',
  				timeout: 5000,
  				icon: 'valid'
  			});
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function(){
  
          location.href="<?=base_url('answer-list')?>"; 	
          
        }, 1000) 		
  				}else if(data.status==403) {
            toastNotif({
                  text: data.message,
                  color: '#da4848',
                  timeout: 5000,
                  icon: 'error'
                });
  
            $(':input[type="submit"]').prop('disabled', false);
  				}else{
            toastNotif({
                text: data.message,
                color: '#da4848',
                timeout: 5000,
                icon: 'error'
              });
            $(':input[type="submit"]').prop('disabled', false);
  				}
  			},
  			error: function(){} 
  			});
  		});
   
</script>