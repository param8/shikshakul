<div class="col-md-8 col-lg-9 col-xl-10">

  <div class="card">
    <div class="card-body">
      <?php //$url = $this->session->userdata('user_type')=='Student' ? base_url('Student/update') : base_url('Faculty/update'); ?>
      <form action="<?=base_url('User/update_password')?>" id="updatePasswordForm" method="POST" enctype="multipart/form-data">
        <div class="row form-row">
          
          <div class="col-12 col-md-6">
            <div class="form-group">
              <label>Old Password</label>
              <input type="password" class="form-control" name="old_password" id="old_password" >
            </div>
          </div>
          <div class="col-12 col-md-6">
            <div class="form-group">
              <label>New Password</label>
              <input type="password" class="form-control" name="new_password" id="new_password" >
            </div>
          </div>

          <div class="col-12 col-md-6">
            <div class="form-group">
              <label>Confirm Password</label>
                <input type="password" name="confirm_password" id="confirm_password" class="form-control" >
            </div>
          </div>
   
   
        </div>
        <div class="submit-section">
          <button type="submit" class="btn btn-primary submit-btn">Change Password</button>
        </div>
      </form>
    </div>
  </div>
</div>
</div>
</div>
</div>
<script>
$("form#updatePasswordForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        //$('.modal').modal('hide');
        toastNotif({
          text: data.message,
          color: '#5bc83f',
          timeout: 5000,
          icon: 'valid'
        });
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {
          location.href = "<?=base_url('Authantication/logout')?>";
        }, 1000)
      } else if (data.status == 403) {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});

</script>