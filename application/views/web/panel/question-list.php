<div class="col-md-8 col-lg-8 col-xl-10">
  <h3 class="pb-3"><?=$page_title?></h3>
  <div class="tab-pane show active" id="mentee-list">
    <div class="card card-table">
      <div class="card-body">
        <div class="table_bg p-3 table-responsive">
          <?php if($this->session->userdata('user_type') == 'Faculties'){ ?> 
          <div class="upload-img">
            <div class="change-photo-btn">
              <!-- <input type="file" class="upload" name="question_file" id="question_file"> -->
              <i class="fa fa-upload"></i><button onclick="questionModal()" class="btn btn-primary">Upload Question</button>
            </div>
          </div>
          <?php } ?>
          <table class="table table-hover table-center mb-0" id="qustionDataTable">
            <thead>
              <tr>
                <th>SNo.</th>
                <th>Title</th>
                <th>Course</th>
                <th>Subject</th>
                <th><?= $this->session->userdata('user_type') == 'Faculties' ? 'Creted by' : '' ?></th>
                <!-- <th>File</th> -->
                <th><?= $this->session->userdata('user_type') == 'Faculties' ? 'Created at' : 'Action' ?></th>
            
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
<!-- Question Modal -->
<div class="modal fade" id="question_modal" tabindex="0" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form action="<?=base_url('Quiz/storeQuestion')?>" method="POST" id="questionForm" enctype="multipart/form-data">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Upload Question</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="closeModal()">
          <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <input class="form-control" type="text" name="title" id="title" placeholder="title">
          <lable>Course</lable>
          <select class="form-control" name="course" id="course" onclick="getSubject(this.value)">
            <option value="">Select Course</option>
            <?php 
              foreach($courses as $course){ ?>
            <option value="<?=$course->id?>"><?=$course->name?></option>
            <?php }
              ?>
          </select>
          <lable>Subject</lable>
          <select class="form-control" name="subject" id="subject">
            <option value="">Choose Subject</option>
          </select>
          <lable>File</lable>
          <input class="form-control" type="file" name="questionFile" id="questionFile">
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!--End Question modal-->
<!-- Answer Modal -->
<div class="modal fade" id="answer_modal" tabindex="0" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form action="<?=base_url('Quiz/storeAnswer')?>" method="POST" id="answerForm" enctype="multipart/form-data">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Upload Answer</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="closeAnswerModal()">
          <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <input class="form-control" type="file" name="answerFile" id="answerFile">
          <input type="hidden" name="quesid" id="quesid" value="">
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Upload</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!--End Answer modal-->
<script>
  $(document).ready(function() {
       var dataTable = $('#qustionDataTable').DataTable({
           "processing": true,
           "serverSide": true,
           buttons: [{
               extend: 'excelHtml5',
               text: 'Download Excel'
           }],
           "order": [],
           "ajax": {
               url: "<?=base_url('Quiz/ajaxQuestion')?>",
               type: "POST"
           },
           "columnDefs": [{
               "targets": [0],
               "orderable": false,
           }, ],
       });
   });
  
    function questionModal(){
    $.ajax({
     url: '<?=base_url("Quiz/getCourse")?>',
     type: 'POST',
     success: function (data) {
      $('#question_modal').modal('show');
    $('body').removeClass('modal-open');
     },
   }); 
   }
  
   function closeModal(){
    $('#question_modal').modal('hide');
   }
  
   function getSubject(courseID){
    //alert(courseID);
  $.ajax({
     url: '<?=base_url("Quiz/getSubject")?>',
     type: 'POST',
     data: {courseID},  
     success: function (data) {
      $('#subject').html(data);
     },
   });
  }
  
  $(document).ready(function() {
    getSubject(courseID);
          });
  
  
  
   $("form#questionForm").submit(function(e) {
  			$(':input[type="submit"]').prop('disabled', true);
  			e.preventDefault();    
  			var formData = new FormData(this);  
  			$.ajax({
  			url: $(this).attr('action'),
  			type: 'POST',
  			data: formData,
  			cache: false,
  			contentType: false,
  			processData: false,
  			dataType: 'json',
  			success: function (data) { 
  				if(data.status==200) {
  				//$('.modal').modal('hide');
          toastNotif({
  				text: data.message,
  				color: '#5bc83f',
  				timeout: 5000,
  				icon: 'valid'
  			});
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function(){
  
          location.href="<?=base_url('question-list')?>"; 	
          
        }, 1000) 		
  				}else if(data.status==403) {
            toastNotif({
                  text: data.message,
                  color: '#da4848',
                  timeout: 5000,
                  icon: 'error'
                });
  
            $(':input[type="submit"]').prop('disabled', false);
  				}else{
            toastNotif({
                text: data.message,
                color: '#da4848',
                timeout: 5000,
                icon: 'error'
              });
            $(':input[type="submit"]').prop('disabled', false);
  				}
  			},
  			error: function(){} 
  			});
  		});
  
     function uploadAnswer(id){
      //alert(id);
      $('#answer_modal').modal('show');
      $('body').removeClass('modal-open');
      $('#quesid').val(id);
      }
  
      function closeAnswerModal(){
        $('#answer_modal').modal('hide');
      }
  
  $("form#answerForm").submit(function(e) {
  			$(':input[type="submit"]').prop('disabled', true);
  			e.preventDefault();    
  			var formData = new FormData(this);  
  			$.ajax({
  			url: $(this).attr('action'),
  			type: 'POST',
  			data: formData,
  			cache: false,
  			contentType: false,
  			processData: false,
  			dataType: 'json',
  			success: function (data) { 
  				if(data.status==200) {
  				//$('.modal').modal('hide');
          toastNotif({
  				text: data.message,
  				color: '#5bc83f',
  				timeout: 5000,
  				icon: 'valid'
  			});
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function(){
          location.href="<?=base_url('question-list')?>"; 	
          
        }, 1000) 		
  				}else if(data.status==403) {
            toastNotif({
                  text: data.message,
                  color: '#da4848',
                  timeout: 5000,
                  icon: 'error'
                });
  
            $(':input[type="submit"]').prop('disabled', false);
  				}else{
            toastNotif({
                text: data.message,
                color: '#da4848',
                timeout: 5000,
                icon: 'error'
              });
            $(':input[type="submit"]').prop('disabled', false);
  				}
  			},
  			error: function(){} 
  			});
  		});
  
      
</script>