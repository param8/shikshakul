<div class="col-md-8 col-lg-8 col-xl-10">
  <h3 class="pb-3"><?=$page_title?></h3>
  <div class="row">
    <div class="col-md-8 col-lg-8 col-xl-8">
      <span>
      <a href="javascript:void(0)" onclick="set_order_type('Course')"
        class="btn <?=$this->session->userdata('order_type') == 'Course' ? 'btn-success' : 'btn-primary'?>">Course
        Booking</a>
   
      <a href="javascript:void(0)" onclick="set_order_type('Test Series')"
        class="btn <?=$this->session->userdata('order_type') == 'Test Series' ? 'btn-success' : 'btn-primary'?>">Test
        Series Booking</a></span>
    </div>
  </div>
  <hr>

  <div class="tab-pane show active" id="mentee-list">
    <div class="card card-table">
      <div class="card-body">
        <div class="table_bg p-3 table-responsive">
          <table class="table table-hover table-center mb-0" id="bookingDataTable">
            <thead>
              <tr>
                <th>SNo.</th>
                <th>Course Image</th>
                <th>Order Name</th>
                <th>Order ID</th>
                <th>Price</th>
                <th>Payment Status</th>
                <th>Payment Date</th>
              </tr>
            </thead>
            <tbody>

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>

<script>
$(document).ready(function() {
  //alert('dfgfgf');
  // $('#example').DataTable();
  // } );
  var dataTable = $('#bookingDataTable').DataTable({
    "processing": true,
    "serverSide": true,
    buttons: [{
      extend: 'excelHtml5',
      text: 'Download Excel'
    }],
    "order": [],
    "ajax": {
      url: "<?=base_url('Cart/ajaxBooking')?>",
      type: "POST"
    },
    "columnDefs": [{
      "targets": [0],
      "orderable": false,
    }, ],
  });
});

function set_order_type(value) {
  $.ajax({
    url: '<?=base_url('Ajax_controller/set_order_type')?>',
    type: 'POST',
    data: {
      value,
    },
    success: function(data) {
      location.reload();
    },
  });
}
</script>