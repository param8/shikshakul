<div class="col-md-8 col-lg-9 col-xl-10">
  <div class="row">
    <div class="col-md-12 col-lg-4 dash-board-list blue">
      <div class="dash-widget">
        <div class="circle-bar">
          <div class="icon-col">
            <i class="fas fa-book"></i>
          </div>
        </div>
        <div class="dash-widget-info">
          <h3><?=count($courses)?></h3>
          <h6>Courses</h6>
        </div>
      </div>
    </div>
    <div class="col-md-12 col-lg-4 dash-board-list yellow">
      <div class="dash-widget">
        <div class="circle-bar">
          <div class="icon-col">
            <i class="fas fa-calendar-check"></i>
          </div>
        </div>
        <div class="dash-widget-info">
          <h3><?=count($bookings)?></h3>
          <h6>Booked Courses</h6>
        </div>
      </div>
    </div>
    <div class="col-md-12 col-lg-4 dash-board-list pink">
      <div class="dash-widget">
        <div class="circle-bar">
          <div class="icon-col">
            <i class="fas fa-wallet"></i>
          </div>
        </div>
        <div class="dash-widget-info">
          <h3>₹ 0</h3>
          <h6>Total Earned</h6>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 col-lg-6 dash-board-list yellow">
      <div class="dash-widget">
        <div class="circle-bar">
          <h3>Today Classes</h3>
          <hr>
          <?php 
            $earlier = new DateTime(date("d-m-Y"));
            $later = new DateTime(date('d-m-Y',strtotime($this->session->userdata('created_at'))));
            $registrationDate = $later->diff($earlier)->format("%a");
            $orderStatus = array();
            foreach($courses as $course){
            $this->db->select('items.*,orders.orderID as order_id,orders.discount as discount,orders.gst as gst,orders.price as orderPrice,orders.total_price as total_price,users.name as userName,orders.payment_status as paymentStatus,orders.coupon_code as order_coupon_code,orders.status as orderStatus');
            $this->db->from('orders');
            $this->db->join('items', 'orders.id=items.orderID','left');
            $this->db->join('users', 'users.id=orders.userID','left');
            $this->db->where(array('items.courseID'=>$course->id,'orders.userID'=>$this->session->userdata('id'),'orders.status'=>1,'items.order_type'=>'Course'));
            $this->db->group_by('items.orderID');
            $this->db->order_by('orders.id', 'desc');
            $orders = $this->db->get()->result();
            foreach ($orders as $order){
              $orderStatus[] = $order->orderStatus;
            }
            
             $orderStatus =  !empty(array_filter($orderStatus)) ? array_filter($orderStatus) : array(0);
  
            //echo $registrationDate .'<' .$course->demo_classes;
            ?>
          <p><?=$course->name?> <i class="fas fa-arrow-right"> </i> <b>
              <?php if($registrationDate > $course->demo_classes && in_array(0,$orderStatus) ){ ?>
              <a href="javascript:void(0)" class="text-danger">Please Buy Course</a>
              <?php 
              }else{ ?>
              <a href="<?=$course->class_link?>" target="_blank" class="text-primary"><?=$course->class_link?></a>
              <?php } ?>
            </b>
          </p>
          <hr>
          <?php } ?>
        </div>
      </div>
    </div>
    <!--               
      <div><iframe scrolling src="https://test3015.livebox.co.in/livebox/player/?chnl=maths" width="400px" height="400px" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen allow="autoplay" ></iframe></div>
      </div> -->
    <div class="col-md-12 col-lg-12 dash-board-list yellow">
      <div class="dash-widget">
        <div class="circle-bar">
          <h3>Class Schedule</h3>
          <hr>
          <div class="table-responsive1">
            <table class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Course</th>
                  <th>Subject</th>
                  <th>Teacher</th>
                  <th>Topic</th>
                  <th>Start Time</th>
                  <th>End Time</th>
                  <th>Date</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                  $earlier = new DateTime(date("d-m-Y"));
                  $later = new DateTime(date('d-m-Y',strtotime($this->session->userdata('created_at'))));
                  $registrationDate = $later->diff($earlier)->format("%a");
                  $orderStatus = array();
                  foreach($schedules as $schedule){
                  $this->db->select('items.*,orders.orderID as order_id,orders.discount as discount,orders.gst as gst,orders.price as orderPrice,orders.total_price as total_price,users.name as userName,orders.payment_status as paymentStatus,orders.coupon_code as order_coupon_code,orders.status as orderStatus');
                  $this->db->from('orders');
                  $this->db->join('items', 'orders.id=items.orderID','left');
                  $this->db->join('users', 'users.id=orders.userID','left');
                  $this->db->where(array('items.courseID'=>$schedule->courseID,'orders.userID'=>$this->session->userdata('id'),'orders.status'=>1,'items.order_type'=>'Course'));
                  $this->db->group_by('items.orderID');
                  $this->db->order_by('orders.id', 'desc');
                  $orders = $this->db->get()->result();
                  foreach ($orders as $order){
                    $orderStatus[] = $order->orderStatus;
                  }
                   $orderStatus =  !empty($orderStatus) ? array_filter($orderStatus) : array(0);
                  //  print_r($orderStatus);
                  //echo $registrationDate .'<' .$course->demo_classes;
                  ?>
                <tr>
                  <td><?=$schedule->coursesName?></td>
                  <td><?=$schedule->subjectName?></td>
                  <td><?=$schedule->facultyName?></td>
                  <td><?=$schedule->topics?></td>
                  <td><?=date('H:i A',$schedule->start_time)?></td>
                  <td><?=date('H:i A',$schedule->end_time)?></td>
                  <td class="text-danger"><?=date('d-M-Y',strtotime($schedule->class_date))?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>