
            <div class="col-md-7 col-lg-8 col-xl-9">
              <h3 class="pb-3">Booking Summary</h3>
              <div class="tab-pane show active" id="mentee-list">
                <div class="card card-table">
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-hover table-center mb-0">
                        <thead>
                          <tr>
                            <th>SNo.</th>
                            <th>Course Image</th>
                            <th class="text-center">Course Name</th>
                            <th class="text-center">Order ID</th>
                            <th class="text-center">Price</th>
                            <th class="text-center">Payment Status</th>
                            <th class="text-center">Payment Date</th>
                          </tr>
                        </thead>
                        <tbody>
                       <?php foreach($orders as $key =>$order){?>
                          <tr>

                           <td><?=$key+1?></td>
                            <td>
                              <h2 class="table-avatar">
                                <a href="javascript:void(0)" class="avatar avatar-sm me-2"><img class="avatar-img rounded-circle" src="<?=base_url($order->coursePic)?>" alt="User Image"></a>

                              </h2>
                            </td>
                           
                            <td class="text-center"><span ><?=$order->courseName?></span></td>
                            <td class="text-center"><span ><?=$order->order_id?></span></td>
                            <td class="text-center"><span class="btn btn-sm" > ₹ <?=$order->price?></span></td>
                            <td class="text-center"><a class="<?=$order->orderStatus == 1 ? 'btn btn-sm text-success' : 'btn btn-sm text-danger'?>"> <?=$order->orderStatus == 1 ? 'Active' : 'Pending'?></a></td>
                            <td class="text-center"><?=date('d F Y H : i : s',strtotime($order->created_at))?></td>
                          </tr>
              <?php } ?>
       
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

     
      