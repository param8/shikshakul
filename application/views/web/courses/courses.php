<div class="breadcrumb-bar">
  <div class="container-fluid">
    <div class="row align-items-center">
      <div class="col-md-12 col-12">
        <nav aria-label="breadcrumb" class="page-breadcrumb">
          <ol class="breadcrumb">
            <li class=""><a href="<?=base_url('home')?>">Home</a></li>
            /
            <li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
          </ol>
        </nav>
        <h2 class="breadcrumb-title"><?=$page_title?></h2>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div class="row blog-grid-row">
          <?php 
            $course_id = array();
            $subject_id = array();
            //print_r($subjects);
             foreach($subjects as $subject){
              $course_id[] = $subject->courseID;
              $subject_id[$subject->courseID] = $subject->id;
             }
            foreach($courses as $course){
              if($this->session->userdata('email')){
                if($this->session->userdata('verification_user')==0){
                  $url = base_url('course-detail/'.$course->slug);
                }else{
                  $url = base_url('course-detail-panel/'.$course->slug);
                }
               
              }else{
                $url = base_url('course-detail/'.$course->slug);
              }
              ?>
          <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-6">
            <div class="blog grid-blog">
              <div class="blog-image">
                <a href="<?=$url?>"><img class="img-fluid" src="<?=base_url($course->image)?>" alt="Post Image" style="width:100%;height:230px;"></a>
              </div>
              <h5 class="title">
                <a href="<?=$url?>"><?=$course->name?></a>
                <i class="fas fa-check-circle verified"></i>
              </h5>
              <ul class="available-info">
                <li><i class="fa fa-book"></i><?=$course->categoryName?></li>
                <li><i class="fa fa-clock"></i>Days <?=$course->duration?> </li>
                <li class="text-danger"><b>₹ <?=$course->price==0 ? 'Free' : $course->price;?></b> </li>
              </ul>
              <div class="row row-sm">
                <div class="col-6">
                  <a href="<?=$url?>" class="btn view-btn">View Course</a>
                </div>
                <div class="col-6">
                  <?php if($this->session->userdata('email')){
                    if($this->session->userdata('verification_user')==0){
                    ?>
                    <a href="<?=base_url('verification')?>" class="btn book-btn">Book Now</a>
                    <?php }else{?>
                      <a href="javascript:void(0)" onclick="addToCart(<?=$course->id?>)" class="btn book-btn">Book Now</a>
                    <?php } } else{ ?>
                    <a href="<?=base_url('login')?>" class="btn book-btn">Book Now</a>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $(document).ready(function () {
    
    var courseDataTable = $('#courseDataTable').DataTable({
      "processing":true,
      "serverSide":true,
      buttons: [
      { extend: 'excelHtml5', text: 'Download Excel' }
    ],
      "order":[],
      "ajax":{
          url:"<?=base_url('Courses/ajax_course_datatable')?>",
          type:"POST"
      },
      "columnDefs":[
       {
           "targets":[0],
           "orderable":false,
       },
     ],
  
  });
  });
</script>