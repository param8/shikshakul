<div class="main-wrapper">
<div class="bg-pattern-style bg-pattern-style-register mt-5 pb-4">
  <div class="content">
    <div class="account-content">
      <div class="account-box">
        <div class="login-right">
          <div class="login-header">
            <h3><span><?=$page_title?></span> </h3>
          </div>
          
          <form action="<?=base_url('Authantication/verify_otp')?>" id="otpVerifyForm" method="post">
            <div class="row">
              <div class="col-lg-12">
                <div class="form-group">
                  <label class="form-control-label">Email OTP <span class="text-danger">*</span></label>
                  <input  type="text" class="form-control" id="email_otp" name="email_otp" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                </div>
                <p><a href="javascript:void(0)" onclick="resendOTPEmail('<?=$this->session->userdata('email')?>')"><b>Resend Email OTP</b></a></p>
              </div>
              <hr>
              <div class="col-lg-12">
                <div class="form-group">
                  <label class="form-control-label">Phone OTP. <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" name="phone_otp" id="phone_otp"  oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                </div>
                <p><a href="javascript:void(0)" onclick="resendOTPPhone('<?=$this->session->userdata('phone')?>')"><b>Resend Phone OTP</b></a></p>
              </div>
              <button class="btn btn-primary login-btn" type="submit">Verify OTP</button>
          </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $("form#otpVerifyForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();    
  var formData = new FormData(this);
  formData.append("user_type", 'Student');
  formData.append("created_by", 'Self');
  $.ajax({
  url: $(this).attr('action'),
  type: 'POST',
  data: formData,
  cache: false,
  contentType: false,
  processData: false,
  dataType: 'json',
  success: function (data) {
  if(data.status==200) {
    toastNotif({
				text: data.message,
				color: '#5bc83f',
				timeout: 5000,
				icon: 'valid'
			});
  $(':input[type="submit"]').prop('disabled', false);
  setTimeout(function(){
    if(data.user_type=='Student'){
        location.href="<?=base_url('student-dashboard')?>";
    }else{
      location.href="<?=base_url('faculty-dashboard')?>";
    }
      	
    
  }, 1000) 
  
  }else if(data.status==403) {
    toastNotif({
				text: data.message,
				color: '#da4848',
				timeout: 5000,
				icon: 'error'
			});

  $(':input[type="submit"]').prop('disabled', false);
  }else{
    toastNotif({
				text: data.message,
				color: '#da4848',
				timeout: 5000,
				icon: 'error'
			});
     $(':input[type="submit"]').prop('disabled', false);
  }
  },
  error: function(){} 
  });
  });
  function resendOTPEmail(email){
    $.ajax({
    url: '<?=base_url('Ajax_controller/get_email_otp')?>',
    type: 'POST',
    data: {email},
    dataType: 'json',
    success: function (data) {
      if(data.status==200) {
    toastNotif({
				text: data.message,
				color: '#5bc83f',
				timeout: 5000,
				icon: 'valid'
			});
  
  }else if(data.status==403) {
    toastNotif({
				text: data.message,
				color: '#da4848',
				timeout: 5000,
				icon: 'error'
			});

  }else{
    toastNotif({
				text: data.message,
				color: '#da4848',
				timeout: 5000,
				icon: 'error'
			});
  }
    }
    });
  }

  function resendOTPPhone(phone){
    $.ajax({
    url: '<?=base_url('Ajax_controller/get_phone_otp')?>',
    type: 'POST',
    data: {phone},
    dataType: 'json',
    success: function (data) {
      if(data.status==200) {
    toastNotif({
				text: data.message,
				color: '#5bc83f',
				timeout: 5000,
				icon: 'valid'
			});
  
  }else if(data.status==403) {
    toastNotif({
				text: data.message,
				color: '#da4848',
				timeout: 5000,
				icon: 'error'
			});

  }else{
    toastNotif({
				text: data.message,
				color: '#da4848',
				timeout: 5000,
				icon: 'error'
			});
  }
    }
    });
  }
</script>