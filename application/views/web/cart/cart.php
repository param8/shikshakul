<div class="breadcrumb-bar">
  <div class="container-fluid">
    <div class="row align-items-center">
      <div class="col-md-12 col-12">
        <nav aria-label="breadcrumb" class="page-breadcrumb">
          <ol class="breadcrumb">
            <li class=""><a href="<?=base_url('home')?>">Home</a></li>''
            <li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
          </ol>
        </nav>
        <h2 class="breadcrumb-title"><?=$page_title?></h2>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12 ">
      <div class="table-responsive">
            <table class="table check-tbl">
              <thead>
                <tr>
                  <th>Image</th>
                  <th>Name</th>
                  <th>Price</th>
                  <th>Quantity</th>
                  <th>Total</th>
                  <th >Action</th>
                </tr>
              </thead>
              <tbody>
                <?php
                  $cartItems=$this->cart->contents();
                  	if(count($cartItems) > 0) {
                                             //print_r($cartItems);
                  foreach($cartItems as $item){?>
                <tr>
                  <td class="product-item-img"><img src="<?= $item['image']?>" alt=""></td>
                  <td class="product-item-name"><?= $item['items'] == 0 ? $item['name'] : $item['name']?></td>
                  <td class="product-item-price"><?= $item['price']?></td>
                  <td class="product-item-quantity">
                    <div class="quantity btn-quantity style-1 me-3">
                      <input id="" type="text" style="width: 50px;" value="<?= $item['qty']?>" onchange="update_cart(this.value,'<?=$item['rowid']?>')" minlenght="1" maxlength="3" name="demo_vertical2" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" min="1"/>
                    </div>
                  </td>
                  <td class="product-item-totle"><i class="fa fa-inr"><?= $item['subtotal']?></i></td>
                  <td class="float-right">
                  <?php if($item['items'] == 0 ){?>
                    <a href="javascript:void(0);" onclick="removeCartItem('<?=$item['rowid']?>')"class="text-danger"><i class="fa fa-times"></i></a>
                    <?php }else{?>
                    <a href="javascript:void(0);" onclick="cartDetail('<?=$item['rowid']?>')"class=""><i class="fa fa-eye"></i></a>
                    <a href="javascript:void(0);" onclick="removeCartItem('<?=$item['rowid']?>')"class="text-danger"><i class="fa fa-times"></i></a>
                  <?php }?>
                  </td>
                </tr>
                <?php }}else{
										echo "<tr>
										<td colspan='6' class='text-center'>Cart is Empty ..</td></tr>";
									}  ?>
              </tbody>
            </table>
          </div>
      </div>
    </div>
    <div class="row">

        <div class="col-lg-6">
          <div class="widget">
            <h4 class="widget-title">Cart Subtotal</h4>
            <table class="table-bordered check-tbl m-b25">
              <tbody>
                <tr>
                  <td>Order Subtotal</td>
                  <td><i class="fa fa-inr" aria-hidden="true"> <?=$this->cart->total()?></i></td>
                </tr>
                <tr>
                  <td>Shipping Charge</td>
                  <td><i class="fa fa-inr" aria-hidden="true"> 100</i></td>
                </tr>
                <tr>
                  <td>Coupon</td>
                  <td><i class="fa fa-inr" aria-hidden="true"> 0</i></td>
                </tr>
                <tr>
                  <td>Total</td>
                  <td><i class="fa fa-inr" aria-hidden="true"> <?=($this->cart->total()+100)?></i></td>
                </tr>
              </tbody>
            </table>
            <div class="form-group m-b25">
              <!--<a href="<?=base_url('checkout')?>" class="btn btn-primary btnhover" type="button">Proceed to Checkout</a>-->
              <a href="<?=base_url('checkout')?>" class="btn btn-primary btnhover" type="button">Proceed to Checkout</a>
            </div>
          </div>
        </div>
      </div>
  </div>
</div>