<div class="breadcrumb-bar">
            <div class="container-fluid">
               <div class="row align-items-center">
                  <div class="col-md-8 col-12">
                     <nav aria-label="breadcrumb" class="page-breadcrumb">
                        <ol class="breadcrumb">
                           <li class=""><a href="<?=base_url('home')?>">Home</a></li>/
                           <li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
                        </ol>
                     </nav>
                     <h2 class="breadcrumb-title"><?=$page_title?></h2>
                  </div>
                  <div class="col-md-4 col-12 d-md-block d-none">
                     <div class="sort-by">
                        <span class="sort-title">Sort by</span>
                        <span class="sortby-fliter">
                           <select class="select">
                              <option>Select</option>
                              <option class="sorting">Rating</option>
                              <option class="sorting">Popular</option>
                              <option class="sorting">Latest</option>
                              <option class="sorting">Free</option>
                           </select>
                        </span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="content">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-md-12 col-lg-4 col-xl-3 theiaStickySidebar">
                     <div class="card search-filter">
                        <div class="card-header">
                           <h4 class="card-title mb-0">Search Filter</h4>
                        </div>
                        <div class="card-body">
                           <div class="filter-widget">
                              <div class="cal-icon">
                                 <input type="text" class="form-control datetimepicker" placeholder="Select Date">
                              </div>
                           </div>
                           <div class="filter-widget">
                              <h4>Gender</h4>
                              <div>
                                 <label class="custom_check">
                                 <input type="checkbox" name="gender_type" checked>
                                 <span class="checkmark"></span> Male
                                 </label>
                              </div>
                              <div>
                                 <label class="custom_check">
                                 <input type="checkbox" name="gender_type">
                                 <span class="checkmark"></span> Female
                                 </label>
                              </div>
                           </div>
                           <div class="filter-widget">
                              <h4>Select Courses</h4>
                              <div>
                                 <label class="custom_check">
                                 <input type="checkbox" name="select_specialist" checked>
                                 <span class="checkmark"></span> Digital Marketer
                                 </label>
                              </div>
                              <div>
                                 <label class="custom_check">
                                 <input type="checkbox" name="select_specialist" checked>
                                 <span class="checkmark"></span> UNIX, Calculus, Trigonometry
                                 </label>
                              </div>
                              <div>
                                 <label class="custom_check">
                                 <input type="checkbox" name="select_specialist">
                                 <span class="checkmark"></span> Computer Programming
                                 </label>
                              </div>
                              <div>
                                 <label class="custom_check">
                                 <input type="checkbox" name="select_specialist">
                                 <span class="checkmark"></span> ASP.NET,Computer Gaming
                                 </label>
                              </div>
                              <div>
                                 <label class="custom_check">
                                 <input type="checkbox" name="select_specialist">
                                 <span class="checkmark"></span> HTML, Css
                                 </label>
                              </div>
                              <div>
                                 <label class="custom_check">
                                 <input type="checkbox" name="select_specialist">
                                 <span class="checkmark"></span> VB, VB.net
                                 </label>
                              </div>
                           </div>
                           <div class="btn-search">
                              <button type="button" class="btn btn-block w-100">Search</button>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-12 col-lg-8 col-xl-9">
                    <div class="row">
                       <?php
                        if(count($subjects) > 0){
                        foreach($subjects as $subject){
                        $url = base_url('course-detail/'.base64_encode($subject->id));
                        ?>
                        <div class="col-sm-6 col-md-4 col-xl-4">
                          <div class="profile-widget">
                            <label class="custom_check" style="position: absolute; top: -3px; left:-3px; z-index:9">
                               <input type="checkbox" name="gender_type">
                               <span class="checkmark"></span>                                 
                            </label>
                            <div class="user-avatar">
                               <a href="<?=$url?>"><img class="img-fluid" src="<?=base_url($subject->image)?>" alt="Post Image" style="width:100%;height:246px;"></a>
                              <a href="javascript:void(0)" class="fav-btn">
                                <i class="far fa-bookmark"></i>
                              </a>
                            </div>
                            <div class="pro-content">
                              <h3 class="title">
                                <a href="<?=$url?>"><?=$subject->name?></a>
                                <i class="fas fa-check-circle verified"></i>
                              </h3>
                             
                              <!-- <div class="rating">
                                <i class="fas fa-star filled"></i>
                                <i class="fas fa-star filled"></i>
                                <i class="fas fa-star filled"></i>
                                <i class="fas fa-star filled"></i>
                                <i class="fas fa-star filled"></i>
                                <span class="d-inline-block average-rating">4.3</span>
                              </div>                               -->
                              <ul class="available-info"> 
                                <li><i class="fa fa-book"></i><?=$subject->courseName?></li>       
                                <li><i class="fa fa-clock"></i>Days <?=$subject->duration?> </li>
                                <li class="text-danger"><b>₹ <?=$subject->price==0 ? 'Free' : $subject->price;?></b> </li>
                              </ul>
                              <div class="row row-sm">
                                <div class="col-6">
                                  <a href="<?=$url?>" class="btn view-btn">View Subject</a>
                                </div>
                                <div class="col-6">
                                  <a href="<?=$url?>" class="btn book-btn">Book Now</a>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <?php } } else{ ?>
                        <div class="col-sm-6 col-md-4 col-xl-6">
                          <div class="profile-widget">
                            <div class="user-avatar">
                                <a href="javascript:void(0)"><img class="img-fluid" src="<?=base_url('public/no-data-found.avif')?>" alt="Post Image">
                            </div>
                          </div>
                        </div>
                         <?php } ?>    
                    </div> 
                  </div>
               </div>
            </div>
         </div>

<script>
  $(document).ready(function () {
    
    var courseDataTable = $('#courseDataTable').DataTable({
      "processing":true,
      "serverSide":true,
      buttons: [
      { extend: 'excelHtml5', text: 'Download Excel' }
    ],
      "order":[],
      "ajax":{
          url:"<?=base_url('Courses/ajax_course_datatable')?>",
          type:"POST"
      },
      "columnDefs":[
       {
           "targets":[0],
           "orderable":false,
       },
     ],
  
  });
  });
</script>