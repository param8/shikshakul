<div class="breadcrumb-bar">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-md-12 col-12">
        <nav aria-label="breadcrumb" class="page-breadcrumb">
          <ol class="breadcrumb">
            <li class=""><a href="<?=base_url('home')?>">Home</a></li> <b>-</b>
            <li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
          </ol>
        </nav>
        <h2 class="breadcrumb-title"><?=$page_title?></h2>
      </div>
    </div>
  </div>
</div>
<div class="content pb-0">
<section class="footer_get_touch_outer">
    <div class="container">
      <div class="footer_get_touch_inner grid-70-30">
        <div class="colmun-70 get_form">
          <div class="get_form_inner">
            <div class="get_form_inner_text">
              <h3>Get In Touch</h3>
            </div>
            <form action="#">
              <div class="grid-50-50">
                <input type="text" placeholder="First Name">
                <input type="text" placeholder="Last Name">
                <input type="email" placeholder="Email">
                <input type="tel" placeholder="Phone/Skype">
              </div>
              <div class="grid-full">
                <textarea placeholder="About Your Project" cols="30" rows="10"></textarea>
                <input type="submit" value="Submit">
              </div>
            </form>
          </div>
        </div>
        <div class="colmun-30 get_say_form">
          <h5>Say Hi!</h5>
          <ul class="get_say_info_sec">
            <li>
              <i class="fas fa-envelope"></i>
              <a href="mailto:">support@shikshkul.com</a>
            </li>
            <li>
              <i class="fas fa-phone"></i>
              <a href="tel:">+91 8962339248</a>
            </li>
            <li>
              <i class="fas fa-globe"></i>
              <a href="#">www.shikshakul.com</a> 
            </li>
          </ul>  
          
          <div class="social-icon">
                <ul class="get_say_social-icn">
                  <li>
                    <a href="" target="_blank"><i class="fab fa-facebook-f"></i> </a>
                  </li>
                  <!-- <li>
                    <a href="#" target="_blank"><i class="fab fa-twitter"></i> </a>
                  </li> -->
                  <li>
                    <a href="" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                  </li>
                  <li>
                    <a href="" target="_blank"><i class="fab fa-instagram"></i></a>
                  </li>
                  <li>
                    <a href="" target="_blank"><i class="fab fa-youtube"></i> </a>
                  </li>
                </ul>
              </div>
                  
        </div>        
      </div>
    </div>
  </section>
  <section class="mt-5">
  <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d3502.454555912569!2d77.33525647507817!3d28.616135675673778!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sB-7%2C%20Sector-65%2C%20Noida%2C%20Uttar%20Pradesh-201301!5e0!3m2!1sen!2sin!4v1688127563569!5m2!1sen!2sin" width="100%" height="500" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
  </section>
<!-- end-contant -->
</div>