<div class="breadcrumb-bar">
  <div class="container-fluid">
    <div class="row align-items-center">
      <div class="col-md-12 col-12">
        <nav aria-label="breadcrumb" class="page-breadcrumb">
          <ol class="breadcrumb">
            <li class=""><a href="<?=base_url('home')?>">Home</a></li>
            /
            <li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
          </ol>
        </nav>
        <h2 class="breadcrumb-title"><?=$page_title?></h2>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-md-6">       
        <div class="co_ur_se card p-3">
          <h3>Current Affiars</h3>
        <div class="C_list">
          <table class="table table-hover table-center mb-0" id="currentAffairDataTable">
            <thead>
              <tr>
                <th></th>
        
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>

            <?php 
             //foreach($current_affairs as $current_affair){
              ?>
            <!-- <div class="detial_item"> -->
                  <!-- <a href="javascript:void(0)" class="toggle"><span><?=$current_affair->title?></span></a> -->
                  <!-- <ul>
                      <li><a href="<?=base_url('current-affair/'.base64_encode($current_affair->id))?>"><?=$current_affair->title?></a></li>
                  </ul>
            </div> -->
          <?php //} ?>
      </div>
        </div>
        <div class="col-lg-6 col-md-6">       
        <div class="co_ur_se card p-3">
          <h3>State News</h3>
        <div class="C_list">
          <table class="table table-hover table-center mb-0" id="stateAffairDataTable">
            <thead>
              <tr>
                <th></th>
        
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>

            <?php 
             //foreach($current_affairs as $current_affair){
              ?>
            <!-- <div class="detial_item"> -->
                  <!-- <a href="javascript:void(0)" class="toggle"><span><?=$current_affair->title?></span></a> -->
                  <!-- <ul>
                      <li><a href="<?=base_url('current-affair/'.base64_encode($current_affair->id))?>"><?=$current_affair->title?></a></li>
                  </ul>
            </div> -->
          <?php //} ?>
      </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $(document).ready(function() {
     //alert('dfgfgf');
       // $('#example').DataTable();
       // } );
       var dataTable = $('#currentAffairDataTable').DataTable({
           "processing": true,
           "serverSide": true,
           buttons: [{
               extend: 'excelHtml5',
               text: 'Download Excel'
           }],
           "order": [],
           "ajax": {
               url: "<?=base_url('Home/ajaxCurrentAffair/'.base64_encode('Current Affairs'))?>",
               type: "POST"
           },
           "columnDefs": [{
               "targets": [0],
               "orderable": false,
           }, ],
       });
   });

   $(document).ready(function() {
     //alert('dfgfgf');
       // $('#example').DataTable();
       // } );
       var dataTable = $('#stateAffairDataTable').DataTable({
           "processing": true,
           "serverSide": true,
           buttons: [{
               extend: 'excelHtml5',
               text: 'Download Excel'
           }],
           "order": [],
           "ajax": {
               url: "<?=base_url('Home/ajaxCurrentAffair/'.base64_encode('State News'))?>",
               type: "POST"
           },
           "columnDefs": [{
               "targets": [0],
               "orderable": false,
           }, ],
       });
   });
   </script>