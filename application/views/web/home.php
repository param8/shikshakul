
<section class="section section-search-eight pos">
  <div class="ban_ner">
    <div class="slider">
      <div class="home_slider slick">
        <?php foreach($banners as $banner){?>
        <div class="mendor-box border-0">
          <div class="banne_r pos">
            <a href="<?=$banner->link_url?>"> <img src="<?=$banner->banner_img?>" alt="1" class="w100" height=""></a>
          </div>
        </div>
        <?php }?>
        <!--<div class="mendor-box border-0 ">
          <div class="banne_r pos">
          <div class="tag_line banner-header">
              <h1> Building futures, one success story at a time - Choose our  <br>  <span>coaching institute!</span>  </h1>
              </div>
              <img src="public/web/assets/img/homebanner/b2.png" alt="1" class="w100" height="">
          </div>
          </div>
          <div class="mendor-box border-0">
          <div class="banne_r pos">
          <div class="tag_line banner-header">
              <h1> Empowering aspirants, shaping leaders - Enroll in our civil - services  <br>  <span> coaching program!"</span>  </h1>
              </div>
              <img src="public/web/assets/img/homebanner/b3.png" alt="1" class="w100" height="">
          </div>
          </div> -->
        <!-- <div class="form-group search-info-eight loc">
          <i class="material-icons">location_city</i>
          <input type="text" class="form-control"
            placeholder="Unravel the secrets of success with our civil services coaching program!">
          </div>
          <button type="submit" class="btn search-btn-eight eg_bt mt-0">Search <i
          class="fas fa-long-arrow-alt-right"></i></button>
          </div> -->
      </div>
    </div>
    <!-- <div class="container">
      <div class="banner-wrapper-eight m-auto text-center top_b">
            <div class="banner-header aos" data-aos="fade-up">
              <h1>Search Teacher in <span>Mentoring</span> Appointment</h1>
              <p>Discover the best Mentors & institutions the city nearest to you.</p>
          </div>  
          <div class="search-box-eight aos" data-aos="fade-up">
              <form action="">
                  <div class="form-search">
                      <div class="form-inner">
                           <div class="form-group search-location-eight">
                          <i class="material-icons">my_location</i>
                          <select class="form-control select">
                          <option>Location111</option>
                          <option>Japan</option>
                          <option>France</option>
                          </select>
                      </div> 
                          <div class="form-group search-info-eight loc">
                              <i class="material-icons">location_city</i>
                              <input type="text" class="form-control"
                                  placeholder="Unravel the secrets of success with our civil services coaching program!">
                          </div>
                          <button type="submit" class="btn search-btn-eight eg_bt mt-0">Search <i
                                  class="fas fa-long-arrow-alt-right"></i></button>
                      </div>
                  </div>
              </form>
          </div>
      </div>
      </div> -->
  </div>
</section>
<section class="upssc pb60 pt60">
  <div class="container-fluid">
    <div class="section-header text-center aos aos-init aos-animate" data-aos="fade-up">
      <!-- <span>Mentoring Goals</span> -->
      <h2>Online Institute For UPPSC Aspirants!</h2>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="upssc text-center">
          <p>Shikshakul offers a comprehensive program tailored specifically for UPPSC exams. Our team of
            experienced educators, renowned subject experts, and dedicated mentors ensure that you receive
            top-notch guidance and support throughout your UPPSC preparation journey. Gain access to
            meticulously curated study materials, interactive live classes, extensive practice tests, and
            personalized doubt-solving sessions, all from the comfort of your home. Stay motivated, stay
            focused, and achieve excellence with the best online institute for UPPSC preparation. Enroll
            today and embark on your path to success!
          </p>
        </div>
      </div>
      <div class="col-md-12">
        <div class="new_btn text-center">
          <div class="right_btn1">
            <div class="search">
              <a href="<?=$this->session->userdata('email') ? base_url('course') : base_url('register');?>">Enroll Now</a>
            </div>
            <div class="search">
              <a href="<?=$this->session->userdata('email') ? base_url('course') : base_url('register');?>" class="bg_c">Request an Enquiry</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- end -->
<!-- ---start-about--- -->
<section class="learning pb60 pt60">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6">
        <div class="abou_t">
          <h2>Best Learning <span>Education Platform</span></h2>
          <p>Shikshakul is one of the best online education learning platforms. With a wide range of courses,
            expert instructors, and flexible learning options, we provide accessible and high-quality
            education to learners. Expand your knowledge, acquire new skills, and enhance your career
            prospects with our renowned platforms.
          </p>
          <div class="check_point">
            <ul>
              <li> <img src="<?=base_url('public/web/img/banner/check.png')?>" alt=""> Dive deep into
                the syllabus.
              </li>
              <li> <img src="<?=base_url('public/web/img/banner/check.png')?>" alt=""> Access
                top-notch study materials.
              </li>
              <li> <img src="<?=base_url('public/web/img/banner/check.png')?>" alt=""> Test your
                knowledge with practice quizzes.
              </li>
              <li> <img src="<?=base_url('public/web/img/banner/check.png')?>" alt=""> Learn from
                expert mentors.
              </li>
              <li> <img src="<?=base_url('public/web/img/banner/check.png')?>" alt=""> Track your
                progress and stay on top of your game!
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="abou_t">
          <img src="<?=base_url('public/web/img/banner/about.png')?>" alt="" class="w100">
        </div>
      </div>
    </div>
  </div>
</section>
<!-- ---start-about--- -->
<!-- ---Building Futures--- -->
<section class="section how-it-works-section  pb-5">
  <div class="container">
    <div class="suaacess">
      <div class="Feature_suaacess">
        <div class="section-header-eight text-center aos" data-aos="fade-up">
          <!-- <span>Mentoring Flow</span> -->
          <h2 class="mt-5">Building Futures, Fne Success Story At A Time </h2>
          <span>Choose Our Coaching Institute!</span>
        </div>
        <div class="row justify-content-center feature-list">
          <div class="col-md-12">
            <div class="slider">
              <div class="feature_slider slick">

                <?php 
                //$buildings = array('d1.png','d2.png','d3.png','d4.png','d5.png','d6.png','d7.png','d8.png',);
                foreach($buildings as $building){
              ?>
                <div class="mendor-box mb-0 aos" data-aos="fade-up">
                  <div class="feature">
                    <?php  $url = empty($building->url)  ? 'javascript:void(0)' : base_url($building->url);?>
                    <img src="<?=base_url($building->image) ?>" alt="" class="w100">
                   <div class="detai_l text-center">
                    <div class="search">
                      <a href="<?=$url?>" class="bg_c"><?=$building->button_name?></a>
                    </div>
                      <p><?=$building->description?></p>
                    </div>
                  </div>
                </div>
                <?php } ?>



                <!--  -->
              </div>
            </div>
          </div>
        </div>
      </div>
      <!------ Fne Success---end -->
      <div class="Quez_z mt-3">
        <div class="container">
          <div class="row">
            <div class="col-sm-6 col-md-6 col-lg-6 col-xl-4 col-xxl-4">
              <div class="quezz_body pos">
                <div class="test_quies">
                  <div class="panel--header  bg_color">
                    <h3> <img src="<?=('public/web/img/feature/f_icon1.png') ?>" class="w100">
                     <?=$quiz->title?>
                    </h3>
                    <!-- <ul class="quiz-page" id="quizpage">
                      <li class="active">1</li>
                      <li>2</li>
                      <li>3</li>
                      <li>4</li>
                      <li>5</li>
                    </ul> -->
                  </div>
                </div>
                <div class="panal_body">
                  <div id="quiz1"></div>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-6 col-xl-4 col-xl-4  ">
              <div class="quezz_body pos">
                <div class="test_quies">
                  <div class="panel--header bg_color ">
                    <h3><img src="<?=('public/web/img/feature/f_icon2.png') ?>" class="w100">
                      Current Affairs & Editorials
                    </h3>
                  </div>
                </div>
                <div class="panal_body"
                  style="    background-color: #fff!important;    box-shadow: 0 4px 14px rgba(185, 185, 185, .12)">
                  <div class="corent_afeirs mt-3">
                    <?php foreach($affairs as $affair){?>
                    <div class="editorial-box pl-3">
                      <span class="label bg-red "><a
                          href="<?=base_url('current-affair/'.$affair->slug)?>"><?=$affair->title?></a></span>
                      <!-- <p><a
                        href="<?//=base_url('current-affair/'.base64_encode($affair->id))?>"><?//=substr($affair->description,0,100)?></a>
                        </p> -->
                      <ul class="actions">
                        <li class="date"><?=date('d F Y',strtotime($affair->created_at))?></li>
                      </ul>
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-6 col-xl-4 col-xl-4">
              <div class="quezz_body pos">
                <div class="test_quies">
                  <div class="panel--header  bg_color">
                    <h3><img src="<?=('public/web/img/feature/f_icon3.png') ?>" class="w100"> What's
                      New
                    </h3>
                  </div>
                </div>
                <div class="panal_body q_last">
                  <?php foreach($whatsNews as $news){?>
                  <div class="editorial-box mt-3  pl-3">
                    <span class="label bg-red "> <a href="<?=!empty($news->link) ? $news->link  : 'javascript:void(0)' ?>" class="news_d m-0" <?=!empty($news->link) ? 'target="_blank"'  : '' ?>><?=$news->title?>
                      </a></span>

                    <ul class="actions m-0 mt-1">
                      <li class="date"><?=date('d F Y',strtotime($news->created_at))?></li>
                    </ul>
                  </div>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end -->
    </div>
  </div>
  </div>
</section>
<!----Building Futures end -->
<!----Topper end -->
<section class="topp_er">
  <div class="section-header-eight text-center aos aos-init aos-animate" data-aos="fade-up">
    <div class="container">
      <a href="<?=base_url('toppers-talk')?>"><h2 class="topper pt-3 pb-3">Topper's <span class="talk"><img
            src="<?=base_url('public/web/img/topper/talk.png') ?>">
          Talk</span> <span class="t_bg">Topper's Talk</span></a>
      </h2>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <!-- end -->
      <div class="mendor-slider slick">
        <?php foreach($toppers as $topper){?>
        <div class="mendor-box">
          <div class="top_pe_r">
            <div class="us_erd">
              <div class="user_l   text-center">
                  <a href="<?=$topper->video_link?>" target="_blank"><img src="<?=base_url($topper->image) ?>" alt=""></a>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>
      <!-- end -->
    </div>
</section>
<!-- end -->
<!-- start -->
<section class="mt-5 pb60">
  <div class="section-header-eight text-center aos aos-init aos-animate" data-aos="fade-up">
    <div class="container">
      <a href="<?=base_url('interview')?>"><h2 class="topper pt-3 pb-3"> <span class="talk"><img src="<?=base_url('public/web/img/topper/talk.png') ?>">
          Mock</span> InterView <span class="t_bg">Mock InterView</span></a>
      </h2>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <!-- end -->
      <div class="InterView">
        <div class="mendor-slider moc_k_interview slick">

          <?php foreach($interviews as $interview){?>
          <div class="mendor-box">
            <div class="feature moc_k">
              <!-- <div class="mock_user"> -->
              <!-- <div class="user_img text-center">
                  <img src="<?//=base_url($interview->image) ?>" alt="">
                   <div class="de_tail">
                    <h3 class="m_name"><?//=$interview->title ?> </h3>
                  </div> -
                </div> -->
              <!-- </div> -->
              <!-- <img src="<?//=base_url($interview->image) ?>" alt=""> -->
              <img src="<?=base_url($interview->image) ?>" alt="" class="w100">
              <div class="detai_l  m_o_ck text-center">
                <a href="<?=$interview->video_link?>" tabindex="-1" target="_blank">Watch Now</a>
              </div>
            </div>
          </div>
          <?php } ?>

        </div>
      </div>
      <!-- end -->
    </div>
</section>
<!-- start -->
<!----Topper end -->
<section class="topp_er pb60  ">
  <div class="section-header-eight text-center aos aos-init aos-animate" data-aos="fade-up">
    <div class="container">
      <h2 class="h_eading">Course</h2>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="course   slick">
        <?php foreach($courses as $course){?>
        <div class="mendor-box">
          <!-- end -->
          <div class="feature moc_k">
            <div class="cours_e">
              <img src="<?=base_url($course->image) ?>" alt="">
              <?php if(empty($this->session->userdata('email'))){?>
              <div class="search c_btn">
                <a href="<?=base_url('register')?>">Register</a>
              </div>
              <?php } else{?>
              <div class="search c_btn">
                <a href="javascript:void(0)" onclick="addToCart(<?=$course->id?>)">Buy Now</a>
              </div>
              <?php } ?>
            </div>
          </div>
          <!-- end -->
        </div>
        <?php } ?>
      </div>
    </div>
  </div>
</section>
<!-- end -->
<!-- ----start--- -->
<section class="facu_lty pb60  pt60 " style=" background-image:  url(<?=base_url('public/web/img/faculity_bg.png')?>);
    background-size: 100% 100%;">
  <div class="section-header-eight text-center aos aos-init aos-animate" data-aos="fade-up">
    <div class="container">
      <h2 class="h_eading  ">Meet Our Faculty</h2>
      <p class="fac_deatial col_main">Shikshakul is one of the best online education learning platforms. With a wide range
        of courses, expert instructors, and flexible learning options, we provide accessible and high-quality
        education to learners. Expand your knowledge, acquire new skills, and enhance your career prospects with
        our renowned platforms.</p>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="  faculty   slick1">
        <?php foreach($faculties as $faculty){?>
        <div class="mendor-box">
          <!-- end -->
          <div class="faculty_user text-center">
            <img src="<?=base_url($faculty->profile_pic)?>" alt="">
            <div class="de_tail">
              <h3 class="m_name"><?=$faculty->name?> </h3>
              <a href="<?=base_url('faculty-detail/'.base64_encode($faculty->id))?>"
                tabindex="0"><?=$faculty->user_subject?></a>
            </div>
          </div>
          <!-- end -->
        </div>
        <?php } ?>

      </div>
    </div>
  </div>
</section>
<!-- end -->
<!-- ----start--- -->
<section class="vide_os pb60  pt60 ">
  <div class="section-header-eight text-center aos aos-init aos-animate" data-aos="fade-up">
    <div class="container">
      <h2 class="h_eading  ">Our Most Viewed Videos</h2>
      <p class="fac_deatial col_main">Shikshakul is one of the best online education learning platforms. With a
        wide range of courses, expert instructors, and flexible learning options, we provide accessible and
        high-quality education to learners. </p>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="video-list text-center">
        <div class="row  ">
          <div class="video-slider slick">
            <?php foreach($videos as $video){?>
            <div class="mendor-box  border-0 ">
              <div class="video_box">
                <!-- <video width="100%" height="200px" controls> -->
                <iframe style="width: 97%; height: 210px; margin: auto;"
                  src="https://www.youtube.com/embed/<?=$video->video?>?autoplay=1" frameborder="0"></iframe>
                <!-- </video> -->
              </div>
            </div>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- end -->
<section class="section how-it-works-section  pb-5">
  <div class="container">
    <div class="suaacess">
      <div class="Feature_suaacess">
        <div class="section-header-eight text-center aos" data-aos="fade-up">
          <!-- <span>Mentoring Flow</span> -->
          <h2 class="mt-5">What Our Student </h2>
          <p class="sub-title"> Say About Us</p>
        </div>
        <div class="row justify-content-center feature-list">
          <div class="col-md-12">
            <div class="slider">
              <div class="feature_slider slick">
                <?php foreach($remarks as $remark){?>
                <div class="mendor-box mb-0 aos" data-aos="fade-up">
                  <div class="student ">
                    <img src="<?=base_url($remark->image) ?>" alt="" class="w100">
                  </div>

                </div>
                <?php } ?>


                <!--  -->
              </div>
            </div>
          </div>
        </div>
        <!-- end -->
        <div class="row justify-content-center">
          <div class="col-md-6">
            <div class="search_demo text-center pt-5 pb-5">
              <div class="search_d_c" v="">
                <a href="<?=$this->session->userdata('email') ? base_url('course') : base_url('register');?>"
                  class="btn  s_inpput">
                  Book Your Free Demo Now <img src="http://localhost/shikshakul/public/web/img/student/s_icon.png"
                    alt="">
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!------ Fne Success---end -->
    <!-- end -->
  </div>
  </div>
</section>
<section class="contact_detail pb60 pt60"
  style="background-image: url(<?=base_url('public/web/img/contact/bgimg.png')?>);background-size: 100% 100%;">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="footer-widget footer-contact">
          <h2 class="footer-title cont">Contact <a href="#">Information</a></h2>
          <div class="footer-contact-info">
            <p class="mb-1">
              <i class="fas fa-phone-alt"></i>
              <?=$siteinfo->site_contact?>
            </p>
            <p class="mb-1">
              <i class="fas fa-globe"></i>
              <a href="javascript:void(0)" class="_cf_email_"
                data-cfemail="1e737b706a716c7770795e7b667f736e727b307d7173"><?=$siteinfo->site_url?></a>
            </p>
            <p class="mb-1">
              <i class="fas fa-envelope"></i>
              <a href="javascript:void(0)" class="_cf_email_"
                data-cfemail="1e737b706a716c7770795e7b667f736e727b307d7173"><?=$siteinfo->site_email?></a>
            </p>
            <div class="footer-address">
              <span><i class="loc fas fa-map-marker-alt"></i></span>
              <p><?=$siteinfo->site_address?>
              </p>
            </div>
          </div>
          <!-- end -->
          <div class="footer-widget footer-about">
            <div class="footer-about-content">
              <p class="follow">Follow Us social Media</p>
              <div class="social_icon">
                <ul>
                  <li>
                    <a href="<?=$siteinfo->facebook_url?>" target="_blank"><i class="fab fa-facebook-f"></i> </a>
                  </li>
                  <!-- <li>
                    <a href="#" target="_blank"><i class="fab fa-twitter"></i> </a>
                    </li> -->
                  <li>
                    <a href="<?=$siteinfo->linkedin_url?>" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                  </li>
                  <li>
                    <a href="<?=$siteinfo->insta_url?>" target="_blank"><i class="fab fa-instagram"></i></a>
                  </li>
                  <li>
                    <a href="<?=$siteinfo->youtube_url?>" target="_blank"><i class="fab fa-youtube"></i> </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- script-start -->
<script src="https://storage.googleapis.com/chydlx/plugins/dlx-quiz/js/main.js"></script>
<script>
$("#quiz1").dlxQuiz({
  quizData: {
    questions: [
      <?php 
    $correct_ans = array();
    foreach($questions as $key=>$question){
  
      foreach($question['option'] as $key_option=>$options){
        foreach($question[$key_option]['answer'] as $key_ans=>$answer){
        $correct_ans[$question['id']] = $answer;
        }
      }
    
    ?> {
        q: "<?=$question['question']?>",
        a: "<?=$correct_ans[$question['id']]?>",
        options: [
          <?php foreach($question['option'] as $option){?> "<?=$option['option']?>",
          <?php } ?>
        ]
      },
      <?php } ?>


    ]
  }
});
</script>
