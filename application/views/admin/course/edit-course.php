<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">
      <div class="page-header">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"><?=$page_title?></h4>
              </div>
              <div class="card-body pt-0">
                <form id="editCourseform" method="post" action="<?=base_url('Course/update');?>" enctype="multipart/form-data">
                  <div class="settings-form row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Course Name <span class="star-red">*</span></label>
                        <input type="text" name="name" id="name" value="<?=$course->name?>" class="form-control" placeholder="Enter Subject Name">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Category  <span class="star-red">*</span></label>
                        <select class="form-control" name="categoryID" id="categoryID">
                          <option value="">Choose Course</option>
                          <?php
                            foreach($categories as $category){
                            ?> 
                          <option value="<?=$category->id;?>" <?=$course->categoryID == $category->id ? 'selected' : '' ?>><?=$category->name; ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Faculty<span class="star-red">*</span></label>
                        <select class="form-control js-example-basic-multiple" name="faculty[]"  id="faculty" multiple="multiple">
                          <option value="">Choose faculty</option>
                          <?php
                            foreach($faculties as $facultiy){
                            ?> 
                          <option value="<?=$facultiy->id?>" <?= in_array($facultiy->id,explode(',',$course->facultyID)) ? 'selected' : '' ?>> <?=$facultiy->name?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Price<span class="star-red">*</span></label>
                        <input type="text" class="form-control"name="price" id="price" value="<?=$course->price?>" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Start Date <span class="star-red">*</span></label>
                        <input type="text" onfocus="(this.type='date')" value="<?=$course->start_date?>" class="form-control" min="<?= date('Y-m-d'); ?>" name="start_date" id="start_date" onchange="endDateValidate(this.value)" placeholder="Start Date">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group" id="endDate_div">
                        <label>End Date<span class="star-red">*</span></label>
                        <input type="text" class="form-control"  placeholder="End Date" name="end_date" id="end_date" value="<?=$course->end_date?>">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Duration <span class="star-red">*</span></label>
                        <input type="text" class="form-control"name="duration" id="duration" value="<?=$course->duration?>" readonly>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Capacity <span class="star-red">*</span></label>
                        <input type="text" class="form-control" value="<?=$course->capacity?>" name="capacity" id="capacity" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Demo Classes </label>
                        <input type="text" class="form-control"name="demo_classes" id="demo_classes" value="<?=$course->demo_classes?>" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <p class="settings-label">Image <span class="star-red">*</span></p>
                        <div class="settings-btn">
                          <input type="file" accept="image/*" name="image" id="file" onchange="loadFile(event)" class="hide-input">
                          <label for="file" class="upload">
                          <i class="feather-upload"></i>
                          </label>
                        </div>
                        <h6 class="settings-size">Recommended image size is <span>150px x 150px</span></h6>
                        <div class="upload-images">
                          <img src="<?=base_url($course->image)?>" alt="Image">
                          <a href="javascript:void(0);" class="btn-icon logo-hide-btn">
                          <i class="feather-x-circle"></i>
                          </a>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Class Type<span class="star-red">*</span></label>
                        <select class="form-control" name="class_type"  id="class_type">
                          <option value="">Choose Class Type</option>
                          <?php $class_types = array('Live Class','Video Course','Text Course');
                            foreach($class_types as $class_type){
                            ?> 
                          <option value="<?=$class_type?>" <?=$course->class_type==$class_type ? 'selected': ''?>><?=$class_type?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div> 
                   <div class="col-md-6">
                      <div class="form-group">
                        <label>Class Link </label>
                        <input type="text" name="class_link" id="class_link" class="form-control" value="<?=$course->class_link?>" placeholder="Class Link">
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Discount Type</label>
                        <select class="form-control" name="discount_type"  id="discount_type">
                          <option value="">Discount type</option>
                          <?php $discount_types = array('Percentage','Amount');
                            foreach($discount_types as $discount_type){
                            ?> 
                          <option value="<?=$discount_type?>" <?=$course->discount_type==$discount_type ? 'selected': ''?>><?=$discount_type?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div> 
                   <div class="col-md-6">
                      <div class="form-group">
                        <label>Discount </label>
                        <input type="text" name="discount" id="discount" class="form-control" value="<?=$course->discount?>" placeholder="Discount" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Description <span class="star-red">*</span></label>
                        <textarea  class="form-control"name="description" id="description" ><?=$course->description?></textarea>
                      </div>
                    </div>
                    <div class="form-group mb-0">
                      <div class="settings-btns">
                        <button type="submit" class="btn btn-orange">Edit</button>
                        <!-- <button type="reset" class="btn btn-grey">Cancel</button> -->
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $("form#editCourseform").submit(function(e) {
  			$(':input[type="submit"]').prop('disabled', true);
  			e.preventDefault();    
  			var formData = new FormData(this);
        formData.append("courseID", '<?=$course->id?>')
  			$.ajax({
  			url: $(this).attr('action'),
  			type: 'POST',
  			data: formData,
  			cache: false,
  			contentType: false,
  			processData: false,
  			dataType: 'json',
  			success: function (data) { 
  				if(data.status==200) {
  				//$('.modal').modal('hide');
          toastNotif({
  				text: data.message,
  				color: '#5bc83f',
  				timeout: 5000,
  				icon: 'valid'
  			});
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function(){
  
          location.href="<?=base_url('courses')?>"; 	
          
        }, 1000) 		
  				}else if(data.status==403) {
            toastNotif({
                  text: data.message,
                  color: '#da4848',
                  timeout: 5000,
                  icon: 'error'
                });
  
            $(':input[type="submit"]').prop('disabled', false);
  				}else{
            toastNotif({
                text: data.message,
                color: '#da4848',
                timeout: 5000,
                icon: 'error'
              });
            $(':input[type="submit"]').prop('disabled', false);
  				}
  			},
  			error: function(){} 
  			});
  		});

      function endDateValidate(start_date){
        $('#endDate_div').html(' <label>End Date<span class="star-red">*</span></label><input type="date" class="form-control" min="'+start_date+'" onchange="get_duration(this.value)" name="end_date" id="end_date" value="<?=$subject->end_date?>" placeholder="End Date">');
      }
  
      function get_duration(endDate){
        var startDate = $('#start_date').val();
        $.ajax({
  			url: "<?=base_url('Ajax_controller/get_course_duration')?>",
  			type: 'POST',
  			data: {startDate,endDate},
  			dataType: 'json',
  			success: function (data) { 
  				$('#duration').val(data.duration);
  			},
  			error: function(){} 
  			});
        }

        $('#description').summernote({
            height:500,
        });
</script>