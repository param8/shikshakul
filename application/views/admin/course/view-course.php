<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">

      <div class="page-header">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"><?=$page_title?></h4>
              </div>
              <div class="card-body pt-0">
                <form>
                  <div class="settings-form row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Subject Name <span class="star-red">*</span></label>
                        <input type="text" name="name" id="name" value="<?=$course->name?>" class="form-control"
                          placeholder="Enter Course Name" readonly>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Category <span class="star-red">*</span></label>
                        <input type="text" value="<?=$course->categoryName?>" class="form-control"
                          placeholder="Enter Subject Name" readonly>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Faculty<span class="star-red">*</span></label>
                        <select class="form-control js-example-basic-multiple" name="faculty[]" id="faculty"
                          multiple="multiple" disabled>
                          <option value="">Choose faculty</option>
                          <?php
                        foreach($faculties as $facultiy){
                        ?>
                          <option value="<?=$facultiy->id?>"
                            <?= in_array($facultiy->id,explode(',',$course->facultyID)) ? 'selected' : '' ?>>
                            <?=$facultiy->name?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Price<span class="star-red">*</span></label>
                        <input type="text" class="form-control" name="price" id="price" value="<?=$course->price?>"
                          readonly>
                      </div>
                    </div>




                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Start Date <span class="star-red">*</span></label>
                        <input type="text" value="<?=$course->start_date?>" class="form-control"
                          placeholder="Start Date" readonly>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group" id="endDate_div">
                        <label>End Date<span class="star-red">*</span></label>
                        <input type="text" class="form-control" placeholder="End Date" value="<?=$course->end_date?>"
                          readonly>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Duration <span class="star-red">*</span></label>
                        <input type="text" class="form-control" value="<?=$course->duration?>" readonly>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Capacity <span class="star-red">*</span></label>
                        <input type="text" class="form-control" value="<?=$course->capacity?>" readonly>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Demo Classes </label>
                        <input type="text" class="form-control" name="demo_classes" id="demo_classes"
                          value="<?=$course->demo_classes?>" readonly>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <p class="settings-label">Image <span class="star-red">*</span></p>
                        <div class="upload-images">
                          <img src="<?=base_url($course->image)?>" alt="Image">
                          <a href="javascript:void(0);" class="btn-icon logo-hide-btn">
                            <i class="feather-x-circle"></i>
                          </a>
                        </div>
                      </div>
                    </div>


                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Description <span class="star-red">*</span></label>
                        <textarea type="text" class="form-control" id="description" value=""
                          readonly><?=$course->description?></textarea>
                      </div>
                    </div>

                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


</div>
<script>
$('#description').summernote({
            height:500,
        });
</script>