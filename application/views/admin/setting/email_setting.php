<div class="card">
  
<form id="product_form" method="post" action="<?=base_url('store-email-form');?>">
<div class="row col-md-12">
   <div class="col-md-6">      
         <div class="card-header d-flex justify-content-between align-items-center">
                <h5 class="card-title">PHP Mail</h5>
                <div class="status-toggle d-flex justify-content-between align-items-center">
                    <input type="checkbox" id="status" name="status" class="check" <?php ($emailinfo->status==1)?'checked':''?>>
                    <label for="status" class="checktoggle">checkbox</label>
                </div>
            </div>
         <div class="card-body pt-0">            
               <div class="settings-form">
                  <div class="form-group form-placeholder">
                     <label>Email From Address <span class="star-red">*</span></label>
                     <input type="text" class="form-control" name="from_address" value="<?=$emailinfo->email_address?>">
                  </div>
                  <div class="form-group form-placeholder">
                     <label>Email Password <span class="star-red">*</span></label>
                     <input type="text" class="form-control" name="password" value="<?=$emailinfo->email_pswrd?>">
                  </div>
                  <div class="form-group form-placeholder">
                     <label>Emails From Name <span class="star-red">*</span></label>
                     <input type="text" class="form-control" name="from_name" value="<?=$emailinfo->email_name?>">
                  </div>
                  <div class="form-group mb-0">
                     <div class="settings-btns">
                        <button type="submit" class="btn btn-orange">Submit</button>
                        <button type="submit" class="btn btn-grey">Cancel</button>
                     </div>
                  </div>
               </div>            
           </div>
      </div>
    <div class="col-md-6">
            <div class="card-header d-flex justify-content-between align-items-center">
                    <h5 class="card-title">SMTP</h5>                    
            </div>
                <div class="card-body pt-0">            
                <div class="settings-form">
                    <div class="form-group form-placeholder">
                        <label>Email User name <span class="star-red">*</span></label>
                        <input type="text" class="form-control" name="username" value="<?=$emailinfo->user_name?>">
                    </div>
                    <div class="form-group form-placeholder">
                        <label>Email Host <span class="star-red">*</span></label>
                        <input type="text" class="form-control" name="host" value="<?=$emailinfo->email_host?>">
                    </div>
                    <div class="form-group form-placeholder">
                        <label>Email Port <span class="star-red">*</span></label>
                        <input type="text" class="form-control" name="port" value="<?=$emailinfo->email_port?>">
                    </div>
                </div>
                </div>
        </div>
    </div>
            </form>
      </div>
   </div>
</div>
</div>
</div>
</div>
</div>
<script>
$("form#product_form").submit(function(e) {
			$(':input[type="submit"]').prop('disabled', true);
			e.preventDefault();    
			var formData = new FormData(this);
			$.ajax({
			url: $(this).attr('action'),
			type: 'POST',
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			dataType: 'json',
			success: function (data) { 
				if(data.status==200) {
				//$('.modal').modal('hide');
        toastNotif({
				text: data.message,
				color: '#5bc83f',
				timeout: 5000,
				icon: 'valid'
			});
      $(':input[type="submit"]').prop('disabled', false);
      setTimeout(function(){

        location.href="<?=base_url('email-setting')?>"; 	
        
      }, 1000) 		
				}else if(data.status==403) {
          toastNotif({
                text: data.message,
                color: '#da4848',
                timeout: 5000,
                icon: 'error'
              });

          $(':input[type="submit"]').prop('disabled', false);
				}else{
          toastNotif({
              text: data.message,
              color: '#da4848',
              timeout: 5000,
              icon: 'error'
            });
          $(':input[type="submit"]').prop('disabled', false);
				}
			},
			error: function(){} 
			});
		});


</script>