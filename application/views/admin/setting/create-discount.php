<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">
      <div class="page-header">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"><?=$page_title?></h4>
              </div>
              <div class="card-body pt-0">
                <form id="createDiscountform" method="post" action="<?=base_url('Setting/store_discount');?>" enctype="multipart/form-data">
                  <div class="settings-form row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Title<span class="star-red">*</span></label>
                        <input type="text" name="title" id="title" class="form-control" placeholder="Enter Title">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Courses <span class="star-red">*</span></label>
                        <select class="form-control" name="courseID" id="courseID">
                          <option value="">Choose Course</option>
                          <?php
                            foreach($courses as $course){
                            ?> 
                          <option value="<?=$course->id; ?>"><?=$course->name; ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Discount Type<span class="star-red">*</span></label>
                        <select class="form-control" name="discount_type"  id="discount_type" onchange="get_discount_type(this.value)">
                          <option value="">Discount type</option>
                          <?php $discount_types = array('Percentage','Amount');
                            foreach($discount_types as $discount_type){
                            ?> 
                          <option value="<?=$discount_type?>"><?=$discount_type?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div> 

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Discount <span id="discount_type_id"></span><span class="star-red">*</span></label>
                        <input type="text" name="discount" id="discount" class="form-control" placeholder="Discount" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Min Order </label>
                        <input type="text" name="min_order" id="min_order" class="form-control" placeholder="Min Order" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Coupon Code<span class="star-red">*</span></label>
                        <input type="text" class="form-control"name="coupon" id="coupon" placeholder="Coupon Code" >
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Expiry Date <span class="star-red">*</span></label>
                        <input type="text" onfocus="(this.type='date')" class="form-control" min="<?= date('Y-m-d'); ?>" name="expiry_date" id="expiry_date"  placeholder="Expiry Date">
                      </div>
                    </div>
                    
                    <div class="form-group mb-0">
                      <div class="settings-btns">
                        <button type="submit" class="btn btn-orange">Submit</button>
                        <!-- <button type="reset" class="btn btn-grey">Cancel</button> -->
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $("form#createDiscountform").submit(function(e) {
  			$(':input[type="submit"]').prop('disabled', true);
  			e.preventDefault();    
  			var formData = new FormData(this);
  			$.ajax({
  			url: $(this).attr('action'),
  			type: 'POST',
  			data: formData,
  			cache: false,
  			contentType: false,
  			processData: false,
  			dataType: 'json',
  			success: function (data) { 
  				if(data.status==200) {
  				//$('.modal').modal('hide');
          toastNotif({
  				text: data.message,
  				color: '#5bc83f',
  				timeout: 5000,
  				icon: 'valid'
  			});
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function(){
  
          location.href="<?=base_url('discount')?>"; 	
          
        }, 1000) 		
  				}else if(data.status==403) {
            toastNotif({
                  text: data.message,
                  color: '#da4848',
                  timeout: 5000,
                  icon: 'error'
                });
  
            $(':input[type="submit"]').prop('disabled', false);
  				}else{
            toastNotif({
                text: data.message,
                color: '#da4848',
                timeout: 5000,
                icon: 'error'
              });
            $(':input[type="submit"]').prop('disabled', false);
  				}
  			},
  			error: function(){} 
  			});
  		});
  
     function get_discount_type(type){
      if(type=='Percentage'){
        $('#discount_type_id').html('in Percentage(%)');
      }else if(type=='Amount') {
        $('#discount_type_id').html('in Fixed Amount(₹)');
      }else{
        $('#discount_type_id').html('');
      }
     }
</script>