<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header d-flex justify-content-between align-items-center">
        <h5 class="card-title">Privacy Policy Form</h5>
      </div>
      <div class="card-body pt-0">
        <form id="privacy_form" action="<?=base_url('store-privacy-form');?>" method="post">
          <div class="settings-form">
            <div class="form-group form-placeholder">
              <label>Privacy Policy Detail <span class="star-red">*</span></label>
              <textarea name="privacy" id="privacy"><?=$siteinfo->privacy_policy?></textarea>
            </div>
            
            <div class="form-group mb-0">
              <div class="settings-btns">
                <button type="submit" class="btn btn-orange">Save</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
$("form#privacy_form").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        //$('.modal').modal('hide');
        toastNotif({
          text: data.message,
          color: '#5bc83f',
          timeout: 5000,
          icon: 'valid'
        });
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {

          location.href = "<?=base_url('privacy-policy')?>";

        }, 1000)
      } else if (data.status == 403) {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});

$('#privacy').summernote({
            height:500,
        });
</script>