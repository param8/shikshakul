        <div class="row">
          <div class="col-md-6">
            <div class="card">
              <div class="card-header">
                <h5 class="card-title">SEO Settings</h5>
              </div>
              <div class="card-body pt-0">
                <form id="product_form" action="<?=base_url('store-seo-form');?>" method="post">
                  <div class="settings-form">
                    <div class="form-group form-placeholder">
                      <label>Meta Title <span class="star-red">*</span></label>
                      <input type="text" class="form-control" name="meta_title" value="<?=$seo->meta_title;?>">
                    </div>
                    <div class="form-group form-placeholder">
                      <label>Image Alt Text <span class="star-red">*</span></label>
                      <input type="text" class="form-control" name="img_alt_text" value="<?=$seo->img_alt_text;?>">
                    </div>
                    <div class="form-group">
                      <label>Meta Keywords <span class="star-red">*</span></label>
                      <input type="text" data-role="tagsinput" class="input-tags form-control"
                        placeholder="Meta Keywords" name="keywords" id="keywords" value="<?=$seo->meta_keywords;?>">
                    </div>
                    <div class="form-group">
                      <label>Meta Description <span class="star-red">*</span></label>
                      <textarea class="form-control" name="description"><?=$seo->meta_description;?></textarea>
                    </div>
                    <div class="form-group mb-0">
                      <div class="settings-btns">
                        <button type="submit" class="btn btn-orange">Submit</button>
                        <button type="reset" class="btn btn-grey">Cancel</button>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        </div>
        </div>
        </div>
        </div>

        <script>
$("form#product_form").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        //$('.modal').modal('hide');
        toastNotif({
          text: data.message,
          color: '#5bc83f',
          timeout: 5000,
          icon: 'valid'
        });
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {

          location.href = "<?=base_url('seo-setting')?>";

        }, 1000)
      } else if (data.status == 403) {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});
        </script>