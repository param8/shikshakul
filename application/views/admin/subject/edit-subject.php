<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">
      <div class="page-header">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"><?=$page_title?></h4>
              </div>
              <div class="card-body pt-0">
                <form id="editSubjectform" method="post" action="<?=base_url('Subject/update');?>" enctype="multipart/form-data">
                  <div class="settings-form row">
                    
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Category <span class="star-red">*</span></label>
                        <select class="form-control" name="categoryID" id="categoryID" onchange="get_courses(this.value)">
                          <option value="">Choose Category</option>
                          <?php
                            foreach($categories as $category){
                            ?> 
                          <option value="<?=$category->id; ?>" <?=$subject->categoryID==$category->id ? 'selected': ''?>><?=$category->name; ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Course<span class="star-red">*</span></label>
                        <select class="form-control" name="courseID"  id="courseID" >
                          <option value="">Choose Course</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Subject Name <span class="star-red">*</span></label>
                        <input type="text" name="name" id="name" class="form-control" value="<?=$subject->name?>" placeholder="Enter Subject Name">
                      </div>
                    </div>
                   
                    <div class="col-md-6">
                      <div class="form-group">
                        <p class="settings-label">Image <span class="star-red">*</span></p>
                        <div class="settings-btn">
                          <input type="file" accept="image/*" name="image" id="file" onchange="loadFile(event)" class="hide-input">
                          <label for="file" class="upload">
                          <i class="feather-upload"></i>
                          </label>
                        </div>
                        <h6 class="settings-size">Recommended image size is <span>150px x 150px</span></h6>
                        <div class="upload-images">
                          <img src="<?=base_url($subject->image)?>" alt="Image">
                          <a href="javascript:void(0);" class="btn-icon logo-hide-btn">
                          <i class="feather-x-circle"></i>
                          </a>
                        </div>
                      </div>
                    </div>
                    <div class="form-group mb-0">
                      <div class="settings-btns">
                        <button type="submit" class="btn btn-orange">Submit</button>
                        <!-- <button type="reset" class="btn btn-grey">Cancel</button> -->
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $("form#editSubjectform").submit(function(e) {
  			$(':input[type="submit"]').prop('disabled', true);
  			e.preventDefault();    
  			var formData = new FormData(this);
        formData.append("subjectID", '<?=$subject->id?>')
  			$.ajax({
  			url: $(this).attr('action'),
  			type: 'POST',
  			data: formData,
  			cache: false,
  			contentType: false,
  			processData: false,
  			dataType: 'json',
  			success: function (data) { 
  				if(data.status==200) {
  				//$('.modal').modal('hide');
          toastNotif({
  				text: data.message,
  				color: '#5bc83f',
  				timeout: 5000,
  				icon: 'valid'
  			});
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function(){
  
          location.href="<?=base_url('subjects')?>"; 	
          
        }, 1000) 		
  				}else if(data.status==403) {
            toastNotif({
                  text: data.message,
                  color: '#da4848',
                  timeout: 5000,
                  icon: 'error'
                });
  
            $(':input[type="submit"]').prop('disabled', false);
  				}else{
            toastNotif({
                text: data.message,
                color: '#da4848',
                timeout: 5000,
                icon: 'error'
              });
            $(':input[type="submit"]').prop('disabled', false);
  				}
  			},
  			error: function(){} 
  			});
  		});
  
      
   

      function get_courses(categoryID){
        var subjectID = <?=$subject->id?>;
        $.ajax({
        url: '<?=base_url('Ajax_controller/get_courses')?>',
        type: 'POST',
        data: {categoryID,subjectID},
        success: function (data) {
          $('#courseID').html(data);
        }
        });
      }

      $( document ).ready(function() {
        get_courses(<?=$subject->categoryID?>)
     });
</script>