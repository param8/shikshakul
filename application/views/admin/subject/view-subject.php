<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">
      <div class="page-header">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"><?=$page_title?></h4>
              </div>
              <div class="card-body pt-0">
                <form id="createSubjectform" method="post" action="<?=base_url('Subject/store');?>" enctype="multipart/form-data">
                  <div class="settings-form row">
                    
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Category <span class="star-red">*</span></label>
                        <select class="form-control" name="categoryID" id="categoryID" onchange="get_courses(this.value)" disabled>
                          <option value="">Choose Category</option>
                          <?php
                            foreach($categories as $category){
                            ?> 
                          <option value="<?=$category->id; ?>" <?=$subject->categoryID==$category->id ? 'selected': ''?>><?=$category->name; ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Course<span class="star-red">*</span></label>
                        <select class="form-control" name="courseID"  id="courseID" disabled>
                          <option value="">Choose Course</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Subject Name <span class="star-red">*</span></label>
                        <input type="text" name="name" id="name" class="form-control" value="<?=$subject->name?>" placeholder="Enter Subject Name" disabled>
                      </div>
                    </div>
                   
                    <div class="col-md-6">
                      <div class="form-group">
                        <p class="settings-label">Image <span class="star-red">*</span></p>
                        <!-- <div class="settings-btn">
                          <input type="file" accept="image/*" name="image" id="file" onchange="loadFile(event)" class="hide-input">
                          <label for="file" class="upload">
                          <i class="feather-upload"></i>
                          </label>
                        </div> -->
                        <!-- <h6 class="settings-size">Recommended image size is <span>150px x 150px</span></h6> -->
                        <div class="upload-images">
                          <img src="<?=base_url($subject->image)?>" alt="Image">
                          <!-- <a href="javascript:void(0);" class="btn-icon logo-hide-btn">
                          <i class="feather-x-circle"></i>
                          </a> -->
                        </div>
                      </div>
                    </div>
                    <!-- <div class="form-group mb-0">
                      <div class="settings-btns">
                        <button type="submit" class="btn btn-orange">Submit</button>
                        <button type="reset" class="btn btn-grey">Cancel</button> 
                      </div>
                    </div> -->
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>

      function get_courses(categoryID){
        var subjectID = <?=$subject->id?>;
        $.ajax({
        url: '<?=base_url('Ajax_controller/get_courses')?>',
        type: 'POST',
        data: {categoryID,subjectID},
        success: function (data) {
          $('#courseID').html(data);
        }
        });
      }

      $( document ).ready(function() {
        get_courses(<?=$subject->categoryID?>)
     });
</script>