<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">
      <div class="page-header">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"><?=$page_title?></h4>
              </div>
              <div class="card-body pt-0">
                <form id="editTestSeriesform" method="post" action="<?=base_url('Test_series/update_test_series');?>"
                  enctype="multipart/form-data">
                  <div class="settings-form row">

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Test Series Title<span class="star-red">*</span></label>
                        <input type="text" class="form-control" name="title" id="title" value="<?=$testSeries->title?>" placeholder="Test Series Title" readonly>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Type<span class="star-red">*</span></label>
                        <select class="form-control" name="series_type" id="series_type" readonly>
                          <?php $series_type = array('Prelims','Mains');?>
                          <option value="">Select Type</option>
                          <?php foreach($series_type as $type){?>
                            <option value="<?=$type?>" <?=$testSeries->series_type==$type ? 'selected' : ''?>><?=$type?></option>
                            <?php } ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Course<span class="star-red">*</span></label>
                        <select class="form-control" name="courseID" id="courseID" onchange="get_subject(this.value)" readonly>
                          <option value="">Select Course</option>
                          <?php foreach($courses as $course){?>
                          <option value="<?=$course->id?>" <?=$testSeries->courseID==$course->id ? 'selected' : ''?>><?=$course->name?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Subject<span class="star-red">*</span></label>
                        <select class="form-control" name="subjectID" id="subjectID" readonly>
                          <option value="">Select Subject</option>

                        </select>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Price<span class="star-red">*</span></label>
                        <input type="text" class="form-control" name="price" id="price" value="<?=$testSeries->price?>" placeholder="Price" readonly>
                      </div>
                    </div>
                     <!-- 
                    <div class="form-group mb-0">
                      <div class="settings-btns">
                        <button type="submit" class="btn btn-orange">Submit</button>
                      
                      </div>
                    </div> -->
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>



function get_subject(courseID,testSeriesID) {
  $.ajax({
    url: '<?=base_url('Ajax_controller/get_subject')?>',
    type: 'POST',
    data: {
      courseID,
      testSeriesID
    },
    success: function(data) {
      $('#subjectID').html(data);
    }
  });
}
$(document).ready(function() {
  get_subject(<?=$testSeries->courseID?>, <?=$testSeries->id?>);
});
</script>