<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">
      <div class="page-header">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"><?=$page_title?></h4>
              </div>
              <div class="card-body pt-0">
                <form id="createTestSeriesform" method="post" action="<?=base_url('Test_series/store_test_series');?>"
                  enctype="multipart/form-data">
                  <div class="settings-form row">

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Test Series Title<span class="star-red">*</span></label>
                        <input type="text" class="form-control" name="title" id="title" placeholder="Test Series Title">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Type<span class="star-red">*</span></label>
                        <select class="form-control" name="series_type" id="series_type">
                          <?php $series_type = array('Prelims','Mains');?>
                          <option value="">Select Type</option>
                          <?php foreach($series_type as $type){?>
                            <option value="<?=$type?>"><?=$type?></option>
                            <?php } ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Course<span class="star-red">*</span></label>
                        <select class="form-control" name="courseID" id="courseID" onchange="get_subject(this.value)">
                          <option value="">Select Course</option>
                          <?php foreach($courses as $course){?>
                          <option value="<?=$course->id?>"><?=$course->name?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Subject<span class="star-red">*</span></label>
                        <select class="form-control" name="subjectID" id="subjectID">
                          <option value="">Select Subject</option>

                        </select>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Price<span class="star-red">*</span></label>
                        <input type="text" class="form-control" name="price" id="price" placeholder="Price" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                      </div>
                    </div>

                    <div class="form-group mb-0">
                      <div class="settings-btns">
                        <button type="submit" class="btn btn-orange">Submit</button>
                        <!-- <button type="reset" class="btn btn-grey">Cancel</button> -->
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$("form#createTestSeriesform").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        //$('.modal').modal('hide');
        toastNotif({
          text: data.message,
          color: '#5bc83f',
          timeout: 5000,
          icon: 'valid'
        });
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {

          location.href = "<?=base_url('test-seriess')?>";

        }, 1000)
      } else if (data.status == 403) {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});


function get_subject(courseID) {
  $.ajax({
    url: '<?=base_url('Ajax_controller/get_subject')?>',
    type: 'POST',
    data: {
      courseID
    },
    success: function(data) {
      $('#subjectID').html(data);
    }
  });
}
</script>