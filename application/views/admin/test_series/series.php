<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">
      <div class="page-header">
        <div class="row">
          <div class="col-sm-6">
            <h3 class="page-title"><?=$page_title?></h3>
            <ul class="breadcrumb">
              <li><a href="<?=base_url('dashboard')?>">Dashboard/</a></li>
              <li class="breadcrumb-item"><a href="javascript:(0);"><?=$page_title?></a></li>
            </ul>
          </div>
          <div class="col-sm-3">
            <!-- <div class="float-right">
              <button type="button" class="btn btn-warning btn-sm" onclick="open_model()" style="float: right">Upload
                Quiz</button>
            </div> -->
          </div>
          <div class="col-sm-3">
            <div class="float-left">
              <a type="button" class="btn btn-primary btn-sm" href="<?=base_url('create-series/'.base64_encode($testSeriesID))?>"
                style="float: right">Create <?=$page_title?></a>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <table class=" table table-hover table-center mb-0" id="seriesDataTable">
                  <thead>
                    <tr>
                      <th>S.no.</th>
                      <th>Test Series</th>
                      <th>Series</th>
                      <th>Type</th>
                      <th>Created Date</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<script>
$(document).ready(function() {
  //alert('dfgfgf');
  // $('#example').DataTable();
  // } );
  var dataTable = $('#seriesDataTable').DataTable({
    "processing": true,
    "serverSide": true,
    buttons: [{
      extend: 'excelHtml5',
      text: 'Download Excel'
    }],
    "order": [],
    "ajax": {
      url: "<?=base_url('Test_series/ajaxSeries/'.$testSeriesID)?>",
      type: "POST"
    },
    "columnDefs": [{
      "targets": [0],
      "orderable": false,
    }, ],
  });
});



function delete_series(id) {

  Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
    if (result.value) {
      $.ajax({
        url: '<?=base_url('Test_series/delete_series')?>',
        type: 'POST',
        data: {
          id
        },
        dataType: 'json',
        success: function(data) {
          if (data.status == 200) {
            toastNotif({
              text: data.message,
              color: '#5bc83f',
              timeout: 5000,
              icon: 'valid'
            });
            setTimeout(function() {

              location.href = "<?=base_url('series/'.base64_encode($testSeriesID))?>"

            }, 1000)


          } else if (data.status == 302) {
            toastNotif({
              text: data.message,
              color: '#da4848',
              timeout: 5000,
              icon: 'error'
            });

          }
        }
      })
    }
  })
}

function get_subject(courseID) {
  $.ajax({
    url: '<?=base_url('Ajax_controller/get_subject')?>',
    type: 'POST',
    data: {
      courseID
    },
    success: function(data) {
      $('#subjectID').html(data);
    }
  });
}
</script>