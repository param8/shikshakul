<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">
      <div class="page-header">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"><?=$page_title?></h4>
              </div>
              <div class="card-body pt-0">
                <form id="editSeriesform" method="post" action="<?=base_url('Test_series/update_series');?>"
                  enctype="multipart/form-data">
                  <div class="settings-form row">

                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Test Series<span class="star-red">*</span></label>
                        <input type="text" class="form-control" name="test_series_title" id="test_series_title"
                          value="<?=$series->testSeries?>" readonly>
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Series Title<span class="star-red">*</span></label>
                        <input type="text" class="form-control" name="title" id="title" value="<?=$series->title?>" placeholder="Series Title" >
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Series Mode<span class="star-red">*</span></label>
                        <?php $modes = array('series_mode_sub'=>'Subjective','series_mode_obj'=>'Objective')?>
                        <div>
                          <?php 
                           $series_mode_sub = explode(',',$series->series_mode);
                          foreach($modes as $key=>$mode){?>
                            <input type="checkbox" class="" name="<?=$key?>" id="<?=$key?>"
                              value="<?=$mode?>" <?= in_array($mode,$series_mode_sub) ? 'checked' : ''?> onclick="get_subjective_objective()"> <?=$mode?>
                          <?php } ?>
                          
                        </div>
                      </div>
                    </div>
                    <div id="subjective_id" style="display:none">
                    <div class="col-md-6" >
                      <div class="form-group">
                        <label>Subjective</label>
                        <input type="file" class="form-control" name="subjective" id="subjective" placeholder="">
                      </div>
                    </div>
                    <?php if(!empty($series->subjective)){?>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Subjective</label>
                        <div>
                        <a href="<?=base_url($series->subjective)?>" class="btn btn-primary" target="_blank">View Subjective</a>
                        </div>
                      </div>
                    </div>
                    <?php } ?>
                    </div>
                    <hr>
                    <div class="col-md-12" id="objective_id" style="display:none">
                      <h5 class="settings-label">Objective Questions </h5>
                      <div class="quiz-form">
                        <div class="links-info">
                          <div class="row form-row links-cont">

                           <?php 
                           if(in_array('Objective',$series_mode_sub)){

                           $answer_correct = array();
                           foreach($questions as $key=>$question){

                            ?>
                            <div class="col-12 col-md-12">
                            <p class="settings-label">Questions <?=$key+1;?></p>
                              <div class="form-group form-placeholder d-flex">
                                <textarea  class="form-control" name="question[<?=$key?>]"  placeholder="question" ><?=$question['question']?></textarea>
                              </div>
                            </div>
                            <?php 
                             $option_key=array();
                            foreach($question['option'] as $key_options=>$options){
                              $key_option = $key_options+1;
                              ?>
                            <div class="col-6 col-md-6">
                              <p class="settings-label">Option <?=$key_option;?> </p>
                              <div class="form-group form-placeholder d-flex">
                                <input type="text" class="form-control" name="option[question<?=$key_option?><?=$key?>]" value="<?=$options['option']?>"  placeholder="Option 1" >
                              </div>
                            </div>
                            <?php 
                            
                            foreach($question[$key_options]['answer'] as $key_ans=>$answer){
                              $answer_correct[$question['id']] = $answer;
                              } 
                            }
                                ?>
                            <div class="col-6 col-md-6">
                              <p class="settings-label">Answer </p>
                              <div class="form-group form-placeholder d-flex">
                                <input type="text" class="form-control" name="answer[question<?=$key?>]" value="<?=$answer_correct[$question['id']]?>" placeholder="Answer" >
                              </div>
                            </div>
                          
                            <hr>
                            <?php } } else{?>
                            <div class="col-12 col-md-12" >
                              <p class="settings-label">Questions </p>
                              <div class="form-group form-placeholder d-flex">
                                <textarea class="form-control" name="question[0]" placeholder="question"></textarea>
                              </div>
                            </div>
                            <div class="col-6 col-md-6">
                              <p class="settings-label">Option 1 </p>
                              <div class="form-group form-placeholder d-flex">
                                <input type="text" class="form-control" name="option[question10]"
                                  placeholder="Option 1">
                              </div>
                            </div>
                            <div class="col-6 col-md-6">
                              <p class="settings-label">Option 2 </p>
                              <div class="form-group form-placeholder d-flex">
                                <input type="text" class="form-control" name="option[question20]"
                                  placeholder="Option 2">
                              </div>
                            </div>
                            <div class="col-6 col-md-6">
                              <p class="settings-label">Option 3 </p>
                              <div class="form-group form-placeholder d-flex">
                                <input type="text" class="form-control" name="option[question30]"
                                  placeholder="Option 3">
                              </div>
                            </div>
                            <div class="col-6 col-md-6">
                              <p class="settings-label">Option 4 </p>
                              <div class="form-group form-placeholder d-flex">
                                <input type="text" class="form-control" name="option[question40]"
                                  placeholder="Option 4">
                              </div>
                            </div>
                            <div class="col-6 col-md-6">
                              <p class="settings-label">Answer </p>
                              <div class="form-group form-placeholder d-flex">
                                <input type="text" class="form-control" name="answer[question0]" placeholder="Answer">
                              </div>
                            </div>
                            <div class="col-12 col-md-1">
                              <p class="settings-label">Remove </p>
                              <a href="#" class="btn trash btn-danger ml-5">
                                <i class="feather-trash-2"></i>
                              </a>
                            </div>
                            <?php } ?>
                          
                          </div>
                        </div>
                      </div>
                      <?php if(!in_array('Objective',$series_mode_sub)){?>
                      <div class="form-group">
                        <a href="javascript:void(0);" class="btn add-links">
                        <i class="fa fa-plus me-1"></i> Add More
                        </a>
                      </div>
                      <?php } ?>
                    </div>

                    <div class="form-group mb-0">
                      <div class="settings-btns">
                        <button type="submit" class="btn btn-orange">Submit</button>
                        <!-- <button type="reset" class="btn btn-grey">Cancel</button> -->
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$("form#editSeriesform").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  formData.append("testSeriesID", '<?=$series->testSeriesID?>');
  formData.append("seriesID", '<?=$series->id?>');
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        //$('.modal').modal('hide');
        toastNotif({
          text: data.message,
          color: '#5bc83f',
          timeout: 5000,
          icon: 'valid'
        });
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {

          location.href = "<?=base_url('series/'.base64_encode($series->testSeriesID))?>";

        }, 1000)
      } else if (data.status == 403) {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});


function get_subjective_objective(){
 var subjective = $('#series_mode_sub').is(":checked");
 var objective = $('#series_mode_obj').is(":checked");
if(subjective == true && objective == true){
  $('#objective_id').show();
  $('#subjective_id').show();
}

if(subjective == false && objective == false){
  $('#objective_id').hide();
  $('#subjective_id').hide();
}

if(subjective == true && objective == false){
  $('#objective_id').hide();
  $('#subjective_id').show();
}

if(subjective == false && objective == true){
  $('#objective_id').show();
  $('#subjective_id').hide();
}

}

$( document ).ready(function() {
  get_subjective_objective();
});
</script>