<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">
      <div class="page-header">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"><?=$page_title?></h4>
              </div>
              <div class="card-body pt-0">
                <form id="editCategoryform" method="post" action="<?=base_url('Category/update');?>" enctype="multipart/form-data">
                  <div class="settings-form row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Course Name <span class="star-red">*</span></label>
                        <input type="text" name="name" id="name" class="form-control" value="<?=$category->name?>" placeholder="Enter Course Name">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Language <span class="star-red">*</span></label>
                        <!-- <input type="text" name="language" id="language" class="form-control" value="<?=$category->language?>" placeholder="Enter Language"> -->
                        <select class="form-control" name="language" id="language">
                      <option value="">Choose Language</option>
                      <?php $languages = array('English','Hindi');
                      foreach($languages as $language){
                      ?> 
                    <option value="<?=$language?>" <?=$language == $category->language ? 'selected' : ''?>><?=$language?></option>
                    <?php } ?>
                  </select>
                      </div>
                    </div>
              
                    <div class="col-md-6">
                      <div class="form-group">
                        <p class="settings-label">Image </p>
                        <div class="settings-btn">
                          <input type="file" accept="image/*" name="image" id="file" onchange="loadFile(event)" class="hide-input">
                          <label for="file" class="upload">
                          <i class="feather-upload"></i>
                          </label>
                        </div>
                        <h6 class="settings-size">Recommended image size is <span>150px x 150px</span></h6>
                        <div class="upload-images">
                          <img src="<?=base_url($category->image)?>" alt="Image">
                          <a href="javascript:void(0);" class="btn-icon logo-hide-btn">
                          <i class="feather-x-circle"></i>
                          </a>
                          </div>
                      </div>
                    </div>
                    <div class="form-group mb-0">
                      <div class="settings-btns text-center">
                        <button type="submit" class="btn btn-orange">Edit</button>
                        <!-- <button type="reset" class="btn btn-grey">Cancel</button> -->
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $("form#editCategoryform").submit(function(e) {
  			$(':input[type="submit"]').prop('disabled', true);
  			e.preventDefault(); 
  			var formData = new FormData(this);
        formData.append("categoryID", '<?=$category->id?>');   
  			$.ajax({
  			url: $(this).attr('action'),
  			type: 'POST',
  			data: formData,
  			cache: false,
  			contentType: false,
  			processData: false,
  			dataType: 'json',
  			success: function (data) { 
  				if(data.status==200) {
  				//$('.modal').modal('hide');
          toastNotif({
  				text: data.message,
  				color: '#5bc83f',
  				timeout: 5000,
  				icon: 'valid'
  			});
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function(){
  
          location.href="<?=base_url('category')?>"; 	
          
        }, 1000) 		
  				}else if(data.status==403) {
            toastNotif({
                  text: data.message,
                  color: '#da4848',
                  timeout: 5000,
                  icon: 'error'
                });
  
            $(':input[type="submit"]').prop('disabled', false);
  				}else{
            toastNotif({
                text: data.message,
                color: '#da4848',
                timeout: 5000,
                icon: 'error'
              });
            $(':input[type="submit"]').prop('disabled', false);
  				}
  			},
  			error: function(){} 
  			});
  		});

      CKEDITOR.replace('description');
  
</script>