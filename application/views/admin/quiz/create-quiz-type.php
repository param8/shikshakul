<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">
      <div class="page-header">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"><?=$page_title?></h4>
              </div>
              <div class="card-body pt-0">
                <form id="createCourseQuizform" method="post" action="<?=base_url('Quiz/store_course_quiz');?>" enctype="multipart/form-data">
                  <div class="settings-form row">
              
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Quiz Title<span class="star-red">*</span></label>
                        <input type="text" class="form-control" name="title" id="title"  placeholder="Quiz Title">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Course<span class="star-red">*</span></label>
                        <select class="form-control" name="courseID" id="courseID">
                           <option value="">Select Course</option>
                           <?php foreach($courses as $course){?>
                            <option value="<?=$course->id?>"><?=$course->name?></option>
                            <?php } ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Quiz type<span class="star-red">*</span></label>
                        <select class="form-control" name="quizType" id="quizType">
                           <option value="">Select Quiz Type</option>
                            <option value="Weekly">Weekly</option>  
                            <option value="Current Affair">Current Affair</option>  
                        </select>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <p class="settings-label">Add Questions </p>
                      <div class="quiz-form">
                        <div class="links-info">
                          <div class="row form-row links-cont">
                            <div class="col-12 col-md-12">
                            <p class="settings-label">Questions </p>
                              <div class="form-group form-placeholder d-flex">
                                <textarea  class="form-control" name="question[0]"  placeholder="question"></textarea>
                              </div>
                            </div>
                            <div class="col-6 col-md-6">
                              <p class="settings-label">Option 1 </p>
                              <div class="form-group form-placeholder d-flex">
                                <input type="text" class="form-control" name="option[question10]"  placeholder="Option 1" >
                              </div>
                            </div>
                            <div class="col-6 col-md-6">
                              <p class="settings-label">Option 2 </p>
                              <div class="form-group form-placeholder d-flex">
                                <input type="text" class="form-control" name="option[question20]"  placeholder="Option 2" >
                              </div>
                            </div>
                            <div class="col-6 col-md-6">
                              <p class="settings-label">Option 3 </p>
                              <div class="form-group form-placeholder d-flex">
                                <input type="text" class="form-control" name="option[question30]"  placeholder="Option 3" >
                              </div>
                            </div>
                            <div class="col-6 col-md-6">
                              <p class="settings-label">Option 4 </p>
                              <div class="form-group form-placeholder d-flex">
                                <input type="text" class="form-control" name="option[question40]"  placeholder="Option 4" >
                              </div>
                            </div>
                            <div class="col-6 col-md-6">
                              <p class="settings-label">Answer </p>
                              <div class="form-group form-placeholder d-flex">
                                <input type="text" class="form-control" name="answer[question0]"  placeholder="Answer" >
                              </div>
                            </div>
                            <div class="col-12 col-md-1">
                            <p class="settings-label">Remove </p>
                              <a href="#" class="btn trash btn-danger ml-5">
                              <i class="feather-trash-2"></i>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <a href="javascript:void(0);" class="btn add-links">
                        <i class="fa fa-plus me-1"></i> Add More
                        </a>
                      </div>
                    </div>

                    <div class="form-group mb-0">
                      <div class="settings-btns">
                        <button type="submit" class="btn btn-orange">Submit</button>
                        <!-- <button type="reset" class="btn btn-grey">Cancel</button> -->
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $("form#createCourseQuizform").submit(function(e) {
  			$(':input[type="submit"]').prop('disabled', true);
  			e.preventDefault();    
  			var formData = new FormData(this);  
  			$.ajax({
  			url: $(this).attr('action'),
  			type: 'POST',
  			data: formData,
  			cache: false,
  			contentType: false,
  			processData: false,
  			dataType: 'json',
  			success: function (data) { 
  				if(data.status==200) {
  				//$('.modal').modal('hide');
          toastNotif({
  				text: data.message,
  				color: '#5bc83f',
  				timeout: 5000,
  				icon: 'valid'
  			});
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function(){
  
          location.href="<?=base_url('course-quiz')?>"; 	
          
        }, 1000) 		
  				}else if(data.status==403) {
            toastNotif({
                  text: data.message,
                  color: '#da4848',
                  timeout: 5000,
                  icon: 'error'
                });
  
            $(':input[type="submit"]').prop('disabled', false);
  				}else{
            toastNotif({
                text: data.message,
                color: '#da4848',
                timeout: 5000,
                icon: 'error'
              });
            $(':input[type="submit"]').prop('disabled', false);
  				}
  			},
  			error: function(){} 
  			});
  		});
  
     
      function get_courses(categoryID){
        $.ajax({
      url: '<?=base_url('Ajax_controller/get_courses')?>',
      type: 'POST',
      data: {categoryID},
      success: function (data) {
        $('#courseID').html(data);
      }
      });
      }
</script>