<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">
      <div class="page-header">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"><?=$page_title?></h4>
              </div>
              <div class="card-body pt-0">
                <form id="editQuizform" method="post" action="<?=base_url('Quiz/update_quiz');?>"
                  enctype="multipart/form-data">
                  <div class="settings-form row">

                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Quiz Title<span class="star-red">*</span></label>
                        <input type="text" class="form-control" name="title" id="title" value="<?=$quiz->title?>"
                          placeholder="Quiz Title">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Course<span class="star-red">*</span></label>
                        <select class="form-control" name="courseID" id="courseID" onchange="get_subject(this.value)">
                          <option value="">Select Course</option>
                          <?php foreach($courses as $course){?>
                          <option value="<?=$course->id?>" <?= $course->id==$quiz->courseID ? 'selected' : ''?>>
                            <?=$course->name?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Subject<span class="star-red">*</span></label>
                        <select class="form-control" name="subjectID" id="subjectID">
                          <option value="">Select Subject</option>

                        </select>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Quiz type<span class="star-red">*</span></label>
                        <select class="form-control" name="quizType" id="quizType">
                          <option value="">Select Quiz Type</option>
                          <?php foreach($quizTypes as $quizType){?>
                          <option value="<?=$quizType->id?>" <?= $quizType->id==$quiz->quizTypeID ? 'selected' : ''?>>
                            <?=$quizType->title?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <h5 class="settings-label"> Questions </h5>
                      <div class="quiz-form">
                        <div class="links-info">
                          <div class="row form-row links-cont">

                           <?php 
                           $answer_correct = array();
                           foreach($questions as $key=>$question){

                            ?>
                            <div class="col-12 col-md-12">
                            <p class="settings-label">Questions <?=$key+1;?></p>
                              <div class="form-group form-placeholder d-flex">
                                <textarea  class="form-control" name="question[<?=$key?>]"  placeholder="question" ><?=$question['question']?></textarea>
                              </div>
                            </div>
                            <?php 
                             $option_key=array();
                            foreach($question['option'] as $key_options=>$options){
                              $key_option = $key_options+1;
                              ?>
                            <div class="col-6 col-md-6">
                              <p class="settings-label">Option <?=$key_option;?> </p>
                              <div class="form-group form-placeholder d-flex">
                                <input type="text" class="form-control" name="option[question<?=$key_option?><?=$key?>]" value="<?=$options['option']?>"  placeholder="Option 1" >
                              </div>
                            </div>
                            <?php 
                            
                            foreach($question[$key_options]['answer'] as $key_ans=>$answer){
                              $answer_correct[$question['id']] = $answer;
                              } 
                            }
                                ?>
                            <div class="col-6 col-md-6">
                              <p class="settings-label">Answer </p>
                              <div class="form-group form-placeholder d-flex">
                                <input type="text" class="form-control" name="answer[question<?=$key?>]" value="<?=$answer_correct[$question['id']]?>" placeholder="Answer" >
                              </div>
                            </div>
                          
                            <hr>
                            <?php } ?>
                          
                          </div>
                        </div>
                      </div>
                      <!-- <div class="form-group">
                        <a href="javascript:void(0);" class="btn add-links">
                        <i class="fa fa-plus me-1"></i> Add More
                        </a>
                      </div> -->
                    </div>

                    <div class="form-group mb-0">
                      <div class="settings-btns">
                        <button type="submit" class="btn btn-orange">Submit</button>
                        <!-- <button type="reset" class="btn btn-grey">Cancel</button> -->
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$("form#editQuizform").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  formData.append("quizID", '<?=$quiz->id?>');
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        //$('.modal').modal('hide');
        toastNotif({
          text: data.message,
          color: '#5bc83f',
          timeout: 5000,
          icon: 'valid'
        });
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {

          location.href = "<?=base_url('quiz')?>";

        }, 1000)
      } else if (data.status == 403) {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});


function get_subject(courseID, quizID) {
  $.ajax({
    url: '<?=base_url('Ajax_controller/get_subject')?>',
    type: 'POST',
    data: {
      courseID,
      quizID
    },
    success: function(data) {
      $('#subjectID').html(data);
    }
  });
}

$(document).ready(function() {
  get_subject(<?=$quiz->courseID?>, <?=$quiz->id?>);
});
</script>