<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">
      <div class="page-header">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"><?=$page_title?></h4>
              </div>
              <div class="card-body pt-0">
                <form id="createTopicform" method="post" action="<?=base_url('Topics/update');?>" enctype="multipart/form-data">
                  <div class="settings-form row">
                    <div class="col-md-4">
                      <div class="form-group">
                      <label>Category <span class="star-red">*</span></label>
                        <select class="form-control" name="categoryID" id="categoryID" onchange="get_courses(this.value)" disabled>
                          <option value="">Choose Category</option>
                          <?php
                            foreach($categories as $category){
                            ?> 
                          <option value="<?=$category->id; ?>" <?=$topic->categoryID==$category->id ? 'selected' : '' ?>><?=$category->name; ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Course<span class="star-red">*</span></label>
                        <select class="form-control " name="courseID"  id="courseID" disabled onchange="get_subject(this.value)">
                          <option value="">Choose Course</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Subject<span class="star-red">*</span></label>
                        <select class="form-control " name="subjectID"  id="subjectID" disabled>
                          <option value="">Choose Subject</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <p class="settings-label">Add PDF Topics </p>
                      <div class="row form-row">
                        <div class="col-6 col-md-6">
                          <p class="settings-label">Title </p>
                        </div>
                        <div class="col-3 col-md-3">
                          <p class="settings-label">Upload PDF </p>
                        </div>
                        <div class="col-3 col-md-3">
                          <p class="settings-label">Action </p>
                        </div>
                      </div>
                      <div class="topic-form">
                        <div class="links-info">

                        <?php
                        //if(!empty($topic->pdf)){
                          $decode_pdfs = json_decode($topic->pdf);
                          //print_r($pdfs);
                          $count = 0;
                           foreach($decode_pdfs as $pdfs){
                            foreach($pdfs as $pdf_title_key=>$pdf){
                           
                            ?>
                          <div class="row form-row links-cont">
                            <div class="col-6 col-md-6">
                              <div class="form-group form-placeholder d-flex">
                                <input type="text" class="form-control" name="title_old[]" value="<?=$pdf_title_key?>"  placeholder="Title">
                              </div>
                            </div>
                            <div class="col-3 col-md-3">
                              <div class="form-group form-placeholder d-flex">
                                <span>
                                <a href="<?=base_url($pdf)?>" target="_blank" class="btn btn-warning"> View PDF</a>
                                <input type="hidden" name="pdf_file_hidden[]" value="<?=$pdf?>"></span>
                                
                              </div>
                            </div>
                            <div class="col-12 col-md-1">
                              <a href="#" class="btn trash btn-danger">
                              <i class="feather-trash-2"></i>
                              </a>
                            </div>
                          </div>
                        <?php $count++; } } //}?>
                          <div class="row form-row links-cont">
                            <div class="col-6 col-md-6">
                              <div class="form-group form-placeholder d-flex">
                                <input type="text" class="form-control" name="title[]"  placeholder="Title">
                              </div>
                            </div>
                            <div class="col-3 col-md-3">
                              <div class="form-group form-placeholder d-flex">
                                <input type="file" class="form-control" name="pdf_file[]"  placeholder="PDF File" accept="application/pdf">
                              </div>
                            </div>
                            <div class="col-12 col-md-1">
                              <a href="#" class="btn trash btn-danger">
                              <i class="feather-trash-2"></i>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <a href="javascript:void(0);" class="btn add-links">
                        <i class="fa fa-plus me-1"></i> Add More
                        </a>
                      </div>
                    </div>

                    <div class="col-md-12">
                      <p class="settings-label">Add Video Topics </p>
                      <div class="row form-row">
                        <div class="col-6 col-md-6">
                          <p class="settings-label">Title </p>
                        </div>
                        <div class="col-3 col-md-3">
                          <p class="settings-label">Video Link </p>
                        </div>
                        <div class="col-3 col-md-3">
                          <p class="settings-label">Action </p>
                        </div>
                      </div>
                      <div class="topic-video-form">
                        <div class="links-info">
                          <?php //if(count($topic->video) > 0){ 
                              $decode_videos = json_decode($topic->video, true);
                              foreach($decode_videos as $videos){ 
                              foreach($videos as $video_title_key=>$video){?>
                            <div class="row form-row links-cont">
                            <div class="col-6 col-md-6">
                              <div class="form-group form-placeholder d-flex">
                                <input type="text" class="form-control" name="video_title[]" value="<?=$video_title_key?>"  placeholder="Title">
                              </div>
                            </div>
                            <div class="col-3 col-md-3">
                              <div class="form-group form-placeholder d-flex">
                                <input type="text" class="form-control" name="video_link[]" value="<?=$video?>"  placeholder="Video Link">
                              </div>
                            </div>
                            <div class="col-12 col-md-1">
                              <a href="#" class="btn trash btn-danger">
                              <i class="feather-trash-2"></i>
                              </a>
                            </div>
                          </div>
                            <?php } } //}?>
                          <div class="row form-row links-cont">
                            <div class="col-6 col-md-6">
                              <div class="form-group form-placeholder d-flex">
                                <input type="text" class="form-control" name="video_title[]"  placeholder="Title">
                              </div>
                            </div>
                            <div class="col-3 col-md-3">
                              <div class="form-group form-placeholder d-flex">
                                <input type="text" class="form-control" name="video_link[]"  placeholder="Video Link">
                              </div>
                            </div>
                            <div class="col-12 col-md-1">
                              <a href="#" class="btn trash btn-danger">
                              <i class="feather-trash-2"></i>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <a href="javascript:void(0);" class="btn add-links1">
                        <i class="fa fa-plus me-1"></i> Add More
                        </a>
                      </div>
                    </div>
                    <div class="form-group mb-0">
                      <div class="settings-btns">
                        <button type="submit" class="btn btn-orange">Submit</button>
                        <!-- <button type="reset" class="btn btn-grey">Cancel</button> -->
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $("form#createTopicform").submit(function(e) {
  			$(':input[type="submit"]').prop('disabled', true);
  			e.preventDefault();    
  			var formData = new FormData(this); 
        formData.append("courseID", <?=$topic->courseID?>); 
        formData.append("categoryID", <?=$topic->categoryID?>); 
        formData.append("subjectID", <?=$topic->subjectID?>);
        formData.append("topicID", <?=$topic->id?>);  
  			$.ajax({
  			url: $(this).attr('action'),
  			type: 'POST',
  			data: formData,
  			cache: false,
  			contentType: false,
  			processData: false,
  			dataType: 'json',
  			success: function (data) { 
  				if(data.status==200) {
  				//$('.modal').modal('hide');
          toastNotif({
  				text: data.message,
  				color: '#5bc83f',
  				timeout: 5000,
  				icon: 'valid'
  			});
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function(){
  
          location.href="<?=base_url('topics')?>"; 	
          
        }, 1000) 		
  				}else if(data.status==403) {
            toastNotif({
                  text: data.message,
                  color: '#da4848',
                  timeout: 5000,
                  icon: 'error'
                });
  
            $(':input[type="submit"]').prop('disabled', false);
  				}else{
            toastNotif({
                text: data.message,
                color: '#da4848',
                timeout: 5000,
                icon: 'error'
              });
            $(':input[type="submit"]').prop('disabled', false);
  				}
  			},
  			error: function(){} 
  			});
  		});
  
     
      function get_courses(categoryID){
        var topicID = <?=$topic->id?>;
        $.ajax({
        url: '<?=base_url('Ajax_controller/get_courses')?>',
        type: 'POST',
        data: {categoryID, topicID},
        success: function (data) {
          $('#courseID').html(data);
        }
        });
      }

      function get_subject(courseID){
        var topicID = <?=$topic->id?>;
        $.ajax({
        url: '<?=base_url('Ajax_controller/get_subject')?>',
        type: 'POST',
        data: {courseID,topicID},
        success: function (data) {
          $('#subjectID').html(data);
        }
        });
      }

      $( document ).ready(function() {
        get_subject(<?=$topic->courseID?>)
        get_courses(<?=$topic->categoryID?>)
     });
</script>