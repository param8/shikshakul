<div class="page-wrapper">
  <div class="content container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-sm-6">
          <h3 class="page-title">Settings</h3>
          <ul class="breadcrumb">
            <li class=""><a href="#">Dashboard</a></li>/
            <li class=""><a href="#">Settings</a></li>/
            <li class="breadcrumb-item active"><?=$page_title?></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="settings-menu-links">
      <ul class="nav nav-tabs menu-tabs">
        <li class="nav-item <?=($page_title=='General-Setting')?'active':'';?>">
          <a class="nav-link" href="<?=base_url('general-setting');?>">General Settings</a>
        </li>
        <li class="nav-item <?=($page_title=='Banner')?'active':'';?>">
          <a class="nav-link" href="<?=base_url('banner');?>">Banner</a>
        </li>
        <li class="nav-item <?=($page_title=='About')?'active':'';?>">
          <a class="nav-link" href="<?=base_url('about');?>">About</a>
        </li>
        <li class="nav-item <?=($page_title=='Privacy-Policy')?'active':'';?>">
          <a class="nav-link" href="<?=base_url('privacy-policy');?>">Privacy Policy</a>
        </li>
        <li class="nav-item <?=($page_title=='Terms-Condition')?'active':'';?>">
          <a class="nav-link" href="<?=base_url('terms-condition');?>">Terms & Condition</a>
        </li>
        <li class="nav-item <?=($page_title=='Refund-Policy')?'active':'';?>">
          <a class="nav-link" href="<?=base_url('refund-policy');?>">Refund Policy</a>
        </li>
        <li class="nav-item <?=($page_title=='Payment-Setting')?'active':'';?>">
          <a class="nav-link" href="<?=base_url('payment-setting');?>">Payment Settings</a>
        </li>
        <li class="nav-item <?=($page_title=='Email-Setting')?'active':'';?>">
          <a class="nav-link" href="<?=base_url('email-setting');?>">Email Settings</a>
        </li>
        <!-- <li class="nav-item">
          <a class="nav-link" href="social-settings.html">Social Media Login</a>
        </li> -->
        <li class="nav-item <?=($page_title=='Social-Setting')?'active':'';?>">
          <a class="nav-link" href="<?=base_url('social-setting');?>">Social Links</a>
        </li>
        <li class="nav-item <?=($page_title=='Seo-Setting')?'active':'';?>">
          <a class="nav-link" href="<?=base_url('seo-setting');?>">SEO Settings</a>
        </li>
        <!-- <li class="nav-item">
          <a class="nav-link" href="others-settings.html">Others</a>
        </li> -->
      </ul>
    </div>