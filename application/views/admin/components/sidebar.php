<div class="sidebar" id="sidebar">
        <div class="sidebar-inner slimscroll">
          <div id="sidebar-menu" class="sidebar-menu">
            <ul>
              <!-- <li class="menu-title">
                <span><i class="fe fe-home"></i> Main</span>
              </li> -->
              <?php if($this->session->userdata('user_type')=='Admin'){?>
              <li class="active">
                <a href="<?=base_url('dashboard')?>"><span><i class="fe fe-home"></i> Dashboard</span></a>
              </li>
              <li class="menu-title">
                <span> Users</span>
              </li>
              <li class="submenu">
                <a href="#"><span><i class="fa fa-users" style="font-size:16px" aria-hidden="true"></i>  Users</span> <span class="menu-arrow"></span></a>
                <ul style="display: none;">
                  <li><a href="<?=base_url('staffs')?>">Staff</a></li>
                  <li><a href="<?=base_url('students')?>">Students</a></li>
                  <li><a href="<?=base_url('faculties')?>">Faculties</a></li>
                  <li><a href="<?=base_url('create')?>">New</a></li>
                </ul>
              </li>
              <li class="menu-title">
                <span> EDUCATION</span>
              </li>
              <li>
              <a href="<?=base_url('category')?>"><span><i class="fa fa-list-alt" style="font-size:18px;"></i> Category</span></a>
              </li>
              <li>
                <a href="<?=base_url('courses')?>"><span><i class="fas fa-graduation-cap" style="font-size:18px;"></i> Courses</span></a>
              </li>
              <li>
                <a href="<?=base_url('subjects')?>"><span><i class="fas fa-book" style="font-size:18px;"></i> Subjects</span></a>
              </li>
              <li>
                <a href="<?=base_url('topics')?>"><span><i class="fe fe-book"></i> Topics</span></a>
              </li>

              <li class="menu-title">
                <span> Quiz</span>
              </li>
              <li>
                <a href="<?=base_url('quiz-type')?>"><span><i class="fa fa-question" style="font-size:16px;"></i>Quiz Type</span></a>
              </li>

              <li>
                <a href="<?=base_url('quiz')?>"><span><i class="fa fa-question" style="font-size:16px;"></i>Quiz</span></a>
              </li>

              <li>
                <a href="<?=base_url('test-seriess')?>"><span><i class="fa fa-question" style="font-size:16px;"></i>Test Series</span></a>
              </li>
              
              <li class="menu-title">
                <span> Marketing</span>
              </li>
              <li>
                <a href="<?=base_url('discount')?>"><span><i class="fa fa-percent" style="font-size:16px;"></i>Discount</span></a>
              </li>
              <li class="menu-title">
                <span>Finance</span>
              </li>
              <li>
                <a href="<?=base_url('orders')?>"><span><i class="fa fa-shopping-cart" style="font-size:16px;"></i>Orders</span></a>
              </li>
              <li class="menu-title">
                <span> CONTENT</span>
              </li>
              <li>
                <a href="<?=base_url('current-affairs')?>"><span><i class="fa fa-newspaper-o" style="font-size:18px;"></i> Current Affairs</span></a>
              </li>
              <li>
                <a href="<?=base_url('whats-news')?>"><span><i class="fa fa-newspaper-o" style="font-size:18px;"></i> Whats's News</span></a>
              </li>
              <li>
                <a href="<?=base_url('videos')?>"><span><i class="fa fa-video" style="font-size:16px;"></i>Videos</span></a>
              </li>
              <li>
                <a href="<?=base_url('building-futures')?>"><span><i class="fa fa-cog" style="font-size:16px;"></i>Building Futures</span></a>
              </li>
              <li>
                <a href="<?=base_url('toppers')?>"><span><i class="fa fa-comments-o" style="font-size:16px;"></i>Toppers Talk</span></a>
              </li>

              <li>
                <a href="<?=base_url('mock-interview')?>"><span><i class="fa fa-question-circle" style="font-size:16px;"></i>Mock Interview</span></a>
              </li>
              <li>
                <a href="<?=base_url('student-remark')?>"><span><i class="fa fa-comment" style="font-size:16px;"></i>Student Remarks</span></a>
              </li>
              <li>
                <a href="<?=base_url('vocabulary')?>"><span><i class="fa fa-question-circle" style="font-size:16px;"></i>Vocabulary</span></a>
              </li>
              <li>
                <a href="<?=base_url('upcoming-exam')?>"><span><i class="fa fa-question-circle" style="font-size:16px;"></i>Upcoming Exam</span></a>
              </li>
              <li>
                <a href="<?=base_url('schedule')?>"><span><i class="fa fa-calendar" style="font-size:16px;"></i>Schedule</span></a>
              </li>
              <li class="menu-title">
                <span> Setting</span>
              </li>
              <li>
               <a href="<?=base_url('setting')?>"><span><i class="fa fa-cog" style="font-size:16px" aria-hidden="true"></i> Setting</span></a>
            </li>
            <?php }if($this->session->userdata('user_type')=='Faculties'){?>
              <li class="active">
                <a href="<?=base_url('faculty-dashboard')?>"><span><i class="fe fe-home"></i> Home</span></a>
              </li>
              <li>
                <a href="<?=base_url('test-seriess')?>"><span><i class="fa fa-question" style="font-size:16px;"></i>Test Series</span></a>
              </li>

              <li>
                <a href="<?=base_url('topics')?>"><span><i class="fe fe-book"></i> Topics</span></a>
              </li>
              <li>
                <a href="<?=base_url('schedule')?>"><span><i class="fa fa-calendar" style="font-size:16px;"></i>Schedule</span></a>
              </li>
            <?php } ?>
            </ul>
          </div>
        </div>
      </div>