<!-- <footer class="footer footer-eight">
  <div class="footer-top aos" data-aos="fade-up">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-3 col-md-6">
          <div class="footer-widget footer-about">
            <div class="footer-logo">
              <img src="<?=base_url($siteinfo->site_logo)?>" alt="logo">
            </div>
            <div class="footer-about-content">
              <p><?=$siteinfo->discription?> </p>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6">
          <div class="footer-widget footer-menu">
            <h2 class="footer-title">For Mentee</h2>
            <ul>
              <li><a href="search.html">Search Mentors</a></li>
              <li><a href="login">Login</a></li>
              <li><a href="register">Register</a></li>
              <li><a href="booking.html">Booking</a></li>
              <li><a href="dashboard-mentee.html">Mentee Dashboard</a></li>
            </ul>
          </div>
        </div>
        <div class="col-lg-3 col-md-6">
          <div class="footer-widget footer-menu">
            <h2 class="footer-title">For Mentors</h2>
            <ul>
              <li><a href="appointments.html">Appointments</a></li>
              <li><a href="chat.html">Chat</a></li>
              <li><a href="login">Login</a></li>
              <li><a href="register">Register</a></li>
              <li><a href="dashboard.html">Mentor Dashboard</a></li>
            </ul>
          </div>
        </div>
        <div class="col-lg-3 col-md-6">
          <div class="footer-widget footer-contact">
            <h2 class="footer-title">Contact Us</h2>
            <div class="footer-contact-info">
              <div class="footer-address">
                <p> <?=$siteinfo->site_address?> </p>
              </div>
              <p>
              <?=$siteinfo->site_contact?>
              </p>
              <p class="mb-0">
                <a href="javascript:void(0)" class="__cf_email__" data-cfemail="cfa2aaa1bba0bda6a1a88faab7aea2bfa3aae1aca0a2"><?=$siteinfo->site_email?></a>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-bottom">
    <div class="container-fluid">
      <div class="copyright">
        <div class="row">
          <div class="col-md-6">
            <div class="copyright-text">
              <p class="mb-0">&copy; <?=$siteinfo->footer_contant?>.</p>
            </div>
          </div>
          <div class="col-md-6">
            <div class="social-icon text-md-end">
              <ul>
                <li>
                  <a href="<?=$siteinfo->insta_url?>" target="_blank"><i class="fab fa-instagram"></i></a>
                </li>
                <li>
                  <a href="<?=$siteinfo->linkedin_url?>" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                </li>
                <li>
                  <a href="<?=$siteinfo->facebook_url?>" target="_blank"><i class="fab fa-facebook"></i> </a>
                </li>
                <li>
                  <a href="<?=$siteinfo->youtube_url?>" target="_blank"><i class="fab fa-youtube"></i> </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer> -->

<footer class="footer">
  <div class="footer-top">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-3 col-md-6">
          <div class="footer-widget footer-about">
            <div class="footer-logo">
              <img src="<?=base_url($siteinfo->site_logo)?>" alt="logo">
            </div>
            <div class="footer-about-content">
              <p><?=$siteinfo->discription?></p>
              <div class="social-icon">
                <ul>
                  <li>
                    <a href="<?=$siteinfo->facebook_url?>" target="_blank"><i class="fab fa-facebook-f"></i> </a>
                  </li>
                  <!-- <li>
                    <a href="#" target="_blank"><i class="fab fa-twitter"></i> </a>
                  </li> -->
                  <li>
                    <a href="<?=$siteinfo->linkedin_url?>" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                  </li>
                  <li>
                    <a href="<?=$siteinfo->insta_url?>" target="_blank"><i class="fab fa-instagram"></i></a>
                  </li>
                  <li>
                    <a href="<?=$siteinfo->youtube_url?>" target="_blank"><i class="fab fa-youtube"></i> </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-6 col-md-6">
          <div class="footer-widget footer-menu">
            <h2 class="footer-title">About Link</h2>
            <ul>
              <li><a href="<?=base_url('about-us')?>">About Us</a></li>
              <li><a href="<?=base_url('contact-us')?>">Contact Us</a></li>
              <li><a href="<?=base_url('course')?>">Courses</a></li>
              <li><a href="<?=base_url('blogs')?>">Blogs</a></li>
            </ul>
          </div>
        </div>
        <div class="col-lg-2 col-6 col-md-6">
          <div class="footer-widget footer-menu">
            <h2 class="footer-title">Useful Link</h2>
            <ul>
              <li><a href="<?=base_url('current-affair-list')?>">Current Affairs</a></li>
              <li><a href="<?=base_url('privacy')?>">Privacy Policy</a></li>
              <li><a href="<?=base_url('refund')?>">Refund Policy</a></li>
              <li><a href="<?=base_url('terms-uses')?>">Terms Of Use</a></li>
            </ul>
          </div>
        </div>
        <div class="col-lg-2 col-md-6">
          <div class="footer-widget footer-menu">
            <h2 class="footer-title">Bank Details</h2>
            <ul class="bank_d">
              <li><b>Account Name :</b> Shikshakul classes private limited</li>
              <li>
                <b>Account No. :</b> 158005004271
              </li>
              <li><b>IFSC Code :</b> ICIC0001580</li>
            </ul>
          </div>
        </div>
        <div class="col-lg-3 col-md-6">
          <div class="footer-widget footer-contact">
            <h2 class="footer-title">Contact Us</h2>
            <div class="footer-contact-info">
              <div class="footer-address">
                <span><i class="fas fa-map-marker-alt"></i></span>
                <p> <?=$siteinfo->site_address?> </p>
              </div>
              <p>
                <i class="fas fa-phone-alt"></i>
                <?=$siteinfo->site_contact?>
              </p>
              <p class="mb-0">
                <i class="fas fa-envelope"></i>
                <a href="javascript:void(0)" class="_cf_email_"
                  data-cfemail="1e737b706a716c7770795e7b667f736e727b307d7173"><?=$siteinfo->site_email?></a>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-bottom">
    <div class="container-fluid">
      <div class="copyright">
        <div class="row">
          <div class="col-12 text-center">
            <div class="copyright-text">
              <p class="mb-0">&copy; <?=$siteinfo->footer_contant?>.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
</div>
</div>

<script src="<?=base_url('public/web/assets/js/bootstrap.bundle.min.js')?>"></script>
<script src="<?=base_url('public/web/assets/plugins/theia-sticky-sidebar/ResizeSensor.js')?>"></script>
<script src="<?=base_url('public/web/assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js')?>"></script>
<script src="<?=base_url('public/web/assets/plugins/select2/js/select2.min.js')?>"></script>
<script src="<?=base_url('public/web/assets/plugins/slick/slick.js')?>"></script>
<script src="<?=base_url('public/web/assets/plugins/aos/aos.js')?>"></script>
<script src="<?=base_url('public/web/assets/js/script.js')?>"></script>
<scrit src="https://cdn.datatables.net/1.13.4/js/dataTables.bootstrap4.min.js"></scrit>
<script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>


<script>
$(document).ready(function() {
  $('.js-example-basic-single').select2();
});

function get_city(stateID) {

  $.ajax({
    url: '<?=base_url('Ajax_controller/get_city')?>',
    type: 'POST',
    data: {
      stateID
    },
    success: function(data) {
      $('.city').html(data);
    }
  });
}
</script>

<script>
$(document).ready(function($) {
  // Declare the body variable
  var $body = $("body");

  // Function that shows and hides the sidebar cart
  $(".cart-button, .close-button, #sidebar-cart-curtain").click(function(e) {
    e.preventDefault();

    // Add the show-sidebar-cart class to the body tag
    $body.toggleClass("show-sidebar-cart");

    // Check if the sidebar curtain is visible
    if ($("#sidebar-cart-curtain").is(":visible")) {
      // Hide the curtain
      $("#sidebar-cart-curtain").fadeOut(500);
    } else {
      // Show the curtain
      $("#sidebar-cart-curtain").fadeIn(500);
    }
  });

  // Function that adds or subtracts quantity when a 
  // plus or minus button is clicked
  $body.on('click', '.plus-button, .minus-button', function() {
    // Get quanitity input values
    var qty = $(this).closest('.qty').find('.qty-input');
    var val = parseFloat(qty.val());
    var max = parseFloat(qty.attr('max'));
    var min = parseFloat(qty.attr('min'));
    var step = parseFloat(qty.attr('step'));

    // Check which button is clicked
    if ($(this).is('.plus-button')) {
      // Increase the value
      qty.val(val + step);
    } else {
      // Check if minimum button is clicked and that value is 
      // >= to the minimum required
      if (min && min >= val) {
        // Do nothing because value is the minimum required
        qty.val(min);
      } else if (val > 0) {
        // Subtract the value
        qty.val(val - step);
      }
    }
  });
});


function addToCart(courseID,order_type, condition = null) {
  $.ajax({
    url: '<?=base_url('cart/store_cart')?>',
    type: 'POST',
    data: {
      courseID,
      order_type
    },
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        if (condition == 'Buy Now') {
          window.location = "<?=base_url('checkout')?>";
        } else {
          location.reload();
        }

      }
    },
    error: function() {}
  });

}

function remove_cart_item(rowid) {
  $.ajax({
    url: '<?=base_url('cart/removeCartItem')?>',
    type: 'POST',
    data: {
      rowid
    },
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        $('#items_div').load(location.href + ' #items_div');
        $('#sidebar-cart').load(location.href + ' #sidebar-cart');
      }
    },
    error: function() {}
  });
}
</script>
</body>

</html>