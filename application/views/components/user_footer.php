<footer class="footer">
  <div class="footer-top">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-3 col-md-6">
          <div class="footer-widget footer-about">
            <div class="footer-logo">
              <img src="assets/img/logo.png" alt="logo">
            </div>
            <div class="footer-about-content">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
              <div class="social-icon">
                <ul>
                  <li>
                    <a href="#" target="_blank"><i class="fab fa-facebook-f"></i> </a>
                  </li>
                  <li>
                    <a href="#" target="_blank"><i class="fab fa-twitter"></i> </a>
                  </li>
                  <li>
                    <a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                  </li>
                  <li>
                    <a href="#" target="_blank"><i class="fab fa-instagram"></i></a>
                  </li>
                  <li>
                    <a href="#" target="_blank"><i class="fab fa-dribbble"></i> </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6">
          <div class="footer-widget footer-menu">
            <h2 class="footer-title">For Mentee</h2>
            <ul>
              <li><a href="search.html">Search Mentors</a></li>
              <li><a href="login.html">Login</a></li>
              <li><a href="register.html">Register</a></li>
              <li><a href="booking.html">Booking</a></li>
              <li><a href="dashboard-mentee.html">Mentee Dashboard</a></li>
            </ul>
          </div>
        </div>
        <div class="col-lg-3 col-md-6">
          <div class="footer-widget footer-menu">
            <h2 class="footer-title">For Mentors</h2>
            <ul>
              <li><a href="appointments.html">Appointments</a></li>
              <li><a href="chat.html">Chat</a></li>
              <li><a href="login.html">Login</a></li>
              <li><a href="register.html">Register</a></li>
              <li><a href="dashboard.html">Mentor Dashboard</a></li>
            </ul>
          </div>
        </div>
        <div class="col-lg-3 col-md-6">
          <div class="footer-widget footer-contact">
            <h2 class="footer-title">Contact Us</h2>
            <div class="footer-contact-info">
              <div class="footer-address">
                <span><i class="fas fa-map-marker-alt"></i></span>
                <p> 3556 Beech Street, San Francisco,<br> California, CA 94108 </p>
              </div>
              <p>
                <i class="fas fa-phone-alt"></i>
                +1 315 369 5943
              </p>
              <p class="mb-0">
                <i class="fas fa-envelope"></i>
                <a href="https://mentoring.dreamguystech.com/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="1e737b706a716c7770795e7b667f736e727b307d7173">[email&#160;protected]</a>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-bottom">
    <div class="container-fluid">
      <div class="copyright">
        <div class="row">
          <div class="col-12 text-center">
            <div class="copyright-text">
              <p class="mb-0">&copy; 2020 Mentoring. All rights reserved.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
</div>
<!-- <script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="assets/js/jquery-3.6.0.min.js"></script>
<script src="assets/js/bootstrap.bundle.min.js"></script>
<script src="assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
<script src="assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>
<script src="assets/js/script.js"></script> -->
<script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
<script src="<?=base_url('public/web/assets/js/jquery-3.6.0.min.js')?>"></script>
<script src="<?=base_url('public/web/assets/js/bootstrap.bundle.min.js')?>"></script>
<script src="<?=base_url('public/web/assets/plugins/select2/js/select2.min.js')?>"></script>
<script src="<?=base_url('public/web/assets/plugins/slick/slick.js')?>"></script>
<script src="<?=base_url('public/web/assets/plugins/aos/aos.js')?>"></script>
<script src="<?=base_url('public/web/assets/js/script.js')?>"></script>
</body>
</html>