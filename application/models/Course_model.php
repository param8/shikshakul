<?php 

class Course_model extends CI_Model

{

	public function __construct()

	{
		parent::__construct();
	}

	function make_query($condition)
  {
	  $this->db->select('courses.*,category.name as categoryName');
    $this->db->from('courses');
    $this->db->join('category', 'category.id = courses.categoryID','left');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('courses.name', $_POST["search"]["value"]);
    $this->db->or_like('courses.description', $_POST["search"]["value"]);
    $this->db->or_like('courses.start_date', $_POST["search"]["value"]);
    $this->db->or_like('courses.end_date', $_POST["search"]["value"]);
    $this->db->or_like('courses.price', $_POST["search"]["value"]);
    $this->db->or_like('courses.capacity', $_POST["search"]["value"]);
    $this->db->or_like('courses.duration', $_POST["search"]["value"]);
    $this->db->or_like('category.name', $_POST["search"]["value"]);

   }
   $this->db->order_by('courses.id','desc');
     
  }
    function make_datatables($condition){
	  $this->make_query($condition,);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array();
  }

  function get_filtered_data($condition){
	  $this->make_query($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  function get_all_data($condition)
  {
    $this->db->select('courses.*,category.name as categoryName');
    $this->db->from('courses');
    $this->db->join('category', 'category.id = courses.categoryID','left');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('courses.name', $_POST["search"]["value"]);
    $this->db->or_like('courses.description', $_POST["search"]["value"]);
    $this->db->or_like('courses.start_date', $_POST["search"]["value"]);
    $this->db->or_like('courses.end_date', $_POST["search"]["value"]);
    $this->db->or_like('courses.price', $_POST["search"]["value"]);
    $this->db->or_like('courses.capacity', $_POST["search"]["value"]);
    $this->db->or_like('courses.duration', $_POST["search"]["value"]);
    $this->db->or_like('category.name', $_POST["search"]["value"]);

   }
   $this->db->order_by('courses.id','desc');
	   return $this->db->count_all_results();
  }

  public function get_course($condition){
    $this->db->select('courses.*,category.name as categoryName,category.image as categoryImage,users.name as userName');
    $this->db->from('courses');
    $this->db->join('category', 'category.id = courses.categoryID', 'left');
    $this->db->join('users', 'users.id = courses.facultyID', 'left');
	  $this->db->where($condition);
	 return $this->db->get()->row();
  }

  public function get_courses($condition){
    $this->db->select('courses.*,category.name as categoryName,category.image as categoryImage,users.name as userName');
    $this->db->from('courses');
    $this->db->join('category', 'category.id = courses.categoryID', 'left');
    $this->db->join('users', 'users.id = courses.facultyID', 'left');
	  $this->db->where($condition);
	  return $this->db->get()->result();
    //echo $this->db->last_query();die;
  }

  public function store_course($data){
    return $this->db->insert('courses',$data);
  }

  public function update_course($data,$subjectID){
	$this->db->where('id',$subjectID);
	return $this->db->update('courses',$data);
  }

  public function delete_course($condition){
    $this->db->where($condition);
    return $this->db->delete('courses');
    }

    public function store_topic($data){
      return $this->db->insert('topics',$data);
    }

    function make_query_topic($condition)
  {
    $this->db->select('topics.*,courses.name as courseName,category.name as categoryName,subject.name as subjectName,users.name as usersName');
    $this->db->from('topics');
    $this->db->join('category', 'topics.categoryID = category.id','left');
    $this->db->join('courses', 'topics.courseID = courses.id','left');
    $this->db->join('subject', 'subject.id = topics.subjectID','left');
    $this->db->join('users', 'users.id = topics.created_by','left');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('category.name', $_POST["search"]["value"]);
    $this->db->or_like('courses.name', $_POST["search"]["value"]);
    $this->db->or_like('users.name', $_POST["search"]["value"]);
   }
   $this->db->order_by('topics.id','desc');
     
  }
    function make_datatables_topic($condition){
	  $this->make_query_topic($condition,);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array();
  }

  function get_filtered_data_topic($condition){
	  $this->make_query_topic($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  function get_all_data_topic($condition)
  {
    $this->db->select('topics.*,courses.name as courseName,category.name as categoryName,subject.name as subjectName,users.name as usersName');
    $this->db->from('topics');
    $this->db->join('category', 'topics.categoryID = category.id','left');
    $this->db->join('courses', 'topics.courseID = courses.id','left');
    $this->db->join('subject', 'subject.id = topics.subjectID','left');
    $this->db->join('users', 'users.id = topics.created_by','left');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('category.name', $_POST["search"]["value"]);
    $this->db->or_like('courses.name', $_POST["search"]["value"]);
    $this->db->or_like('users.name', $_POST["search"]["value"]);
   }
   $this->db->order_by('topics.id','desc');
	   return $this->db->count_all_results();
  }

  public function get_topics($condition){
    $this->db->select('topics.*,courses.name as courseName');
    $this->db->from('topics');
    $this->db->join('courses', 'topics.courseID = courses.id','left');
    $this->db->where($condition);
    $query = $this->db->get()->result_array();

    foreach ($query as $key=>$row){
      $this->db->where('id', $row['subjectID']);
      $query[$key]['subject'] = $this->db->get('subject')->row_array();
    }
    return $query;
  }

  public function get_topic($condition){
    $this->db->select('topics.*,courses.name as courseName,category.name as categoryName');
    $this->db->from('topics');
    $this->db->join('category', 'topics.categoryID = category.id','left');
    $this->db->join('courses', 'topics.courseID = courses.id','left');
    $this->db->where($condition);
    return $this->db->get()->row();
  }

  public function delete_topic($condition){
    $this->db->where($condition);
    return $this->db->delete('topics');
  }

  public function update_topic($data,$condition){
    $this->db->where($condition);
	return $this->db->update('topics',$data);
  }

  public function get_bookings($condition){
    $this->db->select('bookings.*,users.name as userName,courses.name as courseName');
    $this->db->from('bookings');
    $this->db->join('users', 'users.id=bookings.userID','left');
    $this->db->join('courses', 'courses.id=bookings.courseID','left');
    $this->db->where($condition);
    $this->db->order_by('bookings.id', 'desc');
    $this->db->get()->result();
  }

}