<?php 
class Setting_model extends CI_Model 
{

  public function __construct()
  {
      parent::__construct();

  }

  public function update_siteInfo($data){
    $this->db->where('id', 1);
   return  $this->db->update('site_info',$data);
     //echo $this->db->last_query();die;
  }

  public function get_banner(){
      // $this->db->where($condition);
    return $this->db->get('banner_setting')->result();
  }
  

  public function insert_banner($data){
    return $this->db->insert('banner_setting',$data);
  }

  public function update_email_info($data){
    return $this->db->update('email_setting',$data);
  }

  public function update_paymentInfo($data){
    return $this->db->update('payment_setting',$data);
  }

  public function update_seoInfo($data){
    return $this->db->update('seo_setting',$data);
  }


  function make_query_affairs($condition)
  {
    $this->db->select('current_affairs.*');
    $this->db->from('current_affairs');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('title', $_POST["search"]["value"]);
    $this->db->or_like('description', $_POST["search"]["value"]);
   }
   $this->db->order_by('id','desc');
     
  }
    function make_datatables_affairs($condition){
	  $this->make_query_affairs($condition,);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array();
  }

  function get_filtered_data_affairs($condition){
	  $this->make_query_affairs($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  function get_all_data_affairs($condition)
  {
    $this->db->select('current_affairs.*');
    $this->db->from('current_affairs');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('title', $_POST["search"]["value"]);
    $this->db->or_like('description', $_POST["search"]["value"]);
   }
   $this->db->order_by('id','desc');
	   return $this->db->count_all_results();
  }

  function get_affairs($condition)
  {
    $date = "'".date('Y-m-d')."'";
    $this->db->select('current_affairs.*');
    $this->db->from('current_affairs');
    $this->db->where($condition);
    //$this->db->where(DATE_FORMAT("created_at", "%Y-m-d"),$date);
    $this->db->order_by('id','desc');
	  return $this->db->get()->result();
  }

  public function get_current_affair($condition){
    $this->db->select('*');
    $this->db->from('current_affairs');
    $this->db->where($condition);
    return $this->db->get()->result();
  } 

  public function get_current_affairs($condition){
    $this->db->select('*');
    $this->db->from('current_affairs');
    $this->db->where($condition);
    return $this->db->get()->row();
  } 


  public function store_current_affairs($data){
    return $this->db->insert('current_affairs',$data);
  }

  public function update_current_affairs($data,$id){
    $this->db->where('id',$id);
    return $this->db->update('current_affairs',$data);
  }

  public function delete_current_affairs($id){
    $this->db->where('id',$id);
    return $this->db->delete('current_affairs');
  }


  function make_query_whatsNews($condition)
  {
    $this->db->select('whats_news.*');
    $this->db->from('whats_news');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('title', $_POST["search"]["value"]);
    $this->db->or_like('discription', $_POST["search"]["value"]);
   }
   $this->db->order_by('id','desc');
     
  }
    function make_datatables_whatsNews($condition){
	  $this->make_query_whatsNews($condition,);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array();
  }

  function get_filtered_data_whatsNews($condition){
	  $this->make_query_whatsNews($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  function get_all_data_whatsNews($condition)
  {
    $this->db->select('whats_news.*');
    $this->db->from('whats_news');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('title', $_POST["search"]["value"]);
    $this->db->or_like('discription', $_POST["search"]["value"]);
   }
   $this->db->order_by('id','desc');
	   return $this->db->count_all_results();
  }

  function get_whatsNews($condition)
  {
    $date = "'".date('Y-m-d')."'";
    $this->db->select('whats_news.*');
    $this->db->from('whats_news');
    $this->db->where($condition);
    //$this->db->where(DATE_FORMAT("created_at", "%Y-m-d"),$date);
    $this->db->order_by('id','desc');
	  return $this->db->get()->result();
  }

  public function store_whats_news($data){
    return $this->db->insert('whats_news',$data);
  }

  public function get_whats_news($condition){
    $this->db->select('whats_news.*');
    $this->db->from('whats_news');
    $this->db->where($condition);
	  return $this->db->get()->row();
  }

  public function update_whats_news($data,$id){
    $this->db->where('id',$id);
    return $this->db->update('whats_news',$data);
  }
  
  public function delete_whats_news($id){
    $this->db->where('id',$id);
    return $this->db->delete('whats_news');
  }

  function make_query_videos($condition)
  {
    $this->db->select('videos.*');
    $this->db->from('videos');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('title', $_POST["search"]["value"]);
   }
   $this->db->order_by('id','desc');
     
  }
    function make_datatables_videos($condition){
	  $this->make_query_videos($condition,);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array();
  }

  function get_filtered_data_videos($condition){
	  $this->make_query_videos($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  function get_all_data_videos($condition)
  {
    $this->db->select('videos.*');
    $this->db->from('videos');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('title', $_POST["search"]["value"]);
   }
   $this->db->order_by('id','desc');
	   return $this->db->count_all_results();
  }

  public function store_videos($data){
    return $this->db->insert('videos',$data);
  }

  function get_videos($condition)
  {
    $this->db->where($condition);
	  return $this->db->get('videos')->result();
  }

 public function get_video($condition){
  $this->db->where($condition);
  return $this->db->get('videos')->row();
 }

 public function update_videos($data, $videoID){
  $this->db->where('id',$videoID);
  return $this->db->update('videos',$data);
 }

 public function delete_videos($id){
  $this->db->where('id',$id);
  return $this->db->delete('videos');
 }

 function make_query_discount($condition)
  {
    $this->db->select('course_discount.*,courses.name as courseName');
    $this->db->from('course_discount');
    $this->db->join('courses','courses.id = course_discount.courseID','left');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('course_discount.title', $_POST["search"]["value"]);
    $this->db->or_like('course_discount.coupon', $_POST["search"]["value"]);
    $this->db->or_like('course_discount.discount', $_POST["search"]["value"]);
    $this->db->or_like('course_discount.discount_type', $_POST["search"]["value"]);
    $this->db->or_like('courses.name', $_POST["search"]["value"]);
   }
   $this->db->order_by('course_discount.id','desc');
     
  }
    function make_datatables_discount($condition){
	  $this->make_query_discount($condition,);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array();
  }

  function get_filtered_data_discount($condition){
	  $this->make_query_discount($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  function get_all_data_discount($condition)
  {
    $this->db->select('course_discount.*,courses.name as courseName');
    $this->db->from('course_discount');
    $this->db->join('courses','courses.id = course_discount.courseID','left');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('course_discount.title', $_POST["search"]["value"]);
    $this->db->or_like('course_discount.coupon', $_POST["search"]["value"]);
    $this->db->or_like('course_discount.discount', $_POST["search"]["value"]);
    $this->db->or_like('course_discount.discount_type', $_POST["search"]["value"]);
    $this->db->or_like('courses.name', $_POST["search"]["value"]);
   }
   $this->db->order_by('course_discount.id','desc');
	   return $this->db->count_all_results();
  }

 public function get_discount($condition){
  $this->db->select('course_discount.*,courses.name as courseName');
    $this->db->from('course_discount');
    $this->db->join('courses','courses.id = course_discount.courseID','left');
    $this->db->where($condition);
  return $this->db->get()->row();
 }

 public function store_discount($data){
  return $this->db->insert('course_discount',$data);
}

public function update_discount($data, $id){
  $this->db->where('id',$id);
  return $this->db->update('course_discount',$data);
 }

 public function delete_discount($id){
  $this->db->where('id',$id);
  return $this->db->delete('course_discount');
 }

 

public function update_status($data, $id){
  $this->db->where('id',$id);
  return $this->db->update('banner_setting',$data);
}

public function delete_banner($id){
  $this->db->where('id',$id);
  return $this->db->delete('banner_setting');
}

function make_query_toppers($condition)
{
  $this->db->select('toppers_talk.*');
  $this->db->from('toppers_talk');
  $this->db->where($condition);

 if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
 {
  $this->db->like('title', $_POST["search"]["value"]);
 }
 $this->db->order_by('id','desc');
   
}
  function make_datatables_toppers($condition){
  $this->make_query_toppers($condition,);
  if($_POST["length"] != -1)
  {
    $this->db->limit($_POST['length'], $_POST['start']);
  }
  $query = $this->db->get();
  return $query->result_array();
}

function get_filtered_data_toppers($condition){
  $this->make_query_toppers($condition);
  $query = $this->db->get();
  return $query->num_rows();
  //echo $this->db->last_query();die;
}
function get_all_data_toppers($condition)
{
  $this->db->select('toppers_talk.*');
  $this->db->from('toppers_talk');
  $this->db->where($condition);

 if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
 {
  $this->db->like('title', $_POST["search"]["value"]);
 }
 $this->db->order_by('id','desc');
   return $this->db->count_all_results();
}

public function store_toppers($data){
  return $this->db->insert('toppers_talk',$data);
}

function get_toppers($condition)
{
  $this->db->where($condition);
  return $this->db->get('toppers_talk')->result();
}

public function get_topper($condition){
$this->db->where($condition);
return $this->db->get('toppers_talk')->row();
}

public function update_toppers($data, $videoID){
$this->db->where('id',$videoID);
return $this->db->update('toppers_talk',$data);
}

public function delete_toppers($id){
$this->db->where('id',$id);
return $this->db->delete('toppers_talk');
}


function make_query_mock_interview($condition)
{
  $this->db->select('mock_interview.*');
  $this->db->from('mock_interview');
  $this->db->where($condition);

 if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
 {
  $this->db->like('title', $_POST["search"]["value"]);
 }
 $this->db->order_by('id','desc');
   
}
  function make_datatables_mock_interview($condition){
  $this->make_query_mock_interview($condition);
  if($_POST["length"] != -1)
  {
    $this->db->limit($_POST['length'], $_POST['start']);
  }
  $query = $this->db->get();
  return $query->result_array();
}

function get_filtered_data_mock_interview($condition){
  $this->make_query_mock_interview($condition);
  $query = $this->db->get();
  return $query->num_rows();
  //echo $this->db->last_query();die;
}
function get_all_data_mock_interview($condition)
{
  $this->db->select('mock_interview.*');
  $this->db->from('mock_interview');
  $this->db->where($condition);

 if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
 {
  $this->db->like('title', $_POST["search"]["value"]);
 }
 $this->db->order_by('id','desc');
   return $this->db->count_all_results();
}

public function store_mock_interview($data){
  return $this->db->insert('mock_interview',$data);
}

function get_mock_interviews($condition)
{
  $this->db->where($condition);
  return $this->db->get('mock_interview')->result();
}

public function get_mock_interview($condition){
$this->db->where($condition);
return $this->db->get('mock_interview')->row();
}

public function update_mock_interview($data, $videoID){
$this->db->where('id',$videoID);
return $this->db->update('mock_interview',$data);
}

public function delete_mock_interview($id){
$this->db->where('id',$id);
return $this->db->delete('mock_interview');
}

function make_query_student_remark($condition)
{
  $this->db->select('student_remark.*');
  $this->db->from('student_remark');
  $this->db->where($condition);

 if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
 {
  $this->db->like('title', $_POST["search"]["value"]);
 }
 $this->db->order_by('id','desc');
   
}
  function make_datatables_student_remark($condition){
  $this->make_query_student_remark($condition,);
  if($_POST["length"] != -1)
  {
    $this->db->limit($_POST['length'], $_POST['start']);
  }
  $query = $this->db->get();
  return $query->result_array();
}

function get_filtered_data_student_remark($condition){
  $this->make_query_student_remark($condition);
  $query = $this->db->get();
  return $query->num_rows();
  //echo $this->db->last_query();die;
}
function get_all_data_student_remark($condition)
{
  $this->db->select('student_remark.*');
  $this->db->from('student_remark');
  $this->db->where($condition);

 if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
 {
  $this->db->like('title', $_POST["search"]["value"]);
 }
 $this->db->order_by('id','desc');
   return $this->db->count_all_results();
}

public function store_student_remark($data){
  return $this->db->insert('student_remark',$data);
}

function get_student_remarks($condition)
{
  $this->db->where($condition);
  return $this->db->get('student_remark')->result();
}

public function get_student_remark($condition){
$this->db->where($condition);
return $this->db->get('student_remark')->row();
}

public function update_student_remark($data, $videoID){
$this->db->where('id',$videoID);
return $this->db->update('student_remark',$data);
}

public function delete_student_remark($id){
$this->db->where('id',$id);
return $this->db->delete('student_remark');
}

function get_building_futures($condition)
{
  $this->db->where($condition);
  return $this->db->get('building_futures')->result();
}

// function check_buildinFuturs($condition){
//   $this->db->where($condition);
//   return $this->db->get('building_futures')->num_rows();
//    //echo $this->db->last_query(); die;
// }

function get_building_future($condition)
{
  $this->db->where($condition);
  return $this->db->get('building_futures')->row();
}

function make_query_building_futures($condition)
{
  $this->db->select('building_futures.*');
  $this->db->from('building_futures');
  $this->db->where($condition);

 if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
 {
  $this->db->like('title', $_POST["search"]["value"]);
 }
 $this->db->order_by('id','desc');
   
}
  function make_datatables_building_futures($condition){
  $this->make_query_building_futures($condition);
  if($_POST["length"] != -1)
  {
    $this->db->limit($_POST['length'], $_POST['start']);
  }
  $query = $this->db->get();
  return $query->result_array();
}

function get_filtered_data_building_futures($condition){
  $this->make_query_building_futures($condition);
  $query = $this->db->get();
  return $query->num_rows();
  //echo $this->db->last_query();die;
}
function get_all_data_building_futures($condition)
{
  $this->db->select('building_futures.*');
  $this->db->from('building_futures');
  $this->db->where($condition);

 if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
 {
  $this->db->like('title', $_POST["search"]["value"]);
 }
 $this->db->order_by('id','desc');
   return $this->db->count_all_results();
}

function store_building_futures($data){
  return $this->db->insert('building_futures', $data);
}

function update_building_futures($data,$id){
$this->db->where('id',$id);
return $this->db->update('building_futures', $data);
}

function delete_building_futures($id){
  $this->db->where('id',$id);
  return $this->db->delete('building_futures');
}

function get_vocabulary($condition){
  $this->db->where($condition);
  return $this->db->get('vocabulary')->row();
  //echo $this->db->last_query(); die;
}
// function check_vocabulary($condition){
//   $this->db->where($condition);
//   return $this->db->get('vocabulary')->num_rows();
   //echo $this->db->last_query(); die;
//}

function get_vocabularies($condition){
  $this->db->where($condition);
  return $this->db->get('vocabulary')->result();
  //echo $this->db->last_query(); die;
}

function make_query_vocabulary($condition)
{
  $this->db->select('vocabulary.*');
  $this->db->from('vocabulary');
  $this->db->where($condition);

 if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
 {
  $this->db->like('title', $_POST["search"]["value"]);
 }
 $this->db->order_by('id','desc');
   
}
  function make_datatables_vocabulary($condition){
  $this->make_query_vocabulary($condition);
  if($_POST["length"] != -1)
  {
    $this->db->limit($_POST['length'], $_POST['start']);
  }
  $query = $this->db->get();
  return $query->result_array();
}

function get_filtered_vocabulary($condition){
  $this->make_query_vocabulary($condition);
  $query = $this->db->get();
  return $query->num_rows();
  //echo $this->db->last_query();die;
}
function get_all_data_vocabulary($condition)
{
  $this->db->select('vocabulary.*');
  $this->db->from('vocabulary');
  $this->db->where($condition);

 if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
 {
  $this->db->like('title', $_POST["search"]["value"]);
 }
 $this->db->order_by('id','desc');
   return $this->db->count_all_results();
}

function store_vocabulary($data){
  return $this->db->insert('vocabulary', $data);
}

function update_vocabulary($data, $id){
  $this->db->where('id',$id);
  return $this->db->update('vocabulary', $data);
}

function delete_vocabulary($id){
  $this->db->where('id',$id);
  return $this->db->delete('vocabulary');
}

function make_query_upcomingExam($condition)
{
  $this->db->select('upcoming_exam.*');
  $this->db->from('upcoming_exam');
  $this->db->where($condition);

 if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
 {
  $this->db->like('title', $_POST["search"]["value"]);
 }
 $this->db->order_by('id','desc');
   
}
  function make_datatables_upcomingExam($condition){
  $this->make_query_upcomingExam($condition);
  if($_POST["length"] != -1)
  {
    $this->db->limit($_POST['length'], $_POST['start']);
  }
  $query = $this->db->get();
  return $query->result_array();
}

function get_filtered_data_upcomingExam($condition){
  $this->make_query_upcomingExam($condition);
  $query = $this->db->get();
  return $query->num_rows();
  //echo $this->db->last_query();die;
}
function get_all_data_upcomingExam($condition)
{
  $this->db->select('upcoming_exam.*');
  $this->db->from('upcoming_exam');
  $this->db->where($condition);

 if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
 {
  $this->db->like('title', $_POST["search"]["value"]);
 }
 $this->db->order_by('id','desc');
   return $this->db->count_all_results();
}

// function check_upcomingExam($condition){
//   $this->db->where($condition);
//   return $this->db->get('upcoming_exam')->num_rows();
// }

function get_upcoming_exam($condition){
  $this->db->where($condition);
  return $this->db->get('upcoming_exam')->row();
   //echo $this->db->last_query(); die;
}

function store_upcomingExam($data){
 return $this->db->insert('upcoming_exam',$data);
}

function delete_upcomingExam($id){
  $this->db->where('id',$id);
  return $this->db->delete('upcoming_exam');
}

function update_upcomingExam($data, $id){
  $this->db->where('id',$id);
  return $this->db->update('upcoming_exam', $data);
}





}