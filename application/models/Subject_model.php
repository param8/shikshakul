<?php 

class Subject_model extends CI_Model

{

	public function __construct()

	{
		parent::__construct();
	}

	function make_query($condition)
  {
	  $this->db->select('subject.*,category.name as categoryName,courses.name as coursesName');
    $this->db->from('subject');
    $this->db->join('category', 'category.id=subject.categoryID','left');
    $this->db->join('courses', 'courses.id=subject.courseID','left');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('subject.name', $_POST["search"]["value"]);
    $this->db->or_like('category.name', $_POST["search"]["value"]);
    $this->db->or_like('courses.name', $_POST["search"]["value"]);
   }
   $this->db->order_by('subject.id','desc');
     
  }
    function make_datatables($condition){
	  $this->make_query($condition,);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array();
  }

  function get_filtered_data($condition){
	  $this->make_query($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  function get_all_data($condition)
  {
    $this->db->select('subject.*,category.name as categoryName,courses.name as coursesName');
    $this->db->from('subject');
    $this->db->join('category', 'category.id=subject.categoryID','left');
    $this->db->join('courses', 'courses.id=subject.courseID','left');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('subject.name', $_POST["search"]["value"]);
    $this->db->or_like('category.name', $_POST["search"]["value"]);
    $this->db->or_like('courses.name', $_POST["search"]["value"]);
   }
   $this->db->order_by('subject.id','desc');
	   return $this->db->count_all_results();
  }

  public function get_subject($condition){
    $this->db->select('subject.*,category.name as categoryName,courses.name as coursesName');
    $this->db->from('subject');
    $this->db->join('category', 'category.id=subject.categoryID','left');
    $this->db->join('courses', 'courses.id=subject.courseID','left');
    $this->db->where($condition);
	 return $this->db->get()->row();
  }

  public function get_subjects($condition){
    $this->db->select('subject.*,category.name as categoryName,courses.name as coursesName');
    $this->db->from('subject');
    $this->db->join('category', 'category.id=subject.categoryID','left');
    $this->db->join('courses', 'courses.id=subject.courseID','left');
    $this->db->where($condition);
	  return $this->db->get()->result();
    //echo $this->db->last_query();die;
  }

  public function store_subject($data){
    return $this->db->insert('subject',$data);
  }

  public function update_subject($data,$subjectID){
	$this->db->where('id',$subjectID);
	return $this->db->update('subject',$data);
  }

  public function delete_subject($condition){
    $this->db->where($condition);
    return $this->db->delete('subject');
    }

    public function store_topic($data){
      return $this->db->insert('topics',$data);
    }

    function make_query_topic($condition)
  {
	$this->db->select('topics.*,courses.name as courseName,subject.name as subjectName');
    $this->db->from('topics');
    $this->db->join('courses', 'topics.courseID = courses.id','left');
    $this->db->join('subject', 'topics.subjectID = subject.id','left');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('courses.name', $_POST["search"]["value"]);
    $this->db->or_like('subject.name', $_POST["search"]["value"]);
   }
   $this->db->order_by('topics.id','desc');
     
  }
    function make_datatables_topic($condition){
	  $this->make_query_topic($condition,);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array();
  }

  function get_filtered_data_topic($condition){
	  $this->make_query_topic($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  function get_all_data_topic($condition)
  {
    $this->db->select('topics.*,courses.name as courseName,subject.name as subjectName');
    $this->db->from('topics');
    $this->db->join('courses', 'topics.courseID = courses.id','left');
    $this->db->join('subject', 'topics.subjectID = subject.id','left');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('courses.name', $_POST["search"]["value"]);
    $this->db->or_like('subject.name', $_POST["search"]["value"]);
   }
   $this->db->order_by('topics.id','desc');
	   return $this->db->count_all_results();
  }

  public function get_topic($condition){
    $this->db->select('topics.*,courses.name as courseName,subject.name as subjectName');
    $this->db->from('topics');
    $this->db->join('courses', 'topics.courseID = courses.id','left');
    $this->db->join('subject', 'topics.subjectID = subject.id','left');
   $this->db->where($condition);
   return $this->db->get()->row();
  }

  public function delete_topic($condition){
    $this->db->where($condition);
    return $this->db->delete('topics');
  }

  public function update_topic($data,$condition){
    $this->db->where($condition);
	return $this->db->update('topics',$data);
  }

}