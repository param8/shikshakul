<?php 

class Category_model extends CI_Model

{

	public function __construct()

	{
		parent::__construct();
	}



	function make_query($condition)
  {
	$this->db->select('category.*');
    $this->db->from('category');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->or_like('name', $_POST["search"]["value"]);
    $this->db->or_like('language', $_POST["search"]["value"]);
   }
   $this->db->order_by('id','desc');
     
  }
    function make_datatables($condition){
	  $this->make_query($condition,);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array();
  }

  function get_filtered_data($condition){
	  $this->make_query($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  function get_all_data($condition)
  {
	$this->db->select('category.*');
    $this->db->from('category');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->or_like('name', $_POST["search"]["value"]);
    $this->db->or_like('language', $_POST["search"]["value"]);
   }
   $this->db->order_by('id','desc');
	   return $this->db->count_all_results();
  }

  public function get_categories($condition,$page=null){
    $this->db->select('category.*');
    $this->db->from('category'); 
	  $this->db->where($condition);
    if($page=='Home'){
      $this->db->order_by('rand()');
      $this->db->limit('8');
    }
    $this->db->order_by('id','desc');
	  return $this->db->get()->result();
  }

  public function get_category($condition){
    $this->db->select('category.*');
    $this->db->from('category'); 
	  $this->db->where($condition);
	  return $this->db->get()->row();
  }

  public function store($data){
     $this->db->insert('category',$data);
     return $this->db->insert_id();
  }

  public function update($data,$id){
    $this->db->where('id',$id);
    return $this->db->update('category',$data);
  }

  public function delete($id){
    $this->db->where('id',$id);
    return $this->db->delete('category');
  }


}