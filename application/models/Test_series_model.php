<?php 
class Test_series_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}


  function make_query($condition)
  {
    $this->db->select('test_series.*,courses.name as courseName,subject.name as subjectName,users.name as usersName');
    $this->db->from('test_series');
    $this->db->join('courses','courses.id = test_series.courseID','left');
    $this->db->join('subject','subject.id = test_series.subjectID','left');
    $this->db->join('users','users.id = test_series.userID','left');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('test_series.title', $_POST["search"]["value"]);
    $this->db->or_like('courses.name', $_POST["search"]["value"]);
    $this->db->or_like('subject.name', $_POST["search"]["value"]);
    $this->db->or_like('users.name', $_POST["search"]["value"]);
   }
   $this->db->order_by('test_series.id','desc');
    
  }
    function make_datatables($condition){
	  $this->make_query($condition);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array(); 
  // echo $this->db->last_query(); die;
  }

  function get_filtered_data($condition){
	  $this->make_query($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  function get_all_data($condition)
  {
    $this->db->select('test_series.*,courses.name as courseName,subject.name as subjectName,users.name as usersName');
    $this->db->from('test_series');
    $this->db->join('courses','courses.id = test_series.courseID','left');
    $this->db->join('subject','subject.id = test_series.subjectID','left');
    $this->db->join('users','users.id = test_series.userID','left');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('test_series.title', $_POST["search"]["value"]);
    $this->db->or_like('courses.name', $_POST["search"]["value"]);
    $this->db->or_like('subject.name', $_POST["search"]["value"]);
    $this->db->or_like('users.name', $_POST["search"]["value"]);
   }
   $this->db->order_by('test_series.id','desc');
	   return $this->db->count_all_results();
  }

  public function get_test_series($condition){
    $this->db->select('test_series.*,courses.name as courseName,subject.name as subjectName,users.name as usersName');
    $this->db->from('test_series');
    $this->db->join('courses','courses.id = test_series.courseID','left');
    $this->db->join('subject','subject.id = test_series.subjectID','left');
    $this->db->join('users','users.id = test_series.userID','left');
    $this->db->where($condition);
    return $this->db->get()->row();
  }

  public function get_test_seriess($condition){
    $this->db->select('test_series.*,courses.name as courseName,subject.name as subjectName,users.name as usersName');
    $this->db->from('test_series');
    $this->db->join('courses','courses.id = test_series.courseID','left');
    $this->db->join('subject','subject.id = test_series.subjectID','left');
    $this->db->join('users','users.id = test_series.userID','left');
    $this->db->where($condition);
    return $this->db->get()->result();
  }

  public function store_test_series($data){
     $this->db->insert('test_series',$data);
    return $this->db->insert_id();
  }

  public function update_test_series($data,$id){
    $this->db->where('id',$id);
    return $this->db->update('test_series',$data);
 }

 public function delete_test_series($id){
  $this->db->where('id',$id);
  return $this->db->delete('test_series');
 }

 function make_query_series($condition)
 {
   $this->db->select('series.*,test_series.title as testSeries');
   $this->db->from('series');
   $this->db->join('test_series','series.testSeriesID = test_series.id','left');
   $this->db->where($condition);

  if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
  {
   $this->db->like('series.title', $_POST["search"]["value"]);
   $this->db->or_like('test_series.title', $_POST["search"]["value"]);
  }
  $this->db->order_by('series.id','desc');
   
 }
   function make_datatables_series($condition){
   $this->make_query_series($condition);
   if($_POST["length"] != -1)
   {
     $this->db->limit($_POST['length'], $_POST['start']);
   }
   $query = $this->db->get();
   return $query->result_array(); 
 // echo $this->db->last_query(); die;
 }

 function get_filtered_data_series($condition){
   $this->make_query_series($condition);
   $query = $this->db->get();
   return $query->num_rows();
   //echo $this->db->last_query();die;
 }
 function get_all_data_series($condition)
 {
  $this->db->select('series.*,test_series.title as testSeries');
  $this->db->from('series');
  $this->db->join('test_series','series.testSeriesID = test_series.id','left');
  $this->db->where($condition);

 if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
 {
  $this->db->like('series.title', $_POST["search"]["value"]);
  $this->db->or_like('test_series.title', $_POST["search"]["value"]);
 }
 $this->db->order_by('series.id','desc');
    return $this->db->count_all_results();
 }

 public function get_series($condition){
  $this->db->select('series.*,test_series.title as testSeries');
  $this->db->from('series');
  $this->db->join('test_series','series.testSeriesID = test_series.id','left');
  $this->db->where($condition);
  return $this->db->get()->row();
 }

 public function get_seriess($condition){
  $this->db->select('series.*,test_series.title as testSeries');
  $this->db->from('series');
  $this->db->join('test_series','series.testSeriesID = test_series.id','left');
  $this->db->where($condition);
  return $this->db->get()->result();
 }

 public function get_series_count($condition){
  $this->db->select('count(id) as totalSeries');
  $this->db->from('series');
  $this->db->where($condition);
  return $this->db->get()->row();
 }

 public function store_series($data){
  $this->db->insert('series',$data);
  return $this->db->insert_id();
 }

 public function store_series_question($data){
  $this->db->insert('series_questions',$data);
  return $this->db->insert_id();
 }

 public function store_series_option($data){
  return $this->db->insert('series_options',$data);
 }

 public function get_series_option($condition){
  $this->db->where($condition);
  return $this->db->get('series_options')->row();
 }

 public function get_series_options($condition){
  $this->db->where($condition);
  return $this->db->get('series_options')->result();
 }

 public function store_series_answer($data){
  return $this->db->insert('series_answer',$data);
 }

 public function get_series_questions($condition){
  $this->db->where($condition);
  $query = $this->db->get('series_questions')->result_array();
  
  foreach($query as $key=>$row){
   $this->db->select('series_options.*');
   $this->db->from('series_options');
   $this->db->where('series_options.qID',$row['id']);
   $query[$key]['option'] = $this->db->get()->result_array();
  }
  
  foreach($query as $key_option=>$quiz_ans){
    foreach($quiz_ans['option'] as $key_ans=>$ans ){
     $this->db->select('series_options.option as correct_answer');
     $this->db->from('series_answer');
     $this->db->join('series_options', 'series_answer.optionID=series_options.id','left');
     $this->db->where('series_answer.optionID',$ans['id']);
     $query[$key_option][$key_ans]['answer'] = $this->db->get()->row_array();
    }
  }
//echo $this->db->last_query();die;
 return $query;
}

public function delete_series_question($questionID){
  $this->db->where('qID',$questionID);
  $this->db->delete('series_answer');
  $this->db->where('qID',$questionID);
  $this->db->delete('series_options');
  $this->db->where('id',$questionID);
  return $this->db->delete('series_questions');
   //echo $this->db->last_query();die;
}

public function delete_series($id){
  $this->db->where('id',$id);
  return  $this->db->delete('series');
}



}