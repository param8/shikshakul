<?php 
class Chat_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

  public function ownerDetails(){
		if(isset($_SESSION['unique_id'])){
			$this->db->select('users.*,user_detail.dob');
      $this->db->join('user_detail','user_detail.userID = users.id','left');
			$this->db->where('unique_id',$_SESSION['unique_id']);
			$res = $this->db->get('users')->result_array();
			return $res;
		}
	}
	public function allUser(){
		if(isset($_SESSION['unique_id'])){
			$mysession = $_SESSION['unique_id'];
			$this->db->select('users.*,user_detail.dob');
      $this->db->join('user_detail','user_detail.userID = users.id','left');
			$this->db->where('unique_id != ',$mysession);
			$this->db->where('users.status',1);
      if($this->session->userdata('user_type')=='Student'){
        $this->db->where('user_type = ','Faculties');
				$this->db->or_where('user_type = ','Admin');
      }

      if($this->session->userdata('user_type')=='Faculties'){
        $this->db->where('user_type = ','Student');
      }
			$data = $this->db->get('users');
			if($data->num_rows() > 0){
				return $data->result_array();
			}else{
				return false;
			}
		}
	}

	public function sentMessage($data){
	return	$this->db->insert('user_messages',$data);
	}
	public function getmessage($data){
		$this->db->select('*');
		$session_id = $_SESSION['unique_id'];
		$where = "(sender_message_id = '$session_id' AND receiver_message_id = '$data') OR 
		(sender_message_id = '$data' AND receiver_message_id = '$session_id')";
		$this->db->where($where);
		// $this->db->order_by('time', 'ASC');
		$result = $this->db->get('user_messages')->result_array();
    //echo $this->db->last_query();die;
		 return $result;
	}
	public function getLastMessage($data){
		$this->db->select('*');
		$session_id = $_SESSION['unique_id'];
		$where = "sender_message_id = '$session_id' AND receiver_message_id = '$data' OR 
		sender_message_id = '$data' AND receiver_message_id = '$session_id'";
		$this->db->where($where);
		$this->db->order_by('time', 'DESC');
		$result = $this->db->get('user_messages', 1)->result_array();
		return $result;
	}
	public function getIndividual($id){
		$this->db->select('*');
		$this->db->where('unique_id',$id);
		$res = $this->db->get('users')->result_array();
		return $res;
	}

	// public function blockUser($arr){
	// 	$this->db->insert('block_user',$arr);
	// }
	// public function unBlockUser($val1, $val2){
	// 	$this->db->query("DELETE FROM block_user WHERE blocked_from = '$val1' AND blocked_to = '$val2'");
	// }
	// public function getBlockUserData($val1, $val2){
	// 	$this->db->select('*');
	// 	$where = "blocked_from = '$val1' AND blocked_to = '$val2' OR blocked_from = '$val2' AND blocked_to = '$val1'";
	// 	$this->db->where($where);
	// 	$res = $this->db->get('block_user')->result_array();

	// 	return $res;
	// }

  

}
