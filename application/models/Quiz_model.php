<?php 
class Quiz_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

  function make_query_quiz_type($condition)
  {
    $this->db->select('quiz_type.*');
    $this->db->from('quiz_type');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('quiz_type.title', $_POST["search"]["value"]);
   }
   $this->db->order_by('quiz_type.id','desc');
    
  }
    function make_datatables_quiz_type($condition){
	  $this->make_query_quiz_type($condition);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array(); 
  // echo $this->db->last_query(); die;
  }

  function get_filtered_data_quiz_type($condition){
	  $this->make_query_quiz_type($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  function get_all_data_quiz_type($condition)
  {
    $this->db->select('quiz_type.*');
    $this->db->from('quiz_type');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('quiz_type.title', $_POST["search"]["value"]);
   }
   $this->db->order_by('quiz_type.id','desc');
	   return $this->db->count_all_results();
  }


  function get_quiz_types($condition){
    $this->db->where($condition);
    return $this->db->get('quiz_type')->result();
  }

  function get_quiz_type($condition){
    $this->db->where($condition);
    return $this->db->get('quiz_type')->row();
  }

  function store_quiz_type($data){
    return $this->db->insert('quiz_type',$data);
  }

  function update_quiz_type($data,$id){
    $this->db->where('id',$id);
    return $this->db->update('quiz_type',$data);
  }

  function make_query($condition)
  {
    $this->db->select('quiz.*,courses.name as courseName,subject.name as subjectName,quiz_type.title as quizType');
    $this->db->from('quiz');
    $this->db->join('courses','courses.id = quiz.courseID','left');
    $this->db->join('subject','subject.id = quiz.subjectID','left');
    $this->db->join('quiz_type','quiz_type.id = quiz.quizTypeID','left');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('quiz.title', $_POST["search"]["value"]);
    $this->db->or_like('courses.name', $_POST["search"]["value"]);
    $this->db->or_like('subject.name', $_POST["search"]["value"]);
    $this->db->or_like('quiz_type.title', $_POST["search"]["value"]);
   }
   $this->db->order_by('quiz.id','desc');
    
  }
    function make_datatables($condition){
	  $this->make_query($condition);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array(); 
  // echo $this->db->last_query(); die;
  }

  function get_filtered_data($condition){
	  $this->make_query($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  function get_all_data($condition)
  {
    $this->db->select('quiz.*,courses.name as courseName,subject.name as subjectName,quiz_type.title as quizType');
    $this->db->from('quiz');
    $this->db->join('courses','courses.id = quiz.courseID','left');
    $this->db->join('subject','subject.id = quiz.subjectID','left');
    $this->db->join('quiz_type','quiz_type.id = quiz.quizTypeID','left');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('quiz.title', $_POST["search"]["value"]);
    $this->db->or_like('courses.name', $_POST["search"]["value"]);
    $this->db->or_like('subject.name', $_POST["search"]["value"]);
    $this->db->or_like('quiz_type.title', $_POST["search"]["value"]);
   }
   $this->db->order_by('quiz.id','desc');
	   return $this->db->count_all_results();
  }

  public function store_quiz($data){
     $this->db->insert('quiz',$data);
    return $this->db->insert_id();
  }

  public function store_quiz_question($data){
   $this->db->insert('quiz_questions',$data);
    return $this->db->insert_id();
  }

  public function store_quiz_option($data){
    return $this->db->insert('quiz_options',$data);
  }

  public function store_quiz_answer($data){
    return $this->db->insert('quiz_answer',$data);
  }

  public function get_quiz_options($condition){
    $this->db->where($condition);
    return $this->db->get('quiz_options')->result();
  }

  public function get_quiz_option($condition){
    $this->db->where($condition);
    return $this->db->get('quiz_options')->row();
  }

  public function get_quiz($condition){ 
    $this->db->select('quiz.*,courses.name as courseName,subject.name as subjectName,quiz_type.title as quizType');
    $this->db->from('quiz');
    $this->db->join('courses','courses.id = quiz.courseID','left');
    $this->db->join('subject','subject.id = quiz.subjectID','left');
    $this->db->join('quiz_type','quiz_type.id = quiz.quizTypeID','left');
    $this->db->where($condition);
    $this->db->order_by('quiz.id','desc');
    return $this->db->get()->row();
  }

  public function get_quiz_questions($condition){
   $this->db->where($condition);
   $query = $this->db->get('quiz_questions')->result_array();
   
   foreach($query as $key=>$row){
    $this->db->select('quiz_options.*');
    $this->db->from('quiz_options');
    $this->db->where('quiz_options.qID',$row['id']);
    $query[$key]['option'] = $this->db->get()->result_array();
    
   }

   foreach($query as $key_option=>$quiz_ans){
     foreach($quiz_ans['option'] as $key_ans=>$ans ){
      $this->db->select('quiz_options.option as correct_answer');
      $this->db->from('quiz_answer');
      $this->db->join('quiz_options', 'quiz_answer.optionID=quiz_options.id','left');
      $this->db->where('quiz_answer.optionID',$ans['id']);
      $query[$key_option][$key_ans]['answer'] = $this->db->get()->row_array();

     }
   }

  return $query;
  }

  public function delete_quiz_question($questionID){
    $this->db->where('qID',$questionID);
    $this->db->delete('quiz_answer');
    $this->db->where('qID',$questionID);
    $this->db->delete('quiz_options');
    $this->db->where('id',$questionID);
   return  $this->db->delete('quiz_questions');
  }

  public function delete_quiz($id){
    $this->db->where('id',$id);
    return  $this->db->delete('quiz');
  }



public function store_quiz_result($data){
  return $this->db->insert('quiz_result',$data);
}

public function get_quiz_result($condition){
  $this->db->select('quiz_result.*, quiz.title');
  $this->db->from('quiz_result');
  $this->db->join('quiz','quiz.id=quiz_result.quizID','left');
  $this->db->where($condition);
  $this->db->order_by('quiz_result.id','desc');
  return $this->db->get()->result();
}

public function make_result_query($condition){
  $this->db->select('quiz_result.*, quiz.title,quiz_type.title as quiz_type');
  $this->db->from('quiz_result');
  $this->db->join('quiz','quiz.id=quiz_result.quizID','left');
  $this->db->join('quiz_type','quiz_type.id=quiz.quizTypeID','left');
  $this->db->where($condition);

 if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
 {
  $this->db->like('quiz.title', $_POST["search"]["value"]);
 }
 $this->db->order_by('quiz_result.id','desc');
}

public function make_quiz_result_datatables($condition){
  $this->make_result_query($condition);
  if($_POST["length"] != -1)
  {
    $this->db->limit($_POST['length'], $_POST['start']);
  }
  $query = $this->db->get();
  return $query->result_array(); 
 //echo $this->db->last_query(); die;
}

function get_filtered_result_quiz_data($condition){
  $this->make_result_query($condition);
  $query = $this->db->get();
  return $query->num_rows();
  //echo $this->db->last_query();die;
}
function get_result_quiz_all_data($condition)
{
  $this->db->select('quiz_result.*, quiz.title,quiz_type.title as quiz_type');
  $this->db->from('quiz_result');
  $this->db->join('quiz','quiz.id=quiz_result.quizID','left');
  $this->db->join('quiz_type','quiz_type.id=quiz.quizTypeID','left');
  $this->db->where($condition);

 if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
 {
  $this->db->or_like('quiz.title', $_POST["search"]["value"]);
 }
 $this->db->order_by('quiz_result.id','desc');
   return $this->db->count_all_results();
}

public function get_questions($condition){
  $this->db->select('questions.*,courses.name as course,subject.name as subject ');
  $this->db->from('questions');
  $this->db->JOIN('courses','courses.id=questions.courseID','left');
  $this->db->JOIN('subject','subject.id=questions.subjectID','left');
  $this->db->where($condition);
  return $this->db->get()->result();
}

public function get_answers($condition){
  $this->db->select('*');
  $this->db->from('answers');
  $this->db->where($condition);
  return $this->db->get()->result();
}

public function store_question($data){
  return $this->db->insert('questions',$data);
}

public function store_answer($data){
  return $this->db->insert('answers',$data);
}

function make_query_questions()
  {
    $this->db->select('questions.*, users.name as userName, courses.name as course, subject.name as subject');
    $this->db->from('questions');
    $this->db->join('users','users.id=questions.facultyID','left');
    $this->db->join('courses','courses.id=questions.courseID','left');
    $this->db->join('subject','subject.id=questions.subjectID','left');

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('questions.title', $_POST["search"]["value"]);
   }
   $this->db->order_by('questions.id','desc');
    
  }
    function make_datatables_questions(){
	  $this->make_query_questions();
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array(); 
  // echo $this->db->last_query(); die;
  }

  function get_filtered_data_questions(){
	  $this->make_query_questions();
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  function get_all_data_questions()
  {
    $this->db->select('questions.*, users.name as userName, courses.name as course, subject.name as subject');
    $this->db->from('questions');
    $this->db->join('users', 'users.id=questions.facultyID','left');
    $this->db->join('courses','courses.id=questions.courseID','left');
    $this->db->join('subject','subject.id=questions.subjectID','left');
   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('questions.title', $_POST["search"]["value"]);
   }
   $this->db->order_by('questions.id','desc');
	   return $this->db->count_all_results();
  }

  function make_query_answers($condition)
  {
    $this->db->select('answers.*,questions.title as title, users.name as userName, courses.name as course,subject.name as subject');
    $this->db->from('answers');
    $this->db->where($condition);
    $this->db->join('users', 'users.id=answers.studentID','left');
    $this->db->join('questions', 'questions.id=answers.questionID','left');
    $this->db->join('courses','courses.id=questions.courseID','left');
    $this->db->join('subject','subject.id=questions.subjectID','left');

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('users.name', $_POST["search"]["value"]);
   }
   $this->db->order_by('answers.id','desc');
    
  }
    function make_datatables_answers($condition){
	  $this->make_query_answers($condition);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array(); 
  }

  function get_filtered_data_answers($condition){
	  $this->make_query_answers($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  function get_all_data_answers($condition)
  {
    $this->db->select('answers.*,questions.title as title, users.name as userName, courses.name as course,subject.name as subject');
    $this->db->from('answers');
    $this->db->where($condition);
    $this->db->join('users', 'users.id=answers.studentID','left');
    $this->db->join('questions', 'questions.id=answers.questionID','left');
    $this->db->join('courses','courses.id=questions.courseID','left');
    $this->db->join('subject','subject.id=questions.subjectID','left');
   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('users.name', $_POST["search"]["value"]);
    $this->db->or_like('questions.title', $_POST["search"]["value"]);
    $this->db->or_like('courses.name', $_POST["search"]["value"]);
    $this->db->or_like('subject.name', $_POST["search"]["value"]);
   }
   $this->db->order_by('answers.id','desc');
	   return $this->db->count_all_results();
  }

  public function store_result($data,$id){
   $this->db->where('id',$id);
   return $this->db->update('answers', $data);
  }

  public function update_status($data,$id){
    $this->db->where('id',$id);
    return $this->db->update('questions',$data);
  }

}