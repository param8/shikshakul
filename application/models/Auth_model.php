<?php 
class Auth_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('encryption');
	}

    public function login($email, $password) {
		if($email && $password) {
      $encryptPass=md5($password);
        $query = $this->db->query("SELECT * FROM users WHERE (email = '$email' OR phone = '$email') AND `password` = '$encryptPass'");
			//$query = $this->db->query($sql, array($email,$email));
			if($query->num_rows() == 1) {
				$result = $query->row_array();

				if(1==$result['status'])
				{
					
          $session = array(
						'id'                  => $result['id'],
			      'unique_id'           => $result['unique_id'],
            'name'                => $result['name'],
            'email'               => $result['email'],
            'phone'               => $result['phone'],
            'address'             => $result['address'],
            'city'                => $result['city'],
            'state'               => $result['state'],
            'user_type'           => $result['user_type'],
			      'profile_pic'         => $result['profile_pic'],
            'status'              => $result['status'],
						'verification_user'   => $result['verification_user'],
						'created_at'          => $result['created_at'],
            'logged_in'           => TRUE
          );
        $this->session->set_userdata($session);
				if($result['user_type']=='Student'){
						 $this->db->where('userID',$result['id']);
						 $verification = $this->db->get('verification')->row();
						 $verification_session = array(
							'phone_status'        => $verification->phone_status,
							'email_status'        => $verification->email_status,
							'mobile_otp'          => $verification->mobile_otp,
							'email_otp'           => $verification->email_otp,
							'verification_status' => $verification->status,
						 );
						 $this->session->set_userdata($verification_session);
				}
				return $result;	
			}
			else {
				return 302;
			}
			}else {
				return 403;
			}
		}
	}
	
	
		public function payment_login($id) {
         $query = $this->db->query("SELECT * FROM users WHERE id = $id");
		 $result = $query->row_array();
          $session = array(
            'id'                  => $result['id'],
			      'unique_id'           => $result['unique_id'],
            'name'                => $result['name'],
            'email'               => $result['email'],
            'phone'               => $result['phone'],
            'address'             => $result['address'],
            'city'                => $result['city'],
            'state'               => $result['state'],
            'user_type'           => $result['user_type'],
			      'profile_pic'         => $result['profile_pic'],
            'status'              => $result['status'],
						'verification_status' => $result['verification_status'],
						'created_at'          => $result['created_at'],
            'logged_in'           => TRUE
          );
        $this->session->set_userdata($session);
				return $result;	
	}

  public function register($data) {
	   $this->db->insert('users', $data);
     return $this->db->insert_id();
   }

	 public function get_verification($condition){
		$this->db->where($condition);
		return $this->db->get('verification')->row();
	 }

	 public function update_verification($data,$id){
		$this->db->where('userID',$id);
		return $this->db->update('verification',$data);
	 }


}
