<?php 
class Schedule extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('course_model');
		$this->load->model('schedule_model');
        $this->load->model('user_model');
		$this->load->model('subject_model');
	}

    public function index()
	{
	  $this->not_admin_logged_in();
      $data['page_title'] = 'Schedule';
	  $this->admin_template('schedule/schedule',$data);
	}

    public function ajaxSchedule(){
    //$this->not_admin_logged_in(); 
		$condition = $this->session->userdata('user_type') == 'Admin' ? array('schedule.status'=>1) : array('schedule.status'=>1,'schedule.facultyID'=>$this->session->userdata('id'));
		$schedules = $this->schedule_model->make_datatables($condition); // this will call modal function for fetching data
		$data = array();
		foreach($schedules as $key=>$schedule) // Loop over the data fetched and store them in array
		{
			$button = '';
			$sub_array = array();
			// $button .= '<a href="'.base_url('view-schedule/'.base64_encode($schedule['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="View schedule" class="btn  btn-sm  bg-warning-light"><i class="fe fe-eye"></i></a>';
			$button .= '<a href="'.base_url('edit-schedule/'.base64_encode($schedule['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit schedule" class="btn btn-sm bg-success-light"><i class="fe fe-edit"></i> </a>';
      $button .= '<a href="javascript:void(0)" onclick="delete_schedule('.$schedule['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete schedule" class="btn btn-sm bg-danger-light"><i class="fe fe-trash"></i> </a>';
			
			$sub_array[] = $key+1;
			$sub_array[] = $schedule['coursesName'];
			$sub_array[] = $schedule['subjectName'];
			$sub_array[] = date('H:iA',$schedule['start_time']);
			$sub_array[] = date('H:iA',$schedule['end_time']);
			$sub_array[] = $schedule['facultyName'];
			$sub_array[] = $schedule['topics'];
			$sub_array[] = date('d-F-Y', strtotime($schedule['class_date']));
			$sub_array[] = $button;
		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->schedule_model->get_all_data($condition),
			"recordsFiltered"         =>     $this->schedule_model->get_filtered_data($condition),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
    }

    public function create_schedule()
	{
	  $this->not_admin_logged_in();
      $data['page_title'] = 'Schedule';
	  $data['courses'] = $this->course_model->get_courses(array('courses.status' => 1));
	  $data['faculties'] = $this->user_model->get_users(array('users.user_type' => 'Faculties'));
	  $this->admin_template('schedule/create_schedule',$data);
	}

	public function edit_schedule()
	{
	  $this->not_admin_logged_in();
	  $id = base64_decode($this->uri->segment(2));
      $data['page_title'] = 'Schedule';
	  $data['courses'] = $this->course_model->get_courses(array('courses.status' => 1));
	  $data['faculties'] = $this->user_model->get_users(array('users.user_type' => 'Faculties'));
	  $data['schedule'] = $this->schedule_model->get_schedule(array('id' => $id));
	 // print_r($data['schedule']);
	  $this->admin_template('schedule/edit_schedule',$data);
	}

	public function store(){
		$courseID     = $this->input->post('courseID');
		$subjectID    = $this->input->post('subjectID');
		$facultyID    = $this->session->userdata('user_type') == 'Admin' ?  $this->input->post('faculty') : $this->session->userdata('id');
		//$day        = $this->input->post('day');
		$start_time = $this->input->post('start_time');
		$end_time   = $this->input->post('end_time');
		$topics     = $this->input->post('topics');
		$class_date = $this->input->post('class_date');
    //$check_schedule = $this->schedule_model->get_schedule(array('facultyID' => $facultyID,'start_time' =>strtotime($start_time),'end_time' => strtotime($end_time),'days' => implode($day)));
		//print_r($check_schedule); 
		if(empty($courseID)){
			echo json_encode(['status'=>403, 'message'=>'Please Select Course']); 	
			exit();
	    }
		if(empty($subjectID)){
			echo json_encode(['status'=>403, 'message'=>'Please Select Subject']); 	
			exit();
	    }
		if(empty($facultyID)){
			echo json_encode(['status'=>403, 'message'=>'Please Select Faculty']); 	
			exit();
	    }

		if(empty($start_time)){
			echo json_encode(['status'=>403, 'message'=>'Please Select start time']); 	
			exit();
	    }
		if(empty($end_time)){
			echo json_encode(['status'=>403, 'message'=>'Please Select end time']); 	
			exit();
	    }

			if(empty($topics)){
				echo json_encode(['status'=>403, 'message'=>'Please enter topics']); 	
				exit();
				}

				if(empty($class_date)){
					echo json_encode(['status'=>403, 'message'=>'Please enter class date']); 	
					exit();
					}

		// if($check_schedule){
    //         echo json_encode(['status'=>403, 'message'=>'This Faculty already Schedule for this time']); 	
		// 	exit();
		// }

	$data = array(
		'courseID'   => $courseID,
		'subjectID'  => $subjectID,
		'facultyID'  => $this->session->userdata('user_type') == 'Admin' ? $facultyID : $this->session->userdata('id'),
		//'days'       => implode(',',$day),
		'start_time' => strtotime($start_time),
		'end_time'   => strtotime($end_time),
		'topics'     => $topics,
		'class_date' => $class_date
	); 

	$store = $this->schedule_model->store($data);
	if($store){
		echo json_encode(['status'=>200, 'message'=>'Schedule add successfully!']);
	}else{
		echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
	}
}


public function update(){
	$id           = $this->input->post('id');
	$courseID     = $this->input->post('courseID');
	$subjectID    = $this->input->post('subjectID');
	$facultyID    = $facultyID    = $this->session->userdata('user_type') == 'Admin' ?  $this->input->post('faculty') : $this->session->userdata('id');		;
		//$day        = $this->input->post('day');
	$start_time   = $this->input->post('start_time');
	$end_time     = $this->input->post('end_time');
	$topics       = $this->input->post('topics');
	$class_date   = $this->input->post('class_date');
    //$check_schedule = $this->schedule_model->get_schedule(array('facultyID' => $facultyID,'start_time' =>strtotime($start_time),'end_time' => strtotime($end_time),'days' => implode($day)));
		//print_r($check_schedule); 
		if(empty($courseID)){
			echo json_encode(['status'=>403, 'message'=>'Please Select Course']); 	
			exit();
	    }
		if(empty($subjectID)){
			echo json_encode(['status'=>403, 'message'=>'Please Select Subject']); 	
			exit();
	    }
		if(empty($facultyID)){
			echo json_encode(['status'=>403, 'message'=>'Please Select Faculty']); 	
			exit();
	    }

		if(empty($start_time)){
			echo json_encode(['status'=>403, 'message'=>'Please Select start time']); 	
			exit();
	    }
		if(empty($end_time)){
			echo json_encode(['status'=>403, 'message'=>'Please Select end time']); 	
			exit();
	    }

			if(empty($topics)){
				echo json_encode(['status'=>403, 'message'=>'Please enter topics']); 	
				exit();
				}

				if(empty($class_date)){
					echo json_encode(['status'=>403, 'message'=>'Please enter class date']); 	
					exit();
					}

		// if($check_schedule){
    //         echo json_encode(['status'=>403, 'message'=>'This Faculty already Schedule for this time']); 	
		// 	exit();
		// }

	$data = array(
		'courseID'   => $courseID,
		'subjectID'  => $subjectID,
		'facultyID'  => $this->session->userdata('user_type') == 'Admin' ? $facultyID : $this->session->userdata('id'),
		//'days'       => implode(',',$day),
		'start_time' => strtotime($start_time),
		'end_time'   => strtotime($end_time),
		'topics'     => $topics,
		'class_date' => $class_date
	); 

 //print_r($data); die;
	$update = $this->schedule_model->update($data,$id);
	if($update){
		echo json_encode(['status'=>200, 'message'=>'Schedule Update successfully!']);
	}else{
		echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
	}
}

public function delete(){
	$id = $this->input->post('id');
    $delete = $this->schedule_model->delete($id);
	if($delete){
		echo json_encode(['status'=>200, 'message'=>'Schedule delete successfully!']);
	}else{
		echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
	}

}


}