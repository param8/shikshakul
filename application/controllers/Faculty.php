<?php 
class Faculty extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->load->model('course_model');
		$this->load->model('user_model');
		$this->load->model('schedule_model');
	}

	public function index()
	{
		$this->not_admin_logged_in();
    $data['page_title'] = 'Faculties';
	  $this->admin_template('users/faculties',$data);
	}

	public function faculties()
	{
    $data['page_title'] = 'Faculties';
		$condition = array('users.status'=>1,'users.user_type'=>'Faculties');
		$data['faculties'] = $this->user_model->get_users($condition);
	  $this->template('web/faculty/faculty',$data);
	}

	public function dashboard()
	{
		$this->not_logged_in();
        $data['page_title'] = 'Dashboard';
		$data['panel'] = 'Panel';
		$userID = $this->session->userdata('id');
		$data['courses'] = $this->course_model->get_courses(array('courses.status'=>1));
		$data['topics'] = $this->course_model->get_topics(array('topics.created_by'=>$userID));
		$data['schedules'] = $this->schedule_model->get_schedules(array('schedule.facultyID'=>$userID));
  	    $this->template('web/panel/faculty/dashboard',$data);
	}

	public function faculty_deatil(){
		$id = base64_decode($this->uri->segment(2));
        $data['page_title'] = 'View-Faculty';
        $data['user'] = $this->user_model->get_user(array('users.id' => $id));
		$this->template('web/faculty/faculty-detail',$data);
	}

	public function view(){
		$this->not_admin_logged_in();
		$staffID = base64_decode($this->uri->segment(2));
		$data['page_title'] = 'View-Faculty';
		$data['user'] = $this->user_model->get_user(array('users.id' => $staffID));
		$this->admin_template('users/view-faculty',$data);
	}

	public function edit(){
		$this->not_admin_logged_in();
		$staffID = base64_decode($this->uri->segment(2));
		$data['page_title'] = 'Edit-Faculty';
		$data['user'] = $this->user_model->get_user(array('users.id' => $staffID)); 
		$data['states'] = $this->Common_model->get_states(array('country_id'=>101));
		$this->admin_template('users/edit-faculty',$data);
	}

	public function ajaxfaculties(){
		$this->not_admin_logged_in();
		$condition = array('users.status'=>1,'users.id <>'=>1,'users.user_type'=>'Faculties');
		$faculties = $this->user_model->make_datatables($condition); // this will call modal function for fetching data
		$data = array();
		foreach($faculties as $key=>$faculty) // Loop over the data fetched and store them in array
		{
			$button = '';
			$sub_array = array();
			$button .= '<a href="javascript:void(0)" onclick="reset_password('.$faculty['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Reset faculty Password" class="btn  btn-sm  text-success"><i class="fa fa-lock"></i></a>';
			$button .= '<a href="'.base_url('view-faculty/'.base64_encode($faculty['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="View faculty" class="btn  btn-sm  text-warning"><i class="fa fa-eye"></i></a>';
			$button .= '<a href="'.base_url('edit-faculty/'.base64_encode($faculty['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit faculty" class="btn btn-sm  text-primary"><i class="fa fa-edit"></i> </a>';
      $button .= '<a href="javascript:void(0)" onclick="delete_user('.$faculty['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete faculty" class="btn btn-sm  text-danger"><i class="fa fa-trash"></i> </a>';
			
			$sub_array[] = $key+1;
			$profile_pic = $faculty['profile_pic'];
			$sub_array[] = $button;
			$sub_array[] = '<img src="'.base_url($profile_pic).'" height="50" width="50">';
			$sub_array[] = $faculty['name'];
			$sub_array[] = $faculty['phone'];
			$sub_array[] = $faculty['email'];
			$sub_array[] = $faculty['address'];
			$sub_array[] = $faculty['cityName'];
			$sub_array[] = $faculty['stateName'];
			//$sub_array[] = $faculty['pincode'];
			$sub_array[] = date('d-F-Y', strtotime($faculty['created_at']));
		  $data[] = $sub_array;
		}
	
		  $output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->user_model->get_all_data($condition),
			"recordsFiltered"         =>     $this->user_model->get_filtered_data($condition),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
	  }

		public function update(){
			$this->not_admin_logged_in();
			$userID = $this->input->post('userID');
			$name = $this->input->post('name');
			// $email = $this->input->post('email');
			$phone = $this->input->post('phone');
			$address = $this->input->post('address');
			$city = $this->input->post('city');
			$state = $this->input->post('state');
			$pincode = $this->input->post('pincode');

			$subject = $this->input->post('subject');
			$gender = $this->input->post('gender');
			$dob = $this->input->post('dob');

			$about = $this->input->post('about');
			$user = $this->user_model->get_user(array('users.id'=>$userID));
			if(empty($name)){
				echo json_encode(['status'=>403, 'message'=>'Please enter your name']); 	
				exit();
			}
			// if(empty($email)){
			// 	echo json_encode(['status'=>403, 'message'=>'Please enter your email address']); 	
			// 	exit();
			// }
			// $checkEmail = $this->user_model->get_staff(array('email'=>$email,'id <>'=>$userID));
			// if($checkEmail){
			// 	echo json_encode(['status'=>403,'message'=>'This email is already in use']);
			// 	exit();
			// }
		
			if(empty($phone)){
				echo json_encode(['status'=>403, 'message'=>'Please enter your mobile']); 	
				exit();
			}
		
			$checkPhone = $this->user_model->get_user(array('users.phone'=>$phone,'users.id <>'=>$userID));
			if($checkPhone){
				echo json_encode(['status'=>403,'message'=>'This phone no. is already in use']);
				exit();
			}
		
		
		
			if(empty($state)){
				echo json_encode(['status'=>403, 'message'=>'Please select state']); 	
				exit();
			}
			if(empty($city)){
				echo json_encode(['status'=>403, 'message'=>'Please select city']); 	
				exit();
			}
		
			if(empty($address)){
				echo json_encode(['status'=>403, 'message'=>'Please enter address']);  	
				exit();
			}
		
			$this->load->library('upload');
			if($_FILES['profile_pic']['name'] != '')
				{
			$config = array(
				'upload_path' 	=> 'uploads/user',
				'file_name' 	=> str_replace(' ','',$name).uniqid(),
				'allowed_types' => 'jpg|jpeg|png|gif|webp',
				'max_size' 		=> '10000000',
			);
				$this->upload->initialize($config);
			if ( ! $this->upload->do_upload('profile_pic'))
				{
					$error = $this->upload->display_errors();
					echo json_encode(['status'=>403, 'message'=>$error]);
					exit();
				}
				else
				{
				$type = explode('.',$_FILES['profile_pic']['name']);
				$type = $type[count($type) - 1];
				$profile_pic = 'uploads/user/'.$config['file_name'].'.'.$type;
				}
			}else{
				if(!empty($user->profile_pic)){
					$profile_pic = $user->profile_pic;
				}else{
					$profile_pic = 'public/dummy_user.png';
				}
				
			}
			
			$data = array(
				'name'       => $name,
				'phone'       => $phone,
				'address'    => $address ,
				'city'       => $city,
				'state'      => $state,
				'pincode'    => $pincode,
				'profile_pic'=> $profile_pic,
			);
			$update = $this->user_model->update_user($data,$userID);


			if($update){

				$user_data = array(
					'subject' =>$subject,
					'dob'     =>$dob,
					'gender'  =>$gender,
					'about'   =>$about,
				);
    
				 $this->user_model->update_user_detail($user_data,$userID);

				echo json_encode(['status'=>200, 'message'=>'Faculty updated successfully!']);
			}else{
				echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
			}
		
			}

			public function delete(){
				$this->not_admin_logged_in();
				$id = $this->input->post('id');
				$data = array(
				 'status' => 2
				);
				$delete = $this->user_model->update_user($data,$id);
			 if($delete){
				 echo json_encode(['status'=>200, 'message'=>'Faculty delete successfully!']);
			 }else{
				 echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
			 }
			 
			 }
 

			 public function resetPassword(){
				$id = $this->input->post('id');
				$password = $this->input->post('password');
				if(empty($password)){
					echo json_encode(['status'=>403, 'message'=>'Please enter password']); 	
					exit();
				}
		
				if(strlen($password) < 6){
					echo json_encode(['status'=>403, 'message'=>'Password minimum length must be at least 6 characters']); 	
					exit();
				}
				
				$data = array(
					'password' => md5($password)
				);
				$update = $this->user_model->update_user($data,$id);
				if($update){
					echo json_encode(['status'=>200, 'message'=>'Password change successfully!']);
				}else{
					echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
				}
			
			}

			public function get_faculties(){
				$value = $this->input->post('value');
        $condition = array('users.status'=>1,'users.user_type'=>'Faculties');
		    $faculties = $this->user_model->get_faculties($condition,$value);
				foreach($faculties as $faculty){
				?>
				<div class="col-md-6 col-lg-4 col-xl-3">
					<div class="card widget-profile user-widget-profile">
						<div class="card-body">
							<div class="pro-widget-content">
								<div class="profile-info-widget">
									<a href="<?=base_url('faculty-detail/'.base64_encode($faculty->id))?>" class="booking-user-img">
										<img src="<?=base_url($faculty->profile_pic)?>" alt="User Image">
									</a>
									<div class="rating text-center">
										<i class="fas fa-star filled"></i>
										<i class="fas fa-star filled"></i>
										<i class="fas fa-star filled"></i>
										<i class="fas fa-star filled"></i>
										<i class="fas fa-star filled"></i>
									</div>
									<div class="profile-det-info">
										<h3><a href="<?=base_url('faculty-detail/'.base64_encode($faculty->id))?>"><?=$faculty->name?></a></h3>
										<div class="mentee-details">
											<!-- <h5><b>Mentee ID :</b> 16</h5> -->
											<h5 class="mb-0"><i class="fas fa-map-marker-alt"></i> <?=$faculty->cityName.', '.$faculty->stateName?>
											</h5>
										</div>
									</div>
								</div>
							</div>
							<div class="mentee-info">
								<ul>
									<!-- <li>Phone <span  ><?//=$faculty->phone?></span></li> -->
									<!-- <li>Details <span><?//=$faculty->gender?></span></li> -->
									<li>Subject <span><?=$faculty->user_subject?></span></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
       <?php
				}
			}


public function ajaxSchedule(){
	$this->not_admin_logged_in();
	$userID = $this->session->userdata('id');
	$condition = $this->session->userdata('user_type') == 'Admin' ? array('schedule.status'=>1) : array('schedule.status'=>1,'schedule.facultyID'=>$this->session->userdata('id'),'schedule.class_date'=>date('Y-m-d'));
	$schedules = $this->schedule_model->make_datatables($condition); // this will call modal function for fetching data
	$data = array();
	foreach($schedules as $key=>$schedule) // Loop over the data fetched and store them in array
	{
		$sub_array = array();	
		$sub_array[] = $key+1;
		$sub_array[] = $schedule['coursesName'];
		$sub_array[] = $schedule['subjectName'];
		$sub_array[] = $schedule['topics'];
		$sub_array[] = date('H:iA',$schedule['start_time']);
		$sub_array[] = date('H:iA',$schedule['end_time']);
		$sub_array[] = '<span class="text-danger">'.date('d-M-Y',strtotime($schedule['class_date'])).'</span>';

	$data[] = $sub_array;				
	}

	  $output = array(
		"draw"                    =>     intval($_POST["draw"]),
		"recordsTotal"            =>     $this->schedule_model->get_all_data($condition),
		"recordsFiltered"         =>     $this->schedule_model->get_filtered_data($condition),
		"data"                    =>     $data
	);
	
	echo json_encode($output);

}

}