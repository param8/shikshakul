<?php 
class Student extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
		$this->load->model('course_model');
		$this->load->model('order_model');
		$this->load->model('schedule_model');
	}

	public function index()
	{
		$this->not_admin_logged_in();
    $data['page_title'] = 'Student';
	
    $data['courses'] = $this->course_model->get_courses(array('courses.status'=>1));
	$this->admin_template('users/students',$data);
	}

	public function view(){
		$this->not_admin_logged_in();
		$staffID = base64_decode($this->uri->segment(2));
    $data['page_title'] = 'View-Staffs';
    $data['user'] = $this->user_model->get_user(array('users.id' => $staffID));
		$this->admin_template('users/view-student',$data);
	}

	public function edit(){
		$this->not_admin_logged_in();
		$staffID = base64_decode($this->uri->segment(2));
    $data['page_title'] = 'Edit-Staffs';
    $data['user'] = $this->user_model->get_user(array('users.id' => $staffID)); 
		$data['states'] = $this->Common_model->get_states(array('country_id'=>101));
		$this->admin_template('users/edit-student',$data);
	}


	public function dashboard()
	{
		$this->not_logged_in();
		$this->user_verification();
        $data['page_title'] = 'Dashboard';
		$data['panel'] = 'Panel';
		$userID = $this->session->userdata('id');
		$data['courses'] = $this->course_model->get_courses(array('courses.status'=>1));
		$data['schedules'] = $this->schedule_model->get_schedules(array('schedule.status'=>1,'schedule.class_date'=>date('Y-m-d')));
		$data['bookings'] = $this->Common_model->get_orders(array('orders.userID'=>$userID));
	    $dat['schedules'] = $this->schedule_model->get_schedules(array('schedule.status'=> 1));
		//print_r($schedules); die;
  	$this->template('web/panel/student/dashboard',$data);
	}


	public function ajaxStudents(){
		$this->not_admin_logged_in();
		$condition = array('users.status'=>1,'users.user_type'=>'Student');
		$students = $this->user_model->make_datatables($condition); // this will call modal function for fetching data
		$data = array();
		foreach($students as $key=>$student) // Loop over the data fetched and store them in array
		{
			$button = '';
			$sub_array = array();
			$button .= '<a href="javascript:void(0)" onclick="reset_password('.$student['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Reset Student Password" class="btn  btn-sm  text-success"><i class="fa fa-lock"></i></a>';
			$button .= '<a href="javascript:void(0)" onclick="add_course_order('.$student['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Add Student Course Order" class="btn  btn-sm  text-secondary "><i class="fa fa-shopping-cart"></i></a>';
			$button .= '<a href="'.base_url('view-student/'.base64_encode($student['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="View Student Detail" class="btn  btn-sm  text-warning"><i class="fa fa-eye"></i></a>';
			$button .= '<a href="'.base_url('edit-student/'.base64_encode($student['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit Student Detail" class="btn  btn-sm  text-primary"><i class="fa fa-edit"></i> </a>';
      $button .= '<a href="javascript:void(0)" onclick="delete_user('.$student['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete Student" class="btn  btn-sm  text-danger"><i class="fa fa-trash"></i> </a>';
			$profile_pic = $student['profile_pic'];
			$sub_array[] = $key+1;
			$sub_array[] = $button;
			$sub_array[] = '<img src="'.base_url($profile_pic).'" height="50" width="50">';
			$sub_array[] = $student['name'];
			$sub_array[] = $student['phone'];
			$sub_array[] = $student['email'];
			$sub_array[] = $student['address'];
			$sub_array[] = $student['cityName'];
			$sub_array[] = $student['stateName'];
			$sub_array[] = $student['pincode'];
			// $sub_array[] = $student['aadharcard'];
			$sub_array[] = date('d-m-Y', strtotime($student['created_at']));
		
		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->user_model->get_all_data($condition),
			"recordsFiltered"         =>     $this->user_model->get_filtered_data($condition),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
	  }

		public function update(){
			$userID = $this->input->post('userID');
			$name = $this->input->post('name');
			// $email = $this->input->post('email');
			// $phone = $this->input->post('phone');
			$address = $this->input->post('address');
			$city = $this->input->post('city');
			$state = $this->input->post('state');
			$pincode = $this->input->post('pincode');
			$gender = $this->input->post('gender');
			$dob = $this->input->post('dob');

			$about = $this->input->post('about');
			$user = $this->user_model->get_user(array('users.id'=>$userID));
			if(empty($name)){
				echo json_encode(['status'=>403, 'message'=>'Please enter your name']); 	
				exit();
			}
			// if(empty($email)){
			// 	echo json_encode(['status'=>403, 'message'=>'Please enter your email address']); 	
			// 	exit();
			// }
			// $checkEmail = $this->user_model->get_staff(array('email'=>$email,'id <>'=>$userID));
			// if($checkEmail){
			// 	echo json_encode(['status'=>403,'message'=>'This email is already in use']);
			// 	exit();
			// }
		
			// if(empty($phone)){
			// 	echo json_encode(['status'=>403, 'message'=>'Please enter your mobile']); 	
			// 	exit();
			// }
		
			// $checkPhone = $this->user_model->get_staff(array('phone'=>$phone,'id <>'=>$userID));
			// if($checkPhone){
			// 	echo json_encode(['status'=>403,'message'=>'This phone no. is already in use']);
			// 	exit();
			// }
		
		
		
			if(empty($state)){
				echo json_encode(['status'=>403, 'message'=>'Please select state']); 	
				exit();
			}
			if(empty($city)){
				echo json_encode(['status'=>403, 'message'=>'Please select city']); 	
				exit();
			}
		
			if(empty($address)){
				echo json_encode(['status'=>403, 'message'=>'Please enter address']);  	
				exit();
			}
		
			$this->load->library('upload');
			if($_FILES['profile_pic']['name'] != '')
				{
			$config = array(
				'upload_path' 	=> 'uploads/user',
				'file_name' 	=> str_replace(' ','',$name).uniqid(),
				'allowed_types' => 'jpg|jpeg|png|gif|webp',
				'max_size' 		=> '10000000',
			);
				$this->upload->initialize($config);
			if ( ! $this->upload->do_upload('profile_pic'))
				{
					$error = $this->upload->display_errors();
					echo json_encode(['status'=>403, 'message'=>$error]);
					exit();
				}
				else
				{
				$type = explode('.',$_FILES['profile_pic']['name']);
				$type = $type[count($type) - 1];
				$profile_pic = 'uploads/user/'.$config['file_name'].'.'.$type;
				}
			}else{
				if(!empty($user->profile_pic)){
					$profile_pic = $user->profile_pic;
				}else{
					$profile_pic = 'public/dummy_user.png';
				}
				
			}
			
			$data = array(
				'name'       => $name,
				'address'    => $address ,
				'city'       => $city,
				'state'      => $state,
				'pincode'    => $pincode,
				'profile_pic'=> $profile_pic,
			);
			$update = $this->user_model->update_user($data,$userID);
			if($update){
				$user_data = array(
					'subject' =>$subject,
					'dob'     =>$dob,
					'gender'  =>$gender,
					'about'   =>$about,
				);
    
				 $this->user_model->update_user_detail($user_data,$userID);
				echo json_encode(['status'=>200, 'message'=>'Student updated successfully!']);
			}else{
				echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
			}
		
			}
		
			public function delete(){
			 $id = $this->input->post('id');
			 $data = array(
				'status' => 2
			 );
			 $delete = $this->user_model->update_user($data,$id);
			if($delete){
				echo json_encode(['status'=>200, 'message'=>'Student delete successfully!']);
			}else{
				echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
			}
			
			}

  



  public function change_password(){
	$id = $this->session->userdata('id');
	$user = $this->user_model->get_staff(array('id'=>$id));
	$oldPassword = $this->input->post('oldPassword');
	$password = $this->input->post('password');
	$confirmPassword = $this->input->post('confirmPassword');
    
	if(empty($oldPassword)){
		echo json_encode(['status'=>403, 'message'=>'Please enter old password']); 	
		exit();
	}
	if(md5($oldPassword)!=$user->password){
		echo json_encode(['status'=>403, 'message'=>'Old Password is wrong please enter correct password']); 	
		exit();
	}
	if(empty($password)){
		echo json_encode(['status'=>403, 'message'=>'Please enter password']); 	
		exit();
	}

	if(empty($confirmPassword)){
		echo json_encode(['status'=>403, 'message'=>'Please enter Confirm Password']);  	
		exit();
	}

	if($password!=$confirmPassword){
		echo json_encode(['status'=>403, 'message'=>'Password and confirm password is not match']);  	
		exit();
	}

	$data = array(
    'password' => md5($password)
	);
	$update = $this->user_model->update_staff($data,$id);

	if($update){
		echo json_encode(['status'=>200, 'message'=>'Password change successfully!']);
	}else{
		echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
	}
  }

	public function resetPassword(){
		$id = $this->input->post('id');
		$password = $this->input->post('password');
		if(empty($password)){
			echo json_encode(['status'=>403, 'message'=>'Please enter password']); 	
			exit();
		}

		if(strlen($password) < 6){
	  	echo json_encode(['status'=>403, 'message'=>'Password minimum length must be at least 6 characters']); 	
		  exit();
	  }

		$data = array(
			'password' => md5($password)
		);
		$update = $this->user_model->update_user($data,$id);
		if($update){
			echo json_encode(['status'=>200, 'message'=>'Password change successfully!']);
		}else{
			echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
		}
	}

	public function course_order(){
		$id = $this->input->post('studentID');
		$courseID = $this->input->post('course');
		if(empty($courseID)){
			echo json_encode(['status'=>403, 'message'=>'Please select a course']); 	
			exit();
		}
		$course = $this->course_model->get_course(array('courses.id' => $courseID));
		$order_id = 'SHIKSHAKUL'.uniqid();
		$gst =  ($course->price*18)/100 ;
		$totalAmount = $course->price+$gst;
		$data =array(
			'orderID'        => $order_id,
			'userID'         => $id,
			'price'          => $course->price,
			'total_price'    => $totalAmount,
			'discount'       => 0,
			'gst'            => $gst,
			'payment_type'   => 'Offline',
			'payment_detail' => '',
			'coupon_code'    => '',
			'status'         => 1
		);
		
		$orderID = $this->order_model->store_order($data);

		if($orderID){
			$item_data = array(
				'orderID'            => $orderID,
				'courseID'           => $courseID,
				'qty'                => 1,
				'price'              => $course->price,
				'course_discount'    =>  0,
				'total_course_price' => $totalAmount,
				'coupen_course_code' =>  '',
				'order_type'         => 'Course'
			 );
			
			$store_item = $this->order_model->store_item($item_data);
			echo json_encode(['status'=>200, 'message'=>'Student Order addes successfully!']);
		}else{
			echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
		}
	 

	}

	public function liveBoxAPI(){
		$url = "https://test3015.livebox.co.in/livebox/api/getEmbedCodeOfTheChannel"; 

$ch = curl_init($url);

$jsonData = array(

   "channelNamename" => "maths",

   "username" => "shikshakul",

   "key" => "admin",

 

); 

$jsonDataEncoded = json_encode($jsonData);
 

curl_setopt($ch, CURLOPT_POST, 1);

curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));

print("<XMP>");

$result = curl_exec($ch);
print_r($result);
//print("</XMP>");

	}





 public function export_userDetail(){
	$this->load->library('excel');

    $object = new PHPExcel();
    $object->setActiveSheetIndex(0);
    $table_columns = array("S.No", "Name", "Phone", "Email", "Address", "City", "State", "Pincode", "Created at");

    $column = 0;

    foreach($table_columns as $field) {
        $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
        $column++;
    }
    
    $users = $this->user_model->get_users(array('users.status'=>1));
    //$expence_data = $this->Project_model->get_expences(array('status'=>1));
    //print_r($expence_data);die;
    $excel_row = 2;
    $count = 1;
    foreach($users as $user) {
        $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $count++);
        $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $user->name);
        $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $user->phone);
        $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $user->email);
        $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $user->address);
		$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $user->cityName);
		$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $user->stateName);
		$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $user->pincode);
		$object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $user->created_at);
        $excel_row++;
    }
    $this_date = "Student".date("YmdHis");
    $filename= $this_date.'.csv'; //save our workbook as this file name
    header('Content-Type: application/vnd.ms-excel; charset=UTF-8'); //mime type
    header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
    header('Cache-Control: max-age=0'); //no cache

    $objWriter = PHPExcel_IOFactory::createWriter($object, 'CSV');
    //ob_end_clean();
    $objWriter->save('php://output');

 }	
}