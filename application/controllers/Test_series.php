<?php 
class Test_series extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('quiz_model');
		$this->load->model('course_model');
		$this->load->model('subject_model');
		$this->load->model('test_series_model');
		$this->load->library('csvimport');
	}

	public function index()
	{
		$this->not_admin_logged_in();
    $data['page_title'] = 'Test Series';
		$data['courses'] = $this->course_model->get_courses(array('courses.status' => 1));
	  $this->admin_template('test_series/test-series',$data);
	}


	public function ajaxTestSeries(){
		$this->not_admin_logged_in();
		
		$condition = $this->session->userdata('user_type')== 'Admin' ? array('test_series.status'=>1) : array('test_series.status'=>1,'test_series.userID'=>$this->session->userdata('id'));
		$tesSeries = $this->test_series_model->make_datatables($condition); // this will call modal function for fetching data
		$data = array();
		foreach($tesSeries as $key=>$series) // Loop over the data fetched and store them in array
		{
			$seriess = $this->test_series_model->get_series_count(array('testSeriesID'=>$series['id']));
		
			$button = '';
			$sub_array = array();
      $button .= '<a href="series/'.base64_encode($series['id']).'"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Series" class="btn btn-sm  btn-success">Series <i class="fa fa-pluse"></i> </a>';
			$button .= '<a href="'.base_url('view-test-series/'.base64_encode($series['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="View Test Series" class="btn  btn-sm  text-warning"><i class="fa fa-eye"></i></a>';
			$button .= '<a href="'.base_url('edit-test-series/'.base64_encode($series['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit Test Series" class="btn btn-sm  text-primary"><i class="fa fa-edit"></i> </a>';
      $button .= '<a href="javascript:void(0)" onclick="delete_test_series('.$series['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete Test Series" class="btn btn-sm  text-danger"><i class="fa fa-trash"></i> </a>';
			
			$sub_array[] = $key+1;
			$sub_array[] = $series['usersName'];
			$sub_array[] = $series['title'];
			$sub_array[] = $series['courseName'];
			$sub_array[] = $series['subjectName'];
			$sub_array[] = $series['series_type'];
			$sub_array[] = '₹ '.$series['price'];
			$sub_array[] = $seriess->totalSeries;
			$sub_array[] = date('d-F-Y', strtotime($series['created_at']));
			$sub_array[] = $button;
		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"             =>     intval($_POST["draw"]),
			"recordsTotal"     =>     $this->test_series_model->get_all_data($condition),
			"recordsFiltered"  =>     $this->test_series_model->get_filtered_data($condition),
			"data"             =>     $data
		);
		
		echo json_encode($output);
	}

		public function create_test_series(){
			$this->not_admin_logged_in();
			$data['page_title'] = 'Create Test Series';
			$data['courses'] = $this->course_model->get_courses(array('courses.status'=>1));
			$this->admin_template('test_series/create-test-series',$data);
		}

	public function edit_test_series(){
		$this->not_admin_logged_in();
		$id = base64_decode($this->uri->segment(2)); 
    $data['page_title'] = 'Edit Test Series';
		$data['courses'] = $this->course_model->get_courses(array('courses.status'=>1));
		$data['testSeries'] = $this->test_series_model->get_test_series(array('test_series.id'=>$id));
	  $this->admin_template('test_series/edit-test-series',$data);
	}

	public function view_test_series(){
		$this->not_admin_logged_in();
		$id = base64_decode($this->uri->segment(2)); 
    $data['page_title'] = 'View Test Series';
		$data['courses'] = $this->course_model->get_courses(array('courses.status'=>1));
		$data['testSeries'] = $this->test_series_model->get_test_series(array('test_series.id'=>$id));
		$this->admin_template('test_series/view-test-series',$data);
	}

	public function store_test_series(){
		$title       = $this->input->post('title');
		$courseID    = $this->input->post('courseID');
		$subjectID   = $this->input->post('subjectID');
		$series_type = $this->input->post('series_type');
		$price       = $this->input->post('price');

		$slug = str_replace(' ','-',strtolower($title.'-'.$series_type));

		$test_series = $this->test_series_model->get_test_series(array('test_series.title'=>$title, 'test_series.series_type'=>$series_type));
  // print_r($test_series);die;
		if(empty($title)){
				echo json_encode(['status'=>403, 'message'=>'Please enter title']); 	
			  exit();
		}

		if($test_series){
			echo json_encode(['status'=>403, 'message'=>'This test series already exits']); 	
			exit();
		}

		if(empty($courseID)){
			echo json_encode(['status'=>403, 'message'=>'Please select course']); 	
			exit();
	 }

	 if(empty($subjectID)){
		echo json_encode(['status'=>403, 'message'=>'Please select subject']); 	
		exit();
  }

	if(empty($series_type)){
		echo json_encode(['status'=>403, 'message'=>'Please select  type']); 	
		exit();
  }

		$data= array(
			'userID' => $this->session->userdata('id'),
			'courseID' => $courseID,
			'subjectID' => $subjectID,
			'title' => $title,
			'slug'  => $slug,
			'series_type' => $series_type,
			'price' => $price,
		);

	$store = $this->test_series_model->store_test_series($data);
  if($store){
		echo json_encode(['status'=>200, 'message'=>'Test Series added successfully ']); 	
		exit();
	}else{
		echo json_encode(['status'=>403, 'message'=>'Some things went wrong']); 	
		exit();
	}
		
	}

	public function update_test_series(){
		$id = $this->input->post('testSeriesID');
		$title       = $this->input->post('title');
		$courseID    = $this->input->post('courseID');
		$subjectID   = $this->input->post('subjectID');
		$series_type = $this->input->post('series_type');
		$price       = $this->input->post('price');

		$slug = str_replace(' ','-',strtolower($title.'-'.$series_type));

		$test_series = $this->test_series_model->get_test_series(array('test_series.title'=>$title, 'test_series.series_type'=>$series_type,'test_series.id<>'=>$id));

		if(empty($title)){
				echo json_encode(['status'=>403, 'message'=>'Please enter title']); 	
			  exit();
		}

		if($test_series){
			echo json_encode(['status'=>403, 'message'=>'This test series already exits']); 	
			exit();
		}

		if(empty($courseID)){
			echo json_encode(['status'=>403, 'message'=>'Please select course']); 	
			exit();
	 }

	 if(empty($subjectID)){
		echo json_encode(['status'=>403, 'message'=>'Please select subject']); 	
		exit();
  }

	if(empty($series_type)){
		echo json_encode(['status'=>403, 'message'=>'Please select  type']); 	
		exit();
  }

		$data= array(
			'courseID' => $courseID,
			'subjectID' => $subjectID,
			'title' => $title,
			'slug'  => $slug,
			'series_type' => $series_type,
			'price' => $price,
		);

	$update = $this->test_series_model->update_test_series($data,$id);
  if($update){
		echo json_encode(['status'=>200, 'message'=>'Test Series updated successfully ']); 	
		exit();
	}else{
		echo json_encode(['status'=>403, 'message'=>'Some things went wrong']); 	
		exit();
	}
	}

	public function delete_test_series(){
		$id = $this->input->post('id');
		$delete = $this->test_series_model->delete_test_series($id);
		if($delete){
			echo json_encode(['status'=>200, 'message'=>'Test Series deleted successfully']); 	
			exit();
		}else{
			echo json_encode(['status'=>403, 'message'=>'Something went wrong']); 	
			exit();
		}
	}

	public function series()
	{
		$this->not_admin_logged_in();
    $data['page_title'] = 'Series';
		$id = base64_decode($this->uri->segment(2));
		$data['testSeriesID'] = $id;
	  $this->admin_template('test_series/series',$data);
	}


	public function ajaxSeries(){
		$this->not_admin_logged_in();
		$testSeriesID = $this->uri->segment(3);
		$condition = array('series.status'=>1,'test_series.id'=>$testSeriesID);
		$series = $this->test_series_model->make_datatables_series($condition); // this will call modal function for fetching data
		// print_r($series);
		$data = array();
		foreach($series as $key=>$serie) // Loop over the data fetched and store them in array
		{
			$button = '';
			$sub_array = array();
			$button .= '<a href="'.base_url('view-series/'.base64_encode($serie['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="View Test Series" class="btn  btn-sm  text-warning"><i class="fa fa-eye"></i></a>';
			$button .= '<a href="'.base_url('edit-series/'.base64_encode($serie['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit Test Series" class="btn btn-sm  text-primary"><i class="fa fa-edit"></i> </a>';
      $button .= '<a href="javascript:void(0)" onclick="delete_series('.$serie['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete Test Series" class="btn btn-sm  text-danger"><i class="fa fa-trash"></i> </a>';
			
			$sub_array[] = $key+1;
			$sub_array[] = $serie['testSeries'];
			$sub_array[] = $serie['title'];
			$sub_array[] = $serie['series_mode'];
			$sub_array[] = date('d-F-Y', strtotime($serie['created_at']));
			$sub_array[] = $button;
		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"             =>     intval($_POST["draw"]),
			"recordsTotal"     =>     $this->test_series_model->get_all_data_series($condition),
			"recordsFiltered"  =>     $this->test_series_model->get_filtered_data_series($condition),
			"data"             =>     $data
		);
		
		echo json_encode($output);
	}

	public function create_series(){
		$this->not_admin_logged_in();
		$data['page_title'] = 'Create Series';
		$id = base64_decode($this->uri->segment(2));
		$data['testSeries'] = $this->test_series_model->get_test_series(array('test_series.id'=>$id));
		$this->admin_template('test_series/create-series',$data);
	}

	public function edit_series(){
		$this->not_admin_logged_in();
		$data['page_title'] = 'Edit Series';
		$id = base64_decode($this->uri->segment(2));
		$data['series'] = $this->test_series_model->get_series(array('series.id' => $id));
		$data['questions'] = $this->test_series_model->get_series_questions(array('quizID' => $id));
		// echo "<pre>";
		// print_r($data['questions']);die;
		$this->admin_template('test_series/edit-series',$data);
	}

	public function view_series(){
		$this->not_admin_logged_in();
		$data['page_title'] = 'View Series';
		$id = base64_decode($this->uri->segment(2));
		$data['series'] = $this->test_series_model->get_series(array('series.id' => $id));
		$data['questions'] = $this->test_series_model->get_series_questions(array('quizID' => $id));
		$this->admin_template('test_series/view-series',$data);
	}

	public function store_series(){
		$test_series_title = $this->input->post('test_series_title');
		$title = $this->input->post('title');
		$testSeriesID = $this->input->post('testSeriesID');
		$series_mode_sub = $this->input->post('series_mode_sub') ? $this->input->post('series_mode_sub') : '';
		$series_mode_obj = $this->input->post('series_mode_obj') ? $this->input->post('series_mode_obj') : '';
		$questions = array_filter($this->input->post('question'));
		$option = array_filter($this->input->post('option'));
		$answers = array_filter($this->input->post('answer'));
		$slug = str_replace(' ','-',strtolower($test_series_title.'-'.$title));

		$series = $this->test_series_model->get_series(array('series.title'=>$title,'testSeriesID'=> $testSeriesID));

		if(empty($title)){
				echo json_encode(['status'=>403, 'message'=>'Please enter title']); 	
			  exit();
		}

		if($series){
			echo json_encode(['status'=>403, 'message'=>'This series already exits']); 	
			exit();
		}

		if(empty($series_mode_sub) && empty($series_mode_obj)){
			echo json_encode(['status'=>403, 'message'=>'Please select at least one series mode']); 	
			exit();
	 }


	 if(!empty($series_mode_sub)){
		$this->load->library('upload');
    if($_FILES['subjective']['name'] != '')
		{
    $config = array(
      'upload_path' 	=> 'uploads/series',
      'file_name' 	=> str_replace(' ','',$name).uniqid(),
      'allowed_types' => '*',
      'max_size' 		=> '10000000',
    );
        $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('subjective'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]);
          exit();
      }
      else
      {
        $type = explode('.',$_FILES['subjective']['name']);
        $type = $type[count($type) - 1];
        $subjective = 'uploads/series/'.$config['file_name'].'.'.$type;
      }
     }else{
			
			echo json_encode(['status'=>403, 'message'=>'Please upload subjective']); 	
			exit();
      
    }
	 }else{
		$subjective = "";
	 }


	 if(!empty($series_mode_obj)){

		if(empty($questions)){
			echo json_encode(['status'=>403, 'message'=>'Please enter question']); 	
			exit();
		}

		if(empty($option)){
			echo json_encode(['status'=>403, 'message'=>'Please enter options']); 	
			exit();
		}

		if(empty($answers)){
			echo json_encode(['status'=>403, 'message'=>'Please enter answers']); 	
			exit();
		}

	 }
    
	 $series_mode = $series_mode_sub.','.$series_mode_obj;

		$series_data= array(
			'testSeriesID' => $testSeriesID,
			'title' => $title,
			'series_mode' => $series_mode,
			'subjective' => $subjective,
			'slug'  => $slug,
		);

	$seriesID = $this->test_series_model->store_series($series_data);
		if(!empty($series_mode_obj)){

			foreach(array_filter($questions) as $key=>$question){
				$question_data =array(
					'quizID'   => $seriesID,
					'question' => trim($question),
				);
				$qID = $this->test_series_model->store_series_question($question_data);
					for($i=1; $i<=4; $i++){
						$option_value = $option['question'.$i.$key];
						$option_data = array(
							'qid'    => $qID,
							'option' => trim($option_value),
						);
						$option_store = $this->test_series_model->store_series_option($option_data);
					}
					
					$options = $this->test_series_model->get_series_options(array('qID'=>$qID,'option'=>$answers['question'.$key]));
          
					foreach($options as $opt){
						$optionID = $opt->id;
						$answer_data = array(
							'qid'      => $qID,
							'optionID' => $optionID,
						);
						$answer_store = $this->test_series_model->store_series_answer($answer_data);
					}
					
			}
		}
		echo json_encode(['status'=>200, 'message'=>'Series added successfully ']); 	
		exit();
	}

	public function update_series(){
 	   $id = $this->input->post('seriesID');
		 $questions = $this->test_series_model->get_series_questions(array('quizID' => $id));
		
		foreach ($questions as $question){
      $this->test_series_model->delete_series_question($question['id']);
		}
		$delete = $this->test_series_model->delete_series($id);

		$test_series_title = $this->input->post('test_series_title');
		$title = $this->input->post('title');
		$testSeriesID = $this->input->post('testSeriesID');
		$series_mode_sub = $this->input->post('series_mode_sub') ? $this->input->post('series_mode_sub') : '';
		$series_mode_obj = $this->input->post('series_mode_obj') ? $this->input->post('series_mode_obj') : '';
		$questions = array_filter($this->input->post('question'));
		$option = array_filter($this->input->post('option'));
		$answers = array_filter($this->input->post('answer'));
		$slug = str_replace(' ','-',strtolower($test_series_title.'-'.$title));

		$series = $this->test_series_model->get_series(array('series.title'=>$title,'testSeriesID'=> $testSeriesID));

		if(empty($title)){
				echo json_encode(['status'=>403, 'message'=>'Please enter title']); 	
			  exit();
		}

		if($series){
			echo json_encode(['status'=>403, 'message'=>'This series already exits']); 	
			exit();
		}

		if(empty($series_mode_sub) && empty($series_mode_obj)){
			echo json_encode(['status'=>403, 'message'=>'Please select at least one series mode']); 	
			exit();
	 }


	 if(!empty($series_mode_sub)){
		$this->load->library('upload');
    if($_FILES['subjective']['name'] != '')
		{
    $config = array(
      'upload_path' 	=> 'uploads/series',
      'file_name' 	=> str_replace(' ','',$name).uniqid(),
      'allowed_types' => '*',
      'max_size' 		=> '10000000',
    );
        $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('subjective'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]);
          exit();
      }
      else
      {
        $type = explode('.',$_FILES['subjective']['name']);
        $type = $type[count($type) - 1];
        $subjective = 'uploads/series/'.$config['file_name'].'.'.$type;
      }
     }else{
			
			echo json_encode(['status'=>403, 'message'=>'Please upload subjective']); 	
			exit();
      
    }
	 }else{
		$subjective = "";
	 }


	 if(!empty($series_mode_obj)){

		if(empty($questions)){
			echo json_encode(['status'=>403, 'message'=>'Please enter question']); 	
			exit();
		}

		if(empty($option)){
			echo json_encode(['status'=>403, 'message'=>'Please enter options']); 	
			exit();
		}

		if(empty($answers)){
			echo json_encode(['status'=>403, 'message'=>'Please enter answers']); 	
			exit();
		}

	 }
    
	 $series_mode = $series_mode_sub.','.$series_mode_obj;

		$series_data= array(
			'testSeriesID' => $testSeriesID,
			'title' => $title,
			'series_mode' => $series_mode,
			'subjective' => $subjective,
			'slug'  => $slug,
		);

	$seriesID = $this->test_series_model->store_series($series_data);
		if(!empty($series_mode_obj)){

			foreach(array_filter($questions) as $key=>$question){
				//print_r($key);
				$question_data =array(
					'quizID'   => $seriesID,
					'question' => trim($question),
				);
				$qID = $this->test_series_model->store_series_question($question_data);
					for($i=1; $i<=4; $i++){
						$option_value = $option['question'.$i.$key];
						$option_data = array(
							'qid'    => $qID,
							'option' => trim($option_value),
						);
						$option_store = $this->test_series_model->store_series_option($option_data);
					}
					// echo "<pre>";
					// print_r($answers['question'.$key].'=='.$question);
					$options = $this->test_series_model->get_series_options(array('qID'=>$qID,'option'=>$answers['question'.$key]));
           // print_r($options);
					foreach($options as $opt){
						$optionID = $opt->id;
						$answer_data = array(
							'qid'      => $qID,
							'optionID' => $optionID,
						);
						$answer_store = $this->test_series_model->store_series_answer($answer_data);
					}
					
			}
		}
		echo json_encode(['status'=>200, 'message'=>'Series updated successfully ']); 	
		exit();
	}

	public function delete_series(){
		$id = $this->input->post('id');
		$questions = $this->test_series_model->get_series_questions(array('quizID' => $id));
		foreach ($questions as $question){
      $this->test_series_model->delete_series_question($question['id']);
		}
		$delete = $this->test_series_model->delete_series($id);
		if($delete){
			echo json_encode(['status'=>200, 'message'=>'Series deleted successfully']); 	
			exit();
		}else{
			echo json_encode(['status'=>403, 'message'=>'Something went wrong']); 	
			exit();
		}
	}

	public function test_series(){
		$data['page_title'] = 'Test Series';
	  $this->template('web/test_series/test-series',$data);
	}

	public function testSeriesAjax(){
		$condition =  array('test_series.status'=>1);
		$tesSeries = $this->test_series_model->make_datatables($condition); // this will call modal function for fetching data
		$data = array();
		$orderStatus =array();
		foreach($tesSeries as $key=>$series) // Loop over the data fetched and store them in array
		{
			$this->db->select('items.*,orders.orderID as order_id,orders.discount as discount,orders.gst as gst,orders.price as orderPrice,orders.total_price as total_price,users.name as userName,orders.payment_status as paymentStatus,orders.coupon_code as order_coupon_code,orders.status as orderStatus');
			$this->db->from('orders');
			$this->db->join('items', 'orders.id=items.orderID','left');
			$this->db->join('users', 'users.id=orders.userID','left');
			$this->db->where(array('items.courseID'=>$series['id'],'orders.userID'=>$this->session->userdata('id'),'orders.status'=>1,'items.order_type'=>'Test Series'));
			$this->db->group_by('items.orderID');
			$this->db->order_by('orders.id', 'desc');
			$orders = $this->db->get()->result();
			//print_r($orders);
			//echo $this->db->last_query();
			foreach ($orders as $order){
				$orderStatus[] = $order->orderStatus;
			}

			$orderStatus =  !empty($orderStatus) ? array_filter($orderStatus) : array(0);
			$seriess = $this->test_series_model->get_series_count(array('testSeriesID'=>$series['id']));

			$url = base_url('series-detail/'.$series['slug']);
			$date = !empty($series['created_at']) ? date("d M, Y",strtotime($series['created_at'])) : '';
			$sub_array = array();
			$order_type = "'Test Series'";
			$order_by = "'Buy Now'";
			$button = empty($this->session->userdata('email')) ? '<div class="quizbutton"><a href="'.base_url('register').'" class="btn btn-danger">Buy Now</a></div>' : ($series['price'] == 0 ? '<div class="quizbutton"><a href="'.$url.'">View</a></div>' : (in_array(1,$orderStatus) ? '<div class="quizbutton"><a href="'.$url.'">View</a></div>' : '<div class="quizbutton"><a href="javascript:void(0)" class="btn-danger" onclick="addToCart('.$series['id'].','.$order_type.','.$order_by.')">Buy Now</a>
		  </div>'));
			$sub_array[] = ' <div class="quizlist">
			<div class="quizparagraph">
				<p>'.$series['title'].'(<b>'.$seriess->totalSeries.' Test</b>)'.'(<b class="text-danger">Price ₹ '.$series['price'].')</b></p>
				<span><i class="fas fa-calendar-alt"></i>'.$date.'</span>
			</div>'.$button.
		'</div>';
		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"             =>     intval($_POST["draw"]),
			"recordsTotal"     =>     $this->test_series_model->get_all_data($condition),
			"recordsFiltered"  =>     $this->test_series_model->get_filtered_data($condition),
			"data"             =>     $data
		);
		
		echo json_encode($output);
	}

	public function series_detail(){
		$data['page_title'] = 'Series';
		$data['testSeriesSlug'] = base64_encode($this->uri->segment(2));
	  $this->template('web/test_series/series',$data);
	}

	public function seriesAjax(){
		
		$slug =  base64_decode($this->uri->segment(3));
		$condition = array('series.status'=>1,'test_series.slug'=>$slug);
		$seriess = $this->test_series_model->make_datatables_series($condition); // this will call modal function for fetching data
	
		$data = array();
		foreach($seriess as $key=>$series) // Loop over the data fetched and store them in array
		{
			$series_type = explode(',',$series['series_mode']);
			$objective_url = $this->session->userdata('email') ? base_url('objective/'.$series['slug']) : base_url('register');
			$subjective_button = in_array('Subjective',$series_type) ? '<a href="'.base_url($series['subjective']).'" target="_blank">Subjective <i class="fa fa-download"></i></a>' : '';
			$sub_array = array();
			$sub_array[] = ' <div class="quizlist">
									<div class="quizparagraph">
										<p>'.$series['title'].' ('.$series['testSeries'].')</p>
										<span><i class="fas fa-calendar-alt"></i>'.date("d M, Y",strtotime($series['created_at'])).'</span>
									</div>
									<div class="quizbutton">
									  <span class="mr-5">'.$subjective_button.'</span>
										<a href="'.$objective_url.'">Objective</a>
									</div>
								</div>';

		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"             =>     intval($_POST["draw"]),
			"recordsTotal"     =>     $this->test_series_model->get_all_data_series($condition),
			"recordsFiltered"  =>     $this->test_series_model->get_filtered_data_series($condition),
			"data"             =>     $data
		);
		
		echo json_encode($output);
		
	}

	public function objective(){
		$this->not_logged_in();
		$this->user_verification();
		$quiz_type =	$this->uri->segment(2);
		$data['panel'] = 'Panel';
		$data['quiz'] =$this->test_series_model->get_series(array('series.slug'=>$quiz_type));
		$data['questions'] = $this->test_series_model->get_series_questions(array('quizID'=>$data['quiz']->id));
		$data['page_title'] = $data['quiz']->title;
		$data['quizID']  = $data['quiz']->id;
		$this->template('web/test_series/objective',$data);
	}


	public function download(){
		$this->load->helper('download');
		$file = base64_decode($this->uri->segment(3));
		$pth  = file_get_contents(base_url($file));
		$nme   =  $file;
		force_download($nme ,$pth);    
	}

}