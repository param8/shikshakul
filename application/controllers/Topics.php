<?php 
class Topics extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();

		
		$this->load->model('category_model');
		$this->load->model('course_model');
    $this->load->model('user_model');
	}

	public function index()
	{
		$this->not_admin_logged_in();
    $data['page_title'] = 'Topics';
	  $this->admin_template('topics/topics',$data);
	}

	public function create_topic(){
		$this->not_admin_logged_in();
		$data['page_title'] = 'Create-Topic';
		$data['categories'] = $this->category_model->get_categories(array('status'=>1));
		$this->admin_template('topics/create-topic',$data);
	}

	public function edit_topic(){
		$this->not_admin_logged_in();
		 $data['page_title'] = 'Edit-Topic';
		 $topicID = base64_decode($this->uri->segment(2));
		 $data['topic'] = $this->course_model->get_topic(array('topics.id'=>$topicID));
		 $data['categories'] = $this->category_model->get_categories(array('status'=>1));
		 $this->admin_template('topics/edit-topic',$data);
		}

		public function view_topic(){
			$this->not_admin_logged_in();
			$data['page_title'] = 'View-Topic';
			$topicID = base64_decode($this->uri->segment(2));
			$data['topic'] = $this->course_model->get_topic(array('topics.id'=>$topicID));
			$data['categories'] = $this->category_model->get_categories(array('status'=>1));
			$this->admin_template('topics/view-topic',$data);
		 }


	public function topic_web()
	{
    $data['page_title'] = 'Subjects';
	  $id = base64_decode($this->uri->segment(2));
		$data['subjects'] =$this->subject_model->get_subjects(array('subject.courseID'=>$id,'subject.status'=>1));
	  $this->template('web/subjects/subject',$data);
	}



	public function ajaxTopics(){
		$this->not_admin_logged_in();

		$condition = $this->session->userdata('user_type')=='Admin' ? array('topics.status'=>1) : array('topics.status'=>1,'created_by'=>$this->session->userdata('id'));
		$topices = $this->course_model->make_datatables_topic($condition); // this will call modal function for fetching data
		$data = array();
		foreach($topices as $key=>$topic) // Loop over the data fetched and store them in array
		{
			$button = '';
			$sub_array = array();
			$button .= '<a href="'.base_url('view-topic/'.base64_encode($topic['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="View faculty" class="btn  btn-sm  bg-warning-light"><i class="fe fe-eye"></i></a>';
			$button .= '<a href="'.base_url('edit-topic/'.base64_encode($topic['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit faculty" class="btn btn-sm bg-success-light"><i class="fe fe-edit"></i> </a>';
      $button .= '<a href="javascript:void(0)" onclick="delete_topic('.$topic['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete faculty" class="btn btn-sm bg-danger-light"><i class="fe fe-trash"></i> </a>';
			
			$sub_array[] = $key+1;
			$sub_array[] = $topic['usersName'];
			$sub_array[] = $topic['categoryName'];
			$sub_array[] = $topic['courseName'];
			$sub_array[] = $topic['subjectName'];
			$sub_array[] = date('d-F-Y', strtotime($topic['created_at']));
			$sub_array[] = $button;
		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->course_model->get_all_data_topic($condition),
			"recordsFiltered"         =>     $this->course_model->get_filtered_data_topic($condition),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
	  }

	  

   public function store(){
		$this->not_admin_logged_in();
		$courseID = $this->input->post('courseID');
		$categoryID = $this->input->post('categoryID'); 
		$subjectID = $this->input->post('subjectID');
		$title = $this->input->post('title');	
		$pdf_file = $this->input->post('pdf_file');
		$video_title = $this->input->post('video_title');
		$video_link = $this->input->post('video_link');
		$type = $this->input->post('type');

		$topic = $this->course_model->get_topic(array('topics.courseID'=>$courseID,'topics.categoryID'=>$categoryID,'topics.subjectID'=>$subjectID));
		if(empty($categoryID)){
			echo json_encode(['status'=>403, 'message'=>'Please choose category']); 	
			exit();
		}
		
		if(empty($courseID)){
			echo json_encode(['status'=>403, 'message'=>'Please please select course']); 	
			exit();
		}

		if(empty($subjectID)){
			echo json_encode(['status'=>403, 'message'=>'Please please select subject']); 	
			exit();
		}


        if($topic){
			echo json_encode(['status'=>403, 'message'=>'This topic course wise or subject wise already exists']); 	
		  exit();
		}

	 $total_title = count(array_filter($title));
	 $array_pdf_file = array();
	if($total_title > 0){

		if(!empty($_FILES['pdf_file']['name'])){
			
			$uid = uniqid();
			 //$total_slip = count($_FILES['pdf_file']['name'])-1;
			for($i = 0; $i < $total_title;$i++){
				if(!empty($_FILES['pdf_file']['name'][$i])){
					$name = $_FILES['pdf_file']['name'][$i];
					$type = $_FILES['pdf_file']['type'][$i];
					$tmp_name = $_FILES['pdf_file']['tmp_name'][$i];
					$error = $_FILES['pdf_file']['error'][$i];
					$size = $_FILES['pdf_file']['size'][$i];
					$newFilePath = "uploads/topics/".$_FILES['pdf_file']['name'][$i];
					move_uploaded_file($tmp_name, $newFilePath);
					$image = "uploads/topics/".$_FILES['pdf_file']['name'][$i];
					$array_pdf_file[]= array($title[$i]=>$image);
					}else{
						echo json_encode(['status'=>403, 'message'=>'Please upload document']);
						exit();
					}           
			}
			
	}
	}

	$json_pdf = json_encode($array_pdf_file);
  
	$array_video = array();
	$json_pdf = json_encode($array_pdf_file);
    $total_video = count(array_filter($video_title));
	for($k=0;$k<$total_video; $k++){
    $array_video[] = array(
			$video_title[$k] => $video_link[$k]
		);
	}
  $json_video = json_encode($array_video);

	

	$data = array(
		'created_by' => $this->session->userdata('id'),
		'categoryID' => $categoryID,
    'courseID'   => $courseID,
		'subjectID'  => $subjectID,
		'pdf'        => $json_pdf,
		'video'      => $json_video,
	);
	$store = $this->course_model->store_topic($data);

	if($store){
		echo json_encode(['status'=>200, 'message'=>'Topic add successfully!']);
	}else{
		echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
	}

   }

  public function update(){
		$this->not_admin_logged_in();
		$topicID = $this->input->post('topicID');
		$courseID = $this->input->post('courseID');
		$categoryID = $this->input->post('categoryID'); 
		$subjectID = $this->input->post('subjectID');
		$title = $this->input->post('title');
		$title_old = $this->input->post('title_old');
		$pdf_file_hidden = $this->input->post('pdf_file_hidden');
		$pdf_file = $this->input->post('pdf_file');
		$video_title = $this->input->post('video_title');
		$video_link = $this->input->post('video_link');
		//$slug = str_replace(' ','-',strtolower($title));

		$topic = $this->course_model->get_topic(array('topics.courseID'=>$courseID,'topics.categoryID'=>$categoryID,'topics.subjectID'=>$subjectID));

		if(empty($categoryID)){
			echo json_encode(['status'=>403, 'message'=>'Please choose category']); 	
			exit();
		}
		
	if(empty($courseID)){
		echo json_encode(['status'=>403, 'message'=>'Please please select course']); 	
		exit();
	}
    
	  $total_titles = count(array_filter($title_old));
	 $array_old_pdf_file = array();
	 $array_new_pdf_file = array();
	 $array_pdf_file = array();
	 //print_r($pdf_file_hidden);
	if($total_titles > 0){
	   foreach($title_old as $key=>$total_title){
// 	       echo "<pre>";
// 	   echo $total_title;
// 	  echo $pdf_file_hidden[$key];
	  	$array_old_pdf_file[]= array($total_title=>$pdf_file_hidden[$key]);
	  }
	     
// 	     for($i = 0; $i < $total_title; $i++){
// 		$image = $this->input->post('pdf_file_hidden')[$i];
// 		$array_old_pdf_file[]= array($title_old[$i]=>$image);
// 	 }
	}
	
	//print_r($array_old_pdf_file);die;
   
	 $total_pdf = count(array_filter($title));
	 if($total_pdf > 0){
		if(!empty($_FILES['pdf_file']['name'])){
			$uid = uniqid();
		    for($j = 0; $j < $total_pdf; $j++){
			$name = $_FILES['pdf_file']['name'][$j];
			$type = $_FILES['pdf_file']['type'][$j];
			$tmp_name = $_FILES['pdf_file']['tmp_name'][$j];
			$error = $_FILES['pdf_file']['error'][$j];
			$size = $_FILES['pdf_file']['size'][$j];
			$newFilePath = "uploads/topics/".$_FILES['pdf_file']['name'][$j];
			move_uploaded_file($tmp_name, $newFilePath);
			$image = "uploads/topics/".$_FILES['pdf_file']['name'][$j];
			$array_new_pdf_file[]= array($title[$j]=>$image);
			}

			}
	 }

	$array_pdf_file = array_merge(array_filter($array_old_pdf_file),array_filter($array_new_pdf_file));
//print_r($array_pdf_file);die;
	$json_pdf = json_encode(array_filter($array_pdf_file));
	
  //print_r($json_pdf);die;
	$array_video = array();

  $total_video = count(array_filter($video_title));
	for($k=0;$k<$total_video; $k++){
    $array_video[] = array(
			$video_title[$k] => $video_link[$k]
		);
	}
  $json_video = json_encode($array_video);

	//$delete = $this->subject_model->delete_topic(array('courseID'=>$courseID,'subjectID'=>$subjectID));

	$data = array(
		'categoryID' => $categoryID,
    'courseID'   => $courseID,
		'subjectID'  => $subjectID,
		'pdf'        => $json_pdf,
		'video'      => $json_video
		
	);
	$store = $this->course_model->update_topic($data,array('id'=>$topicID));

	if($store){
		echo json_encode(['status'=>200, 'message'=>'Topic update successfully!']);
	}else{
		echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
	}
  }

  public function delete(){
		$this->not_admin_logged_in();
		$topicID = $this->input->post('id');
		$delete = $this->course_model->delete_topic(array('id'=>$topicID));

		if($delete){
			echo json_encode(['status'=>200, 'message'=>'Topic delete successfully!']);
		}else{
			echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
		}
	
  }

}