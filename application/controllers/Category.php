<?php 
class Category extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('category_model');
		$this->load->model('subject_model');
		$this->load->model('course_model');
	}

	public function index()
	{
		$this->not_admin_logged_in();
   $data['page_title'] = 'Category';
	 $this->admin_template('category/category',$data);
	}

	// public function courses_web()
	// {
  //   $data['page_title'] = 'Courses';
	// 	$data['courses'] =$this->course_model->get_courses(array('status'=>1));
	// 	$data['subjects'] =  $this->subject_model->get_subjects(array('subject.status'=>1));
	//   $this->template('web/courses/courses',$data);
	// }

	// public function ajax_category_datatable(){
   
	// 	$condition = array('courses.status' => 1);
		 
	// 	$fetch_data = $this->course_model->make_datatables($condition);
		
	// 	$data = array();

	// 	foreach($fetch_data as $key=>$course) // Loop over the data fetched and store them in array
	// 		{
	// 			$sub_array =array();
					 
	// 			$sub_array[] = '';
				 
	// 				$data[] = $sub_array;
	// 		}

	// 		$output = array(
	// 			"draw"                    =>     intval($_POST["draw"]),
	// 			"recordsTotal"            =>     $this->course_model->get_all_data($condition),
	// 			"recordsFiltered"         =>     $this->course_model->get_filtered_data($condition),
	// 			"data"                    =>     $data
	// 	);
		
	// 	echo json_encode($output);
	// }

	public function ajaxCategory(){
		$this->not_admin_logged_in();
		$condition = array('status'=>1);
		$categories = $this->category_model->make_datatables($condition); // this will call modal function for fetching data
		$data = array();
		foreach($categories as $key=>$category) // Loop over the data fetched and store them in array
		{
			$button = '';
			$sub_array = array();
			$button .= '<a href="'.base_url('view-category/'.base64_encode($category['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="View Category" class="btn  btn-sm  bg-warning-light"><i class="fe fe-eye"></i></a>';
			$button .= '<a href="'.base_url('edit-category/'.base64_encode($category['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit Category" class="btn btn-sm bg-success-light"><i class="fe fe-edit"></i> </a>';
      $button .= '<a href="javascript:void(0)" onclick="delete_category('.$category['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete Category" class="btn btn-sm bg-danger-light"><i class="fe fe-trash"></i> </a>';
			
			
			$sub_array[] = $key+1;
			$image = $category['image'];
			$sub_array[] = '<img src="'.base_url($image).'" height="50" width="50">';
			$sub_array[] = $category['name'];
			$sub_array[] = $category['language'];
			$sub_array[] = date('d-F-Y', strtotime($category['created_at']));
			$sub_array[] = $button;
		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->category_model->get_all_data($condition),
			"recordsFiltered"         =>     $this->category_model->get_filtered_data($condition),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
	  }

		

		public function create_category(){
			$this->not_admin_logged_in();
			$data['page_title'] = 'Create-Category';
			$this->admin_template('category/create-category',$data);
		}

   public function store(){
	  $this->not_admin_logged_in();
      $name = $this->input->post('name');
	  $language = $this->input->post('language');
      $slug = str_replace(' ','-',strtolower($name));
      $check_category = $this->category_model->get_category(array('name' => $name));

	if(empty($name)){
		echo json_encode(['status'=>403, 'message'=>'Please enter category ']); 	
		exit();
	}
	if($check_category){
		echo json_encode(['status'=>403, 'message'=>'Course is already exists']); 	
		exit();
	}
	if(empty($language)){
		echo json_encode(['status'=>403, 'message'=>'Please enter language']); 	
		exit();
	}


	$this->load->library('upload');
    if($_FILES['image']['name'] != '')
		{
    $config = array(
      'upload_path' 	=> 'uploads/category',
      'file_name' 	=> str_replace(' ','',$name).uniqid(),
      'allowed_types' => 'jpg|jpeg|png|gif|webp',
      'max_size' 		=> '10000000',
    );
        $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('image'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]);
          exit();
      }
      else
      {
        $type = explode('.',$_FILES['image']['name']);
        $type = $type[count($type) - 1];
        $image = 'uploads/category/'.$config['file_name'].'.'.$type;
      }
     }else{
      $image = 'public/Image_not_available.png';
    }
	//$permission = 
	$user_id = uniqid("ST".date('dmY'));
	$data = array(
    'name'         => $name,
		'language'     => $language,
		'image'        => $image,
		'slug'         => $slug
	);
	 $store = $this->category_model->store($data);

	if($store){
    
   		echo json_encode(['status'=>200, 'message'=>'Category add successfully!']);
	}else{
		echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
	}

   }


	 public function edit_category(){
		$this->not_admin_logged_in();
		$data['page_title'] = 'Edit-Category';
		$categoryID = base64_decode($this->uri->segment(2));
		$data['category'] = $this->category_model->get_category(array('id' => $categoryID));
		$this->admin_template('category/edit-category',$data);
		}

	  public function view_category(){
			$this->not_admin_logged_in();
		$data['page_title'] = 'View-Category';
		$categoryID = base64_decode($this->uri->segment(2));
		$data['category'] = $this->category_model->get_category(array('id' => $categoryID));
		$this->admin_template('category/view-category',$data);
	  }


  public function update(){
	$this->not_admin_logged_in();
	$categoryID = $this->input->post('categoryID');
	$name = $this->input->post('name');
	$language = $this->input->post('language');
    $slug = str_replace(' ','-',strtolower($name));
	$category = $this->category_model->get_category(array('id'=>$categoryID));
	$check_category = $this->category_model->get_category(array('name'=>$name,'id<>'=>$categoryID));
	if(empty($name)){
		echo json_encode(['status'=>403, 'message'=>'Please enter your category']); 	
		exit();
	}
	if($check_category){
		echo json_encode(['status'=>403, 'message'=>'Category is already exists']); 	
		exit();
	}
	if(empty($language)){
		echo json_encode(['status'=>403, 'message'=>'Please enter language']); 	
		exit();
	}


	$this->load->library('upload');
	if($_FILES['image']['name'] != '')
		{
	$config = array(
		'upload_path' 	=> 'uploads/category',
		'file_name' 	=> str_replace(' ','',$name).uniqid(),
		'allowed_types' => 'jpg|jpeg|png|gif|webp',
		'max_size' 		=> '10000000',
	);
		$this->upload->initialize($config);
	if ( ! $this->upload->do_upload('image'))
		{
			$error = $this->upload->display_errors();
			echo json_encode(['status'=>403, 'message'=>$error]);
			exit();
		}
		else
		{
		$type = explode('.',$_FILES['image']['name']);
		$type = $type[count($type) - 1];
		$image = 'uploads/category/'.$config['file_name'].'.'.$type;
		}
	}elseif(!empty($category->image)){
		$image = $category->image;
	}else{
		$image = 'public/Image_not_available.png';
	}
	
	$data = array(
		'name'         => $name,
		'language'     => $language,
		'image'        => $image,
		'slug'         => $slug
	);
	$update = $this->category_model->update($data,$categoryID);

	if($update){
		echo json_encode(['status'=>200, 'message'=>'Category updated successfully!']);
	}else{
		echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
	}

  }

  public function delete(){
		$this->not_admin_logged_in();
	 $categoryID = $this->input->post('id');
	 $delete = $this->category_model->delete($categoryID);

	 if($delete){
		$this->course_model->delete_course(array('categoryID'=>$categoryID));
		$this->subject_model->delete_subject(array('categoryID' => $categoryID));
		$this->course_model->delete_topic(array('categoryID'=>$categoryID));
		echo json_encode(['status'=>200, 'message'=>'Category delete successfully!']);
	 }else{
		echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
	 }
	
  }

 
}